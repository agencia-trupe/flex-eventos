# Flex Eventos #

* CodeIgniter 2.1 + HMVC

## 1a fase ##
* Site Estático (Concluído)

## 2a fase ##
* Módulos dos Sistemas de Anuário (Empresas e Arquitetos) (Concluído)

## 3a fase ##
* Painéis dos Sistemas de Anuário (Concluído)

## 4a fase ##
* Módulo do Sistema de Votação (Concluído)

## 5a fase ##
* Painel para o site gerenciável (Concluído)

## 6a fase ##
* Módulo para o cadastro de Fornecedores (Em Desenvolvimento - fase final)