// Arrays para armazenar os valores de keywords e imagens
var lista_palavras = [];
var lista_imagens = [];

/***************************************************/
/* Funções para manipular os registros de Keywords */
/***************************************************/
// Função para popular o array de keywords com as registradas
// no banco. (Vem encodadas no campo #encoded-keywords)
var alimentaListaKeywords = function(){
	var palavras = $('#encoded-keywords').val();
	try{
		palavras = JSON.parse(palavras);
		for (var i = 0; i < palavras.length; i++) {
			lista_palavras.push(palavras[i]);
		};
	}catch(e){
		lista_palavras = [];
	}
}
// Função para atualizar o campo #encoded-keywords com
// os registros atuais
var atualizaCampoKeywords = function(){
	$('#encoded-keywords').val(JSON.stringify(lista_palavras));
}
/***************************************************/

/***************************************************/
/* Funções para manipular os registros de Imagens  */
/***************************************************/
// Função para popular o array de imagens com as registradas
// no banco. (Vem encodadas no campo #encoded-imagens)
var alimentaListaImagens = function(){
	var imagens = $('#encoded-imagens').val();
	try{
		imagens = JSON.parse(imagens);
		for (var i = 0; i < imagens.length; i++) {
			lista_imagens.push(imagens[i]);
		};
	}catch(e){
		lista_imagens = [];
	}
}
// Função para atualizar o campo #encoded-imagens com
// os registros atuais
var atualizaCampoImagens = function(){
	$('#encoded-imagens').val(JSON.stringify(lista_imagens));
}
/***************************************************/


$('document').ready( function(){

	$('#login-form').submit( function(){
		if($('#input-login').val() == ''){
			alert('Informe o email de login');
			return false;
		}
		if($('#input-pass').val() == ''){
			alert('Informe a senha');
			return false;
		}
	});

	$('#recover-form').submit( function(){
		if($('#input-login').val() == ''){
			alert('Informe o email de login');
			return false;
		}
	});

	$('#recover-pass-form').submit( function(){
		if($('#input-nova-senha').val() == ''){
			alert('Informe uma nova senha!');
			return false;
		}
		if($('#input-nova-senha-conf').val() == ''){
			alert('Informe a confirmação de sua nova senha!');
			return false;
		}
		if ($('#input-nova-senha').val() != $('#input-nova-senha-conf').val()) {
			alert('A confirmação não confere com a senha informada');
			return false;
		}
	});	

	if($("#listar-keyword").length){
		alimentaListaKeywords();
		alimentaListaImagens();
	}

	$('#input-keyword').keydown( function(e){
		if(e.keyCode == 13) {
			e.preventDefault();
			$('#salvar-keyword').click();
		}
	});

	$('#salvar-keyword').click( function(e){
		e.preventDefault();
		var palavra = $('#input-keyword').val().trim();

		if(!palavra){
			alert("Informe a palavra-chave");
		  	return false;
		}else{
		  
		  	if($.inArray(palavra, lista_palavras)  === -1){
		    
		    	// Pegar Sinônimos
		    	$.post("painel/ajax/buscarSinonimos", {
		      		palavra : palavra
		    	}, function(resposta){

		      		if(resposta != ''){
		        		var el = $("<li title='Sinônimos: "+resposta+"'><span>"+palavra+"</span><a href='#' class='excluir-keyword' title='Remover Keyword'>x</a></li>");
		      		}else{
		        		var el = $("<li title='Nenhum Sinônimo Cadastrado'><span>"+palavra+"</span><a href='#' class='excluir-keyword' title='Remover Keyword'>x</a></li>");
		      		}

		      		lista_palavras.push(palavra);
					$('#input-keyword').val('');
					$('#input-keyword').focus();
					$("#listar-keyword").append(el);
					atualizaCampoKeywords();
		    	});
		  	}else{
		    	alert("Esta palavra já está cadastrada");
		    	return false;
		  	}
		}
	});

	$('.excluir-keyword').live('click', function(e){
		e.preventDefault();
		var parent = $(this).parent();
		var val = parent.find('span').html();
		parent.remove();
		for (var i=lista_palavras.length-1; i>=0; i--) {
		    if (lista_palavras[i] === val)
		        lista_palavras.splice(i, 1);
		}
		atualizaCampoKeywords();
	});

	if($.browser.mozilla) {
	  $(document).on('click', 'label', function(e) {
	    if(e.currentTarget === this && e.target.nodeName !== 'INPUT') {
	      $(this.control).click();
	    }
	  });
	}

	var idus = $('#idus').val();

	$('#input-imagem').uploadify({
    	'swf'      : BASE+'uploadify/uploadify.swf',
        'uploader' : BASE+'uploadify/uploadify_fornecedores.php',
        'buttonText' : "SELECIONAR",
        'width' : '100%',
        'auto' : false,
        'height' : '100%',
        'uploadLimit' : 999,
        'multi' : false,
        'queueSizeLimit' : 1,
        'fileTypeExts' : '*.jpg; *.png',
        'buttonCursor' : 'pointer',
        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
        'onUploadSuccess' : function(file, data, response){
			data = JSON.parse(data)
			if(data.erro == false){
				var i = $('<img />').attr('src', '_imgs/fornecedores/imagens/'+idus+'/thumbs/'+data.arquivo).load(function() {

					var legenda = $('#legenda').val();
					var keyword1 = $('#keyword1').val();
					var keyword2 = $('#keyword2').val();
					var keyword3 = $('#keyword3').val();

					$('#legenda').val('');
					$('#keyword1').val('');
					$('#keyword2').val('');
					$('#keyword3').val('');
					$('#edit-img-preview').remove();

					var li = $("<li><a href='#' class='excluir-imagem' title='Remover Imagem'>x</a><a href='#' class='alterar-imagem' title='Alterar Imagem'>editar</a></li>");
					$(li).prepend($("<p>"+legenda+"</p>")).prepend(i);
					$('#listar-imagens').append(li);
					lista_imagens.push({
						'imagem' : data.arquivo,
						'legenda' : legenda,
						'keyword1' : keyword1,
						'keyword2' : keyword2,
						'keyword3' : keyword3
					});
					atualizaCampoImagens();
				});
			}else{
				alert(data.erroMsg);
			}
		}
    });

    $('.excluir-imagem').live('click', function(e){
		e.preventDefault();
		if($('#edit-img-preview').length){
			$('#keyword-cancel').click();
		}
		var parent = $(this).parent();
		var val = parent.find('img').attr('src').split('/').pop();
		parent.remove();
		for (var i=lista_imagens.length-1; i>=0; i--) {
		    if (lista_imagens[i].imagem === val)
		        lista_imagens.splice(i, 1);
		}
		atualizaCampoImagens();
    });

    $('.alterar-imagem').live('click', function(e){
    	e.preventDefault();
		var parent = $(this).parent();
		var val = parent.find('img').attr('src').split('/').pop();
		for (var i=lista_imagens.length-1; i>=0; i--) {
		    if (lista_imagens[i].imagem === val){
		    	var id = i;
		    }
		}

		$('.linha.adicionar').hide();
		$('.linha.alterar').show();

		$('#edit-img-preview').remove();
		$('.toRemove').removeClass('toRemove');

		var img = $("<div id='edit-img-preview'><img src='_imgs/fornecedores/imagens/"+idus+"/thumbs/"+lista_imagens[id].imagem+"'></div>");
		$('.linha:first').before(img);
		$('#legenda').val(lista_imagens[id].legenda);
		$('#keyword1').val(lista_imagens[id].keyword1);
		$('#keyword2').val(lista_imagens[id].keyword2);
		$('#keyword3').val(lista_imagens[id].keyword3);

		$('html, body').animate({ scrollTop : $('#scroll-here').offset().top}, 100);
		parent.addClass('toRemove');
    });

    $('#keyword-cancel').live('click', function(e){
    	e.preventDefault();
    	$('#edit-img-preview').remove();
    	$('#legenda').val('');
		$('#keyword1').val('');
		$('#keyword2').val('');
		$('#keyword3').val('');
		$('.linha.adicionar').show();
		$('.linha.alterar').hide();
		$('#input-imagem').uploadify('cancel');
		$('.toRemove').removeClass('toRemove');
    });

    if($('.alerta').length){
    	setTimeout( function(){
    		$('.alerta').fadeOut('normal');
    	}, 3000);
    }

    if($('#tags').length){
	    var availableTags = $('#tags').val();
	    $( "#input-keyword" ).autocomplete({
	      source: availableTags.split(',')
	    });
    }

    if($('.tag_imagens').length){
	    var availableTags = $('#tags').val();
	    $(".tag_imagens").each(function(){
	    	$(this).autocomplete({
	      		source: availableTags.split(',')
	    	});
	    });
    }

    $('#keyword-save').live('click', function(e){
    	e.preventDefault();
    	$('#input-imagem').uploadify('upload','*');
    });

    $('#keyword-edit').live('click', function(e){
    	e.preventDefault();
    	if($(".uploadify-queue-item").length == 0){
    		// Alterando só legenda/keywords    		
			var parent = $('.toRemove');
			var val = parent.find('img').attr('src').split('/').pop();
			for (var i=lista_imagens.length-1; i>=0; i--) {
			    if (lista_imagens[i].imagem === val){
			    	lista_imagens[i].legenda = $('#legenda').val();
			    	lista_imagens[i].keyword1 = $('#keyword1').val();
			    	lista_imagens[i].keyword2 = $('#keyword2').val();
			    	lista_imagens[i].keyword3 = $('#keyword3').val();
			    	parent.find('p').html(lista_imagens[i].legenda);
			    	$('#legenda').val('');
					$('#keyword1').val('');
					$('#keyword2').val('');
					$('#keyword3').val('');
			    }			        
			}
			$('#edit-img-preview').remove();
			$('.linha.alterar').hide();
			$('.linha.adicionar').show();
			parent.removeClass('toRemove');
			atualizaCampoImagens();
    	}else{
    		//trocando imagem também
    		var parent = $('.toRemove');
			var val = parent.find('img').attr('src').split('/').pop();
			parent.remove();
			for (var i=lista_imagens.length-1; i>=0; i--) {
			    if (lista_imagens[i].imagem === val)
			        lista_imagens.splice(i, 1);
			}
			atualizaCampoImagens();
			$('.linha.alterar').hide();
			$('.linha.adicionar').show();
			$('#input-imagem').uploadify('upload','*');
    	}
    });

	$('.delete-btn').click( function(e){
		e.preventDefault();
		var btn = $(this);
		var id = btn.attr('href').split('/').pop();
		var prt = btn.parent().parent().parent();
		var tipo = $(this).attr('data-tipo');
		if(confirm("Deseja excluir o pedido?")){
			$.post('cadastro-fornecedores/ajax/excluirPedido',{
				id_pedido : id,
				tipo : tipo
			}, function(){
				prt.slideUp('slow', function(){
					if($('.tabela-pedidos tbody .tr:visible').length == 0){
						if($('.tabela-pedidos').hasClass('clicavel'))
							$('.tabela-pedidos').append("<h2>Nenhum Histórico de Pedidos Encontrado</h2>");
						else if(tipo == 'catalogo')
							$('.tabela-pedidos').append("<h2>Nenhum novo pedido</h2>");
					}
					setTimeout( function(){
						prt.remove();
					}, 400);
				})
			});
			return false;
		}else{ return false; }
	});

	$('.tabela-pedidos.clicavel tr').click( function(){
		$.fancybox({
			type : 'ajax',
			href : 'cadastro-fornecedores/contatos/detalhes/'+$(this).attr('data-id')+'/'+$(this).attr('data-tipo')
		});
	});

});