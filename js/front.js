$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	if ($('.chamada-grande').length){
		$('.chamada-grande').cycle();
	}

	$('#form-inscricao-premio, #form-inscricao').on('submit', function(){

		if($("input[type='submit']").hasClass('enviando')){

			return false;

		}else{

			$("input[type='submit']").attr('value', 'ENVIANDO...');
			$("input[type='submit']").addClass('enviando');

			if($("input[name='nome']").val() == ""){
				alert('O campo Nome é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='email']").val() == ""){
				alert('O campo E-mail é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='ddd_fone']").val() == ""){
				alert('O campo DDD do telefone é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='fone']").val() == ""){
				alert('O campo Telefone é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='escritorio']").val() == ""){
				alert('O campo Escritório é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='endereco']").val() == ""){
				alert('O campo Endereço é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='bairro']").val() == ""){
				alert('O campo Bairro é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cep']").val() == ""){
				alert('O campo CEP é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cidade']").val() == ""){
				alert('O campo Cidade é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='estado']").val() == ""){
				alert('O campo Estado é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("select[name='categoria']").val() == ""){
				alert('O campo Categoria da Obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("select[name='setor']").val() == ""){
				alert('O campo Setor da Obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='nome_obra']").val() == ""){
				alert('O campo Nome da Obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cliente']").val() == ""){
				alert('O campo Cliente é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cidade_obra']").val() == ""){
				alert('O campo Cidade da Obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='uf_obra']").val() == ""){
				alert('O campo UF da obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("textarea[name='descricao_obra']").val() == ""){
				alert('O campo Descrição da Obra é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='userfile']").val() == ""){
				alert('O campo Prancha Digital/Projeto é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if(!$("input[name='acordo']").is(':checked')){
				alert('É nevessário aceitar os termos do Regulamento!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}
		}

	});

	$('#form-inscricao-evento, #form-inscricao-bahia').on('submit', function(){

		if($("input[type='submit']").hasClass('enviando')){

			return false;

		}else{

			$("input[type='submit']").attr('value', 'ENVIANDO...');
			$("input[type='submit']").addClass('enviando');

			// if($("input[name='primeiro_nome']").val() == ""){
			// 	alert('O campo Primeiro Nome é obrigatório!');
			// 	$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
			// 	return false;
			// }

			if($("input[name='nome']").val() == ""){
				alert('O campo Nome é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='email']").val() == ""){
				alert('O campo E-mail é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='ddd_fone']").val() == ""){
				alert('O campo DDD do telefone é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='fone']").val() == ""){
				alert('O campo Telefone é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='endereco']").val() == ""){
				alert('O campo Endereço é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='bairro']").val() == ""){
				alert('O campo Bairro é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cep']").val() == ""){
				alert('O campo CEP é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='cidade']").val() == ""){
				alert('O campo Cidade é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}

			if($("input[name='estado']").val() == ""){
				alert('O campo Estado é obrigatório!');
				$("input[type='submit']").attr('value', 'ENVIAR MINHA INSCRIÇÃO');
				return false;
			}
		}

	});

	$('.lista-revistas a').click( function(e){
		e.preventDefault();
		var contexto = $('.lista-revistas li');
		if(!$(this).hasClass('ativo')){
			$('.ativo', contexto).removeClass();
			$(this).addClass('ativo');
			var id_publi = $(this).attr('data-id');
			var alvo = $('#target-'+id_publi);
			var visivel = $('.detalhes-revistas:not(.escondido)');

			visivel.addClass('hid-down');
			visivel.addClass('escondido');
			setTimeout(function(){ visivel.addClass('hid-up');remove.addClass('hid-down') }, 400);

			setTimeout( function(){
				alvo.removeClass('escondido');
				setTimeout( function(){ alvo.removeClass('hid-up'); }, 400);
			}, 400);
		}
	});

	$('#fileinput input').change( function(){
		// Verificar se tem \ ou /
		var nome_arquivo = $(this).val().split('\\').pop();
		$('#fileinput .fakeinput').html(nome_arquivo);
	});

	$('.botao-topo').click( function(e){
		e.preventDefault();
		$('html, body').animate({ 'scrollTop' : 0 }, 500);
	});

	$('#busca-fornecedores-categoria').submit( function(){
		if($('#busca-categoria').val() == ''){
			alert('Selecione a categoria!');
			return false;
		}
	});

	if($('.detalhes .coluna-cinza').length){
		setTimeout( function(){
			if($('.detalhes .coluna-cinza').css('height') < $('.detalhes .coluna-maior').css('height'))
				$('.detalhes .coluna-cinza').css('height', $('.detalhes .coluna-maior').css('height'))
		}, 800);

		$('.coluna-maior .thumb').click( function(e){
			e.preventDefault();
			if(!$(this).hasClass('thumb-ativo')){
				$('.thumb-ativo').removeClass('thumb-ativo');
				$(this).addClass('thumb-ativo');
				var target = $(this).attr('href');
				$('#ampliada img').attr('src', target);
			}
		});
	}

	if(jQuery().fancybox){
		$('.resultado').fancybox({
			'titleShow' : false,
			'width' : 1020,
			'height' : 630,
			'autoDimensions' : true,
			'type' : 'iframe',
		});
	}

	$('.link-solicitacao').live('click', function(e){
		e.preventDefault();
		// verificar quais estão marcados
		// se não tiver nenhum marcado alert
		// juntar os ids dos marcados e abrir fancybox
		var ids = '';
		var tipo = $(this).attr('data-target');

		$('input[type=checkbox]').each( function(){
			if($(this).is(':checked')){
				ids += (ids == '') ? $(this).val() : '_'+$(this).val();
			}
		});

		if(ids == ''){
			alert('Selecione os Fornecedores que deseja solicitar Cotação ou Catálogo');
		}else{
			var url = 'fornecedores/solicitacao/'+tipo+'/'+ids;			
			$.fancybox({
				'href' : url,
				'width' : 1020,
				'height' : 630,
				'autoDimensions' : true,
				'type' : 'iframe',				
			});
		}		
	});

	if(jQuery().fancybox){
		$('.fancybox').fancybox();
	}

	$(".ad").mousedown( function(e){
		$.post("ajax/logClick", { t : $(this).attr('data-track'), u : window.location.href } );
	});

});




































