var fornecedoresIncluido = true;

$('document').ready( function(){

	$('.thumbs .thumb').live('click',  function(e){
		e.preventDefault();
		$('.thumbs .thumb.thumb-ativo').removeClass('thumb-ativo');
		$(this).addClass('thumb-ativo');
		var img = $(this).attr('href');
		var leg = $(this).attr('title');
		$('#ampliada img').attr('src', img);
		$('#legenda').html(leg);
	});

	$('.solicitacoes a').live('click', function(e){
		e.preventDefault();
		var destino = $(this).attr('href');
		$.ajax({
			'type'	  : "POST",
			'cache'	  : false,
			'url'	  : destino,
			'success' : function(data) {
				$('#modal').html(data);
			},
		});
	});

	$('.file-label #selec-file').live('change', function(){
		var filepath = $('.file-label #selec-file').val();		
		if(filepath.indexOf('/') === -1){
			var split = filepath.split('\\');	
		}else{
			var split = filepath.split('/');
		}
		var filename = split[split.length - 1];
		console.log(filename)
		$('.file-label .texto').html(filename);
	});

	$('#form-solic-catalogo').live('submit', function(e){

		var erros_validacao = false;

		// Verifica campos obrigatórios
		$("[required]").each( function(){
			if($(this).val() == ''){
				alert($(this).attr('data-requiredmsg'));
				erros_validacao = true;
				return false;
			}
		});

		if(erros_validacao) return false;
		
	});

	$('#form-solic-cotacao').live('submit', function(e){

		var erros_validacao = false;

		// Verifica campos obrigatórios
		$("[required]").each( function(){
			if($(this).val() == ''){
				alert($(this).attr('data-requiredmsg'));
				erros_validacao = true;
				return false;
			}
		});

		if(erros_validacao) return false;
		
	});

});