// Arrays para armazenar os valores de keywords e imagens
var lista_palavras = [];
var lista_imagens = [];

// Array para armazenar os anúncios Ativos
var objMarcados = {
    itens : [],
    contar : function(tipo){
        var cont = 0;
        for (var i = objMarcados.itens.length - 1; i >= 0; i--) {
            if(objMarcados.itens[i].tipo === tipo){
                cont++;
            }
        };
        return cont;
    },
    remover : function(tipo){
        var c = 0;
        for (var i = 0; i < objMarcados.itens.length; i++) {
            if(objMarcados.itens[i].tipo == tipo && c === 0){
                $("input.chkAtivar[data-ativar='"+objMarcados.itens[i].id+"']").attr('checked', false);
                $("input.chkAtivar[data-ativar='"+objMarcados.itens[i].id+"']").parent('label').find('span').html("Desativado");
                desativarAnuncio(objMarcados.itens[i].tipo, objMarcados.itens[i].id)
                c++;
            }
        };
    }
};

var ativarAnuncio = function(tipo, id){
    $.post("painel/anuncios/ativarAnuncio", {
        ativar: id,
        tipo: tipo
    }, function (retorno) {
        objMarcados.itens.push({
           'tipo' : tipo,
           'id'   : id
        });
    });
};

var desativarAnuncio = function(tipo, id){
    $.post("painel/anuncios/desativarAnuncio", {
        desativar: id,
        tipo: tipo
    }, function (retorno) {
        if(retorno){
            for (var i = objMarcados.itens.length - 1; i >= 0; i--) {
                if(objMarcados.itens[i].id == id){
                    objMarcados.itens.splice(i, 1);
                }
            };
        }
    });
};

/***************************************************/
/* Funções para manipular os registros de Keywords */
/***************************************************/
// Função para popular o array de keywords com as registradas
// no banco. (Vem encodadas no campo #encoded-keywords)
var alimentaListaKeywords = function () {
    var palavras = $('#encoded-keywords').val();
    try {
        palavras = JSON.parse(palavras);
        for (var i = 0; i < palavras.length; i++) {
            lista_palavras.push(palavras[i]);
        }
    } catch (e) {
        lista_palavras = [];
    }
};
// Função para atualizar o campo #encoded-keywords com
// os registros atuais
var atualizaCampoKeywords = function () {
    $('#encoded-keywords').val(JSON.stringify(lista_palavras));
};
/***************************************************/

/***************************************************/
/* Funções para manipular os registros de Imagens  */
/***************************************************/
// Função para popular o array de imagens com as registradas
// no banco. (Vem encodadas no campo #encoded-imagens)
var alimentaListaImagens = function () {
    var imagens = $('#encoded-imagens').val();
    try {
        imagens = JSON.parse(imagens);
        for (var i = 0; i < imagens.length; i++) {
            lista_imagens.push(imagens[i]);
        }
    } catch (e) {
        lista_imagens = [];
    }
};
// Função para atualizar o campo #encoded-imagens com
// os registros atuais
var atualizaCampoImagens = function () {
    $('#encoded-imagens').val(JSON.stringify(lista_imagens));
};
/***************************************************/

/***************************************************/
/*  Função para gerar uma senha aleatória          */
/***************************************************/
var gerarSenha = function () {
    var retorno = '';
    var letras = 'abcdefghijklmnopqrstuvxwyzABCDEFGHIJKLMNOPQRSTUVXWYZ1234567890';
    for (var i = 10; i >= 0; i--) {
        var rand = parseInt(Math.random() * (61 - 0), 10);
        retorno += letras[rand];
    }
    return retorno;
};
/***************************************************/

var Admin = {

    toggleLoginRecovery: function () {
        var is_login_visible = $('#modal-login').is(':visible');
        (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function () {
            (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function () {
                $(this).find('input:text:first').focus();
            });
        });
    },

    scrollTop: function () {
        $('html, body').animate({
            'scrollTop': 0
        }, 300);
    },

    voltar: function () {
        window.history.back();
    },

    mostraAlerta: function () {
        var alerta = $('.alert');
        if (!alerta.hasClass('dont-remove')) {
            setTimeout(function () {
                alerta.slideUp(400, function () {
                    alerta.remove();
                });
            }, 4000);
        }
    },

    mostraBotaoScroll: function () {
        var hasVScroll = parseInt($('body').css('height'), 10) > window.innerHeight;
        if (!hasVScroll) {
            $('#footer .inner .container .right a').hide();
        }
    }
};

$(function () {

    $('.paginas-placeholder img').on('contextmenu', function (event) {
        event.preventDefault();
        alert('Essas thumbs não devem ser utilizadas na diagramação. Clique no link abaixo das páginas para fazer o download das imagens em Alta Resolução');
    });

    $('#link-download-imagens').click(function (e) {
        e.preventDefault();
        $('#mensagem-preparando').html("Preparando arquivo.... <img src='_imgs/loader.gif'>");
        var imagens = $(this).attr('data-imagens');
        var diretorio = $(this).attr('data-diretorio');
        var sistema = $(this).attr('data-sistema');
        var user = $(this).attr('data-user');
        $.post(BASE.replace('painel', '') + 'sistema-anuario/' + sistema + '/ajax/baixarImagens', {
            imagens: imagens,
            diretorio: diretorio,
            user: user
        }, function (retorno) {
            retorno = JSON.parse(retorno);
            var dlif = $('<iframe/>', {
                'src': retorno.path
            }).hide();
            $('body').append(dlif);
            $('#mensagem-preparando').html("");
        });
    });

    $('#botao-reprovar-inscricao').click(function (e) {
        e.preventDefault();
        var form = $('#form-reprovar');
        if ($("textarea[name='texto_reprovacao']").val() === '') {
            alert('Informe o motivo da reprovação');
            return false;
        } else {
            form.submit();
            return true;
        }
    });

    $('.toggle-login-recovery').click(function (e) {
        Admin.toggleLoginRecovery();
        e.preventDefault();
    });

    $('#footer .inner .container .right a').click(function (e) {
        Admin.scrollTop();
        e.preventDefault();
    });

    $('.btn-move').click(function (e) {
        e.preventDefault();
    });

    $('.btn-delete').click(function (e) {
        var destino = $(this).attr('href');
        bootbox.confirm("Deseja Excluir o Registro?", function (result) {
            if (result) {
                window.location = destino;
            }
        });
        e.preventDefault();
    });

    $('.btn-voltar').click(function (e) {
        e.preventDefault();
        Admin.voltar();
    });

    if ($('.alert').length) {
        Admin.mostraAlerta();
    }

    $("table.table-sortable tbody").sortable({
        update: function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function (idx, elm) {
                serial.push(elm.id.split('_')[1]);
            });
            $.post(BASE + '/ajax/gravaOrdem', {
                data: serial,
                tabela: tabela
            });
        },
        helper: function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle: $('.btn-move')
    }).disableSelection();

    $('.datepicker').datepicker();

    $('.monthpicker').monthpicker();

    if($('.datetimepicker').length){
        $('.datetimepicker').datetimepicker({
            step: 30,
            lang: 'pt',
            i18n: {
                pt: {
                    months: [
                        'Janeiro', 'Fevereiro', 'Março', 'Abril',
                        'Maio', 'Junho', 'Julho', 'Agosto',
                        'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                    ],
                    dayOfWeek: [
                        "Dom", "Seg", "Ter", "Qua",
                        "Qui", "Sex", "Sab"
                    ]
                }
            },
            format: 'd/m/Y H:i'
        });
    }  

    if (!$("#tipo-ad-swf").hasClass('aberto')) {
        $("#tipo-ad-swf").hide();
    }

    if (!$("#tipo-ad-img").hasClass('aberto')) {
        $("#tipo-ad-img").hide();
    }

    $("input[name='switch-tipo-ad']").change(function () {
        var tipo = $("input[name='switch-tipo-ad']:checked").val();

        if (tipo == "img") {
            $("#tipo-ad-img").show("slow");
            $("#tipo-ad-swf").hide("slow");
        } else if (tipo == "swf") {
            $("#tipo-ad-swf").show("slow");
            $("#tipo-ad-img").hide("slow");
        }
    });

    $('.select-table-ativos').click(function (e) {
        e.preventDefault();
        var p = $(this).parent().parent();
        $(this).addClass('btn-warning');
        p.find('.select-table-historico').removeClass('btn-warning');
        p.find('.lista-ativos').show("fast");
        p.find('.lista-historico').hide("fast");
    });

    $('.select-table-historico').click(function (e) {
        e.preventDefault();
        var p = $(this).parent().parent();
        $(this).addClass('btn-warning');
        p.find('.select-table-ativos').removeClass('btn-warning');
        p.find('.lista-ativos').hide("fast");
        p.find('.lista-historico').show("fast");
    });

    $('.chkAtivar').each(function () {
        var check = $(this);

        if (check.is(':checked')) {
            objMarcados.itens.push({
                'tipo' : $(this).attr('data-tipo'),
                'id'   : $(this).attr('data-ativar')
            });
        }
    });

    $('.chkAtivar').click(function (e) {
        var check = $(this);
        var label = check.parent('label').find('span');
        var tipo = check.attr("data-tipo");
        var id = check.attr('data-ativar');
        var path = check.get
        label.html("<img src='_imgs/layout/loading.gif'>");

        if (check.is(':checked')) {

            if(tipo === 'destaque'){
                if(objMarcados.contar('destaque') > 0){
                    if(objMarcados.contar('halfbanner') > 0){
                        if(objMarcados.contar('destaque') == 1){
                            objMarcados.remover('destaque');
                        }
                    }else{
                        if(objMarcados.contar('destaque') == 2){
                            objMarcados.remover('destaque');
                        }
                    }
                }
            }else if(tipo === 'halfbanner'){
                if(objMarcados.contar('halfbanner') === 2){
                    objMarcados.remover('halfbanner');
                }
                if(objMarcados.contar('destaque') === 2){
                    objMarcados.remover('destaque');
                }
            }else if(tipo === 'button'){
                if(objMarcados.contar('button') > 0){
                    objMarcados.remover('button');
                }else{
                    if(objMarcados.contar('publiespecial') > 0){
                        objMarcados.remover('publiespecial');
                    }
                }
            }else if(tipo === 'publiespecial'){
                if(objMarcados.contar('publiespecial') > 0){
                    objMarcados.remover('publiespecial');
                }else{
                    if(objMarcados.contar('button') > 0){
                        objMarcados.remover('button');
                    }
                }
            }else if(tipo === 'eventos'){
                if(objMarcados.contar('eventos') > 0){
                    objMarcados.remover('eventos');
                }
            }else if(tipo === 'publicacoes'){
                if(objMarcados.contar('publicacoes') > 0){
                    objMarcados.remover('publicacoes');
                }
            }else if(tipo === 'noticias'){
                if(objMarcados.contar('noticias') > 0){
                    objMarcados.remover('noticias');
                }
            }

            setTimeout(function () {
                ativarAnuncio(tipo, id);
                label.html("Ativo");
            }, 400);

        } else {

            setTimeout(function () {
                desativarAnuncio(tipo, id);
                label.html("Desativado");
            }, 400);
        }
    });    

    Admin.mostraBotaoScroll();

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "video",
        theme: "advanced",
        theme_advanced_buttons1: "styleselect,separator,bold,italic,underline,separator,link,unlink,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,image,youtube,separator,fullscreen",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste,advimage,fullscreen,youtube",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '16px';
                ed.pasteAsPlainText = true;
            });
        },
        height: 450,
        content_css: BASE + "/../css/painel/css/tinymceFull.css?nocache=" + new Date().getTime(),
        style_formats: [{
            title: 'Título',
            block: 'h3',
            attributes: {
                'class': 'preto semmargem-b semmargem-t'
            }
        }, {
            title: 'Sub Vermelho',
            block: 'p',
            attributes: {
                'class': 'vermelho bold semmargem-b semmargem-t'
            }
        }, {
            title: 'Sub Preto',
            block: 'p',
            attributes: {
                'class': 'preto bold semmargem-b semmargem-t'
            }
        }, {
            title: 'Parágrafo Comum',
            block: 'p',
            attributes: {
                'class': 'preto semmargem-b semmargem-t'
            }
        }]
    });

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "imagem",
        theme: "advanced",
        theme_advanced_buttons1: "styleselect,separator,bold,italic,underline,separator,link,unlink,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,image,separator,fullscreen",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste,advimage,fullscreen",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '16px';
                ed.pasteAsPlainText = true;
            });
        },
        height: 450,
        content_css: BASE + "/../css/painel/css/tinymceFull.css?nocache=" + new Date().getTime(),
        style_formats: [{
            title: 'Título',
            block: 'h3',
            attributes: {
                'class': 'preto semmargem-b semmargem-t'
            }
        }, {
            title: 'Sub Vermelho',
            block: 'p',
            attributes: {
                'class': 'vermelho bold semmargem-b semmargem-t'
            }
        }, {
            title: 'Sub Preto',
            block: 'p',
            attributes: {
                'class': 'preto bold semmargem-b semmargem-t'
            }
        }, {
            title: 'Parágrafo Comum',
            block: 'p',
            attributes: {
                'class': 'preto semmargem-b semmargem-t'
            }
        }]
    });

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "olho",
        theme: "advanced",
        theme_advanced_buttons1: "bold,italic,underline",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '16px';
                ed.getDoc().body.classList.add("olho");
                ed.pasteAsPlainText = true;
            });
        },
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px",
        content_css: BASE + "/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
    });

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "inscricoes",
        theme: "advanced",
        theme_advanced_buttons1: "styleselect,separator,bold,italic,underline",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '16px';
                ed.getDoc().body.classList.add("inscricoes");
                ed.pasteAsPlainText = true;
            });
        },
        height: 200,
        content_css: BASE + "/../css/painel/css/tinymce.css?nocache=" + new Date().getTime(),
        style_formats: [{
            title: 'Vermelho',
            block: 'p',
            attributes: {
                'class': 'vermelho'
            }
        }, {
            title: 'Preto',
            block: 'p',
            attributes: {
                'class': 'preto'
            }
        }]
    });

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "completo",
        theme: "advanced",
        theme_advanced_buttons1: "formatselect,separator,bold,italic,underline,separator,link,unlink,fontsizeselect",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_blockformats: "h1, h2, p",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '14px';
                ed.pasteAsPlainText = true;
            });
        },
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px",
        content_css: BASE + "/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
    });

    tinyMCE.init({
        language: "pt",
        mode: "specific_textareas",
        editor_selector: "basico",
        theme: "advanced",
        theme_advanced_buttons1: "bold,italic,underline,link,unlink,fontsizeselect",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_statusbar_location: 'bottom',
        theme_advanced_path: false,
        plugins: "paste",
        paste_text_sticky: true,
        theme_advanced_resizing: true,
        setup: function (ed) {
            ed.onInit.add(function (ed) {
                ed.getDoc().body.style.fontSize = '14px';
                ed.pasteAsPlainText = true;
            });
        },
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
        font_size_style_values: "10px,12px,13px,14px,16px,18px",
        content_css: BASE + "/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
    });

    if ($("#listar-keyword").length) {
        alimentaListaKeywords();
        alimentaListaImagens();
    }

    $('#input-keyword').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#salvar-keyword').click();
        }
    });

    if ($('#tags').length) {
        var availableTags = $('#tags').val();
        $("#input-keyword").autocomplete({
            source: availableTags.split(',')
        });
    }

    $('#salvar-keyword').click(function (e) {
        e.preventDefault();
        var palavra = [$('#input-keyword').val()];
        var palavras_repetidas = [];

        if (!palavra[0]) {
            alert("Informe a palavra-chave");
            return false;
        } else {

            if(palavra[0].indexOf(',') !== -1){
               palavra = palavra[0].split(',');
            }

            for (var i = palavra.length - 1; i >= 0; i--) {
                palavra[i] = $.trim(palavra[i]);
                
                if ($.inArray(palavra[i], lista_palavras) === -1) {

                    // Pegar Sinônimos
                    $.ajax({
                        type : 'POST',
                        url : "painel/ajax/buscarSinonimos",
                        data : { palavra: palavra[i] },
                        async : false,                        
                        success : function (resposta) {
                            var el;
                            
                            if (resposta !== '') {
                                el = $("<li title='Sinônimos: " + resposta + "'><span>" + palavra[i] + "</span><a href='#' class='excluir-keyword' title='Remover Keyword'>x</a></li>");
                            } else {
                                el = $("<li title='Nenhum Sinônimo Cadastrado'><span>" + palavra[i] + "</span><a href='#' class='excluir-keyword' title='Remover Keyword'>x</a></li>");
                            }

                            lista_palavras.push(palavra[i]);
                            $('#input-keyword').val('');
                            $('#input-keyword').focus();
                            $("#listar-keyword").append(el);
                            atualizaCampoKeywords();
                        }
                    });

                } else {
                    palavras_repetidas.push(palavra[i]);                    
                }
            }

            if(palavras_repetidas.length > 0){
                alert("Essas palavras já estavam cadastradas e foram ignoradas : "+palavras_repetidas.join(', '));
            }

        }
    });

    $('.excluir-keyword').live('click', function (e) {
        e.preventDefault();
        var parent = $(this).parent();
        var val = parent.find('span').html();
        parent.remove();
        for (var i = lista_palavras.length - 1; i >= 0; i--) {
            if (lista_palavras[i] === val) {
                lista_palavras.splice(i, 1);
            }
        }
        atualizaCampoKeywords();
    });

    if ($.browser.mozilla) {
        $(document).on('click', 'label', function (e) {
            if (e.currentTarget === this && e.target.nodeName !== 'INPUT') {
                $(this.control).click();
            }
        });
    }

    var idus = $('#idus').val();

    if ($('#input-imagem').length) {

        $('#input-imagem').uploadify({
            'swf': BASE.replace('painel', '') + 'uploadify/uploadify.swf',
            'uploader': BASE.replace('painel', '') + 'uploadify/uploadify_fornecedores.php',
            'buttonText': "SELECIONAR",
            'width': '100%',
            'auto': false,
            'height': '100%',
            'uploadLimit': 999,
            'multi': false,
            'queueSizeLimit': 1,
            'fileTypeExts': '*.jpg; *.png',
            'buttonCursor': 'pointer',
            'formData': {
                'idus': idus,
                'secao': 'empresas'
            },
            'onUploadSuccess': function (file, data, response) {
                data = JSON.parse(data);

                if (data.erro === false) {

                    var i = $('<img />').attr('src', '_imgs/fornecedores/imagens/' + idus + '/thumbs/' + data.arquivo).load(function () {

                        var legenda = $('#legenda').val();
                        var keyword1 = $('#keyword1').val();
                        var keyword2 = $('#keyword2').val();
                        var keyword3 = $('#keyword3').val();

                        $('#legenda').val('');
                        $('#keyword1').val('');
                        $('#keyword2').val('');
                        $('#keyword3').val('');
                        $('#edit-img-preview').remove();

                        var li = $("<li><a href='#' class='excluir-imagem' title='Remover Imagem'>x</a><a href='#' class='alterar-imagem' title='Alterar Imagem'>editar</a></li>");
                        $(li).prepend($("<p>" + legenda + "</p>")).prepend(i);
                        $('#listar-imagens').append(li);
                        lista_imagens.push({
                            'imagem': data.arquivo,
                            'legenda': legenda,
                            'keyword1': keyword1,
                            'keyword2': keyword2,
                            'keyword3': keyword3
                        });
                        atualizaCampoImagens();
                    });

                } else {
                    alert(data.erroMsg);
                }
            }
        });
    }

    $('.excluir-imagem').live('click', function (e) {
        e.preventDefault();
        if ($('#edit-img-preview').length) {
            $('#keyword-cancel').click();
        }
        var parent = $(this).parent();
        var val = parent.find('img').attr('src').split('/').pop();
        parent.remove();
        for (var i = lista_imagens.length - 1; i >= 0; i--) {
            if (lista_imagens[i].imagem === val) {
                lista_imagens.splice(i, 1);
            }
        }
        atualizaCampoImagens();
    });

    $('.alterar-imagem').live('click', function (e) {
        e.preventDefault();
        var parent = $(this).parent();
        var val = parent.find('img').attr('src').split('/').pop();
        for (var i = lista_imagens.length - 1; i >= 0; i--) {
            if (lista_imagens[i].imagem === val) {
                var id = i;
            }
        }
    

        $('.linha.adicionar').hide();
        $('.linha.alterar').show();

        $('#edit-img-preview').remove();
        $('.toRemove').removeClass('toRemove');

        var img = $("<div id='edit-img-preview'><img src='_imgs/fornecedores/imagens/" + idus + "/thumbs/" + lista_imagens[id].imagem + "'></div>");
        $('.linha:first').before(img);
        $('#legenda').val(lista_imagens[id].legenda);
        $('#keyword1').val(lista_imagens[id].keyword1);
        $('#keyword2').val(lista_imagens[id].keyword2);
        $('#keyword3').val(lista_imagens[id].keyword3);

        $('html, body').animate({
            scrollTop: $('#scroll-here').offset().top
        }, 100);
        parent.addClass('toRemove');
    });

    $('#keyword-cancel').live('click', function (e) {
        e.preventDefault();
        $('#edit-img-preview').remove();
        $('#legenda').val('');
        $('#keyword1').val('');
        $('#keyword2').val('');
        $('#keyword3').val('');
        $('.linha.adicionar').show();
        $('.linha.alterar').hide();
        $('#input-imagem').uploadify('cancel');
        $('.toRemove').removeClass('toRemove');
    });

    if ($('#tags').length) {
        var availableTags = $('#tags').val();
        $("#input-keyword").autocomplete({
            source: availableTags.split(',')
        });
    }

    if ($('.tag_imagens').length) {
        var availableTags = $('#tags').val();
        $(".tag_imagens").each(function () {
            $(this).autocomplete({
                source: availableTags.split(',')
            });
        });
    }

    $('#keyword-save').live('click', function (e) {
        e.preventDefault();
        $('#input-imagem').uploadify('upload', '*');
    });

    $('#keyword-edit').live('click', function (e) {
        e.preventDefault();

        if ($(".uploadify-queue-item").length === 0) {
            // Alterando só legenda/keywords        
            var parent = $('.toRemove');
            var val = parent.find('img').attr('src').split('/').pop();
            for (var i = lista_imagens.length - 1; i >= 0; i--) {
                if (lista_imagens[i].imagem === val) {
                    lista_imagens[i].legenda = $('#legenda').val();
                    lista_imagens[i].keyword1 = $('#keyword1').val();
                    lista_imagens[i].keyword2 = $('#keyword2').val();
                    lista_imagens[i].keyword3 = $('#keyword3').val();
                    parent.find('p').html(lista_imagens[i].legenda);
                    $('#legenda').val('');
                    $('#keyword1').val('');
                    $('#keyword2').val('');
                    $('#keyword3').val('');
                }
            }

            $('#edit-img-preview').remove();
            $('.linha.alterar').hide();
            $('.linha.adicionar').show();
            parent.removeClass('toRemove');
            atualizaCampoImagens();

        } else {

            //trocando imagem também
            var parent = $('.toRemove');
            var val = parent.find('img').attr('src').split('/').pop();
            parent.remove();
            for (var i = lista_imagens.length - 1; i >= 0; i--) {
                if (lista_imagens[i].imagem === val) {
                    lista_imagens.splice(i, 1);
                }
            }
            atualizaCampoImagens();
            $('.linha.alterar').hide();
            $('.linha.adicionar').show();
            $('#input-imagem').uploadify('upload', '*');
        }
    });

    $('.btn-down-img').live('click', function (e) {
        //e.preventDefault();
        var linha = $(this).parent().parent().addClass('baixado');
    });

    $('.append-gerar-senha').click(function (e) {
        e.preventDefault();
        $(this).parent().find('input').val(gerarSenha);
    });

    $('#form-edit-cad').submit(function () {
        if ($('#input-nome_fantasia').val() === '') {
            alert('O nome fantasia é obrigatório!');
            return false;
        }
        if ($('#input-nome').val() === '') {
            alert('O nome é obrigatório!');
            return false;
        }
        if ($('#input-email').val() === '') {
            alert('O e-mail é obrigatório!');
            return false;
        }
    });

    $('#form-add-cad').submit(function () {
        if ($('#input-nome_fantasia').val() === '') {
            alert('O nome fantasia é obrigatório!');
            return false;
        }
        if ($('#input-nome').val() === '') {
            alert('O nome é obrigatório!');
            return false;
        }
        if ($('#input-email').val() === '') {
            alert('O e-mail é obrigatório!');
            return false;
        }
        if ($('#input-senha').val() === '') {
            alert('A senha é obrigatória!');
            return false;
        }
    });

    $('.abre-sinonimos').fancybox({
        'titleShow': false
    });

    $('.rdDestaque').change( function(){
        var rdb = $(this);
        var parent = rdb.parent().parent();
        if(rdb.val() == 'evento'){
            parent.find('.sel-evento').show("fast");
            parent.find('.sel-link').hide("fast");
            parent.find('.sel-link').find('input').val('');
        }else if(rdb.val() == 'link'){//link
            parent.find('.sel-link').show("fast");
            parent.find('.sel-evento').hide("fast");
            parent.find('.sel-evento').find('select').val('');
        }else{
            parent.find('.sel-link').hide("fast");
            parent.find('.sel-evento').hide("fast");
            parent.find('.sel-link').find('input').val('');
            parent.find('.sel-evento').find('select').val('');
        }
    });

    $('.ctr-hover').tooltip();
});