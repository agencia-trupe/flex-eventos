function dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + (d <= 9 ? '0' + d : d) + '/' + (m<=9 ? '0' + m : m) + '/' + y;
}

$('document').ready( function(){

	$('.paginar .projeto').Paginacao();

	$('#login-form').submit( function(e){
		e.preventDefault();
		if($('#input-email').val() == "" || $('#input-email').val() == $('#input-email').attr('placeholder')){
			alert('Informe seu email!');
			return false;
		}else{
			var email = $('#input-email').val();
			$(this).removeClass('aberto');
			$.post(BASE+'votacao/ajax/cadastraEmail', { email : email }, function(retorno){
				if(retorno == 'visitante'){
					$('.msg-box').html("<span class='grd'>OS VOTOS SÃO LIBERADOS SOMENTE PARA JURADOS SELECIONADOS.</span><span class='med'>Não encontramos seu nome na lista de jurados. Verifique o email informado e tente novamente.</span><a href='javascript:window.history.back()' class='btn-voltar'>&larr; voltar</a>").addClass('aberto');
				}else{
					$('.msg-box').addClass('aberto');
				}
			});
		}
	});

	$('.projeto .coluna .botoes a').hover( function(){
		$('.lit').removeClass('lit');
		$(this).prevAll().addBack().addClass('lit');
	}, function(){
		$('.lit').removeClass('lit');
	});

	$('.projeto .coluna .botoes a').click( function(e){
		e.preventDefault();
		var parent = $(this).parent();
		$('.lit-selecionado', parent).removeClass('lit-selecionado');
		$(this).prevAll().addBack().addClass('lit-selecionado');
	});

	$('.paginacao .vermais').click( function(e){
		e.preventDefault();
		$('.paginar .projeto.begone:lt(5)').removeClass('begone');
		if($('.paginar .projeto.begone').length == 0){
			$('.paginacao .vermais').remove();
		}
	});

	$('.projeto .coluna .enviar a').click( function(e){
		e.preventDefault();
		var parent = $(this).parent().parent();
		var notas = parent.find('.botoes');
		var nota = $('.lit-selecionado', parent).length;
		var projeto = parent.attr('data-projeto');
		if(nota == 0){
			alert("Selecione uma nota antes de enviar!");
			return false;
		}else{
			$.post(BASE+'votacao/ajax/votar',{
				nota : nota,
				id_projeto : projeto
			}, function(retorno){
				console.log(retorno)
				parent.find('span').html("Na minha opinião esse projeto mereceu nota:");
				notas.html($("<img src='_imgs/layout/nota-"+nota+".png'>"));
				notas.addClass('fechado');
				parent.find('.enviar').html("<div class='data'>VOTAÇÃO REALIZADA EM:<br>"+dateToYMD(new Date())+"</div>").css('margin-top', 0);
				setTimeout( function(){
					parent.parent().parent().addClass('begone');
					setTimeout( function(){
						parent.parent().parent().remove();
						$('.paginar .projeto').Paginacao();
					}, 400);
				}, 1000);
			});
		}
	});

});