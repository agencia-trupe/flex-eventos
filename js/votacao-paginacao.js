jQuery.fn.Paginacao = function(){

	var contador = 0;
	var total = this.length;

	atualizarContador = function(){
		if($('#contador').length){
			if(total > 0){
				if($('.avaliado').length > 0){
					$('#contador').html(total+" projetos foram avaliados.");
				}else{
					if(total > 1)
						$('#contador').html(total+" projetos a serem avaliados.");
					else
						$('#contador').html(total+" projeto a ser avaliado.");
				}
			}else{
				$('#contador').html("Não há projetos a serem avaliados nesta categoria.<br><a href='votacao/votar/index'>&laquo; Voltar</a>");
				$('#contador').addClass('sem-projetos');
				$('.paginacao .vermais').remove();
			}
		}
	};

	atualizarContador();

	if(total <= 5)
		$('.paginacao .vermais').remove();

    return this.each(function(){

    	if(contador > 4){
    		$(this).removeClass('animado').addClass('begone');
    	}else{
    		$(this).addClass('animado').removeClass('begone');
    	}
    	contador++;
    });
};