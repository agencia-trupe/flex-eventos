function Layout(numeroPaginas){
	this.numeroPaginas = numeroPaginas;
	this.paginas = [];
}

function Pagina(numero){
	this.numero = numero;
	this.layoutEspecial = false;
	this.layoutSelecionado = false;
	this._numImagens = 0;
	this.imagens = [];
	this.legendas = [];
}

function criarPagina(numero){

	if(numero%2==0){
		var novaPagina = "";

		if (numero == 2)
			novaPagina += "<a href='#' title='Reiniciar o layout' class='resetar-pagina'>REINICIAR LAYOUT</a>";

	novaPagina += "<div class='paginas-placeholder'>";
		novaPagina += "<div class='pagina esquerda'>";
			novaPagina += "<div class='aviso-lateral'>";
				novaPagina += "Clique na<br> página para<br> selecionar o<br> layout desejado";
			novaPagina += "</div>";
			novaPagina += "<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>";
				novaPagina += "CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA";
			novaPagina += "</a>";
			novaPagina += "<div class='opcoes-layout'>";
				novaPagina += "<span>Selecione o modelo de layout desta página</span>";
				novaPagina += "<div class='opcoes'>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='1' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay1.png' alt='Layout 1'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='2' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay2.png' alt='Layout 2'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='3' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay3.png' alt='Layout 3'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='4' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay4.png' alt='Layout 4'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='5' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay5.png' alt='Layout 5'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='6' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay6.png' alt='Layout 6'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='7' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay7.png' alt='Layout 7'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='8' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay8.png' alt='Layout 8'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='9' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay9.png' alt='Layout 9'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='10' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay10.png' alt='Layout 10'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='11' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay11.png' alt='Layout 11'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='12' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay12.png' alt='Layout 12'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='13' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay13.png' alt='Layout 13'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='14' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay14.png' alt='Layout 14'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='15' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay15.png' alt='Layout 15'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='16' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay16.png' alt='Layout 16'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='17' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay17.png' alt='Layout 17'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='18' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay18.png' alt='Layout 18'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='19' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay19.png' alt='Layout 19'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='20' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay20.png' alt='Layout 20'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='21' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay21.png' alt='Layout 21'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='22' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay22.png' alt='Layout 22'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='23' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay23.png' alt='Layout 23'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='24' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay24.png' alt='Layout 24'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='25' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay25.png' alt='Layout 25'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='26' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay26.png' alt='Layout 26'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='27' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay27.png' alt='Layout 27'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='28' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay28.png' alt='Layout 28'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='29' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay29.png' alt='Layout 29'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='30' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay30.png' alt='Layout 30'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='31' data-pagina='"+(numero - 1)+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay31.png' alt='Layout 31'></a>";
				novaPagina += "</div>";
			novaPagina += "</div>";
			novaPagina += "<div class='layout-fill'>";
				novaPagina += "<div class='target'>";
				novaPagina += "</div>";
			novaPagina += "</div>";
			novaPagina += "<div class='numero-pagina'>";
				novaPagina += "página "+(numero - 1)+" - esquerda";
			novaPagina += "</div>";
		novaPagina += "</div>";
		novaPagina += "<div class='pagina direita'>";
			novaPagina += "<div class='aviso-lateral'>";
				novaPagina += "Clique na<br> página para<br> selecionar o<br> layout desejado";
			novaPagina += "</div>";
			novaPagina += "<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>";
				novaPagina += "CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA";
			novaPagina += "</a>";
			novaPagina += "<div class='opcoes-layout'>";
				novaPagina += "<span>Selecione o modelo de layout desta página</span>";
				novaPagina += "<div class='opcoes'>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='1' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay1.png' alt='Layout 1'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='2' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay2.png' alt='Layout 2'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='3' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay3.png' alt='Layout 3'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='4' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay4.png' alt='Layout 4'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='5' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay5.png' alt='Layout 5'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='6' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay6.png' alt='Layout 6'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='7' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay7.png' alt='Layout 7'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='8' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay8.png' alt='Layout 8'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='9' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay9.png' alt='Layout 9'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='10' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay10.png' alt='Layout 10'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='11' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay11.png' alt='Layout 11'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='12' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay12.png' alt='Layout 12'></a>";
					novaPagina += "<a href='#' title='Selecionar este layout' data-layout='13' data-pagina='"+numero+"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay13.png' alt='Layout 13'></a>";
				novaPagina += "</div>";
			novaPagina += "</div>";
			novaPagina += "<div class='layout-fill'>";
				novaPagina += "<div class='target'>";
				novaPagina += "</div>";
			novaPagina += "</div>";
			novaPagina += "<div class='numero-pagina'>";
				novaPagina += "página "+numero+" - direita";
			novaPagina += "</div>";
		novaPagina += "</div>";
		novaPagina += "<a href='#' class='especial' title='Selecionar layout especial'>";
			novaPagina += "CLIQUE AQUI PARA SELECIONAR<br> LAYOUT DE PÁGINA ESPECIAL<br>";
			novaPagina += "<span>(CASO DESEJADO, PÁGINAS ESPECIAIS SÃO LAYOUTS QUE UTILIZAM IMAGENS CONTÍNUAS NAS DUAS PÁGINAS: ESQUERDA E DIREITA)</span>";
		novaPagina += "</a>";
		novaPagina += "<div class='pagina sel-especial'>";
			novaPagina += "LAYOUT DE PÁGINA ESPECIAL<br>";
			novaPagina += "<span>Selecione o modelo de layout desta página</span>";
			novaPagina += "<div class='opcoes'>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e1' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay1.png' alt='Layout 1'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e2' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay2.png' alt='Layout 2'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e3' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay3.png' alt='Layout 3'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e4' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay4.png' alt='Layout 4'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e5' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay5.png' alt='Layout 5'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e6' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay6.png' alt='Layout 6'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e7' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay7.png' alt='Layout 7'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e8' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay8.png' alt='Layout 8'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e9' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay9.png' alt='Layout 9'></a>";
				novaPagina += "<a href='#' title='Selecionar este layout' data-layout='e10' data-pagina='"+ (numero - 1) +"'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay10.png' alt='Layout 10'></a>";
			novaPagina += "</div>";
			novaPagina += "<div class='layout-fill'>";
				novaPagina += "<div class='target'>";
				novaPagina += "</div>";
				novaPagina += "<div id='linha-bg'></div>";
			novaPagina += "</div>";
		novaPagina += "</div>";
	novaPagina += "</div>";

		// Projeto já iniciado, append nas páginas
		if($('.fundo-cinza').hasClass('iniciado')){
			$('.fundo-cinza').append($(novaPagina));
		}else{
			// Projeto ainda não iniciado, limpar a tela e iniciar
			$('.fundo-cinza').addClass('iniciado');
			$('.fundo-cinza').html('').append($(novaPagina));
		}
	}
}

function contaImagens(){
	var len = layout.numeroPaginas;
	var imgsProntas = 0;
	var imgsNecessarias = 0;
	var checkImagens = false;
	var checkLayout = true;

	//console.log('Número de Páginas : '+len);
	for (var i = 0; i <= len; i++) {
		if(typeof(layout.paginas[i]) !== "undefined"){
			if(layout.paginas[i].layoutSelecionado === false){
				checkLayout = false;
			}
			//console.log('Página '+i);
			len_imagens = layout.paginas[i]._numImagens;
			imgsNecessarias = imgsNecessarias + len_imagens;
			//console.log('Número de imagens '+len_imagens);
			for (var b = 1; b <= len_imagens; b++) {
				//console.log('Imagem '+b+' : '+layout.paginas[i].imagens[b]);
				if(typeof(layout.paginas[i].imagens[b]) !== "undefined")
					imgsProntas++;
			};
			if(layout.paginas[i].layoutEspecial)
				i++;
		};
	};
	//console.log('Imagens selecionadas : '+imgsProntas);
	//console.log('Imagens necessárias para avançar : '+imgsNecessarias);
	if (imgsProntas >= imgsNecessarias) {
		checkImagens = true;
	};

	if(checkImagens && checkLayout){
		//console.log('Mostar o botão de próximo');
		$('#faixa-inferior-layout').addClass('aberto');
	}else{
		//if(!checkLayout) console.log('Faltou selecionar o layout das páginas');
		//if(!checkImagens) console.log('Faltou selecionar todas as imagens');
	};
}

function definePaginas(num){
	// Se o processo já foi iniciado, resetar
	if($('.fundo-cinza').hasClass('iniciado')){
		$('.fundo-cinza').removeClass('iniciado')
	}

	if($('#faixa-inferior-layout').hasClass('aberto')){
		$('#faixa-inferior-layout').removeClass('aberto');
	}

	$(".fundo-cinza object").each( function(){
		var idinput = $(this).parent().attr('id');
		$('#'+idinput).uploadify('destroy');
	});

	var numPaginas = num;
	layout = new Layout(numPaginas);
	for (var i = 1; i <= numPaginas; i++) {
		layout.paginas[i] = new Pagina(i);
		criarPagina(i);
	};
	salvar = false;
}

function placeholder(input){
	input.val(input.attr('placeholder'));
    input.focus(function(){
        if ($(this).val() == $(this).attr('placeholder')){
            $(this).val('');
            $(this).removeClass('placeholder');
        }
    }).blur(function(){
        if ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')){
            $(this).val($(this).attr('placeholder'));
            $(this).addClass('placeholder');
        }
    });
}

var layout;
var salvar = false;

$('document').ready( function(){

	if($.browser.mozilla) {
	  $(document).on('click', 'label', function(e) {
	    if(e.currentTarget === this && e.target.nodeName !== 'INPUT') {
	      $(this.control).click();
	    }
	  });
	}

	// Inicia processo de layout selecionando o numero de páginas
	$('.seleciona-paginas').click( function(e){
		e.preventDefault();

		var num = parseInt($(this).html());
		$('.seleciona-paginas.ativo').removeClass('ativo');
		$(this).addClass('ativo');

		if (salvar) {
			$('#dialog-confirm').dialog({
		    	buttons: {
		        	CONTINUAR: function() {
		          		$(this).dialog( "close" );
		          		definePaginas(num);
		        	},
		        	VOLTAR: function() {
		          		$(this).dialog( "close" );
		        	}
		      	}
		    });

		    $('#dialog-confirm').dialog('open');
		}else{
			definePaginas(num);
		}
	});

	// Abre seleção de layout para a página
	$('.paginas-placeholder .pagina .seleciona-layout').live('click', function(e){
		e.preventDefault();
		$(this).parent().find('.opcoes-layout').addClass('aberto');
		$(this).parent().parent().find('a.especial').hide();
		salvar = true;
	});

	// Escolhe um layout e pega o modelo para mostrar o form
	$('.paginas-placeholder .pagina .opcoes-layout .opcoes a').live('click', function(e){
		e.preventDefault();
		var botao_ativo = $(this);
		botao_ativo.addClass('ativo')
		var id_layout = $(this).attr('data-layout');
		var id_pagina = $(this).attr('data-pagina');
		var idus = $('#idus').attr('data-idus');
		var parent = $(this).parent().parent().parent();

		$.post(BASE+'sistema-anuario/empresas/ajax/pegarLayout',
			{ id_layout : id_layout,
			  id_pagina : id_pagina },
			function(resposta){
				parent.find('.layout-fill .target').html(resposta);
				setTimeout( function(){

					parent.find('.opcoes-layout').removeClass('aberto');
					parent.find('.layout-fill').addClass('aberto');
					parent.find('.aviso-lateral').hide();
					layout.paginas[id_pagina].layoutSelecionado = id_layout;

					$("input[type='file']", parent).each( function(){
						layout.paginas[id_pagina]._numImagens++;
						$(this).uploadify({
					    	'swf'      : BASE+'uploadify/uploadify.swf',
					        'uploader' : BASE+'uploadify/uploadify.php',
					        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
					        'width' : '100%',
					        'height' : '100%',
					        'uploadLimit' : 1,
					        'multi' : false,
					        'fileTypeExts' : '*.jpg; *.png',
					        'buttonCursor' : 'pointer',
					        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
					        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		            			alert('Erro ao enviar o arquivo')
		        			},
		        			'onUploadStart' : function(file) {
		        				$(this)[0].button.css('display', 'none');
					        },
		        			'onUploadSuccess' : function(file, data, response){
		        				data = JSON.parse(data)
		        				if(data.erro == false){
			        				relThis = $(this)[0].button;
			        				id_imagem = relThis[0].id[7];
			        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
			        				layout.paginas[id_pagina].legendas[id_imagem] = "";

									relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
									var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
			        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
			        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

			        					if(!Modernizr.input.placeholder){
			        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
			        						placeholder(input);
										}
			        					relThis.remove();
									});
			        				contaImagens();
		        				}else{
		        					$(this)[0].button.css('display', 'block');
		        					alert(data.erroMsg)
		        				}
		        			}
					    });
					});

				}, 1000);
			}
		);
	});

	// Escolhe um layout e pega o modelo para mostrar o form - Versão Especial
	$('.paginas-placeholder .pagina.sel-especial .opcoes a').live('click', function(e){
		e.preventDefault();
		var botao_ativo = $(this);
		botao_ativo.addClass('ativo')
		var id_layout = $(this).attr('data-layout');
		var id_pagina = $(this).attr('data-pagina');
		var idus = $('#idus').attr('data-idus');
		var parent = $(this).parent().parent();

		$.post(BASE+'sistema-anuario/empresas/ajax/pegarLayout',
			{ id_layout : id_layout,
			  id_pagina : id_pagina },
			function(resposta){
				parent.find('.layout-fill .target').html(resposta);
				setTimeout( function(){

					parent.find('.opcoes-layout').removeClass('aberto');
					parent.find('.layout-fill').addClass('aberto');
					layout.paginas[id_pagina].layoutEspecial = true;
					layout.paginas[id_pagina].layoutSelecionado = id_layout;

					$("input[type='file']", parent).each( function(){
						layout.paginas[id_pagina]._numImagens++;
						$(this).uploadify({
					    	'swf'      : BASE+'uploadify/uploadify.swf',
					        'uploader' : BASE+'uploadify/uploadify.php',
					        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
					        'width' : '100%',
					        'height' : '100%',
					        'uploadLimit' : 1,
					        'multi' : false,
					        'fileTypeExts' : '*.jpg; *.png',
					        'buttonCursor' : 'pointer',
					        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
					        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		            			//alert('Erro ao enviar o arquivo')
		        			},
		        			'onUploadStart' : function(file) {
		        				$(this)[0].button.css('display', 'none');
					        },
		        			'onUploadSuccess' : function(file, data, response){
		        				data = JSON.parse(data)
		        				if(data.erro == false){
			        				relThis = $(this)[0].button;
			        				id_imagem = relThis[0].id[7];
			        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
			        				layout.paginas[id_pagina].legendas[id_imagem] = "";

									relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
									var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
			        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
			        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

			        					if(!Modernizr.input.placeholder){
			        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
			        						placeholder(input);
										}
			        					relThis.remove();
									});
			        				contaImagens();
		        				}else{
		        					$(this)[0].button.css('display', 'block');
		        					alert(data.erroMsg)
		        				}
		        			}
					    });
					});

				}, 1000);
			}
		);
	});

	$('.resetar-pagina').live('click', function(e){
		e.preventDefault();
		$('.seleciona-paginas.ativo').trigger('click');
	});

	$('.paginas-placeholder .especial').live('click', function(e){
		e.preventDefault();
		$(this).next('.sel-especial').addClass('aberto');
		$('.aviso-lateral').hide();
		salvar = true;
	});

	$('#gravar-layout').click( function(e){
		e.preventDefault();
		$('html, body').animate({ 'scrollTop' : 0}, 200);
		$('#overlay-salvar').addClass('aberto');
		$.post(BASE+'sistema-anuario/empresas/ajax/gravarLayout', { layout : JSON.stringify(layout) }, function(retorno){
			if(retorno == 1)
				window.location = BASE+'sistema-anuario/empresas/finalizacao';
			else{
				alert('Erro ao gravar layout.')
				$('#overlay-salvar').removeClass('aberto');
			}
		});
	});

	$('.legenda').live('keyup', function(){
		var leg = $(this).val();
		var fullid = $(this).parent().parent().find('label .uploadify').attr('id');
		var pagid = fullid.charAt(3);
		var imgid = fullid.charAt(7);
		layout.paginas[pagid].legendas[imgid] = leg;
	});

	if(typeof encoded !== 'undefined'){
		encoded = JSON.parse(encoded);
		//Editando um layout salvo
		salvar = true;
		layout = new Layout(encoded.length);
		for (var i = 0; i < encoded.length; i++) {
			layout.paginas[i+1] = new Pagina(encoded[i].numero_pagina);
			layout.paginas[i+1].layoutEspecial = (encoded[i].layout_especial == 1) ? true : false;
			layout.paginas[i+1].layoutSelecionado = encoded[i].id_layout;
			layout.paginas[i+1]._numImagens = 0;
			for (var c = 1; c <= 6; c++) {
				if(encoded[i]['imagem'+c] != null)
					layout.paginas[i+1]._numImagens += 1;
			};
			layout.paginas[i+1].imagens[1] =  encoded[i].imagem1;
			layout.paginas[i+1].imagens[2] =  encoded[i].imagem2;
			layout.paginas[i+1].imagens[3] =  encoded[i].imagem3;
			layout.paginas[i+1].imagens[4] =  encoded[i].imagem4;
			layout.paginas[i+1].imagens[5] =  encoded[i].imagem5;
			layout.paginas[i+1].imagens[6] =  encoded[i].imagem6;
			layout.paginas[i+1].legendas[1] =  encoded[i].legenda1;
			layout.paginas[i+1].legendas[2] =  encoded[i].legenda2;
			layout.paginas[i+1].legendas[3] =  encoded[i].legenda3;
			layout.paginas[i+1].legendas[4] =  encoded[i].legenda4;
			layout.paginas[i+1].legendas[5] =  encoded[i].legenda5;
			layout.paginas[i+1].legendas[6] =  encoded[i].legenda6;
			if($('#pag'+(i+1)+'img1').length){
				$('#pag'+(i+1)+'img1').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
						relThis = $(this)[0].button;
						id_imagem = relThis[0].id[7];
						layout.paginas[i+1].imagens[1] = data;
						layout.paginas[i+1].legendas[1] = "";

						relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data);
						var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data).load(function() {
				            relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data);
							relThis.parent().parent().parent().find('.imagem').addClass('aberto');

							if(!Modernizr.input.placeholder){
								var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
								placeholder(input);
							}

							relThis.remove();
				        });
						contaImagens();
					}
				});
			}
			if($('#pag'+(i+1)+'img2').length){
				$('#pag'+(i+1)+'img2').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
        				data = JSON.parse(data)
        				if(data.erro == false){
	        				relThis = $(this)[0].button;
	        				id_imagem = relThis[0].id[7];
	        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
	        				layout.paginas[id_pagina].legendas[id_imagem] = "";

							relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
							var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
	        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
	        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

	        					if(!Modernizr.input.placeholder){
	        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
	        						placeholder(input);
								}
	        					relThis.remove();
							});
	        				contaImagens();
        				}else{
        					$(this)[0].button.css('display', 'block');
        					alert(data.erroMsg)
        				}
					}
				});
			}
			if($('#pag'+(i+1)+'img3').length){
				$('#pag'+(i+1)+'img3').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
						data = JSON.parse(data)
        				if(data.erro == false){
	        				relThis = $(this)[0].button;
	        				id_imagem = relThis[0].id[7];
	        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
	        				layout.paginas[id_pagina].legendas[id_imagem] = "";

							relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
							var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
	        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
	        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

	        					if(!Modernizr.input.placeholder){
	        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
	        						placeholder(input);
								}
	        					relThis.remove();
							});
	        				contaImagens();
        				}else{
        					$(this)[0].button.css('display', 'block');
        					alert(data.erroMsg)
        				}
					}
				});
			}
			if($('#pag'+(i+1)+'img4').length){
				$('#pag'+(i+1)+'img4').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
						data = JSON.parse(data)
        				if(data.erro == false){
	        				relThis = $(this)[0].button;
	        				id_imagem = relThis[0].id[7];
	        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
	        				layout.paginas[id_pagina].legendas[id_imagem] = "";

							relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
							var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
	        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
	        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

	        					if(!Modernizr.input.placeholder){
	        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
	        						placeholder(input);
								}
	        					relThis.remove();
							});
	        				contaImagens();
        				}else{
        					$(this)[0].button.css('display', 'block');
        					alert(data.erroMsg)
        				}
					}
				});
			}
			if($('#pag'+(i+1)+'img5').length){
				$('#pag'+(i+1)+'img5').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
						data = JSON.parse(data)
        				if(data.erro == false){
	        				relThis = $(this)[0].button;
	        				id_imagem = relThis[0].id[7];
	        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
	        				layout.paginas[id_pagina].legendas[id_imagem] = "";

							relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
							var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
	        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
	        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

	        					if(!Modernizr.input.placeholder){
	        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
	        						placeholder(input);
								}
	        					relThis.remove();
							});
	        				contaImagens();
        				}else{
        					$(this)[0].button.css('display', 'block');
        					alert(data.erroMsg)
        				}
					}
				});
			}
			if($('#pag'+(i+1)+'img6').length){
				$('#pag'+(i+1)+'img6').uploadify({
					'swf'      : BASE+'uploadify/uploadify.swf',
			        'uploader' : BASE+'uploadify/uploadify.php',
			        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
			        'width' : '100%',
			        'height' : '100%',
			        'uploadLimit' : 1,
			        'multi' : false,
			        'fileTypeExts' : '*.jpg; *.png',
			        'buttonCursor' : 'pointer',
			        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
			        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
		    			alert('Erro ao enviar o arquivo')
					},
					'onUploadStart' : function(file) {
        				$(this)[0].button.css('display', 'none');
			        },
					'onUploadSuccess' : function(file, data, response){
						data = JSON.parse(data)
        				if(data.erro == false){
	        				relThis = $(this)[0].button;
	        				id_imagem = relThis[0].id[7];
	        				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
	        				layout.paginas[id_pagina].legendas[id_imagem] = "";

							relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
							var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
	        					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
	        					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

	        					if(!Modernizr.input.placeholder){
	        						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
	        						placeholder(input);
								}
	        					relThis.remove();
							});
	        				contaImagens();
        				}else{
        					$(this)[0].button.css('display', 'block');
        					alert(data.erroMsg)
        				}
					}
				});
			}
		};
	}

	$('.troca-imagem').live('click', function(e){
		e.preventDefault();
		// Remover src da imagem OK
		// Remover nome da imagem do data-img OK
		// Limpar a legenda OK
		// Remover imagem do objeto OK
		// Remover legenda do objeto OK
		// Remover classe aberto para mostrar o input
		// Refazer .uploadify() no input
		$('#faixa-inferior-layout').removeClass('aberto');
		var parent = $(this).parent();
		var id_input = parent.parent().find('label .uploadify').attr('id');
		var id_pagina = id_input[3];
		var id_imagem = id_input[7];
		parent.find('img').attr('src', '');
		parent.attr('data-imagem', '');
		parent.find('.legenda').val('');
		layout.paginas[id_pagina].imagens[id_imagem] = "";
		layout.paginas[id_pagina].legendas[id_imagem] = "";
		parent.removeClass('aberto');
		parent.parent().find('#'+id_input).uploadify('destroy');
		var idus = $('#idus').attr('data-idus');
		$('#'+id_input).uploadify({
			'swf'      : BASE+'uploadify/uploadify.swf',
	        'uploader' : BASE+'uploadify/uploadify.php',
	        'buttonText' : "CLIQUE AQUI PARA ENVIAR A IMAGEM",
	        'width' : '100%',
	        'height' : '100%',
	        'uploadLimit' : 1,
	        'multi' : false,
	        'fileTypeExts' : '*.jpg; *.png',
	        'buttonCursor' : 'pointer',
	        'formData'      : {'idus' : idus, 'secao' : 'empresas'},
	        'onUploadError' :  function(file, errorCode, errorMsg, errorString) {
    			alert('Erro ao enviar o arquivo')
			},
			'onUploadStart' : function(file) {
				$(this)[0].button.css('display', 'none');
	        },
			'onUploadSuccess' : function(file, data, response){
				data = JSON.parse(data)
				if(data.erro == false){
    				relThis = $(this)[0].button;
    				id_imagem = relThis[0].id[7];
    				layout.paginas[id_pagina].imagens[id_imagem] = data.arquivo;
    				layout.paginas[id_pagina].legendas[id_imagem] = "";

					relThis.parent().parent().parent().find('.imagem').attr('data-imagem', data.arquivo);
					var i = $('<img />').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo).load(function() {
    					relThis.parent().parent().parent().find('.imagem').find('img').attr('src', '_imgs/anuario/imagens/empresas/'+idus+'/thumbs/'+data.arquivo);
    					relThis.parent().parent().parent().find('.imagem').addClass('aberto');

    					if(!Modernizr.input.placeholder){
    						var input = relThis.parent().parent().parent().find('.imagem').find('.legenda');
    						placeholder(input);
						}
    					relThis.remove();
					});
    				contaImagens();
				}else{
					$(this)[0].button.css('display', 'block');
					alert(data.erroMsg)
				}
			}
		});
	});

	//////////////////////////////

	$('#salvar-projeto-finalizado').click( function(e){
		e.preventDefault();
		var descritivo_empresa = $("textarea[name='descritivo_empresa']").val();
		var sobre_empresa = $("textarea[name='sobre_empresa']").val();
		var creditos = $("textarea[name='creditos']").val();
		$.post(BASE+'sistema-anuario/empresas/ajax/salvarProjeto', {
			descritivo_empresa : descritivo_empresa,
			sobre_empresa : sobre_empresa,
			creditos : creditos
		}, function(retorno){
			$('#save-confirm').dialog('open');
		});
	})

	$('.revisar form').submit( function(e){
		e.preventDefault();
		var observacao = $(this).find('textarea').val();

		if(!observacao){
			alert('Informe sua observação explicando as alterações.');
			return false;
		}

		var cliente = $(this).attr('data-usuario');
		$.post(BASE+'sistema-anuario/empresas/home/revisar', {
			observacao : observacao,
			usuario : cliente
		}, function(retorno){
			if (retorno == 1) {
				$('.caixa-aprovacao').fadeOut();
				$('.metade').fadeOut();
				$('.visualizar-diagramacao').fadeOut();
				$('.revisar').css('margin', '30px 0').html("<p>Sua solicitação de alterações foi enviada!<br>Assim que as correções forem feitas na diagramação da sua participação você receberá um e-mail.<p>");
			}
		});
	});

	$('.caixa-aprovacao a').click( function(e){
		e.preventDefault();

		var cliente = $(this).attr('data-usuario');

		$.post(BASE+'sistema-anuario/empresas/home/aprovar', {
			usuario : cliente
		}, function(retorno){
			if (retorno == 1) {
				$('.caixa-aprovacao').fadeOut();
				$('.metade').fadeOut();
				$('.visualizar-diagramacao').fadeOut();
				$('.revisar').css('margin', '30px 0').html("<p>Obrigado por ter enviado a aprovação e concluído seu processo de participação no Anuário Coporativo Flex.</p><p>Você será informado por e-mail sobre a data de lançamento oficial do Anuário.</p><p>Gratos. Equipe Flex Editora.</p>");
			}
		});
	});

	$('#show-form-login-btn').click( function(e){
		e.preventDefault();
		$(this).css('top', -250);
	});

	$('#show-form-recovery-btn').click( function(e){
		e.preventDefault();
		$('#form-login').css('top', -250);
	});

	$('#form-recovery').submit( function(e){
		e.preventDefault();
		var email = $(this).find("input[name='loginrecov']");
		var tplogin = $(this).find("input[name='tplogin']");
		$.post('sistema-anuario/empresas/home/recuperarSenha', {
			loginrecov : email.val(),
			tplogin : tplogin.val()
		}, function(retorno){
			alert(retorno);
			email.val('');
			$('#form-login').css('top', 0);			
		});
	});

	$('#redefinicaoSenhaForm').submit( function(e){
		if($("#inputSenha").val() == ''){
			alert('Informe sua nova senha!');
			e.preventDefault();
			return false;
		}
		if($("#inputConfSenha").val() == ''){
			alert('Informe a confirmação de senha!');
			e.preventDefault();
			return false;
		}
		if($("#inputSenha").val() != $("#inputConfSenha").val()){
			alert('As senhas estão diferentes!');
			e.preventDefault();
			return false;
		}
	});

	$("#form-identificacao #fake-label input[type=file]").change( function(){
		var nome_arquivo = $(this).val().split('\\').pop();
		if(nome_arquivo)
			$('#form-identificacao #fake-label #fake-file').html(nome_arquivo);
		else
			$('#form-identificacao #fake-label #fake-file').html("SELECIONAR IMAGEM");
	});

	$("#form-identificacao textarea").keyup( function(){
		if($(this).val().length > 300){
			$(this).val($(this).val().substring(0, 300));
		}
		var max = $(this).attr('maxlength');
		var texto_length = max - $(this).val().length;
		$(this).parent().find('p #caracteres-faltam').html(texto_length);
	});

	$("#form-finalizacao textarea").keyup( function(){
		var max = $(this).attr('maxlength');

		if($(this).val().length > max){
			$(this).val($(this).val().substring(0, max));
		}
		var texto_length = max - $(this).val().length;
		$(this).next('p').find('#caracteres-faltam').html(texto_length);
	});


	$('.resetar-pagina-f').click( function(e){
		e.preventDefault();
		var tgt = $(this).attr('href');
		$('#redir-confirm').dialog({
			buttons: {
	        	CONTINUAR: function() {
	          		$(this).dialog("close");
	          		window.location = BASE+tgt;
	        	},
	        	VOLTAR: function() {
	          		$(this).dialog("close");
	        	}
	      	}
      	});
		$('#redir-confirm').dialog('open');
	});

	$('#redir-confirm').dialog({
		resizable: false,
    	height:415,
    	width:538,
    	modal: true,
    	autoOpen : false
    });

	$('#dialog-confirm').dialog({
    	resizable: false,
    	height:415,
    	width:538,
    	modal: true,
    	autoOpen : false,
    	buttons: {
        	CONTINUAR: function() {
          		$( this ).dialog( "close" );
          		return true;
        	},
        	VOLTAR: function() {
          		$( this ).dialog( "close" );
        		return false;
        	}
      	}
    });

	$('#save-confirm').dialog({
    	resizable: false,
    	height:420,
    	width:538,
    	modal: true,
    	autoOpen : false,
    	buttons: {
			OK: function() {
          		$( this ).dialog( "close" );
        		window.location = BASE+'sistema-anuario/empresas/home/logout';
        	}
      	}
    });

	$('.legenda').on('keydown', function(e) {
    	if (e.keyCode == 9 || e.which == 9)
        	e.preventDefault();
    });

	$('#form-identificacao').submit( function(){

		if($("#input_nome_fantasia").val() == ""){
			alert('O campo Nome Fantasia é obrigatório!');
			return false;
		}
		if($("#input-razao_social").val() == ""){
			alert('O campo Razão Social é obrigatório!');
			return false;
		}
		if($("#input-endereco").val() == ""){
			alert('O campo Endereço é obrigatório!');
			return false;
		}
		if($("#input-numero").val() == ""){
			alert('O campo Número é obrigatório!');
			return false;
		}
		if($("#input-bairro").val() == ""){
			alert('O campo Bairro é obrigatório!');
			return false;
		}
		if($("#input-cidade").val() == ""){
			alert('O campo Cidade é obrigatório!');
			return false;
		}
		if($("#input-estado").val() == ""){
			alert('O campo Estado é obrigatório!');
			return false;
		}
		if($("#input-cep").val() == ""){
			alert('O campo CEP é obrigatório!');
			return false;
		}
		if($("#input-telefone").val() == ""){
			alert('O campo Telefone é obrigatório!');
			return false;
		}
		if($("#input-nome").val() == ""){
			alert('O campo Nome Completo é obrigatório!');
			return false;
		}
		if($("#input-email").val() == ""){
			alert('O campo E-mail é obrigatório!');
			return false;
		}

		if($("input[name='categoria']").val() == ""){
			alert('O campo Categoria é obrigatório!');
			return false;
		}
		if(!$(this).hasClass('logado')){
			if($("#input-senha").val() == ""){
				alert('O campo Senha é obrigatório!');
				return false;
			}
			if($("#input-senha").val() != $("#confirma_senha").val()){
				alert('A confirmação de senha não confere com a senha informada!');
				return false;
			}
			if($("input[name='imagem']").val() == ""){
				alert('O campo Imagem é obrigatório!');
				return false;
			}
		}

	});

});