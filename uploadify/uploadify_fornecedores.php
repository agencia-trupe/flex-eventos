<?php
ini_set('memory_limit', '512M');
require('SimpleImage.php');

function slug($str){
	$str = htmlentities($str, ENT_COMPAT, "UTF-8");
    $str = preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde|cedil);/','$1',$str);
    $str = html_entity_decode($str);

	$search		= '-';
	$replace	= '_';

	$trans = array(
		'&\#\d+?;'				=> '',
		'&\S+?;'				=> '',
		'\s+'					=> $replace,
		'[^a-z0-9\-\._]'		=> '',
		$replace.'+'			=> $replace,
		$replace.'$'			=> $replace,
		'^'.$replace			=> $replace,
		'\.+$'					=> ''
	);

	$str = strip_tags($str);

	foreach ($trans as $key => $val)
		$str = preg_replace("#".$key."#i", $val, $str);

	$str = strtolower($str);
	return trim(stripslashes($str));
}

$idUsuario = (int) $_POST['idus'];
$secao = $_POST['secao'];

if (is_int($idUsuario) && is_numeric($idUsuario)) {

	$targetFolder = "/flex/_imgs/fornecedores/imagens/$idUsuario/"; // Remover /flex em produção
	//$targetFolder = "/_imgs/fornecedores/imagens/$idUsuario/";

	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$targetFolder))
		mkdir($_SERVER['DOCUMENT_ROOT'].$targetFolder, 0777);

	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$targetFolder.'thumbs/'))
		mkdir($_SERVER['DOCUMENT_ROOT'].$targetFolder.'thumbs/', 0777);

	if (!empty($_FILES)) {
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
		$return = slug($_FILES['Filedata']['name']);
		$targetFile = rtrim($targetPath,'/') . '/' . $return;

		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'); // File extensions
		$fileParts = pathinfo($_FILES['Filedata']['name']);

		if (in_array($fileParts['extension'],$fileTypes)) {

			$contador = 1;
			while(file_exists($targetFile)){
				list($fname, $fext) = explode('.', $targetFile);
				$targetFile = $fname.$contador.'.'.$fext;
				$contador++;
			}
			$xpd = explode('/', $targetFile);
			$return = end($xpd);

			move_uploaded_file($tempFile,$targetFile);

			$thumbFile = str_replace("/$idUsuario/", "/$idUsuario/thumbs/", $targetFile);
			try {
			    $img = new SimpleImage($targetFile);
			    $img->best_fit(200, 133)->save($thumbFile);
			} catch(Exception $e) {
			    echo 'Erro ao redimensionar a imagem: ' . $e->getMessage();
			}

			echo json_encode(array('erro' => false, 'arquivo' => $return, 'erroMsg' => ''));
		} else {
			echo json_encode(array('erro' => true, 'arquivo' => '', 'erroMsg' => "Arquivo Inválido"));
		}
	}

}else{
	echo json_encode(array('erro' => true, 'arquivo' => '', 'erroMsg' => "Usuário Inválido"));
}
?>