<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Simplelogin Class
 *
 * Makes authentication simple
 *
 * Simplelogin is released to the public domain
 * (use it however you want to)
 *
 * Simplelogin expects this database setup
 * (if you are not using this setup you may
 * need to do some tweaking)
 *
 *
 */
class Simplelogin
{
	var $CI;
	var $user_table = 'usuarios';
    var $user_field = 'username';
    var $pass_field = 'password';
    var $log_table = '';
    var $finalizacao_table = '';

	function Simplelogin()
	{
		// get_instance does not work well in PHP 4
		// you end up with two instances
		// of the CI object and missing data
		// when you call get_instance in the constructor
		//$this->CI =& get_instance();
	}

	/**
	 * Login and sets session variables
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login($user = '', $password = '', $tplogin = 1) {

		switch ($tplogin) {
			case 1:
				$nomesessao = "logged_in";
				$this->user_table = "site_painel";
				$this->user_field = "login";
				$this->pass_field = "senha";
				break;
			case 2:
				$nomesessao = "logged_in_anuario_arquiteto";
				$this->user_table = "cadastro_anuario_arquitetos";
				$this->user_field = "email";
				$this->pass_field = "senha";
				$this->log_table = "cadastro_anuario_passos_arquitetos";
				$this->finalizacao_table = "cadastro_anuario_finalizacao_arquitetos";
				break;
			case 3:
				$nomesessao = "logged_in_anuario_empresa";
				$this->user_table = "cadastro_anuario_empresas";
				$this->user_field = "email";
				$this->pass_field = "senha";
				$this->log_table = "cadastro_anuario_passos_empresas";
				$this->finalizacao_table = "cadastro_anuario_finalizacao_empresas";
				break;
			case 4:
				$nomesessao = "logged_in_painel_anuario_arquitetos";
				$this->user_table = "cadastro_anuario_painel";
				$this->user_field = "login";
				$this->pass_field = "senha";
				break;
			case 5:
				$nomesessao = "logged_in_painel_anuario_empresas";
				$this->user_table = "cadastro_anuario_painel";
				$this->user_field = "login";
				$this->pass_field = "senha";
				break;
			case 6:
				$nomesessao = "logged_in_painel_votacao";
				$this->user_table = "premios_painel";
				$this->user_field = "login";
				$this->pass_field = "senha";
				break;
			case 7:
				$nomesessao = "logged_in_anuario_fornecedores";
				$this->user_table = "fornecedores";
				$this->user_field = "email";
				$this->pass_field = "senha";
				break;
		}

		//Put here for PHP 4 users
		$this->CI =& get_instance();

		$this->CI->session->sess_destroy();

		//Make sure login info was sent
		if($user == '' OR $password == '') {
			return false;
		}

		//Check if already logged in
		if($this->CI->session->userdata($nomesessao)) {
			//User is already logged in.
			return false;
		}

		//Check against user table
		$this->CI->db->where($this->user_field, $user);

		if ($tplogin == 2 || $tplogin == 3)
			$this->CI->db->where('tipo_cadastro', $tplogin);

		$query = $this->CI->db->get($this->user_table);

		if ($query->num_rows() > 0) {
			$row = $query->row_array();

			//Check against password
			if(cripto($password) != $row[$this->pass_field]) {
				return false;
			}

			//Destroy old session
			$this->CI->session->sess_destroy();

			//Create a fresh, brand new session
			$this->CI->session->sess_create();

			//Remove the password field
			unset($row[$this->pass_field]);

			//Set session data
			$this->CI->session->set_userdata($row);

			if($tplogin == 2 || $tplogin == 3){
				$qry_passo3 = $this->CI->db->where('id_cadastro_anuario', $row['id'])
											->where('passo', 3)
											->get($this->log_table)
											->num_rows();

				if($qry_passo3 > 0){
					$qry_finalizacao = $this->CI->db->where('id_cadastro_anuario', $row['id'])
														->where('ano', date('Y'))
														->get($this->finalizacao_table);

					if($qry_finalizacao->num_rows() > 0){
						$row2 = $qry_finalizacao->row_array();
						unset($row2['id_cadastro_anuario']);
						unset($row2['id']);
						$this->CI->session->set_userdata($row2);
					}
				}
			}

			//Set logged_in to true
			$this->CI->session->set_userdata(array($nomesessao => true));

			//Login was successful
			return true;
		} else {
			//No database result found
			return false;
		}

	}

	/**
	 * Logout user
	 *
	 * @access	public
	 * @return	void
	 */
	function logout() {
		//Put here for PHP 4 users
		$this->CI =& get_instance();

		//Destroy session
		$this->CI->session->sess_destroy();
	}
}
?>