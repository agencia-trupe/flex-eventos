<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manutencao{
   
    var $CI;

    public function manutencao()
    {
        $this->CI =& get_instance();
        if($this->CI->config->item("maintenance_mode") === TRUE && ip() != '186.203.215.129')
        {
            $_error =& load_class('Exceptions', 'core');
            echo $_error->show_error("", "", 'manutencao', 200);
            exit;
        }
    }
}