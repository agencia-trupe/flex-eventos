<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends CI_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'cadastro_anuario_arquitetos';
	}

	function pegarTodos($order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarPorId($id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function numeroResultados(){
		return $this->db->get($this->tabela)->num_rows();
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function alterar($id){
		$nome_fantasia = $this->input->post('nome_fantasia');
		$razao_social = $this->input->post('razao_social');
		$endereco = $this->input->post('endereco');
		$numero = $this->input->post('numero');
		$complemento = $this->input->post('complemento');
		$bairro = $this->input->post('bairro');
		$cidade = $this->input->post('cidade');
		$estado = $this->input->post('estado');
		$cep = $this->input->post('cep');
		$telefone = $this->input->post('telefone');
		$website = $this->input->post('website');
		$nome = $this->input->post('nome');
		$email = $this->input->post('email');
		$apresentacao = $this->input->post('apresentacao');

		$imagem = $this->sobeImagem();

		if ($imagem)
			$this->db->set('imagem', $imagem);

        $update = $this->db->set('nome_fantasia', $nome_fantasia)
                           ->set('razao_social', $razao_social)
                           ->set('endereco', $endereco)
                           ->set('numero', $numero)
                           ->set('complemento', $complemento)
                           ->set('bairro', $bairro)
                           ->set('cidade', $cidade)
                           ->set('estado', $estado)
                           ->set('cep', $cep)
                           ->set('telefone', $telefone)
                           ->set('website', $website)
                           ->set('email', $email)
                           ->set('nome', $nome)
                           ->set('apresentacao', $apresentacao)
                           ->where('id', $id)
                           ->update($this->tabela);
        return $update;
	}

    private function sobeImagem(){

        $this->load->library('upload');
        //ini_set('memory_limit', '256M');

        $original = array(
            'campo' => 'userfile',
            'dir' => '_imgs/anuario/marcas/'
        );
        $campo = $original['campo'];

        $uploadconfig = array(
            'upload_path' => $original['dir'],
            'allowed_types' => 'jpg|png|jpeg|bmp|gif',
            'max_size' => '0',
            'max_width' => '0',
            'max_height' => '0'
        );

        $this->upload->initialize($uploadconfig);

        if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                return false;
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name'], 'underscore', true);
                rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

                return $filename;
            }
        }else{
            return false;
        }
    }

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
            $this->db->where('id_cadastro_anuario', $id)->delete('cadastro_anuario_layout_arquitetos');
            $this->db->where('id_cadastro_anuario', $id)->delete('cadastro_anuario_passos_arquitetos');
            $this->db->where('id_cadastro_anuario', $id)->delete('cadastro_anuario_finalizacao_arquitetos');
            $this->db->where('id_cadastro_anuario', $id)->delete('cadastro_anuario_diagramado_arquitetos');
            $this->recursive_remove_directory("_imgs/anuario/imagens/arquitetos/".$id."/");
			return $this->db->where('id', $id)->delete($this->tabela);
            //return true;
		}
	}

    function recursive_remove_directory($directory, $empty=FALSE){
        if(substr($directory,-1) == '/'){
            $directory = substr($directory,0,-1);
        }
        if(!file_exists($directory) || !is_dir($directory)){
            return FALSE;
        }elseif(is_readable($directory)){
            $handle = opendir($directory);
            while (FALSE !== ($item = readdir($handle))){
                if($item != '.' && $item != '..'){
                    $path = $directory.'/'.$item;
                    if(is_dir($path)){
                        $this->recursive_remove_directory($path);
                    }else{
                        unlink($path);
                    }
                }
            }
            closedir($handle);
            if($empty == FALSE){
                if(!rmdir($directory)){
                    return FALSE;
                }
            }
        }
        return TRUE;
    }
}