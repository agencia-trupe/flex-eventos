<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Layout extends MX_Controller{

	private $hasLayout, $headervar, $footervar;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();
        if(!$this->session->userdata('logged_in_anuario_arquiteto'))
            redirect('sistema-anuario/arquitetos/home/');

        if(finalizado(2))
            redirect('sistema-anuario/arquitetos/home/finalizado');
    }

    function index(){

        $qry_passo = $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))
                              ->where('passo', 2)->get('cadastro_anuario_passos_arquitetos')->num_rows();

        if ($qry_passo > 0) {
            // Já tem um layout armazenado
            $data['layout'] = $this->db->order_by('numero_pagina', 'asc')
                               ->where('id_cadastro_anuario', $this->session->userdata('id'))
                               ->where('ano', date('Y'))
                               ->get('cadastro_anuario_layout_arquitetos')
                               ->result();

            $data['encoded'] = json_encode($data['layout']);

            foreach ($data['layout'] as $key => $value) {

                $tipo = ($value->numero_pagina%2==0) ? "par" : "impar";

                if($value->layout_especial)
                    $tipo = "especial";

                $html = $this->htmlLayout($tipo, $value->id_layout);
                // Substitui numero da página nos inputs
                $html = preg_replace('/pag#/', 'pag'.$value->numero_pagina, $html, -1);
                // Posiciona imagem 1
                $html = preg_replace('/#I#/', $value->imagem1, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem1, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda1, $html, 1);
                // Posiciona imagem 2
                $html = preg_replace('/#I#/', $value->imagem2, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem2, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda2, $html, 1);
                // Posiciona imagem 3
                $html = preg_replace('/#I#/', $value->imagem3, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem3, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda3, $html, 1);
                // Posiciona imagem 4
                $html = preg_replace('/#I#/', $value->imagem4, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem4, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda4, $html, 1);
                // Posiciona imagem 5
                $html = preg_replace('/#I#/', $value->imagem5, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem5, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda5, $html, 1);
                // Posiciona imagem 6
                $html = preg_replace('/#I#/', $value->imagem6, $html, 1);
                $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$this->session->userdata('id').'/thumbs/'.$value->imagem6, $html, 1);
                $html = preg_replace('/#L#/', $value->legenda6, $html, 1);
                $value->layout_fill = $html;
            }
        }else{
            //$this->load->view('common/header', $this->headervar);
            //$this->load->view('layout');
            //$this->load->view('common/footer', $this->footervar);

            // Não tem um layout armazenado
            $data['layout'] = false;
        }
        $this->load->view('common/header', $this->headervar);
        $this->load->view('layout', $data);
        $this->load->view('common/footer', $this->footervar);
    }

    function reset(){
        $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))->delete('cadastro_anuario_layout_arquitetos');
        $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))->where('passo', 2)->delete('cadastro_anuario_passos_arquitetos');
        redirect('sistema-anuario/arquitetos/layout');
    }

    private function htmlLayout($tipo, $idLayout){
        $retorno['impar'][1] = <<<STR
<div class="layout">
    <div class="arquivo w190 h245 mb">
        <div class="dimensoes">190 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][2] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img6">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][3] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][4] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][7] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][8] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][9] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][10] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][11] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][12] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][13] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][14] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][15] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][16] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][17] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][18] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][19] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][20] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][21] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][22] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][23] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][24] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][25] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][26] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 mb">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][27] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr fl">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][28] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][29] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][30] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 fl mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][31] = <<<STR
<div class="layout">
    <div class="arquivo w220 h290">
        <div class="dimensoes">220 x 290 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][1] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][2] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][3] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $retorno['par'][4] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto" style='margin-top:-160px'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $retorno['par'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $retorno['par'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-300px'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $retorno['especial']['e1'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['especial']['e2'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h160 mb mr">
        <div class="dimensoes">296 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-160px;'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['especial']['e3'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h160 mr">
        <div class="dimensoes">296 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>

</div>
STR;
    $retorno['especial']['e4'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h245 mr fl">
        <div class="dimensoes">296 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $retorno['especial']['e5'] = <<<STR
<div class="layout">
    <div class="arquivo w320 h290 mr fl">
        <div class="dimensoes">320 x 290 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem aberto" data-imagem="#I#">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="#T#">
            <input type="text" value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    return (isset($retorno[$tipo][$idLayout])) ? $retorno[$tipo][$idLayout] : "";
    }
}