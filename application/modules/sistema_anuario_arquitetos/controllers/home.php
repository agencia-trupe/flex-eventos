<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Home extends MX_Controller{

	private $hasLayout, $headervar, $footervar;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();
    }

    function index(){
        if(!$this->session->userdata('logged_in_anuario_arquiteto')){
            $this->load->view('common/header', $this->headervar);
            $this->load->view('login');
            $this->load->view('common/footer', $this->footervar);
        }else{
    		redirect('sistema-anuario/arquitetos/identificacao');
        }
    }

    function login(){
        if(!$this->simplelogin->login($this->input->post('login'), $this->input->post('pass'), $this->input->post('tplogin')))
            $this->session->set_flashdata('errlogin', true);

        redirect('sistema-anuario/arquitetos');
    }

    function logout(){
        $this->simplelogin->logout();
        redirect('sistema-anuario/arquitetos');
    }

    function finalizado(){

        $data['diagramado'] = $this->db->order_by('versao', 'DESC')
                                        ->get_where('cadastro_anuario_diagramado_arquitetos', array('id_cadastro_anuario' => $this->session->userdata('id')))
                                        ->result();

        $this->load->view('common/header', $this->headervar);
        $this->load->view('finalizado', $data);
        $this->load->view('common/footer', $this->footervar);
    }

    function revisar(){
        $observacao = $this->input->post('observacao');
        $usuario = $this->input->post('usuario');

        $check = $this->db->order_by('versao', 'DESC')
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->get('cadastro_anuario_diagramado_arquitetos')
                            ->result();

        $atual = $check[0];

        $update = $this->db->set('aprovado', 2)
                            ->set('observacao', $observacao)
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->where('id', $check[0]->id)
                            ->update('cadastro_anuario_diagramado_arquitetos');

        $update = $this->db->set('diagramado', 3)
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->update('cadastro_anuario_layout_arquitetos');
        echo ($update) ? 1 : 0;
    }

    function aprovar(){
        $usuario = $this->input->post('usuario');

        $check = $this->db->order_by('versao', 'DESC')
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->get('cadastro_anuario_diagramado_arquitetos')
                            ->result();

        $atual = $check[0];

        $update = $this->db->set('aprovado', 3)
                            ->set('data_aprovacao', date('Y-m-d H:i:s'))
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->where('id', $check[0]->id)
                            ->update('cadastro_anuario_diagramado_arquitetos');

        $update = $this->db->set('diagramado', 4)
                            ->where('id_cadastro_anuario', $this->session->userdata('id'))
                            ->update('cadastro_anuario_layout_arquitetos');
        echo ($update) ? 1 : 0;
    }

    function gerarNovaSenha(){ echo cripto(''); }

    function recoveryForm($token = ''){

    	if(!$token) redirect('sistema-anuario/arquitetos');

    	$data = array(
    		'token' => $token
    	);
    	
    	$this->load->view('common/header', $this->headervar);
        $this->load->view('recuperacao/form', $data);
        $this->load->view('common/footer', $this->footervar);
	}

    function recovery(){

    	$token = $this->input->post('token');
    	$tabela = 'cadastro_anuario_arquitetos';

    	if(!$token) redirect('sistema-anuario/arquitetos');

    	if($this->db->get_where($tabela, array('token' => $token))->num_rows() == 0) redirect('sistema-anuario/arquitetos');

    	$usuario = $this->db->get_where($tabela, array('token' => $token))->result();

    	if(!isset($usuario[0])) redirect('sistema-anuario/arquitetos');

    	$this->db->set('token', '')->set('senha', cripto($this->input->post('senha')))->where('id', $usuario[0]->id)->update('cadastro_anuario_arquitetos');

    	$this->load->view('common/header', $this->headervar);
        $this->load->view('recuperacao/senhaAlterada');
        $this->load->view('common/footer', $this->footervar);
    }

    function recuperarSenha()
    {
    	$email = $this->input->post('loginrecov');
    	$tipo = $this->input->post('tplogin');

    	$tabela = ($tipo == '3') ? 'cadastro_anuario_empresas' : 'cadastro_anuario_arquitetos';

    	if($this->db->get_where($tabela, array('email' => $email))->num_rows() > 0){
    		$retorno = $this->enviaEmailRecovery($email, $tabela);
    		if($retorno !== true){
    			echo $retorno;
    		}else{
    			echo "Um link para iniciar a recuperação de senha foi enviado para o seu e-mail";
    		}
    	}else{
    		echo "O e-mail informado não está cadastrado";
    	}
    }

    private function enviaEmailRecovery($email, $tabela){
    	if(!$email)
    		return 'erro 1';

    	$usuario = $this->db->get_where($tabela, array('email' => $email))->result();

    	if(!isset($usuario[0]))
    		return 'erro 2';

    	$tokenHash = cripto('token'.$usuario[0]->email);
    	$this->db->set('token', $tokenHash)->where('id', $usuario[0]->id)->update($tabela);

        $nome = $usuario[0]->nome;
        $email = $usuario[0]->email;
    
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'anuario@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        $to = $email;
        $bcc = 'bruno@trupe.net';
        $assunto = 'Recuperação de Senha - Sistema Anuário Flex';

        $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Recuperação de Senha - Sistema Anuário Flex</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Acesse este link para definir uma nova senha: <strong><a href='http://www.flexeventos.com.br/sistema-anuario/arquitetos/home/recoveryForm/{$tokenHash}' title='Clique aqui para redefinir sua senha'>RECUPERAÇÃO DE SENHA</a></strong>
    </p>
    <p>
        Gratos<br>
        Flex Editora
    </p>
</body>
</html>
EML;

		$plain = <<<EML
Acesse este link para definir uma nova senha: http://www.flexeventos.com.br/sistema-anuario/arquitetos/home/recoveryForm/{$tokenHash}\r\n
Gratos\r\n
Flex Editora
EML;

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($from);

        $this->email->subject($assunto);
        $this->email->message($emailbody);
        $this->email->set_alt_message($plain);

        if($this->email->send()){
        	return true;
        }else{
        	return $this->email->print_debugger();
        }
    }    
}