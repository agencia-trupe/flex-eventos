<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Usuarios extends MY_AdmincontrollerModular {

   function __construct(){
      parent::__construct('logged_in_painel_anuario_arquitetos');

      $this->load->model('usuarios_model', 'model');

      $this->titulo = "Usuários";
      $this->unidade = "Usuário";
   }

   function excluir($id){
      if($this->model->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      if($this->session->userdata('id') == $id)
         redirect('sistema-anuario/arquitetos/painel/home/logout');

      redirect('sistema-anuario/arquitetos/painel/usuarios', 'refresh');
   }

    function inserir(){
        if($this->model->inserir()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('sistema-anuario/arquitetos/painel/'.$this->router->class.'/index', 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('sistema-anuario/arquitetos/painel/'.$this->router->class.'/index', 'refresh');
    }
}