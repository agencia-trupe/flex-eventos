<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Aprovados extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_anuario_arquitetos');

        $this->titulo = "";
        $this->unidade = "";
        $this->load->model('participacoes_model', 'model');
	}

    function index(){
        $data['registros'] = $this->model->pegarTodos(4);

        foreach ($data['registros'] as $key => $value) {
        	$arquivo = $this->model->arquivo($value->id_cadastro_anuario);
        	$value->data_envio = (isset($arquivo[0]) && $arquivo[0]->data_envio) ? formataTimestamp($arquivo[0]->data_envio) : '--';
        }

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/aprovados/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function detalhes($id_cadastro = false){
        if (!$id_cadastro)
            redirect('sistema-anuario/arquitetos/painel/aguardando');

        $data['cliente'] = $this->model->cadastro($id_cadastro);
        $data['layout'] = $this->model->layout($id_cadastro);
        $data['finalizacao'] = $this->model->finalizacao($id_cadastro);
        $data['arquivo'] = $this->model->arquivo($id_cadastro);
        $data['todas_imagens'] = array();

        foreach ($data['layout'] as $key => $value) {

            $tipo = ($value->numero_pagina%2==0) ? "par" : "impar";

            if($value->layout_especial == '1')
                $tipo = "especial";

            $html = $this->htmlLayout($tipo, $value->id_layout);
            // Substitui numero da página nos inputs
            $html = preg_replace('/pag#/', 'pag'.$value->numero_pagina, $html, -1);
            // Posiciona imagem 1
            $html = preg_replace('/#I#/', $value->imagem1, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem1, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda1, $html, 1);
            // Posiciona imagem 2
            $html = preg_replace('/#I#/', $value->imagem2, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem2, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda2, $html, 1);
            // Posiciona imagem 3
            $html = preg_replace('/#I#/', $value->imagem3, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem3, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda3, $html, 1);
            // Posiciona imagem 4
            $html = preg_replace('/#I#/', $value->imagem4, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem4, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda4, $html, 1);
            // Posiciona imagem 5
            $html = preg_replace('/#I#/', $value->imagem5, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem5, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda5, $html, 1);
            // Posiciona imagem 6
            $html = preg_replace('/#I#/', $value->imagem6, $html, 1);
            $html = preg_replace('/#T#/', '_imgs/anuario/imagens/arquitetos/'.$value->id_cadastro_anuario.'/thumbs/'.$value->imagem6, $html, 1);
            $html = preg_replace('/#L#/', $value->legenda6, $html, 1);
            $value->layout_fill = $html;

            if($value->imagem1)
                $data['todas_imagens'][] = $value->imagem1;
            if($value->imagem2)
                $data['todas_imagens'][] = $value->imagem2;
            if($value->imagem3)
                $data['todas_imagens'][] = $value->imagem3;
            if($value->imagem4)
                $data['todas_imagens'][] = $value->imagem4;
            if($value->imagem5)
                $data['todas_imagens'][] = $value->imagem5;
            if($value->imagem6)
                $data['todas_imagens'][] = $value->imagem6;
        }

        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/detalhes', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }


    private function htmlLayout($tipo, $idLayout){
        $retorno['impar'][1] = <<<STR
<div class="layout">
    <div class="arquivo w190 h245 mb">
        <div class="dimensoes">190 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][2] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][3] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][4] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][7] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][8] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][9] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][10] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][11] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][12] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][13] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][14] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][15] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][16] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][17] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][18] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][19] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][20] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][21] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][22] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][23] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][24] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][25] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][26] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 mb">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][27] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr fl">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][28] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][29] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][30] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 fl mr">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $retorno['impar'][31] = <<<STR
<div class="layout">
    <div class="arquivo w220 h290">
        <div class="dimensoes">220 x 290 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][1] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.

    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][2] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.

    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['par'][3] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    $retorno['par'][4] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto" style='margin-top:-160px'>
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    $retorno['par'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    $retorno['par'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-300px'>
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    $retorno['especial']['e1'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr">
        ÁREA DE TEXTO.

    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['especial']['e2'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h160 mb mr">
        <div class="dimensoes">296 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-160px;'>
        ÁREA DE TEXTO.

    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $retorno['especial']['e3'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h160 mr">
        <div class="dimensoes">296 x 160 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.

    </div>

</div>
STR;
    $retorno['especial']['e4'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h245 mr fl">
        <div class="dimensoes">296 x 245 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    $retorno['especial']['e5'] = <<<STR
<div class="layout">
    <div class="arquivo w320 h290 mr fl">
        <div class="dimensoes">320 x 290 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <div class="imagem aberto" data-imagem="#I#">
            <img src="#T#">
            <input type="text" disabled value="#L#" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.

    </div>
</div>
STR;
    return (isset($retorno[$tipo][$idLayout])) ? $retorno[$tipo][$idLayout] : "";
    }
}