<div id="faixa-superior">
	<a href="sistema-anuario/arquitetos/identificacao" title="Voltar ao 1o passo - Identificação" id="primeiro-passo">
		1o passo - Identificação
	</a>

	<a href="sistema-anuario/arquitetos/layout" title="Voltar ao 2o passo - Layout" id="segundo-passo">
		2o passo - Layout
	</a>

	<div id="terceiro-passo" class="ativo">
		3o passo - Finalização
	</div>
</div>

<p class="aviso-entrada">
	INFORME OS DADOS DESCRITIVOS DO SEU PROJETO:
</p>

<form action="sistema-anuario/arquitetos/finalizacao/enviar/" method="post" id="form-finalizacao">

	<div class="centro">

		<label>
			Nome da Obra <input type="text" name="nome_obra" required value="<?=$this->session->userdata('nome_obra')?>">
		</label>

		<label class="inline-label medium-label">
			Cidade <input type="text" name="cidade_obra" required value="<?=$this->session->userdata('cidade_obra')?>">
		</label>

		<label class="inline-label small-label">
			UF <input type="text" name="estado_obra" maxlength="2" required value="<?=$this->session->userdata('estado_obra')?>">
		</label>

		<label>
			Equipe Técnica <input type="text" name="equipe_tecnica" required value="<?=$this->session->userdata('equipe_tecnica')?>">
		</label>

		<label class="no-inset" for="txt-conceito">
			Conceito e Solução<br>
			<span>(máximo 950 caracteres)</span>
		</label>

		<textarea name="conceito_obra" class="alto" id="txt-conceito" required maxlength="950"><?=$this->session->userdata('conceito_obra')?></textarea>
		<p class="menor">Restam ainda <span id="caracteres-faltam">950</span> caracteres.</p>

		<label class="no-inset" for="txt-creditos">
			Crédito de Fotógrafo e/ou Ilustrador<br><span>(não obrigatório)</span>
		</label>

		<textarea name="creditos" id="txt-creditos" maxlength="300"><?=$this->session->userdata('creditos')?></textarea>

		<p class="obs vermelho">
			OBSERVAÇÃO: É expressamente proibido citar nomes de fornecedores em quaisquer dos textos acima.
		</p>

	</div>

	<div class="caixa-vermelha">

		<h1>VOCÊ FINALIZOU O PREENCHIMENTO DOS DADOS NECESSÁRIOS PARA COMPLETAR SUA PARTICIPAÇÃO NO 16&ordm; ANUÁRIO CORPORATIVO 2014.</h1>

		<div class="botao salvar">
			Você pode salvar para revisar os dados e fazer o envio posteriormente
			<a href="#" title="Salvar" id="salvar-projeto-finalizado">salvar</a>
			SEU LAYOUT NÃO SERÁ ENVIADO AINDA.
		</div>
		<div class="botao enviar">
			Você pode enviar imediatamente. (não será possível fazer alterações após o envio)
			<input type="submit" value="ENVIAR">
			SEU LAYOUT SERÁ ENVIADO IMEDIATAMENTE.
		</div>

	</div>

</form>

<div id="save-confirm">
	<p>
		Obrigado por preencher previamente os conteúdos da sua participação no Anuário Flex.
	</p>
	<p>
		Você salvou os conteúdos preenchidos até o momento, mas ainda não enviou para a Equipe do Anuário Flex. Você deve retornar a este sistema com seu login e senha para revisar e fazer o envio posteriormente.
	</p>
	<p>
		Gratos.<br>
		Flex Editora
	</p>
</div>