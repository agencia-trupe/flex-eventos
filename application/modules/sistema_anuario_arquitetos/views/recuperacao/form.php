<div class="caixa-cinza-final">
	<h1>DEFINIÇÃO DE NOVA SENHA</h1>

	<p>Informe sua nova senha</p>

	<form action="sistema-anuario/arquitetos/home/recovery" method="post" id="redefinicaoSenhaForm">

		<label>
			Nova senha<br>
			<input type="password" name="senha" id="inputSenha" required>
		</label>

		<br>

		<label>
			Confirme a nova senha<br>
			<input type="password" name="confirma-senha" id="inputConfSenha" required>
		</label>

		<br>

		<input type="hidden" name="token" value="<?=$token?>">

		<input type="submit" value="OK">
		
	</form>
	
</div>