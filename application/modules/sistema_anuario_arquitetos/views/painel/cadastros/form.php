<?$area='arquitetos'?>
<div class="container top">

    <div class="page-header users-header">
        <h2>
            Informações do cadastro
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        	<form method="post" action="sistema-anuario/<?=$area?>/painel/cadastros/alterar/<?=$registro->id?>" style='padding:10px; border:1px #CCCCCC solid' enctype="multipart/form-data">

		        <label>
		        	<strong>Nome Fantasia</strong> : <input type="text" name="nome_fantasia" value="<?=$registro->nome_fantasia?>">
		    	</label>
		        <label>
		        	<strong>Razão Social</strong> : <input type="text" name="razao_social" value="<?=$registro->razao_social?>">
		    	</label>
		        <label>
		        	<strong>Endereço</strong> : <input type="text" name="endereco" value="<?=$registro->endereco?>">
		    	</label>
		        <label>
		        	<strong>Número</strong> : <input type="text" name="numero" value="<?=$registro->numero?>">
		    	</label>
		        <label>
		        	<strong>Complemento</strong> : <input type="text" name="complemento" value="<?=$registro->complemento?>">
		    	</label>
		        <label>
		        	<strong>Bairro</strong> : <input type="text" name="bairro" value="<?=$registro->bairro?>">
		    	</label>
		        <label>
		        	<strong>Cidade</strong> : <input type="text" name="cidade" value="<?=$registro->cidade?>">
		    	</label>
		        <label>
		        	<strong>Estado</strong> : <input type="text" name="estado" value="<?=$registro->estado?>">
		    	</label>
		        <label>
		        	<strong>Cep</strong> : <input type="text" name="cep" value="<?=$registro->cep?>">
		    	</label>
		        <label>
		        	<strong>Telefone</strong> : <input type="text" name="telefone" value="<?=$registro->telefone?>">
		    	</label>
		        <label>
		        	<strong>Website</strong> : <input type="text" name="website" value="<?=$registro->website?>">
		    	</label>
		        <label>
		        	<strong>Nome</strong> : <input type="text" name="nome" value="<?=$registro->nome?>">
		    	</label>
		        <label>
		        	<strong>E-mail</strong> : <input type="text" name="email" value="<?=$registro->email?>">
		    	</label>
		        <label>
		        	<strong>Imagem</strong> :<br> <img style="width:500px" src="_imgs/anuario/marcas/<?=$registro->imagem?>"><br>
		        	<input type="file" name="userfile">
		    	</label>
		        <label>
		        	<strong>Apresentação</strong> : <textarea name="apresentacao"><?=$registro->apresentacao?></textarea>
		    	</label>
		        <input type="submit" value="Gravar" class="btn btn-success">
			</form>

        	<a href="#" class="btn btn-warning btn-large btn-voltar"><i class="icon-white icon-arrow-left"></i> Voltar</a>

   		</div>
  	</div>