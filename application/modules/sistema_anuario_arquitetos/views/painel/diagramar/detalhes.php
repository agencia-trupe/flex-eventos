<div class="container top">

    <div class="page-header users-header">
        <h2>
            Material para Diagramar
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        <h3>Identificação</h3>
        <strong>Nome Fantasia</strong> : <?=$cliente[0]->nome_fantasia?><br>
        <strong>Razão Social</strong> : <?=$cliente[0]->razao_social?><br>
        <strong>Endereço</strong> : <?=$cliente[0]->endereco?><br>
        <strong>Número</strong> : <?=$cliente[0]->numero?><br>
        <strong>Complemento</strong> : <?=$cliente[0]->complemento?><br>
        <strong>Bairro</strong> : <?=$cliente[0]->bairro?><br>
        <strong>Cidade</strong> : <?=$cliente[0]->cidade?><br>
        <strong>Estado</strong> : <?=$cliente[0]->estado?><br>
        <strong>Cep</strong> : <?=$cliente[0]->cep?><br>
        <strong>Telefone</strong> : <?=$cliente[0]->telefone?><br>
        <strong>Website</strong> : <?=$cliente[0]->website?><br>
        <strong>Nome</strong> : <?=$cliente[0]->nome?><br>
        <strong>E-mail</strong> : <?=$cliente[0]->email?><br>
        <strong>Imagem</strong> :<br> <img style="width:500px" src="_imgs/anuario/marcas/<?=$cliente[0]->imagem?>"><br>
        <strong>Apresentação</strong> : <p class="txt-formatado"><?=$cliente[0]->apresentacao?></p><br>

        <hr>

        <h3>Layout</h3>

        <?php foreach ($layout as $indice => $pagina): ?>
            <?php
            $numero = $indice + 1;
            $proxima = $numero + 1;
            ?>
                <?php if($pagina->layout_especial): ?>

                    <div class='paginas-placeholder'>
                        <div class='pagina esquerda'>
                            <a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
                                CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
                            </a>
                            <div class='numero-pagina'>
                                página <?=$numero?> - esquerda
                            </div>
                        </div>
                        <div class='pagina direita'>
                            <a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
                                CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
                            </a>
                            <div class='numero-pagina'>
                                página <?=$proxima?> - direita
                            </div>
                        </div>
                        <div class='pagina sel-especial aberto'>
                            LAYOUT DE PÁGINA ESPECIAL<br>
                            <span>Selecione o modelo de layout desta página</span>
                            <div class='opcoes'>
                                <a href='#' title='Selecionar este layout' data-layout='e1' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-dupla/lay1.png' alt='Layout 1'></a>
                                <a href='#' title='Selecionar este layout' data-layout='e2' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-dupla/lay2.png' alt='Layout 2'></a>
                                <a href='#' title='Selecionar este layout' data-layout='e3' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-dupla/lay3.png' alt='Layout 3'></a>
                                <a href='#' title='Selecionar este layout' data-layout='e4' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-dupla/lay4.png' alt='Layout 4'></a>
                                <a href='#' title='Selecionar este layout' data-layout='e5' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-dupla/lay5.png' alt='Layout 5'></a>
                            </div>
                            <div class='layout-fill aberto'>
                                <div class='target'>
                                    <?=$pagina->layout_fill?>
                                </div>
                                <div id='linha-bg'></div>
                            </div>
                        </div>
                    </div>

                <?php else: ?>

                    <?php if($pagina->numero_pagina%2 != 0): ?>

                        <div class='paginas-placeholder'>
                            <div class='pagina esquerda'>
                                <div class='aviso-lateral' style='display:none;'>
                                    Clique na<br> página para<br> selecionar o<br> layout desejado
                                </div>
                                <a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
                                    CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
                                </a>
                                <div class='opcoes-layout'>
                                    <span>Selecione o modelo de layout desta página</span>
                                    <div class='opcoes'>
                                        <a href='#' title='Selecionar este layout' data-layout='1' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay1.png' alt='Layout 1'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='2' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay2.png' alt='Layout 2'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='3' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay3.png' alt='Layout 3'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='4' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay4.png' alt='Layout 4'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='5' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay5.png' alt='Layout 5'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='6' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay6.png' alt='Layout 6'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='7' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay7.png' alt='Layout 7'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='8' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay8.png' alt='Layout 8'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='9' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay9.png' alt='Layout 9'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='10' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay10.png' alt='Layout 10'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='11' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay11.png' alt='Layout 11'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='12' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay12.png' alt='Layout 12'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='13' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay13.png' alt='Layout 13'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='14' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay14.png' alt='Layout 14'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='15' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay15.png' alt='Layout 15'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='16' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay16.png' alt='Layout 16'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='17' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay17.png' alt='Layout 17'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='18' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay18.png' alt='Layout 18'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='19' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay19.png' alt='Layout 19'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='20' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay20.png' alt='Layout 20'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='21' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay21.png' alt='Layout 21'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='22' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay22.png' alt='Layout 22'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='23' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay23.png' alt='Layout 23'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='24' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay24.png' alt='Layout 24'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='25' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay25.png' alt='Layout 25'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='26' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay26.png' alt='Layout 26'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='27' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay27.png' alt='Layout 27'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='28' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay28.png' alt='Layout 28'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='29' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay29.png' alt='Layout 29'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='30' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay30.png' alt='Layout 30'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='31' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay31.png' alt='Layout 31'></a>
                                    </div>
                                </div>
                                <div class='layout-fill aberto'>
                                    <div class='target'>
                                        <?php if(!$pagina->layout_especial): ?>
                                            <?=$pagina->layout_fill?>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class='numero-pagina'>
                                    página <?=$pagina->numero_pagina?> - esquerda
                                </div>
                            </div>

                    <?php else: ?>

                            <div class='pagina direita'>
                                <div class='aviso-lateral' style='display:none;'>
                                    Clique na<br> página para<br> selecionar o<br> layout desejado
                                </div>
                                <a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
                                    CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
                                </a>
                                <div class='opcoes-layout'>
                                    <span>Selecione o modelo de layout desta página</span>
                                    <div class='opcoes'>
                                        <a href='#' title='Selecionar este layout' data-layout='1' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay1.png' alt='Layout 1'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='2' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay2.png' alt='Layout 2'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='3' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay3.png' alt='Layout 3'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='4' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay4.png' alt='Layout 4'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='5' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay5.png' alt='Layout 5'></a>
                                        <a href='#' title='Selecionar este layout' data-layout='6' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/arquitetos-pag-direita/lay6.png' alt='Layout 6'></a>
                                    </div>
                                </div>
                                <div class='layout-fill aberto'>
                                    <div class='target'>
                                        <?=$pagina->layout_fill?>
                                    </div>
                                </div>
                                <div class='numero-pagina'>
                                    página <?=$pagina->numero_pagina?> - direita
                                </div>
                            </div>
                        </div>

                    <?php endif ?>

                <?php endif; ?>
        <?php endforeach ?>

        <a href="" title="Fazer Download das Imagens em Alta" class="btn btn-info btn-large" id="link-download-imagens" data-imagens="<?=implode(',',$todas_imagens)?>" data-diretorio="<?=$cliente[0]->id?>" data-sistema="arquitetos" data-user="<?=$cliente[0]->nome?>"><i class="icon-white icon-download"></i> Fazer Download das Imagens em Alta</a>
        <span id="mensagem-preparando"></span>

        <hr>

        <h3>Texto Formatado</h3>

        <p class="txt-formatado inline">
            <?=$cliente[0]->endereco?>,&nbsp;<?=$cliente[0]->numero?>
            <?php if($cliente[0]->complemento): ?>
                &nbsp;&middot;&nbsp;<?=$cliente[0]->complemento?>
            <?php endif; ?>
            <br><?=$cliente[0]->cep?>&nbsp;&middot;&nbsp;<?=$cliente[0]->cidade?>&nbsp;&middot;&nbsp;<?=$cliente[0]->estado?><br>
            Telefone&nbsp;<?=$cliente[0]->telefone?><br>
            <?=$cliente[0]->email?><br>
            <?php if($cliente[0]->website): ?>
                <br><?=$cliente[0]->website?>
            <?php endif; ?>
        </p>

        <hr>

        <h3>Finalização</h3>
        <strong>Nome da Obra</strong> : <?=$finalizacao[0]->nome_obra?><br>
        <strong>Cidade da Obra</strong> : <?=$finalizacao[0]->cidade_obra?><br>
        <strong>Estado da Obra</strong> : <?=$finalizacao[0]->estado_obra?><br><br>
        <strong>Equipe Técnica</strong> : <p class="txt-formatado"><?=$finalizacao[0]->equipe_tecnica?></p><br>
        <strong>Conceito da Obra</strong> : <p class="txt-formatado"><?=$finalizacao[0]->conceito_obra?></p><br>
        <strong>Créditos</strong> : <p class="txt-formatado"><?=$finalizacao[0]->creditos?></p><br>

        <hr>

        <form action="sistema-anuario/arquitetos/painel/diagramar/enviarDiagramado" method="post" enctype="multipart/form-data">
            <h2>Enviar Arquivo Diagramado</h2>
            <br>
            <label>
                Arquivo PDF diagramado:<br>
                <input type="file" name="userfile" required>
            </label>
            <input type="hidden" name="cliente" value="<?=$cliente[0]->id?>">
            <br>
            <input type="submit" value="Enviar" class="btn btn-success btn-large">
        </form>

        <br><br>

        <a href="#" class="btn btn-warning btn-large btn-voltar"><i class="icon-white icon-arrow-left"></i> Voltar</a>

    </div>
  </div>