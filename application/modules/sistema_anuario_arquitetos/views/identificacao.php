<div id="faixa-superior">

	<div id="primeiro-passo" class="ativo">
		1o passo - Identificação
	</div>

	<div id="segundo-passo" onclick="javascript: $('#form-identificacao').submit();">
		2o passo - Layout
	</div>

	<div id="terceiro-passo">
		3o passo - Finalização
	</div>

</div>

<div id="conteudo">

	<form action="sistema-anuario/arquitetos/identificacao/enviar" method="post" id="form-identificacao" enctype="multipart/form-data" <?php if($this->session->userdata('logged_in_anuario_arquiteto'))echo "class='logado'"?>>

		<div class="coluna-esquerda">
			<label>
				Nome Fantasia * <input type="text" name="nome_fantasia" required value="<?=$this->session->userdata('nome_fantasia')?>" id="input_nome_fantasia">
			</label>
			<label>
				Razão Social * <input type="text" name="razao_social" required value="<?=$this->session->userdata('razao_social')?>" id="input-razao_social">
			</label>
			<label>
				Endereço [logradouro] * <input type="text" name="endereco" required value="<?=$this->session->userdata('endereco')?>" id="input-endereco">
			</label>
			<label>
				Número * <input type="text" name="numero" required value="<?=$this->session->userdata('numero')?>" class="w100" id="input-numero">
			</label>
			<label>
				Complemento <input type="text" name="complemento" value="<?=$this->session->userdata('complemento')?>" class="w274" id="input-complemento">
			</label>
			<label>
				Bairro * <input type="text" name="bairro" required value="<?=$this->session->userdata('bairro')?>" id="input-bairro">
			</label>
			<label>
				Cidade * <input type="text" name="cidade" required value="<?=$this->session->userdata('cidade')?>" id="input-cidade">
			</label>
			<label>
				Estado [UF] * <input type="text" name="estado" maxlength="2" required value="<?=$this->session->userdata('estado')?>" class="w50" id="input-estado">
			</label>
			<label>
				CEP * <input type="text" name="cep" required value="<?=$this->session->userdata('cep')?>" class="w100" id="input-cep">
			</label>
			<label>
				<span style='font-size:12px;'>DDD e Telefone de Contato *</span> <input type="text" name="telefone" required value="<?=$this->session->userdata('telefone')?>" class="w274" id="input-telefone">
			</label>
			<label>
				Website <input type="text" name="website" value="<?=$this->session->userdata('website')?>" id="input-website">
			</label>
			<label class="espacador">
				Nome Completo * <input type="text" name="nome" required value="<?=$this->session->userdata('nome')?>" id="input-nome">
			</label>
			<label>
				E-mail * <input type="email" name="email" required value="<?=$this->session->userdata('email')?>" <?php if($this->session->userdata('logged_in_anuario_arquiteto'))echo" disabled"?> id="input-email">
			</label>
			<?php if($this->session->userdata('logged_in_anuario_arquiteto')): ?>
				<p class="pequeno">Só informe uma nova senha caso deseje alterá-la</p>
				<label>
					nova senha <input type="password" name="senha" id="input-senha" value="">
				</label>
				<label>
					repetir senha <input type="password" name="confirma_senha" id="confirma_senha" value="">
				</label>
			<?php else: ?>
				<label>
					senha * <input type="password" name="senha" required id="input-senha">
				</label>
				<label>
					repetir senha * <input type="password" name="confirma_senha" id="confirma_senha" required>
				</label>
			<?php endif; ?>
		</div>

		<div class="coluna-direita">
			<div class="caixa-cinza">
				<h3>FOTO DO(s) ARQUITETO(s)</h3>
				<?php if($this->session->userdata('logged_in_anuario_arquiteto')): ?>
					<img src="_imgs/anuario/marcas/<?=$this->session->userdata('imagem')?>">
					<label id="fake-label">
						<div id="fake-file">SELECIONAR IMAGEM</div>
						<input type="file" name="imagem" id="input-imagem">
					</label>
					<p>A imagem a ser enviada deve estar em formato JPG ou TIFF, 300 dpis, CMYK ou RGB em tamanho mínimo de 5 x 5 cm.</p>
				<?php else: ?>
					<label id="fake-label">
						<div id="fake-file">SELECIONAR IMAGEM</div>
						<input type="file" name="imagem" required id="input-imagem">
					</label>
					<p>A imagem a ser enviada deve estar em formato JPG ou TIFF, 300 dpis, CMYK ou RGB em tamanho mínimo de 5 x 5 cm.</p>
				<?php endif; ?>
			</div>
			<div class="caixa-cinza">
				<h3>DESCRITIVO DE APRESENTAÇÃO - ÍNDICE</h3>
				<p>Pequeno texto sobre a empresa - Máximo 300 caracteres. Será publicado no índice ao lado da foto dos arquitetos.</p>
				<textarea name="apresentacao" required maxlength="300" id="input-apresentacao"><?=$this->session->userdata('apresentacao')?></textarea>
				<p class="menor">Restam ainda <span id="caracteres-faltam">300</span> caracteres.</p>
			</div>
		</div>

		<div id="faixa-inferior">
			<div id="primeiro-passo">
				<input type="submit" value="PROSSEGUIR">
			</div>
			<div id="segundo-passo">
				2o PASSO
			</div>
			<div id="terceiro-passo">
				3o PASSO
			</div>
		</div>


	</form>

</div>

<?php if ($this->session->flashdata('mensagem_erro')): ?>
	<script defer>
	$('document').ready( function(){
		alert("<?=$this->session->flashdata('mensagem_erro')?>");
	});
	</script>
<?php endif ?>