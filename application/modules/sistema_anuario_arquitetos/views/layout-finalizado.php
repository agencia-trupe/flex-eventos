<div id="faixa-superior">
	<a href="sistema-anuario/arquitetos/identificacao" title="Voltar ao 1o passo - Identificação" id="primeiro-passo">
		1o passo - Identificação
	</a>

	<div id="segundo-passo" class="ativo">
		2o passo - Layout
	</div>

	<a href="sistema-anuario/arquitetos/finalizacao" title="Seguri para o 3 o passo" id="terceiro-passo">
		3o passo - Finalização
	</a>
</div>

<div style='height:200px;'>
	<p class="aviso-entrada">
		Seu layout já foi finalizado no primeiro preenchimento e está gravado em nossos servidores. Caso deseje realizar alterações, você terá que refazer todo o processo, enviando todo o conteúdo novamente. Deseja refazer todo o processo de envio de imagens?
	</p>

	<a href='sistema-anuario/arquitetos/layout/reset' title='Reiniciar o layout' class='resetar-pagina-f' style="margin-top:40px">SIM. REINICIAR LAYOUT</a>

</div>

<div id="faixa-inferior-layout" class="aberto">
	<div class="primeiro-passo-inferior">
		1o passo
	</div>
	<a href="sistema-anuario/arquitetos/finalizacao" title="Prosseguir" class="segundo-passo-inferior">
		PROSSEGUIR
	</a>
	<div class="terceiro-passo-inferior">
		3o passo
	</div>
</div>

<div id="redir-confirm" title="Deseja Reiniciar?">
	<h1>AVISO</h1>
	<p>
		Se você alterar o layout da página depois de ter enviado imagens você reiniciará o processo e terá que reenviar todas as imagens da página.
	</p>
</div>