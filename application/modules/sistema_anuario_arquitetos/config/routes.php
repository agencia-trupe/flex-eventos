<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['sistema_anuario_arquitetos'] = "home";
$route['sistema_anuario_arquitetos/painel'] = "painel/home";
$route['404_override'] = 'home';


/* End of file routes.php */
/* Location: ./application/config/routes.php */