<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['votacao'] = "home";
$route['votacao/painel'] = "painel/home";

$route['votacao/votar/(Profissionais|Estudantes)/(:num)'] = "votar/index/$1/$2";


$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */