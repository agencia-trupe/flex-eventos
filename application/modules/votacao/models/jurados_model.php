<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurados_model extends CI_Model {

	var $tabela;

	function __construct(){
		parent::__construct();

		$this->tabela = "premios_jurados";

		$this->dados = array('nome', 'email');
        $this->dados_tratados = array();
	}

	function backupJurados(){
		$query = $this->pegarTodos();
		$ano = '2013';
		foreach ($query as $key => $value) {
			$this->db->set('email', $value->email)
					 ->set('nome', $value->nome)
					 ->set('token', $value->token)
					 ->set('ano', $ano)
					 ->insert('premios_jurados_backup');
			$this->excluir($value->id);
		}
		
		return $ano;
	}

	function pegarTodos($order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function numeroResultados(){
		return $this->db->get($this->tabela)->num_rows();
	}

	function pegarPorId($id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function inserir($token){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		$this->db->set('token', $token);

		$insert = $this->db->insert($this->tabela);

		$id = $this->db->insert_id();

		$permissoes = $this->input->post('permissoes');
		foreach ($permissoes as $key => $value) {
			$this->db->set('id_premios_jurados', $id)->set('id_premios_setores', $value)->insert("premios_jurados_setores");
		}
		return $insert;
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			$update = $this->db->where('id', $id)->update($this->tabela);

			$this->db->where('id_premios_jurados', $id)->delete('premios_jurados_setores');
			$permissoes = $this->input->post('permissoes');
			foreach ($permissoes as $key => $value) {
				$this->db->set('id_premios_jurados', $id)->set('id_premios_setores', $value)->insert("premios_jurados_setores");
			}

			return $update;
		}
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}
}