<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Votacao_model extends CI_Model {

	var $tabela_setores,$tabela_premios,$tabela_participacoes;

	function __construct(){
		parent::__construct();

		$this->tabela_premios = "premios";
		$this->tabela_participacoes = "premios_inscricoes";
		$this->tabela_setores = "premios_setores";
	}

	function tabela(){ return $this->tabela; }
	function tabela_participacoes(){ return $this->tabela_participacoes; }

	function votacaoAberta($tipo = 'visitante'){
		$check = $this->db->get($this->tabela_premios)->result();

		if(!$check || !isset($check[0]))
			return false;

		if($tipo == 'visitante')  
			//return $check[0]->periodo_votacao_popular == 1 ? true : false;
			return false;
		elseif($tipo == 'jurado')
			return $check[0]->periodo_votacao_jurados == 1 ? true : false;
		else
			return false;
	}

	function premio(){
		$query = $this->db->where('inscricoes_abertas', 1)->get($this->tabela_premios)->result();
		if ($query) {
			if(isset($query[0]))
				return $query[0];
			else
				return false;
		}else
			return false;
	}

	function pegarNaoAprovados(){
		return $this->db->get_where($this->tabela_participacoes, array('projeto_aceito' => 0, 'YEAR(data_submissao)' => Date('Y')))->result();
	}

	function pegarReprovados(){
		return $this->db->get_where($this->tabela_participacoes, array('projeto_aceito' => 2, 'YEAR(data_submissao)' => Date('Y')))->result();
	}

	function pegarAprovados(){
		return $this->db->get_where($this->tabela_participacoes, array('projeto_aceito' => 1, 'YEAR(data_submissao)' => Date('Y')))->result();
	}

	function participacao($id_participacao){
		$query = $this->db->where('id', $id_participacao)->get($this->tabela_participacoes)->result();
		if ($query) {
			if(isset($query[0]))
				return $query[0];
			else
				return false;
		}else
			return false;
	}

	function aprovar($id_participacao){
		return $this->db->set('projeto_aceito', 1)
						->set('data_aceitacao', date('Y-m-d H:i:s'))
						->where('id', $id_participacao)
						->update($this->tabela_participacoes);
	}

	function reprovar($id_participacao){
		return $this->db->set('projeto_aceito', 2)
						->set('data_aceitacao', date('Y-m-d H:i:s'))
						->where('id', $id_participacao)
						->update($this->tabela_participacoes);
	}

	function pegarSetor($id_setor = false){
		if ($id_setor) {
			$query = $this->db->get_where($this->tabela_setores, array('id' => $id_setor))->result();
			if ($query) {
				if(isset($query[0]))
					return $query[0];
				else
					return false;
			}else
				return false;
		}else{
			return $this->db->order_by('id', 'asc')->get($this->tabela_setores)->result();
		}
	}

	// Verifica se o email possui permissão para votar em um determinado setor
	function permissao($id_setor, $email){
		$jurado = $this->jurado($email);
		$checkPermissao = $this->db->where('id_premios_jurados', $jurado->id)
									->where('id_premios_setores', $id_setor)
									->get('premios_jurados_setores')
									->num_rows();

		return ($checkPermissao > 0) ? true : false;
	}

	// Lista todos os setores que o Jurado pode votar
	function pegarSetoresPermitidos($email_jurado){
		$qry = <<<QRY
SELECT trupe_premios_setores.* FROM trupe_premios_jurados
LEFT JOIN trupe_premios_jurados_setores ON trupe_premios_jurados.id = trupe_premios_jurados_setores.id_premios_jurados
LEFT JOIN trupe_premios_setores ON trupe_premios_jurados_setores.id_premios_setores = trupe_premios_setores.id WHERE
trupe_premios_jurados.email = '$email_jurado';
QRY;
		return $this->db->query($qry)->result();
	}

	// Pega todos os projetos aceitos
	function pegarProjetos($tipo, $email, $categoria, $id_setor, $avaliados = false){
		$ano = Date('Y');

		if ($tipo == 'jurado') {

			$qryJurado = $this->jurado($email);
			$id_jurado = $qryJurado->id;

			if(!$avaliados){
				// Projetos aceitos não avaliados pelo jurado
				$query = <<<QRY
SELECT inscricoes.*, null as nota, null as data_votacao FROM trupe_premios_inscricoes inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND id NOT IN ( select id_premios_inscricoes from trupe_premios_votos_jurados where id_premios_jurados = '$id_jurado')
QRY;
			}else{
				// Projetos aceitos já avaliados pelo jurado
				$query = <<<QRY
SELECT inscricoes.*, votos.nota, votos.data as data_votacao FROM trupe_premios_inscricoes inscricoes
LEFT OUTER JOIN trupe_premios_votos_jurados votos ON inscricoes.id = votos.id_premios_inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND votos.id_premios_jurados = $id_jurado ORDER BY nota ASC;
QRY;
			}

		} else {
			$qryVisitante = $this->visitante($email);
			$id_visitante = $qryVisitante->id;

			if(!$avaliados){
				// Projetos aceitos não avaliados pelo visitante
				$query = <<<QRY
SELECT inscricoes.*, null as nota, null as data_votacao FROM trupe_premios_inscricoes inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND id NOT IN (select id_premios_inscricoes from trupe_premios_votos_visitantes where id_premios_visitantes = '$id_visitante')
QRY;
			}else{
				// Projetos aceitos já avaliados pelo visitante
				$query = <<<QRY
SELECT inscricoes.*, votos.nota, votos.data as data_votacao FROM trupe_premios_inscricoes inscricoes
LEFT OUTER JOIN trupe_premios_votos_visitantes votos ON inscricoes.id = votos.id_premios_inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND votos.id_premios_visitantes = $id_visitante ORDER BY nota ASC;
QRY;
			}
		}

		return $this->db->query($query)->result();
	}

	function numeroProjetos($tipo, $email, $categoria, $id_setor){
		$ano = Date('Y');
		if ($tipo == 'jurado') {

			$qryJurado = $this->jurado($email);
			$id_jurado = $qryJurado->id;

			$query = <<<QRY
SELECT * FROM(
SELECT inscricoes.*, null as nota, null as data_votacao FROM trupe_premios_inscricoes inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND id NOT IN (select id_premios_inscricoes from trupe_premios_votos_jurados where id_premios_jurados = '$id_jurado')
UNION ALL
SELECT inscricoes.*, votos.nota, votos.data as data_votacao FROM trupe_premios_inscricoes inscricoes
LEFT OUTER JOIN trupe_premios_votos_jurados votos ON inscricoes.id = votos.id_premios_inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND votos.id_premios_jurados = $id_jurado) as projetos ORDER BY nota ASC;
QRY;

		} else {

			$qryVisitante = $this->visitante($email);
			$id_visitante = $qryVisitante->id;

			$query = <<<QRY
SELECT * FROM(
SELECT inscricoes.*, null as nota, null as data_votacao FROM trupe_premios_inscricoes inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND id NOT IN (select id_premios_inscricoes from trupe_premios_votos_visitantes where id_premios_visitantes = '$id_visitante')
UNION ALL
SELECT inscricoes.*, votos.nota, votos.data as data_votacao FROM trupe_premios_inscricoes inscricoes
LEFT OUTER JOIN trupe_premios_votos_visitantes votos ON inscricoes.id = votos.id_premios_inscricoes
WHERE inscricoes.categoria = '$categoria'
AND inscricoes.setor = $id_setor
AND inscricoes.projeto_aceito = 1
AND YEAR(inscricoes.data_submissao) = $ano
AND votos.id_premios_visitantes = $id_visitante) as projetos ORDER BY nota ASC;
QRY;

		}

		return $this->db->query($query)->num_rows();
	}

	function listarParticipacoes($categoria, $id_setor, $aprovados = 1){
		return $this->db->where('categoria', $categoria)
						->where('setor', $id_setor)
						->where('YEAR(data_submissao)', Date('Y'))
						->where('projeto_aceito', $aprovados)
						->get($this->tabela_participacoes)
						->result();
	}

	function nota($id, $tabela){
		$nota = 0;
		$query = $this->db->where('id_premios_inscricoes', $id)->get($tabela)->result();
		foreach ($query as $key => $value) {
			$nota += $value->nota;
		}
		return $nota;
	}

	// Pega detalhes do jurado pelo email
	function jurado($email){
		$qry = $this->db->get_where('premios_jurados', array('email' => $email))->result();
		if ($qry && isset($qry[0])) {
			return $qry[0];
		}else{
			return false;
		}
	}

	// Pega detalhes do visitante pelo email
	function visitante($email = false){
		if ($email) {
			$qry = $this->db->get_where('premios_visitantes', array('email' => $email))->result();
			if ($qry && isset($qry[0])) {
				return $qry[0];
			}else{
				return false;
			}
		}else{
			return $this->db->get('premios_visitantes')->result();
		}
	}

	function pegarVisitantesPaginado($per_page, $pag){
		return $this->db->get('premios_visitantes', $per_page, $pag)->result();
	}

	function numeroVisitantes(){
		return $this->db->get('premios_visitantes')->num_rows();
	}

	function excluirVisitante($id){
		return $this->db->where('id', $id)->delete('premios_visitantes');
	}

	function excluirParticipacao($id){
		return $this->db->where('id', $id)->delete($this->tabela_participacoes);
	}
}