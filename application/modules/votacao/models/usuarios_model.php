<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'premios_painel';

		if($this->input->post('senha') != ''){

			$this->dados = array('login', 'senha', 'email');
			$this->dados_tratados = array(
				'senha' => cripto($this->input->post('senha'))
			);

		}else{
			$this->dados = array('login', 'email');
			$this->dados_tratados = array();
		}

	}

/*
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
*/

}