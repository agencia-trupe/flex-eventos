<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('votacao/home');
   	}

   	function cadastraEmail(){
   		$email = $this->input->post('email');

   		if($this->db->get_where('premios_jurados', array('email' => $email))->num_rows() > 0){
   			$token = $this->gerarToken();
   			$this->db->set('token', $token)->where('email', $email)->update('premios_jurados');
   			$this->enviaEmail('jurado',$email,$token);
   		}else{
        echo 'visitante';
      }
   	}

   	private function gerarToken(){
      $length = 22;
   		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return sha1($randomString);
   	}

   	private function enviaEmail($tipo,$email,$token){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'eventos@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        $to = $email;
        $bcc = 'bruno@trupe.net';
        $assunto = 'Acesso a Votação - Flex Eventos';

        $corpoemail = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Acesso a Votação</title>
    <meta charset="utf-8">
</head>
<body>
	<p>
		Seu e-mail foi cadastrado com sucesso no sistema de votação.
	</p>
	<p>
		<a href="http://www.flexeventos.com.br/votacao/votar/logar/$tipo/$email/$token">Clique aqui para acessar o sistema</a>
	</p>
</body>
</html>
EML;

        $plain = <<<EML
Seu e-mail foi cadastrado com sucesso no sistema de votação.\r\n\r\n
<a href="http://www.flexeventos.com.br/votacao/votar/logar/$tipo/$email/$token">Clique aqui para acessar o sistema</a>
EML;

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($email);

        $this->email->subject($assunto);
        $this->email->message($corpoemail);
        $this->email->set_alt_message($plain);

        $this->email->send();
   	}

    function votar(){
        // FAZER O ENVIO DA NOTA
        // AO SALVAR - CERTIFICAR QUE SÓ HAJA 1 NOTA POR JURADO
        // CERTIFICAR QUE A NOTA É UM INTEIRO ENTRE 1 E 5

        $this->load->model('votacao_model', 'model');

        $nota = $this->input->post('nota');
        $projeto = $this->input->post('id_projeto');
        $email = $_COOKIE['loginVotacaoFlexE'];
        $token = $_COOKIE['loginVotacaoFlexT'];
        $tipo = $_COOKIE['loginVotacaoFlexS'];
        $ip = ip();

        if($tipo == 'jurado'){

            $tabela_votos = "premios_votos_jurados";

            $jurado = $this->model->jurado($email);

            $checkVoto = $this->db->where('id_premios_inscricoes', $projeto)
                                  ->where('id_premios_jurados', $jurado->id)
                                  ->get($tabela_votos)
                                  ->num_rows();

            if($checkVoto == 0){
                $notas = array(1, 2, 3, 4, 5);
                if(in_array( $nota, $notas)){
                    if($this->logado()){
                        $this->db->set('id_premios_inscricoes', $projeto)
                                 ->set('id_premios_jurados', $jurado->id)
                                 ->set('nota', $nota)
                                 ->set('data', date('Y-m-d H:i:s'))
                                 ->set('ip', $ip)
                                 ->insert($tabela_votos);
                    }
                }
            }

        }elseif($tipo == 'visitante'){

            $tabela_votos = "premios_votos_visitantes";

            $visitante = $this->model->visitante($email);

            $checkVoto = $this->db->where('id_premios_inscricoes', $projeto)
                                  ->where('id_premios_visitantes', $visitante->id)
                                  ->get($tabela_votos)
                                  ->num_rows();

            if($checkVoto == 0){
                $notas = array(1, 2, 3, 4, 5);
                if(in_array( $nota, $notas)){
                    if($this->logado()){
                        $this->db->set('id_premios_inscricoes', $projeto)
                                 ->set('id_premios_visitantes', $visitante->id)
                                 ->set('nota', $nota)
                                 ->set('data', date('Y-m-d H:i:s'))
                                 ->insert($tabela_votos);
                    }
                }
            }
        }
    }

    private function logado(){
        $email = isset($_COOKIE['loginVotacaoFlexE']) ? $_COOKIE['loginVotacaoFlexE'] : false;
        $token = isset($_COOKIE['loginVotacaoFlexT']) ? $_COOKIE['loginVotacaoFlexT'] : false;
        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : false;

        if(!$email || !$token || !$tipo)
            return false;

        $tabela = ($tipo == 'jurado') ? "premios_jurados" : "premios_visitantes";

        if($this->db->where('email', $email)->where('token', $token)->get($tabela)->num_rows() > 0)
            return true;
        else
            return false;
    }
}
