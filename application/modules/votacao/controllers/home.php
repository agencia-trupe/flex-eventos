<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Home extends MX_Controller{

	function __construct(){
   		parent::__construct();

        $this->load->model('votacao_model', 'model');

        $qry_img = $this->db->get('trupe_premios')->result();
        $this->session->set_userdata('IMG-PREMIO', $qry_img[0]->imagem);
        $this->session->set_userdata('IMG-PREMIO-THUMB', $qry_img[0]->thumb_caixa_inscricao);
    }

    function index(){
        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : 'cookie-nao-encontrado';

        if ($this->logado()){

            $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : 'cookie-nao-encontrado';

            if($this->model->votacaoAberta($tipo)){
                redirect('votacao/votar/index');
            }else{
                $this->load->view('common/header');
                $this->load->view('common/menu');
                $this->load->view('votacoes-encerradas');
                $this->load->view('common/footer');
            }

        }else{
            $data['setores'] = $this->model->pegarSetor();

            $this->load->view('common/header');
            $this->load->view('common/menu');
            $this->load->view('lista-setores', $data);
            $this->load->view('common/footer');
        }
    }

    function logado(){
        $email = isset($_COOKIE['loginVotacaoFlexE']) ? $_COOKIE['loginVotacaoFlexE'] : false;
        $token = isset($_COOKIE['loginVotacaoFlexT']) ? $_COOKIE['loginVotacaoFlexT'] : false;
        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : false;

        if(!$email || !$token || !$tipo)
            return false;

        $tabela = ($tipo == 'jurado') ? "premios_jurados" : "premios_visitantes";

        if($this->db->where('email', $email)->where('token', $token)->get($tabela)->num_rows() > 0)
            return true;
        else
            return false;
    }

    function is_jurado(){
        return (isset($_COOKIE['loginVotacaoFlexS']) && $_COOKIE['loginVotacaoFlexS'] == 'jurado') ? true : false;
    }

    function visitante(){
        $this->load->view('common/header');
        $this->load->view('common/menu');
        $this->load->view('visitante', $data);
        $this->load->view('common/footer');
    }
}