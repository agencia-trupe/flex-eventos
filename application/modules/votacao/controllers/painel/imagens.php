<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Imagens extends MY_AdmincontrollerModular {

    function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->load->model('votacao_model', 'model');
    }

    function index(){

        $data['registros'] = $this->model->pegarAprovados();

        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function download($arquivo){
        $this->load->helper('download');

        $data = file_get_contents("_fichas/$arquivo");
        $name = $arquivo;

        force_download($name, $data);
    }
}