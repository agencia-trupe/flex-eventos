<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Jurados extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->titulo = "Cadastro de Jurados";
        $this->unidade = "Jurado";
        $this->load->model('jurados_model', 'model');
	}


    function backupJurados(){
        $ano = $this->model->backupJurados();
        echo "Backup do ano $ano OK";
    }

    function form($id = false){
    	$this->load->model('votacao_model', 'votacao');
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('votacao/painel/'.$this->router->class);

            $data['setores'] = array();
            $permitidos = $this->votacao->pegarSetoresPermitidos($data['registro']->email);
            foreach ($permitidos as $key => $value) {
            	$data['permissoes'][] = $value->id;
            }

        }else{
            $data['registro'] = FALSE;
        }

        $data['setores'] = $this->votacao->pegarSetor();
        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/form', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function inserir(){
        $token = $this->gerarToken();
        if($this->model->inserir($token)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        $this->enviaEmail('jurado', $this->input->post('email'), $token);

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }

    function excluir($id){
        if($this->model->excluir($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }

    function reenviarEmail($id){

        $jurado = $this->model->pegarPorId($id);

        if($jurado){
            $this->enviaEmail('jurado', $jurado->email, $jurado->token);

            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Email reenviado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Erro ao reenviar o email');
        }

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }

    private function gerarToken(){
        $length = 22;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return sha1($randomString);
    }

    private function enviaEmail($tipo,$email,$token){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $qry_evento = $this->db->get('premios')->result();
        $evento = $qry_evento[0];
        $titulo_evento = $evento->titulo;
        $imagem_evento = $evento->thumb_caixa_inscricao;

        $from = 'eventos@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        $to = $email;
        $bcc = false;
        $assunto = 'Ricardo Aronovich - Votação do X Grande Prêmio';

        $corpoemail = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Ricardo Aronovich - Votação do X Grande Prêmio</title>
    <meta charset="utf-8">
</head>
<body>
    <img src="cid:$titulo_evento">
    <p>
        Prezado Jurado,
    </p>
    <p>
        Você está recebendo a vossa série de trabalhos para votação no X Grande Prêmio de Arquitetura Corporativa.
        Precisamos que esta votação seja concluida até o dia 06 de setembro.
    </p>
    <p>
        Agradecemos vossa participação.<br>
        Atenciosamente,
    </p>
    <p>
        Ricardo Aronovich<br>
        Flex Eventos
    </p>
    <p>
        <a href="http://www.flexeventos.com.br/votacao/votar/logar/$tipo/$email/$token">Clique aqui para acessar o sistema</a>
    </p>
</body>
</html>
EML;

        $plain = <<<EML
Prezado Jurado,\r\n
Você está recebendo a vossa série de trabalhos para votação no X Grande Prêmio de Arquitetura Corporativa.Precisamos que esta votação seja concluida até o dia 06 de setembro.\r\n
Agradecemos vossa participação.\r\n
Atenciosamente,\r\n
Ricardo Aronovich\r\n
Flex Eventos\r\n
<a href="http://www.flexeventos.com.br/votacao/votar/logar/$tipo/$email/$token">Clique aqui para acessar o sistema</a>
EML;

        $this->email->attach("_imgs/premio/".$imagem_evento);
        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($email);

        $this->email->subject($assunto);
        $this->email->message($corpoemail);
        $this->email->set_alt_message($plain);

        $this->email->send();
    }
}