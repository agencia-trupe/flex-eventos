<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Aprovar extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->titulo = "Aprovar Participações";
        $this->unidade = "";
        $this->load->model('votacao_model', 'model');
	}

	function index(){
		$data['premio'] = $this->model->premio();
        $data['registros'] = $this->model->pegarNaoAprovados();

        foreach ($data['registros'] as $key => $value) {
        	$value->setor = $this->model->pegarSetor($value->setor);
        }

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}

	function detalhes($id = false){
		if (!$id)
			redirect('votacao/painel/aprovar/index');

		$data['detalhes'] = $this->model->participacao($id);

		$this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/detalhes', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}

	function aprovarParticipacao(){
		if($this->model->aprovar($this->input->post('id_participacao'))){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Participação aprovada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao aprovar participação');
        }

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
	}

    function reprovarParticipacao($id = false){
        if($id){
            if($this->model->reprovar($id)){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Participação reprovada com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao reprovar participação');
            }
        }
        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }

    function excluir($id = false){
        if($id){
            if($this->model->excluirParticipacao($id)){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Participação excluida com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluida participação');
            }
        }
        redirect('votacao/painel/'.$this->router->class.'/index');
    }

    function extrair(){

        if(isset($_POST['aprovados'])){

            $this->hasLayout = FALSE;
            $this->load->dbutil();

            $aprovados = $this->input->post('aprovados');

$qry = <<<QRY
SELECT
inscricoes.codigo_inscricao as 'Código de Inscrição',
inscricoes.nome as 'Nome',
inscricoes.email as 'E-mail',
CONCAT('(',inscricoes.ddd_fone,') ', inscricoes.fone) as 'Telefone',
CONCAT('(',inscricoes.ddd_celular,') ', inscricoes.celular) as 'Celular',
inscricoes.escritorio as 'Escritório',
inscricoes.endereco as 'Endereço',
inscricoes.bairro as 'Bairro',
inscricoes.cep as 'CEP',
inscricoes.cidade as 'Cidade',
inscricoes.estado as 'Estado',
inscricoes.categoria as 'Categoria',
setores.titulo as 'Setor',
inscricoes.nome_obra as 'Nome da Obra',
inscricoes.cliente as 'Cliente',
inscricoes.cidade_obra as 'Cidade da Obra',
inscricoes.uf_obra as 'Estado da Obra',
inscricoes.descricao_obra as 'Descrição da Obra',
CONCAT('http://www.flexeventos.com.br/_fichas/', inscricoes.prancha) as 'Prancha',
DATE_FORMAT( inscricoes.data_submissao, '%d/%m/%Y %H:%i:%s' ) as 'Data de Submissão',
(CASE inscricoes.projeto_aceito
WHEN 0 THEN 'Aguardando Aprovação'
WHEN 1 THEN 'Aprovado'
WHEN 2 THEN 'Reprovado'
END) as 'Projeto Aceito',
DATE_FORMAT( inscricoes.data_aceitacao, '%d/%m/%Y %H:%i:%s' ) as 'Data de Aprovação',
inscricoes.responsavel as 'Responsável pelo projeto',
inscricoes.gerenciadora as 'Gerenciadora do projeto',
inscricoes.responsavel_gerenciadora as 'Responsável pela Gerenciadora'
FROM flexeventos.trupe_premios_inscricoes inscricoes
LEFT JOIN trupe_premios premios ON
inscricoes.id_trupe_premios = premios.id
LEFT JOIN trupe_premios_setores setores ON
inscricoes.setor = setores.id
QRY;

            if($aprovados != 3)
                $qry .= " WHERE inscricoes.projeto_aceito = '$aprovados'";

            $filename = "participacoes_".Date('d-m-Y_H-i-s').'.csv';

            $inscricoes = $this->db->query($qry);

            if($inscricoes->num_rows() == 0){
                $this->session->set_flashdata('zero_cadastros', true);
                redirect('votacao/painel/aprovar/extrair');
            }

            $delimiter = ";";
            $newline = "\r\n";

            $this->output->set_header('Content-Type: application/force-download');
            $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
            $this->output->set_content_type('text/csv')->set_output(utf8_decode($this->dbutil->csv_from_result($inscricoes, $delimiter, $newline)), "ISO-8859-1", "UTF-8");

        }else{
            $this->load->view('painel/common/header', $this->headervar);
            $this->load->view('painel/common/menu', $this->menuvar);
            $this->load->view('painel/aprovar/extrair');
            $this->load->view('painel/common/footer', $this->footervar);
        }
    }
}