<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Reprovadas extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->titulo = "Participações Reprovadas";
        $this->unidade = "";
        $this->load->model('votacao_model', 'model');
	}

	function index(){
		$data['premio'] = $this->model->premio();
        $data['registros'] = $this->model->pegarReprovados();

        foreach ($data['registros'] as $key => $value) {
        	$value->setor = $this->model->pegarSetor($value->setor);
        }

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}

	function detalhes($id = false){
		if (!$id)
			redirect('votacao/painel/reprovadas/index');

		$data['detalhes'] = $this->model->participacao($id);

		$this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/detalhes', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}

    function excluir($id = false){
        if($id){
            if($this->model->excluirParticipacao($id)){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Participação excluida com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluida participação');
            }
        }
        redirect('votacao/painel/'.$this->router->class.'/index');
    }
}

?>