<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Visitantes extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->titulo = "Visitantes que Votaram";
        $this->load->model('votacao_model', 'model');
	}

    function index($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("votacao/painel/".$this->router->class."/index/"),
            'per_page' => 30,
            'uri_segment' => 5,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroVisitantes()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarVisitantesPaginado($pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function excluir($id){
        if($this->model->excluirVisitante($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Visitante excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Visitante');
        }

        redirect('votacao/painel/'.$this->router->class.'/index', 'refresh');
    }
}