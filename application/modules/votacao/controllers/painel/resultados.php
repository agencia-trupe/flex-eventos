<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Resultados extends MY_AdmincontrollerModular {

	function __construct(){
        parent::__construct('logged_in_painel_votacao');

        $this->load->model('votacao_model', 'model');
	}

	private static function ordenar($a, $b) {
	    return $b->soma - $a->soma;
	}


	function index($categoria = 'Profissionais', $setor = 1){

		$data['setores'] = $this->model->pegarSetor();
		$data['participacoes'] = $this->model->listarParticipacoes($categoria, $setor);

		foreach ($data['participacoes'] as $key => $value) {
			$value->nota_jurados = $this->model->nota($value->id, 'premios_votos_jurados');
			$value->nota_visitantes = $this->model->nota($value->id, 'premios_votos_visitantes');
			$value->soma = $value->nota_jurados + $value->nota_visitantes;
		}

		usort($data['participacoes'], array('Resultados','ordenar'));

		$data['categselecionada'] = $categoria;
		$data['setorselecionada'] = $setor;

		$this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}

	function detalhes($id){
		if (!$id)
			redirect('votacao/painel/resultados/index');

		$data['detalhes'] = $this->model->participacao($id);

		$this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/detalhes', $data);
        $this->load->view('painel/common/footer', $this->footervar);
	}
}