<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Votar extends MX_Controller{

	function __construct(){
   		parent::__construct();

        $this->load->model('votacao_model', 'model');

        $qry_img = $this->db->get('trupe_premios')->result();
        $this->session->set_userdata('IMG-PREMIO', $qry_img[0]->imagem);
        $this->session->set_userdata('IMG-PREMIO-THUMB', $qry_img[0]->thumb_caixa_inscricao);
    }

    function unsetCookies(){
        setcookie('loginVotacaoFlexE', '', (time() + (4 * 3600)), '/', 'flexeventos.com.br');
        setcookie('loginVotacaoFlexT', '', (time() + (4 * 3600)), '/', 'flexeventos.com.br');
        setcookie('loginVotacaoFlexS', '', (time() + (4 * 3600)), '/', 'flexeventos.com.br');
        echo "Cookies deletados.<br><a href='".base_url('votacao')."'>voltar</a>";
    }

    // Categoria = 'Profissionais' || 'Estudantes'
    // Setores = INT
    function index($categoria = false, $id_setor = false){

        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : 'jurado';

        if(!$this->model->votacaoAberta($tipo)){
            $this->load->view('common/header');
            $this->load->view('common/menu');
            $this->load->view('votacoes-encerradas');
            $this->load->view('common/footer');
        }else{

            $data['id_setor'] = $id_setor;
            $data['categoria'] = $categoria;
            $data['is_jurado'] = $this->is_jurado();

            if($id_setor)
                $data['setor'] = $this->model->pegarSetor($id_setor);
            else
                $data['setor']->titulo = "Não selecionado";

            if($this->logado()){
                if(!$categoria || $data['setor'] == 'Não selecionado'){

                    if ($_COOKIE['loginVotacaoFlexS'] == 'jurado') {
                        $data['setores'] = $this->model->pegarSetoresPermitidos($_COOKIE['loginVotacaoFlexE']);
                    }else{
                        $data['setores'] = $this->model->pegarSetor();
                    }

                    $this->load->view('common/header');
                    $this->load->view('common/menu');
                    $this->load->view('lista-setores', $data);
                    $this->load->view('common/footer');
                }else{

                    if ($_COOKIE['loginVotacaoFlexS'] == 'visitante') {
                        $data['permissao'] = false;
                    }else{
                        $data['permissao'] = $this->model->permissao($id_setor, $_COOKIE['loginVotacaoFlexE']);
                    }

                    $data['projetos'] = $this->model->pegarProjetos($_COOKIE['loginVotacaoFlexS'], $_COOKIE['loginVotacaoFlexE'], $categoria, $id_setor, FALSE);
                    //$data['debug'] = $this->db->last_query();

                    $this->load->view('common/header');
                    $this->load->view('common/menu');
                    if ($data['permissao']) {
                        $this->load->view('lista-projetos', $data);
                    }else{
                        $this->load->view('sem-permissao', $data);
                    }
                    $this->load->view('common/footer');
                }

            }else{

                if(!$data['categoria'])
                    $data['categoria'] = "Não selecionado";

                $this->load->view('common/header');
                $this->load->view('common/menu');
                $this->load->view('login', $data);
                $this->load->view('common/footer');

            }
        }
    }

    function avaliados($categoria = false, $id_setor = false){

        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : 'cookie-nao-encontrado';

        if(!$this->model->votacaoAberta($tipo)){
            $this->load->view('common/header');
            $this->load->view('common/menu');
            $this->load->view('votacoes-encerradas');
            $this->load->view('common/footer');
        }else{

            $data['id_setor'] = $id_setor;
            $data['categoria'] = $categoria;
            $data['is_jurado'] = $this->is_jurado();

            if($id_setor)
                $data['setor'] = $this->model->pegarSetor($id_setor);
            else
                $data['setor']->titulo = "Não selecionado";

            if($this->logado()){
                if(!$categoria || $data['setor'] == 'Não selecionado'){

                    if ($_COOKIE['loginVotacaoFlexS'] == 'jurado') {
                        $data['setores'] = $this->model->pegarSetoresPermitidos($_COOKIE['loginVotacaoFlexE']);
                    }else{
                        $data['setores'] = $this->model->pegarSetor();
                    }

                    $this->load->view('common/header');
                    $this->load->view('common/menu');
                    $this->load->view('lista-setores', $data);
                    $this->load->view('common/footer');
                }else{

                    if ($_COOKIE['loginVotacaoFlexS'] == 'visitante') {
                        $data['permissao'] = true;
                    }else{
                        $data['permissao'] = $this->model->permissao($id_setor, $_COOKIE['loginVotacaoFlexE']);
                    }

                    $data['projetos'] = $this->model->pegarProjetos($_COOKIE['loginVotacaoFlexS'], $_COOKIE['loginVotacaoFlexE'], $categoria, $id_setor, TRUE);

                    $this->load->view('common/header');
                    $this->load->view('common/menu');
                    if ($data['permissao']) {
                        $this->load->view('lista-projetos', $data);
                    }else{
                        $this->load->view('sem-permissao', $data);
                    }
                    $this->load->view('common/footer');
                }

            }else{

                if(!$data['categoria'])
                    $data['categoria'] = "Não selecionado";

                $this->load->view('common/header');
                $this->load->view('common/menu');
                $this->load->view('login', $data);
                $this->load->view('common/footer');

            }
        }
    }

    function logar($tipo,$email,$token){

        if(!$tipo || !$email || !$token)
            redirect('votacao/index');

        if($tipo == 'visitante'){
            /*
            if($this->db->where('email', $email)->where('token', $token)->get('premios_visitantes')->num_rows() > 0){
                setcookie('loginVotacaoFlexE', $email, (time() + (4 * 3600)), '/', 'flexeventos.com.br'); // Válido por 2 horas
                setcookie('loginVotacaoFlexT', $token, (time() + (4 * 3600)), '/', 'flexeventos.com.br'); // Válido por 2 horas
                setcookie('loginVotacaoFlexS', $tipo, (time() + (4 * 3600)), '/', 'flexeventos.com.br'); // Válido por 2 horas
            }
            */
            redirect('votacao/home/visitante');
        }elseif($tipo == 'jurado'){
            if($this->db->where('email', $email)->where('token', $token)->get('premios_jurados')->num_rows() > 0){
                setcookie('loginVotacaoFlexE', $email, (time() + (60 * 60 * 24 * 180)), '/', 'flexeventos.com.br'); // Válido por 180 dias / 6 meses
                setcookie('loginVotacaoFlexT', $token, (time() + (60 * 60 * 24 * 180)), '/', 'flexeventos.com.br'); // Válido por 180 dias / 6 meses
                setcookie('loginVotacaoFlexS', $tipo, (time() + (60 * 60 * 24 * 180)), '/', 'flexeventos.com.br'); // Válido por 180 dias / 6 meses
            }
        }
        redirect('votacao/home/index');
    }

    private function logado(){
        $email = isset($_COOKIE['loginVotacaoFlexE']) ? $_COOKIE['loginVotacaoFlexE'] : false;
        $token = isset($_COOKIE['loginVotacaoFlexT']) ? $_COOKIE['loginVotacaoFlexT'] : false;
        $tipo = isset($_COOKIE['loginVotacaoFlexS']) ? $_COOKIE['loginVotacaoFlexS'] : false;

        if(!$email || !$token || !$tipo)
            return false;

        $tabela = ($tipo == 'jurado') ? "premios_jurados" : "premios_visitantes";

        if($this->db->where('email', $email)->where('token', $token)->get($tabela)->num_rows() > 0)
            return true;
        else
            return false;
    }

    function is_jurado(){
        return (isset($_COOKIE['loginVotacaoFlexS']) && $_COOKIE['loginVotacaoFlexS'] == 'jurado') ? true : false;
    }
}