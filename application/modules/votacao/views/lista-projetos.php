<div class="banner-baixo">
	<h1 style="background-image:url('_imgs/premio/<?=$this->session->userdata('IMG-PREMIO-THUMB')?>');">VOTAÇÃO</h1>
	<div class="vermelho">
		<div class="categoria"><?=strtoupper($categoria)?></div>
		<div class="setor"><?=$setor->titulo?></div>
	</div>
</div>

<div class="submenu">
	<a href="votacao/votar/<?=$categoria?>/<?=$id_setor?>" <?if($this->router->method=='index')echo"class='ativo'"?>title="Mostrar participações não avaliadas">Não Avaliados</a>
	<a href="votacao/votar/avaliados/<?=$categoria?>/<?=$id_setor?>" <?if($this->router->method=='avaliados')echo"class='ativo'"?>title="Mostrar participações avaliadas">Avaliados</a>
</div>


<?php if ($projetos): ?>

	<?if($is_jurado):?>
		<div id="contador"></div>
	<?else:?>
		<div id="contador-visitantes">Projetos a serem avaliados</div>
	<?endif;?>

	<?php $notas = array(1,2,3,4,5) ?>

	<div class="paginar">

		<?php foreach ($projetos as $key => $value): ?>

			<?php if (in_array($value->nota, $notas)): ?>

				<div class="projeto animado avaliado">
					<div class="prancha">
						<img src="_fichas/reduzido/<?=$value->prancha?>" alt="Prancha do Projeto">
					</div>
					<div class="coluna">
						<div class="cod">Projeto: X<?=date('y')?>.<?=str_pad($value->codigo_inscricao, 5, '0', STR_PAD_LEFT)?></div>
						<div class="detalhes">
							<strong>Arquiteto: </strong> <?=$value->nome?> <br>
							<strong>Escritório: </strong> <?=$value->escritorio?> <br>
							<strong>Nome da Obra: </strong> <?=$value->nome_obra?> <br>
							<strong>Cliente: </strong> <?=$value->cliente?> <br>
							<strong>Local da Obra: </strong> <?=$value->cidade_obra .'-'. $value->uf_obra?> <br>
							<strong>Responsável: </strong> <?=$value->responsavel?> <br>
							<strong>Gerenciadora: </strong> <?=$value->gerenciadora?> <br>
							<strong>Responsável Gerenciadora: </strong> <?=$value->responsavel_gerenciadora?>
						</div>
						<div class="notas" data-projeto="<?=$value->id?>">
							<span>Na minha opinião esse projeto mereceu nota:</span>
							<div class="botoes">
								<img src="_imgs/layout/nota-<?=$value->nota?>.png">
							</div>
							<div class="enviar">
								<div class="data">
									VOTAÇÃO REALIZADA EM:<br><?=formataData($value->data_votacao, 'mysql2br')?>
								</div>
							</div>
						</div>
					</div>
					<?if($is_jurado):?>
						<div class="descricao_obra"><?=$value->descricao_obra?></div>
					<?endif;?>
				</div>

			<?php else: ?>

				<div class="projeto animado">
					<div class="prancha">
						<img src="_fichas/reduzido/<?=$value->prancha?>" alt="Prancha do Projeto">
					</div>
					<div class="coluna">
						<div class="cod">Projeto: X<?=date('y')?>.<?=str_pad($value->codigo_inscricao, 5, '0', STR_PAD_LEFT)?></div>
						<div class="detalhes">
							<strong>Arquiteto: </strong> <?=$value->nome?> <br>
							<strong>Escritório: </strong> <?=$value->escritorio?> <br>
							<strong>Nome da Obra: </strong> <?=$value->nome_obra?> <br>
							<strong>Cliente: </strong> <?=$value->cliente?> <br>
							<strong>Local da Obra: </strong> <?=$value->cidade_obra .'-'. $value->uf_obra?> <br>
							<strong>Responsável: </strong> <?=$value->responsavel?> <br>
							<strong>Gerenciadora: </strong> <?=$value->gerenciadora?> <br>
							<strong>Responsável Gerenciadora: </strong> <?=$value->responsavel_gerenciadora?>
						</div>
						<div class="notas" data-projeto="<?=$value->id?>">
							<span>Na minha opinião esse projeto merece nota:</span>
							<div class="botoes">
								<a href="#" data-nota="1" class="nota-1" title="Nota 1"><img src="_imgs/layout/1-estrela.png" alt="Nota 1"></a>
								<a href="#" data-nota="2" class="nota-2" title="Nota 2"><img src="_imgs/layout/2-estrela.png" alt="Nota 2"></a>
								<a href="#" data-nota="3" class="nota-3" title="Nota 3"><img src="_imgs/layout/3-estrela.png" alt="Nota 3"></a>
								<a href="#" data-nota="4" class="nota-4" title="Nota 4"><img src="_imgs/layout/4-estrela.png" alt="Nota 4"></a>
								<a href="#" data-nota="5" class="nota-5" title="Nota 5"><img src="_imgs/layout/5-estrela.png" alt="Nota 5"></a>
							</div>
							<div class="enviar">
								<a href="#" class="envio" title="VOTAR">VOTAR</a>
							</div>
						</div>
					</div>
					<?if($is_jurado):?>
						<div class="descricao_obra"><?=$value->descricao_obra?></div>
					<?endif;?>
				</div>

			<?php endif ?>

		<?php endforeach ?>

	</div>

	<div class="paginacao">
		<a href="#" class="vermais">VER MAIS &raquo;</a>
	</div>


<?php else: ?>

	<div id="contador" class="sem-projetos">
		<h1>Não há projetos a serem avaliados nesta categoria.</h1>
		<a href="javascript:window.history.back()" title="Voltar">VOLTAR</a>
	</div>

<?php endif ?>