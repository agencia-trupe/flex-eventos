<div class="banner" style="background-image:url('_imgs/premio/<?=$this->session->userdata('IMG-PREMIO')?>');">
	<h1>VOTE NOS PROJETOS PARTICIPANTES!</h1>
	<h2>SELECIONE ABAIXO AS CATEGORIAS PARA VER TODOS OS PROJETOS PARTICIPANTES E ATRIBUA SUA NOTA A CADA UM DELES.</h2>
	<h3>É necessário informar um e-mail de contato para votar e receber<br> o convite para participar do evento de premiação</h3>
</div>

<h1>Selecione a categoria para votar:</h1>

<?php if ($setores): ?>

	<table id="lista-setores">
		<thead>
			<tr>
				<td></td>
				<td>PROFISSIONAL</td>
				<td>ESTUDANTE</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($setores as $key => $value): ?>
				<tr>
					<td class="cinza"><?=$value->titulo?></td>
					<td class="vermelho"><a href="votacao/votar/Profissionais/<?=$value->id?>" title="Ver Projetos e Votar">VER PROJETOS E VOTAR</a></td>
					<td class="escuro"><a href="votacao/votar/Estudantes/<?=$value->id?>" title="Ver Projetos e Votar">VER PROJETOS E VOTAR</a></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

<?php endif ?>