<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="votacao/painel/jurados/index/">Jurados</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="votacao/painel/jurados/form/"><?=$titulo?></a>
    	</li>
  	</ul>

  	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
    		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('votacao/painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Email
		<input type="email" name="email" required autofocus value="<?=$registro->email?>"></label>

		<label>Nome
		<input type="text" name="nome" required  value="<?=$registro->nome?>"></label>

		<hr>

		Setores que o Jurado pode votar<br><br>
		<?php foreach ($setores as $key => $value): ?>
			<label style="width:400px;padding-left:15px;"><input type="checkbox" name="permissoes[]" <?if(in_array($value->id, $permissoes))echo" checked"?> value="<?=$value->id?>"> <?=$value->titulo?></label>
		<?php endforeach ?>

		<hr>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('votacao/painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Email
		<input type="email" name="email" required autofocus></label>

		<label>Nome
		<input type="text" name="nome" required></label>

		<hr>

		Setores que o Jurado pode votar<br><br>
		<?php foreach ($setores as $key => $value): ?>
			<label style="width:400px;padding-left:15px;"><input type="checkbox" name="permissoes[]" value="<?=$value->id?>"> <?=$value->titulo?></label>
		<?php endforeach ?>

		<hr>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>