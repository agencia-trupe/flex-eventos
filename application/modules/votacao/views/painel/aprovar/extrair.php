<div class="container top">

    <div class="page-header users-header">
        <h2>
            Extrair Relatório de Participações
        </h2>
    </div>


    <form action="votacao/painel/aprovar/extrair" method="post" style="text-align:center">

        <?php if ($this->session->flashdata('zero_cadastros')): ?>
            <div class="alert alert-block alert-error fade in" data-dismiss="alert">Não foram encontradas participações nos parâmetros especificados</div>
        <?php endif ?>

        <div style="width:270px; text-align:left; display:block; margin:10px auto">
            <label><input type="radio" name="aprovados" value="0" checked> Participações Aguardando Aprovação</label>
            <label><input type="radio" name="aprovados" value="1"> Participações Aprovadas</label>
            <label><input type="radio" name="aprovados" value="2"> Participações Reprovadas</label>
            <label><input type="radio" name="aprovados" value="3"> Todos</label>
        </div>

        <br><br>

        <input type="submit" class="btn btn-success btn-large" title="Download" value="Download">

    </form>