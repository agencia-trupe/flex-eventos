<div class="container top">

    <div class="page-header users-header">
        <h2>
            Aprovar Participação
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        <h3>Participação:</h3>
        <form action="votacao/painel/aprovar/aprovarParticipacao" method="post">
	        <strong>Nome</strong> : <?=$detalhes->nome?><br>
	        <strong>E-mail</strong> : <?=$detalhes->email?><br>
	        <strong>Telefone</strong> : <?=$detalhes->ddd_fone?> <?=$detalhes->fone?><br>
	        <strong>Celular</strong> : <?=$detalhes->ddd_celular?> <?=$detalhes->celular?><br>
	        <strong>Escritório</strong> : <?=$detalhes->escritorio?><br>
	        <strong>Endereço</strong> : <?=$detalhes->endereco?><br>
	        <strong>Bairro</strong> : <?=$detalhes->bairro?><br>
	        <strong>CEP</strong> : <?=$detalhes->cep?><br>
	        <strong>Cidade</strong> : <?=$detalhes->cidade?><br>
	        <strong>Estado</strong> : <?=$detalhes->estado?><br>
	        <hr>
	        <strong>Categoria</strong> : <?=$detalhes->categoria?><br>
	        <?$setor = $this->model->pegarSetor($detalhes->setor)?>
	        <strong>Setor</strong> : <?=$setor->titulo?><br>
	        <strong>Nome da Obra</strong> : <?=$detalhes->nome_obra?><br>
	        <strong>Cliente</strong> : <?=$detalhes->cliente?><br>
	        <strong>Cidade e Estado da Obra</strong> : <?=$detalhes->cidade_obra?> - <?=$detalhes->uf_obra?><br><br>
	        <strong>Descrição</strong> : <br><?=$detalhes->descricao_obra?><br><br>
	        <strong>Prancha</strong> : <img src="_fichas/reduzido/<?=$detalhes->prancha?>"><br>
	        <a href="_fichas/<?=$detalhes->prancha?>" target="_blank" title="ver tamanho original">ver tamanho original</a><br><br>
	        <strong>Data de Submissão</strong> : <?=formataTimestamp($detalhes->data_submissao)?><br>
	        <hr>
	        <strong>Responsável</strong> : <?=$detalhes->responsavel?><br>
	        <strong>Gerenciadora</strong> : <?=$detalhes->gerenciadora?><br>
	        <strong>Responsável Gerenciadora</strong> : <?=$detalhes->responsavel_gerenciadora?><br><br>
	        <input type="hidden" name="id_participacao" value="<?=$detalhes->id?>">
			<input type="submit" class="btn btn-large btn-success" style="vertical-align:top" value="APROVAR">
			<a href="votacao/painel/aprovar/reprovarParticipacao/<?=$detalhes->id?>" style="vertical-align:top" title="Reprovar Participação" class="btn btn-large btn-danger">REPROVAR</a>
	        <hr>
		</form>
        <a href="#" class="btn btn-warning btn-large btn-voltar"><i class="icon-white icon-arrow-left"></i> Voltar</a>

    </div>
  </div>