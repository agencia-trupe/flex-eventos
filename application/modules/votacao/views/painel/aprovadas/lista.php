<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

    <div class="page-header users-header">
        <h2>
            Participações Aprovadas
        </h2>
    </div>

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Prêmio</th>
              <th class="header">Nome</th>
              <th class="header">Categoria</th>
              <th class="header">Setor</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                    <td class="nowrap"><?=nomePremio($value->id_trupe_premios)?></td>
                    <td><?=$value->nome?></td>
                    <td><?=$value->categoria?></td>
                    <td><?=$value->setor->titulo?></td>
                    <td class="crud-actions" style="width:141px;">
                        <a href="votacao/painel/<?=$this->router->class?>/detalhes/<?=$value->id?>" class="btn btn-primary">visualizar</a>
                        <a href="votacao/painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                    </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <div class="page-header users-header">
          <h2>
              Nenhuma Participação
          </h2>
        </div>

      <?php endif ?>

    </div>
  </div>