<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            Visitantes
        </h2>
    </div>

<div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">E-mail</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                    <td><?=$value->email?></td>
                    <td class="crud-actions" style="width:60px;">
                        <a href="votacao/painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                    </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>
            <div class="pagination">
                <ul>
                    <?=$paginacao?>
                </ul>
            </div>
        <?php endif ?>

      <?php else:?>

        <h3>Nenhum Visitante Cadastrado</h3>

      <?php endif ?>

    </div>
  </div>