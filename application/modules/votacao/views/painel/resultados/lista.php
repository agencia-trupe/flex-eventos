<div class="container top">

    <div class="page-header users-header">
        <h2>
            Resultado da Votação
        </h2>
    </div>

    Setor:<br>
    <?php foreach ($setores as $key => $value): ?>
        <a href="votacao/painel/resultados/index/<?=$categselecionada?>/<?=$value->id?>" style="margin:3px 0" class="btn<?if($value->id==$setorselecionada)echo" btn-info"?>"><?=$value->titulo?></a>
    <?php endforeach ?>
    <hr>

    <div class="row">
        <div class="span12 columns">

        <?php if ($participacoes): ?>

            <table class="table table-striped table-bordered table-condensed table-sortable">

              <thead>
                <tr>
                    <th class="yellow header headerSortDown">Projeto</th>
                    <th class="header">Nome</th>
                    <th class="header">E-mail</th>
                    <th class="header">Nota Total - Jurados</th>
                    <th class="red header">Ações</th>
                </tr>
              </thead>

              <tbody>
                <?php foreach ($participacoes as $key => $value): ?>

                    <tr class="tr-row">
                        <td><?=$value->nome_obra?></td>
                        <td><?=$value->nome?></td>
                        <td><?=$value->email?></td>
                        <td><?=$value->nota_jurados?> estrelas</td>
                        <td class="crud-actions" style="width:60px;">
                            <a href="votacao/painel/<?=$this->router->class?>/detalhes/<?=$value->id?>" class="btn btn-success">detalhes</a>
                        </td>
                    </tr>

                <?php endforeach ?>
              </tbody>

            </table>

        <?else:?>

            <h3>Nenhuma Participação (Listando somente Participações APROVADAS)</h3>

        <?endif;?>

        </div>
    </div>

</div>