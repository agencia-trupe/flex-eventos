<div class="navbar navbar-fixed-top">

	<div class="navbar-inner">

		<div class="container">

	  		<a href="votacao/painel/home" class="brand">Votação Prêmio Flex</a>

	  		<ul class="nav">
	  			<li <?if($this->router->class=='home')echo" class='active'"?>><a href="votacao/painel/home">Início</a></li>
	  			<li class="dropdown <?if($this->router->class=='aprovar'||$this->router->class=='reprovadas')echo"active"?>">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Participações <b class="caret"></b></a>
		          	<ul class="dropdown-menu">
		            	<li><a href="votacao/painel/aprovar">Aguardando Aprovação</a></li>
		            	<li><a href="votacao/painel/reprovadas">Não Aprovadas</a></li>
		            	<li><a href="votacao/painel/aprovadas">Aprovadas</a></li>
		            	<li><a href="votacao/painel/aprovar/extrair">Extrair Participações</a></li>
		            	<li><a href="votacao/painel/imagens">Extrair Imagens em Alta</a></li>
		          	</ul>
		        </li>
				<li <?if($this->router->class=='jurados')echo" class='active'"?>><a href="votacao/painel/jurados">Jurados</a></li>
				<!-- <li <?if($this->router->class=='visitantes')echo" class='active'"?>><a href="votacao/painel/visitantes">Visitantes</a></li> -->
				<li class="dropdown <?if($this->router->class=='resultados')echo"active"?>">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Resultados <b class="caret"></b></a>
		          	<ul class="dropdown-menu">
		            	<li><a href="votacao/painel/resultados/index/Profissionais">Profissionais</a></li>
		            	<li><a href="votacao/painel/resultados/index/Estudantes">Estudantes</a></li>
		          	</ul>
		        </li>
				<li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
		          	<ul class="dropdown-menu">
		            	<li><a href="votacao/painel/usuarios">Usuários</a></li>
		            	<li><a href="votacao/painel/home/logout">Logout</a></li>
		          	</ul>
		        </li>
	  		</ul>

		</div>
  	</div>
</div>