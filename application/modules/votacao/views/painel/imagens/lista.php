<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            Download de Imagens de Participações (Aprovadas)
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

            <table class="table table-striped table-bordered table-condensed">

                <thead>
                    <tr>
                        <th class="yellow header headerSortDown">Participante</th>
                        <th class="header">Imagem (thumb)</th>
                        <th class="red header">Ações</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($registros as $key => $value): ?>

                    <tr class="tr-row" id="row_<?=$value->id?>">
                        <td><?=$value->nome?></td>
                        <td><img src="_fichas/thumbs/<?=$value->prancha?>" style="width:120px;"></td>
                        <td class="crud-actions">
                            <a href="votacao/painel/imagens/download/<?=$value->prancha?>" class="btn btn-success btn-down-img"><i class="icon-white icon-download"></i> download</a>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <?php else:?>

                <h3>Nenhum Registro</h2>

            <?php endif ?>

        </div>
    </div>