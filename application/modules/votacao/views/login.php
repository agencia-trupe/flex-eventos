<div class="banner-baixo">
	<h1 style="background-image:url('_imgs/premio/<?=$this->session->userdata('IMG-PREMIO-THUMB')?>');">VOTAÇÃO</h1>
	<div class="vermelho">
		<div class="categoria"><?=strtoupper($categoria)?></div>
		<div class="setor"><?=$setor->titulo?></div>
	</div>
</div>

<div class="login-box">
	Você selecionou a seguinte categoria para votar:<br>
	<span><?=strtoupper($categoria)?> | <?=$setor->titulo?></span><br>

	<form action="" method="" id="login-form" class="aberto">
		Informe seu e-mail para ter seus votos válidos:<br>
		<input type="email" name="email" placeholder="e-mail" autocomplete="off" required id="input-email">
		<input type="submit" value="ENVIAR">
	</form>

	<div class="msg-box">
		<span class="grd">Você recebeu um link em seu e-mail para validar sua participação na votação escolhida.</span>
		<span class="med">Verifique sua caixa postal e clique no link existente no e-mail para ver os projetos e votar.</span>
		<span class="peq mrg">Se não receber o e-mail, verifique sua caixa de lixo eletrônico ou tente repetir o processo a partir do início selecionando a categoria de votação.</span>
		<span class="peq">Se ainda assim tiver problemas com o recebimento, por favor contate nosso administrador através do e-mail: <a href="mailto:webmaster@flexeventos.com.br" title="Entre em contato">webmaster@flexeventos.com.br</a></span>
	</div>

</div>

