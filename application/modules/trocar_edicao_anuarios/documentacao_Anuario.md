# Documentação Sistema de Anuário FLEX

O sistema de anuário é dividido em:
: Empresas
: Arquitetos

A configuração de ambos ambientes é similar

## Empresas

Arquivos em : **application/modules/sistema_anuario_empresas**

Rotas para a Aplicação
: sistema_anuario/empresas
: sistema_anuario_empresas

Rotas para o Painel
: sistema_anuario/empresas/painel
: sistema_anuario_empresas/painel

## Workflow

1. Usuário se cadastra (finaliza passo 1)
2. Usuário seleciona layout e sobe imagens (finaliza passo 2)
3. Usuário informa detalhes da obra
4. Ao finalizar ele pode somente salvar para revisar posteriormente (finaliza passo 3)
5. Ou pode finalizar e enviar para Diagramação (finaliza passo 4)
6. Pedido de diagramação é enviado para o Painel de Controle
7. Responsável baixa as imagens e monta um mockup em pdf e envia para o usuário
8. Usuário recebe e-mail com aviso e ao logar no sistema é levado para tela de aprovação do PDF
9. Caso solicite alterações, o registro volta para o Painel na aba 'Revisão Diagramação'

## Tabelas

trupe_cadastro_anuario_passos_empresas
trupe_cadastro_anuario_passos_arquitetos
: Armazenam o passo atual em que o usuário está (somente para o 1o envio, depois disso o status é controlado pela tabela de layout)

+ 1 = identificação/cadastro completa
+ 2 = escolha de layout completa
+ 3 = participação finalizada mas não enviada
+ 4 = participação finalizada e enviada (pronto para diagramação)

### PASSO 1 - CADASTRO/IDENTIFICAÇÃO

trupe_cadastro_anuario_empresas
trupe_cadastro_anuario_arquitetos
: Armazenam os dados de login dos usuários.

### PASSO 2 - ESCOLHA DE LAYOUT/ENVIO DE IMAGENS

trupe_cadastro_anuario_layout_empresas
trupe_cadastro_anuario_layout_arquitetos
: Armazena o conteúdo da participação - páginas, layouts, imagens e legendas

### PASSO 3 - FINALIZAÇÃO

trupe_cadastro_anuario_finalizacao_empresas
trupe_cadastro_anuario_finalizacao_arquitetos
: Armazena os dados de finalização - detalhes da obra, da empresa, créditos

### PASSO 4 - ENVIO PARA DIAGRAMAÇÃO

trupe_cadastro_anuario_diagramado_empresas
trupe_cadastro_anuario_diagramado_arquitetos
: Armazena os arquivos pdf diagramados, suas versões e status

1. Status 1 : aguardando aprovação
2. Status 2 : enviado para revisão
3. Status 3 : aprovado

Os estágios dos layouts a serem diagramados estão no campo **diagramado** da tabela **trupe_cadastro_anuario_layout_**

1. Enviado para diagramação (aguardando PDF)
2. PDF enviado (aguardando aprovação)
3. PDF retornado para revisão (aguardando nova versão)
4. PDF aprovado


## Troca de Edição

Na troca de edição (2013 para 2014 por exemplo), é necessário apenas arquivar os registros das tabelas

- trupe_cadastro_anuario_passos_empresas
- trupe_cadastro_anuario_passos_arquitetos
- trupe_cadastro_anuario_layout_empresas
- trupe_cadastro_anuario_layout_arquitetos
- trupe_cadastro_anuario_finalizacao_empresas	
- trupe_cadastro_anuario_finalizacao_arquitetos
- trupe_cadastro_anuario_diagramado_empresas
- trupe_cadastro_anuario_diagramado_arquitetos

Interface para arquivar tabelas:
: **/trocar_edicao_anuarios**
: chave -> *nooeHaD39H3WrtVk7YOzFlkZ1VJY2I0l*