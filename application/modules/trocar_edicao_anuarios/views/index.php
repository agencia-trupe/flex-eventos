<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trocar edição do anuário</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>

	<div class="container" style="width:500px; margin:10px auto;">
	    <div class="jumbotron">

	    	<h3 style="margin:0 0 20px 0;">Trocar edição do anuário</h3>

	    	<?php if($this->session->flashdata('response')): ?>
	    		<?php echo $this->session->flashdata('response') ?>
	    	<?php endif; ?>

	    	<form class="form-inline" role="form" method="post" action="trocar_edicao_anuarios/home/arquivar">
	  			<div class="form-group">
	    			<input type="text" class="form-control" placeholder="key" name="senhaAcesso">
	  			</div>
	   			<button type="submit" class="btn btn-default">&rarr;</button>
			</form>
	    </div>		
	</div>

    
  </body>
</html>