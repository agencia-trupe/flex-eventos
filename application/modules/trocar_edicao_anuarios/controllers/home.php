<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Home extends MX_Controller{

	private $hasLayout, $headervar, $footervar, $senhaAcesso;

    function __construct(){
   		parent::__construct();
   		$this->senhaAcesso = 'nooeHaD39H3WrtVk7YOzFlkZ1VJY2I0l';
    }

    function index(){
        $this->load->view('index');
    }

    function arquivar(){
    	$senha = $this->input->post('senhaAcesso');

    	if($senha == $this->senhaAcesso){

	    	$ano = date('Y');
	    	$query = array();

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_passos_empresas_$ano LIKE trupe_cadastro_anuario_passos_empresas";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_passos_empresas_$ano (SELECT * FROM trupe_cadastro_anuario_passos_empresas)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_passos_empresas";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_passos_arquitetos_$ano LIKE trupe_cadastro_anuario_passos_arquitetos";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_passos_arquitetos_$ano (SELECT * FROM trupe_cadastro_anuario_passos_arquitetos)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_passos_arquitetos";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_layout_empresas_$ano LIKE trupe_cadastro_anuario_layout_empresas";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_layout_empresas_$ano (SELECT * FROM trupe_cadastro_anuario_layout_empresas)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_layout_empresas";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_layout_arquitetos_$ano LIKE trupe_cadastro_anuario_layout_arquitetos";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_layout_arquitetos_$ano (SELECT * FROM trupe_cadastro_anuario_layout_arquitetos)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_layout_arquitetos";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_finalizacao_empresas_$ano LIKE trupe_cadastro_anuario_finalizacao_empresas";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_finalizacao_empresas_$ano (SELECT * FROM trupe_cadastro_anuario_finalizacao_empresas)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_finalizacao_empresas";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_finalizacao_arquitetos_$ano LIKE trupe_cadastro_anuario_finalizacao_arquitetos";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_finalizacao_arquitetos_$ano (SELECT * FROM trupe_cadastro_anuario_finalizacao_arquitetos)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_finalizacao_arquitetos";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_diagramado_empresas_$ano LIKE trupe_cadastro_anuario_diagramado_empresas";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_diagramado_empresas_$ano (SELECT * FROM trupe_cadastro_anuario_diagramado_empresas)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_diagramado_empresas";

			$query[] = "CREATE TABLE x_historico_trupe_cadastro_anuario_diagramado_arquitetos_$ano LIKE trupe_cadastro_anuario_diagramado_arquitetos";
			$query[] = "INSERT INTO x_historico_trupe_cadastro_anuario_diagramado_arquitetos_$ano (SELECT * FROM trupe_cadastro_anuario_diagramado_arquitetos)";
			$query[] = "DELETE FROM trupe_cadastro_anuario_diagramado_arquitetos";

			// verificar se a tabela  existe

			if(!$this->db->table_exists("x_historico_trupe_cadastro_anuario_passos_empresas_$ano")){
				foreach ($query as $key => $value) {
					$this->db->query($value);
				}
    			$this->session->set_flashdata('response', "<div class='alert alert-success'>Edição alterada!</div>");    			
			}else{
				$this->session->set_flashdata('response', "<div class='alert alert-warning'>Histórico já existe</div>");	
			}
    		

    	}else{
    		$this->session->set_flashdata('response', "<div class='alert alert-warning'>Verifique a chave</div>");
    	}

    	redirect('trocar_edicao_anuarios', 'refresh');
    }
}
