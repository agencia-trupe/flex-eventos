<?php if ($diagramado): ?>

	<?php if($diagramado[0]->aprovado==3): ?>

		<div class="caixa-cinza-escuro">

			<h2>APROVAÇÃO DE LAYOUT</h2>

			<div class="revisar" style="margin:30px 0;">
				<p>
					Obrigado por ter enviado a aprovação e concluído seu processo de participação no Anuário Coporativo Flex.
				</p>
				<p>
					Você será informado por e-mail sobre a data de lançamento oficial do Anuário.
				</p>
				<p>
					Gratos. Equipe Flex Editora.
				</p>
			</div>

			<div style="text-align:left;">
				<a href="sistema-anuario/empresas/home/logout" title="sair" id='botao-logout' style="margin-left:0;">SAIR</a>
			</div>
		</div>

	<?php elseif($diagramado[0]->aprovado==1): ?>

		<div class="caixa-cinza-escuro">

			<h2>APROVAÇÃO DE LAYOUT</h2>

			<p class='metade'>
				Visualize a diagramação da sua participação no Anuário Corporativo clicando no botão ao lado e envie-nos a aprovação para impressão ou seus comentários para alterações.
			</p>

			<a href="_pdfs/anuario/empresas/<?=$diagramado[0]->id_cadastro_anuario?>/<?=$diagramado[0]->arquivo?>" target="_blank" class="visualizar-diagramacao" title="">VISUALIZAR PDF <img src="_imgs/layout/pdf-button.png"></a>

			<div class="caixa-aprovacao">
				<p>
					CLICANDO NO BOTÃO ABAIXO VOCÊ ESTÁ APROVANDO A ARTE-FINAL APRESENTADA NO PDF ACIMA<br>E AUTORIZANDO A PUBLICAÇÃO DESTA ARTE SEM NENHUMA ALTERAÇÃO NO ANUÁRIO CORPORATIVO FLEX.
				</p>

				<a href="sistema-anuario/empresas/home/aprovar" title="APROVAR E AUTORIZAR PUBLICAÇÃO IMPRESSA" data-usuario="<?=$this->session->userdata('id')?>">APROVAR E AUTORIZAR PUBLICAÇÃO</a>
			</div>

			<div class="revisar">
				<h3>SOLICITAR ALTERAÇÕES</h3>
				<p>Descreva abaixo os pontos que você quer alterar e aguarde novo envio de arte para aprovação.</p>
				<form action="sistema-anuario/empresas/home/revisar" method="post" data-usuario="<?=$this->session->userdata('id')?>">
					<textarea name="observacao" required></textarea>
					<input type="submit" value="ENVIAR">
				</form>
			</div>

			<div style="text-align:left;">
				<a href="sistema-anuario/empresas/home/logout" title="sair" id='botao-logout' style="margin-left:0;">SAIR</a>
			</div>

		</div>

	<?php else: ?>

		<div class="caixa-cinza-escuro">

			<h2>APROVAÇÃO DE LAYOUT</h2>

			<div class="revisar" style="margin:30px 0;">
				<p>
					Sua solicitação de alterações foi enviada!
					<br>
					Assim que as correções forem feitas na diagramação da sua participação você receberá um e-mail.
				</p>
			</div>

			<div style="text-align:left;">
				<a href="sistema-anuario/empresas/home/logout" title="sair" id='botao-logout' style="margin-left:0;">SAIR</a>
			</div>
		</div>

	<?php endif; ?>

<?php else: ?>

	<div style='height:400px;margin:40px 0;text-align:center;'>

		<p class="aviso-entrada">
			Os conteúdos da sua participação no Anuário Flex foram enviados com sucesso. Por favor, aguarde nosso comunicado com instruções para a aprovação da arte diagramada, que será enviado no seu e-mail cadastrado no sistema.

			<br><br>
			Gratos<br>
			Flex Editora
		</p>

		<div style='margin:40px 0'>
			<a href="sistema-anuario/empresas/home/logout" title="sair" id='botao-logout'>SAIR</a>
		</div>
	</div>

<?php endif ?>
