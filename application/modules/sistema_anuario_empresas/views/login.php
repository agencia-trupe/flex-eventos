<p class="aviso-entrada" style="font-size:13px;margin:10px 0">
	Este Sistema de Envio é específico para empresas «Fornecedores de produtos e serviços para o mercado de arquitetura.»<br>
	Caso sua empresa seja um escritório de arquitetura, por favor entre em contato com a Flex Editora para solicitar o link para outro Sistema de Envio.
</p>

<div id="tela-inicial">

	<div class="coluna esquerda">
		<p>
			Caro participante,
		</p>
		<p>
			Nesta edição do Anuário Corporativo, a Flex Editora, modificou o sistema de envio, tanto das imagens quanto dos textos a serem publicados. Com a determinação de aperfeiçoar e facilitar o processo para todos, a Flex desenvolveu uma ferramenta completa. As imagens agora serão carregadas online evitando transtornos de envio de CDs, pen drives ou uploads em sites externos. A escolha de layouts passa a ser mais didática e o participante terá maior facilidade para definir o layout e o local onde cada imagem deverá ser aplicada.
		</p>
		<p>
			O processo de aprovação das páginas também mudou. Assim que forem diagramadas um pdf em baixa resolução será disponibilizado para visualização e aprovação online no mesmo sistema de cadastro. O aviso com o link para o sistema será enviado por e-mail.
		<p>
			Certos de que a mudança irá beneficiar a todos, agradecemos a participação.
		</p>
		<p>
			Equipe Flex Editora
		</p>
	</div>

	<div class="coluna direita">

		<div class="botao vermelha">
			<a href="sistema-anuario/empresas/identificacao" title="Cadastrar">QUERO ME CADASTRAR &raquo;</a>
		</div>

		<div class="botao cinza">

			<a href="#" title="Fazer Login" class="tile" id="show-form-login-btn">JÁ TENHO CADASTRO  &raquo;</a>

			<form action="sistema-anuario/empresas/home/login" id="form-login" method="post">

				<label>
					e-mail (login)
					<input type="text" name="login" required>
				</label>

				<label>
					senha
					<input type="password" name="pass" required>
				</label>

				<input type="hidden" name="tplogin" value="3">

				<input type="submit" value="ENTRAR">

				<a href="" id="show-form-recovery-btn" title="Recuperação de senha">esqueci minha senha</a>

			</form>

			<form action="sistema-anuario/empresas/home/recuperarSenha" id="form-recovery" method="post">
				<label>
					e-mail cadastrado
					<input type="text" name="loginrecov" required>
				</label>
				<input type="hidden" name="tplogin" value="3">
				<input type="submit" value="RECUPERAR">
			</form>

		</div>

	</div>

</div>

<div class="aviso-navegadores">
	O sistema pode apresentar instabilidade em algumas versões do Internet Explorer. Prefira usar o Chrome ou Firefox em suas versões mais recentes.
</div>

<?php if($this->session->flashdata('errlogin')): ?>
	<script defer>
		$('document').ready( function(){
			alert("Verifique usuário e senha");
			setTimeout( function(){
				$('#show-form-login-btn').trigger('click');
			}, 500);
		});
	</script>
<?php endif; ?>