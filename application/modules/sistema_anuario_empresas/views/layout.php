<div id="overlay-salvar">
	<span>SALVANDO...</span>
	<div class='windows8'>
		<div class='wBall' id='wBall_1'>
			<div class='wInnerBall'></div>
		</div>
		<div class='wBall' id='wBall_2'>
			<div class='wInnerBall'></div>
		</div>
		<div class='wBall' id='wBall_3'>
			<div class='wInnerBall'></div>
		</div>
		<div class='wBall' id='wBall_4'>
			<div class='wInnerBall'></div>
		</div>
		<div class='wBall' id='wBall_5'>
			<div class='wInnerBall'></div>
		</div>
	</div>
</div>

<div id="faixa-superior">
	<a href="sistema-anuario/empresas/identificacao" title="Voltar ao 1o passo - Identificação" id="primeiro-passo">
		1o passo - Identificação
	</a>

	<div id="segundo-passo" class="ativo">
		2o passo - Layout
	</div>

	<div id="terceiro-passo">
		3o passo - Finalização
	</div>
</div>

<p class="aviso-entrada">
	Selecione a quantidade total de páginas da sua participação no Anuário:
	<a href="#" class="seleciona-paginas<?if($layout && (sizeof($layout) == 2 || sizeof($layout) + 1 == 2))echo' ativo'?>" title="2 páginas">2</a>
	<a href="#" class="seleciona-paginas<?if($layout && (sizeof($layout) == 4 || sizeof($layout) + 1 == 4))echo' ativo'?>" title="4 páginas">4</a>
	<a href="#" class="seleciona-paginas<?if($layout && (sizeof($layout) == 6 || sizeof($layout) + 1 == 6))echo' ativo'?>" title="6 páginas">6</a>
	<a href="#" class="seleciona-paginas<?if($layout && (sizeof($layout) == 8 || sizeof($layout) + 1 == 8))echo' ativo'?>" title="8 páginas">8</a>
</p>

<div class="fundo-cinza<?if($layout)echo' iniciado'?>" data-idus="<?=$this->session->userdata('id')?>" id="idus">

	<?php if(!$layout): ?>

		<div id="frase-inicial">
			Selecione acima o número de páginas da sua publicação no Anuário.<br>
			A seguir você irá selecionar o tipo de layout desejado.
		</div>

	<?php else: ?>

		<?php foreach ($layout as $indice => $pagina): ?>
			<?php
			$numero = $indice + 1;
			$proxima = $numero + 1;
			?>
				<?php if ($indice == 0): ?>
					<a href='#' title='Reiniciar o layout' class='resetar-pagina'>REINICIAR LAYOUT</a>
				<?php endif; ?>

				<?php if($pagina->layout_especial): ?>

					<div class='paginas-placeholder'>
						<div class='pagina esquerda'>
							<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
								CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
							</a>
							<div class='numero-pagina'>
								página <?=$numero?> - esquerda
							</div>
						</div>
						<div class='pagina direita'>
							<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
								CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
							</a>
							<div class='numero-pagina'>
								página <?=$proxima?> - direita
							</div>
						</div>
						<div class='pagina sel-especial aberto'>
							LAYOUT DE PÁGINA ESPECIAL<br>
							<span>Selecione o modelo de layout desta página</span>
							<div class='opcoes'>
								<a href='#' title='Selecionar este layout' data-layout='e1' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay1.png' alt='Layout 1'></a>
								<a href='#' title='Selecionar este layout' data-layout='e2' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay2.png' alt='Layout 2'></a>
								<a href='#' title='Selecionar este layout' data-layout='e3' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay3.png' alt='Layout 3'></a>
								<a href='#' title='Selecionar este layout' data-layout='e4' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay4.png' alt='Layout 4'></a>
								<a href='#' title='Selecionar este layout' data-layout='e5' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-dupla/lay5.png' alt='Layout 5'></a>
							</div>
							<div class='layout-fill aberto'>
								<div class='target'>
									<?=$pagina->layout_fill?>
								</div>
								<div id='linha-bg'></div>
							</div>
						</div>
					</div>

				<?php else: ?>

					<?php if($pagina->numero_pagina%2 != 0): ?>

						<div class='paginas-placeholder'>
							<div class='pagina esquerda'>
								<div class='aviso-lateral' style='display:none;'>
									Clique na<br> página para<br> selecionar o<br> layout desejado
								</div>
								<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
									CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
								</a>
								<div class='opcoes-layout'>
									<span>Selecione o modelo de layout desta página</span>
									<div class='opcoes'>
										<a href='#' title='Selecionar este layout' data-layout='1' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay1.png' alt='Layout 1'></a>
										<a href='#' title='Selecionar este layout' data-layout='2' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay2.png' alt='Layout 2'></a>
										<a href='#' title='Selecionar este layout' data-layout='3' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay3.png' alt='Layout 3'></a>
										<a href='#' title='Selecionar este layout' data-layout='4' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay4.png' alt='Layout 4'></a>
										<a href='#' title='Selecionar este layout' data-layout='5' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay5.png' alt='Layout 5'></a>
										<a href='#' title='Selecionar este layout' data-layout='6' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay6.png' alt='Layout 6'></a>
										<a href='#' title='Selecionar este layout' data-layout='7' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay7.png' alt='Layout 7'></a>
										<a href='#' title='Selecionar este layout' data-layout='8' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay8.png' alt='Layout 8'></a>
										<a href='#' title='Selecionar este layout' data-layout='9' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay9.png' alt='Layout 9'></a>
										<a href='#' title='Selecionar este layout' data-layout='10' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay10.png' alt='Layout 10'></a>
										<a href='#' title='Selecionar este layout' data-layout='11' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay11.png' alt='Layout 11'></a>
										<a href='#' title='Selecionar este layout' data-layout='12' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay12.png' alt='Layout 12'></a>
										<a href='#' title='Selecionar este layout' data-layout='13' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay13.png' alt='Layout 13'></a>
										<a href='#' title='Selecionar este layout' data-layout='14' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay14.png' alt='Layout 14'></a>
										<a href='#' title='Selecionar este layout' data-layout='15' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay15.png' alt='Layout 15'></a>
										<a href='#' title='Selecionar este layout' data-layout='16' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay16.png' alt='Layout 16'></a>
										<a href='#' title='Selecionar este layout' data-layout='17' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay17.png' alt='Layout 17'></a>
										<a href='#' title='Selecionar este layout' data-layout='18' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay18.png' alt='Layout 18'></a>
										<a href='#' title='Selecionar este layout' data-layout='19' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay19.png' alt='Layout 19'></a>
										<a href='#' title='Selecionar este layout' data-layout='20' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay20.png' alt='Layout 20'></a>
										<a href='#' title='Selecionar este layout' data-layout='21' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay21.png' alt='Layout 21'></a>
										<a href='#' title='Selecionar este layout' data-layout='22' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay22.png' alt='Layout 22'></a>
										<a href='#' title='Selecionar este layout' data-layout='23' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay23.png' alt='Layout 23'></a>
										<a href='#' title='Selecionar este layout' data-layout='24' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay24.png' alt='Layout 24'></a>
										<a href='#' title='Selecionar este layout' data-layout='25' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay25.png' alt='Layout 25'></a>
										<a href='#' title='Selecionar este layout' data-layout='26' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay26.png' alt='Layout 26'></a>
										<a href='#' title='Selecionar este layout' data-layout='27' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay27.png' alt='Layout 27'></a>
										<a href='#' title='Selecionar este layout' data-layout='28' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay28.png' alt='Layout 28'></a>
										<a href='#' title='Selecionar este layout' data-layout='29' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay29.png' alt='Layout 29'></a>
										<a href='#' title='Selecionar este layout' data-layout='30' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay30.png' alt='Layout 30'></a>
										<a href='#' title='Selecionar este layout' data-layout='31' data-pagina='<?=$numero?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/ambos-pag-esq/lay31.png' alt='Layout 31'></a>
									</div>
								</div>
								<div class='layout-fill aberto'>
									<div class='target'>
										<?php if(!$pagina->layout_especial): ?>
											<?=$pagina->layout_fill?>
										<?php endif;?>
									</div>
								</div>
								<div class='numero-pagina'>
									página <?=$pagina->numero_pagina?> - esquerda
								</div>
							</div>

					<?php else: ?>

							<div class='pagina direita'>
								<div class='aviso-lateral' style='display:none;'>
									Clique na<br> página para<br> selecionar o<br> layout desejado
								</div>
								<a class='seleciona-layout' href='#' title='Selecionar o layout desta página'>
									CLIQUE AQUI<br> PARA SELECIONAR O<br> LAYOUT DESTA PÁGINA
								</a>
								<div class='opcoes-layout'>
									<span>Selecione o modelo de layout desta página</span>
									<div class='opcoes'>
										<a href='#' title='Selecionar este layout' data-layout='1' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay1.png' alt='Layout 1'></a>
										<a href='#' title='Selecionar este layout' data-layout='2' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay2.png' alt='Layout 2'></a>
										<a href='#' title='Selecionar este layout' data-layout='3' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay3.png' alt='Layout 3'></a>
										<a href='#' title='Selecionar este layout' data-layout='4' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay4.png' alt='Layout 4'></a>
										<a href='#' title='Selecionar este layout' data-layout='5' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay5.png' alt='Layout 5'></a>
										<a href='#' title='Selecionar este layout' data-layout='6' data-pagina='<?=$pagina->numero_pagina?>'><div class='overlay'><div class='windows8'><div class='wBall' id='wBall_1'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_2'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_3'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_4'><div class='wInnerBall'></div></div><div class='wBall' id='wBall_5'><div class='wInnerBall'></div></div></div></div><img src='_imgs/layout/anuario/layouts/empresas-pag-direita/lay6.png' alt='Layout 6'></a>
									</div>
								</div>
								<div class='layout-fill aberto'>
									<div class='target'>
										<?=$pagina->layout_fill?>
									</div>
								</div>
								<div class='numero-pagina'>
									página <?=$pagina->numero_pagina?> - direita
								</div>
							</div>
						</div>

					<?php endif ?>

				<?php endif; ?>

		<?php endforeach ?>
		<script defer>
			var encoded = '<?=$encoded?>';
		</script>
	<?php endif; ?>
</div>

<div id="faixa-inferior-layout" <?if($layout)echo" class='aberto'"?>>
	<div class="primeiro-passo-inferior">
		1o passo
	</div>
	<a href="sistema-anuario/empresas/identificacao" title="Gravar Layout e Prosseguir" id="gravar-layout" class="segundo-passo-inferior">
		PROSSEGUIR
	</a>
	<div class="terceiro-passo-inferior">
		3o passo
	</div>
</div>

<div id="dialog-confirm" title="Deseja Reiniciar?">
	<h1>AVISO</h1>
	<p>
		Se você alterar o layout da página depois de ter enviado imagens você reiniciará o processo e terá que reenviar todas as imagens da página.
	</p>
</div>