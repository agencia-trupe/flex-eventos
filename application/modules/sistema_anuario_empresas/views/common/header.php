<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Flex Eventos - Sistema de Anuário para Empresas</title>
  <meta name="robots" content="index, nofollow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <link href='http://fonts.googleapis.com/css?family=Oxygen:400,700|Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>

  <?CSS(array('reset', 'uploadify', 'sistema-anuario', 'blitzer/jquery-ui-1.10.3.custom','fontface/stylesheet'))?>

  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min','jquery-ui-1.10.3.custom.min'))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>
    <?JS(array('jquery-ui-1.10.3.custom.min'))?>

  <?endif;?>

</head>
<body>

<header>
    <div class="centro">
        <div class="faixa-vermelha">
            <img src="_imgs/layout/marca-flexeventos.png" alt="Logotipo Flex">
        </div>
        <h1>
            SISTEMA DE ENVIO DE MATERIAIS PARA O ANUÁRIO
        </h1>
    </div>
</header>

<div class="main">

    <div id="banner-topo" class="empresas">
        <img src="_imgs/layout/anuario/banner-topo.jpg" alt="16o anuário corporativo">
        <h2>
            PRODUTOS, EMPRESAS E FORNECEDORES
        </h2>
    </div>