<?$area='empresas'?>
<div class="container top">

    <div class="page-header users-header">
        <h2>
            Informações do cadastro
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        	<?php if ($registro): ?>

	        	<form method="post" action="sistema-anuario/<?=$area?>/painel/cadastros/alterar/<?=$registro->id?>" style='padding:10px; border:1px #CCCCCC solid' enctype="multipart/form-data" id="form-edit-cad">

			        <label>
			        	<strong>Nome Fantasia</strong> : <input type="text" name="nome_fantasia" value="<?=$registro->nome_fantasia?>" required id="input-nome_fantasia">
			    	</label>
			        <label>
			        	<strong>Razão Social</strong> : <input type="text" name="razao_social" value="<?=$registro->razao_social?>">
			    	</label>
			        <label>
			        	<strong>Endereço</strong> : <input type="text" name="endereco" value="<?=$registro->endereco?>">
			    	</label>
			        <label>
			        	<strong>Número</strong> : <input type="text" name="numero" value="<?=$registro->numero?>">
			    	</label>
			        <label>
			        	<strong>Complemento</strong> : <input type="text" name="complemento" value="<?=$registro->complemento?>">
			    	</label>
			        <label>
			        	<strong>Bairro</strong> : <input type="text" name="bairro" value="<?=$registro->bairro?>">
			    	</label>
			        <label>
			        	<strong>Cidade</strong> : <input type="text" name="cidade" value="<?=$registro->cidade?>">
			    	</label>
			        <label>
			        	<strong>Estado</strong> : <input type="text" name="estado" value="<?=$registro->estado?>">
			    	</label>
			        <label>
			        	<strong>Cep</strong> : <input type="text" name="cep" value="<?=$registro->cep?>">
			    	</label>
			        <label>
			        	<strong>Telefone</strong> : <input type="text" name="telefone" value="<?=$registro->telefone?>">
			    	</label>
			        <label>
			        	<strong>Website</strong> : <input type="text" name="website" value="<?=$registro->website?>">
			    	</label>
			        <label>
			        	<strong>Nome do Contato</strong> : <input type="text" name="nome" value="<?=$registro->nome?>" required id="input-nome">
			    	</label>
			        <label>
			        	<strong>E-mail</strong> : <input type="text" name="email" value="<?=$registro->email?>" required id="input-email">
			    	</label>
			        <label>
			        	<strong>Imagem</strong> :<br> <img style="width:500px" src="_imgs/anuario/marcas/<?=$registro->imagem?>"><br>
			        	<input type="file" name="userfile">
			    	</label>
			        <label>
			        	<strong>Categoria</strong> : <br>
			        	<ul style="list-style:none">
							<li><label><input type="radio" name="categoria" <?if($registro->categoria=='mobiliario')echo" checked"?> value="mobiliario" required>MOBILIÁRIO</label></li>
							<li><label><input type="radio" name="categoria" <?if($registro->categoria=='componentes')echo" checked"?> value="componentes">COMPONENTES</label></li>
							<li><label><input type="radio" name="categoria" <?if($registro->categoria=='arquishow')echo" checked"?> value="arquishow">ARQUISHOW</label></li>
							<li><label><input type="radio" name="categoria" <?if($registro->categoria=='hr')echo" checked"?> value="hr">HR &bull; HOSPITAIS E HOTÉIS</label></li>
							<li><label><input type="radio" name="categoria" <?if($registro->categoria=='construtoras')echo" checked"?> value="construtoras">CONSTRUTORAS</label></li>
						</ul>
			    	</label>
			        <input type="submit" value="Gravar" class="btn btn-success">
				</form>

		<?php endif ?>

        	<a href="#" class="btn btn-warning btn-large btn-voltar"><i class="icon-white icon-arrow-left"></i> Voltar</a>

   		</div>
  	</div>