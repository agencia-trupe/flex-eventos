<div class="navbar navbar-fixed-top">

	<div class="navbar-inner">

		<div class="container">

	  		<a href="sistema-anuario/empresas/painel/home" class="brand">Anuário Flex - Empresas</a>

	  		<ul class="nav">
	  			<li <?if($this->router->class=='home')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/home">Início</a></li>
				<li <?if($this->router->class=='diagramar')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/diagramar">Material para Diagramar</a></li>
				<li <?if($this->router->class=='aguardando')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/aguardando">Aguardando Aprovação do Cliente</a></li>
				<li <?if($this->router->class=='revisao')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/revisao">Revisão Diagramação</a></li>
				<li <?if($this->router->class=='aprovados')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/aprovados">Aprovados</a></li>
				<li <?if($this->router->class=='cadastros')echo" class='active'"?>><a href="sistema-anuario/empresas/painel/cadastros">Cadastros</a></li>
				<li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
		          	<ul class="dropdown-menu">
		            	<li><a href="sistema-anuario/empresas/painel/usuarios">Usuários</a></li>
		            	<li><a href="sistema-anuario/empresas/painel/home/logout">Logout</a></li>
		          	</ul>
		        </li>
	  		</ul>

		</div>
  	</div>
</div>