<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

    <div class="page-header users-header">
        <h2>
            Aguardando Aprovação do Cliente
        </h2>
    </div>

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Empresa</th>
              <th class="header">Data de Envio do Material</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                    <td class="nowrap"><?=$value->nome?></td>
                    <td><?=$value->data_envio?></td>
                    <td class="crud-actions" style="width:78px;">
                        <a href="sistema-anuario/empresas/painel/<?=$this->router->class?>/detalhes/<?=$value->id_cadastro_anuario?>" class="btn btn-primary">visualizar</a>
                    </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <h2>Nenhuma Participação</h2>

      <?php endif ?>



    </div>
  </div>