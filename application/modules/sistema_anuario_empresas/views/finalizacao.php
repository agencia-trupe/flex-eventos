<div id="faixa-superior">
	<a href="sistema-anuario/empresas/identificacao" title="Voltar ao 1o passo - Identificação" id="primeiro-passo">
		1o passo - Identificação
	</a>

	<a href="sistema-anuario/empresas/layout" title="Voltar ao 2o passo - Layout" id="segundo-passo">
		2o passo - Layout
	</a>

	<div id="terceiro-passo" class="ativo">
		3o passo - Finalização
	</div>
</div>

<p class="aviso-entrada">
	INFORME OS DADOS QUE ACOMPANHAM SEU CADASTRO
</p>

<form action="sistema-anuario/empresas/finalizacao/enviar/" method="post" id="form-finalizacao">

	<div class="centro">

		<label class="no-inset" for="txt-conceito">
			Descritivo da Empresa<br>para a página de imagens<br><span>(máximo 400 caracteres)</span>
		</label>

		<textarea name="descritivo_empresa" class="alto" id="txt-conceito" required maxlength="400"><?=$this->session->userdata('descritivo_empresa')?></textarea>
		<p class="menor">Restam ainda <span id="caracteres-faltam">400</span> caracteres.</p>

		<!-- -->

		<label class="no-inset" for="txt-conceito">
			Pequeno texto sobre a empresa.<br>Será publicado no índice ao lado da marca.<br><span>(máximo 300 caracteres)</span>
		</label>

		<textarea name="sobre_empresa" class="alto" id="txt-conceito" required maxlength="300"><?=$this->session->userdata('sobre_empresa')?></textarea>
		<p class="menor">Restam ainda <span id="caracteres-faltam">300</span> caracteres.</p>

		<!-- -->

		<label class="no-inset" for="txt-creditos">
			Crédito de Fotógrafo e/ou Ilustrador<br><span>(não obrigatório)</span>
		</label>

		<textarea name="creditos" id="txt-creditos" maxlength="300" style="margin-bottom:60px"><?=$this->session->userdata('creditos')?></textarea>

	</div>

	<div class="caixa-vermelha">

		<h1>VOCÊ FINALIZOU O PREENCHIMENTO DOS DADOS NECESSÁRIOS PARA COMPLETAR SUA PARTICIPAÇÃO NO 16&ordm; ANUÁRIO CORPORATIVO 2014.</h1>

		<div class="botao salvar">
			Você pode salvar para revisar os dados e fazer o envio posteriormente
			<a href="#" title="Salvar" id="salvar-projeto-finalizado">salvar</a>
			SEU LAYOUT NÃO SERÁ ENVIADO AINDA.
		</div>
		<div class="botao enviar">
			Você pode enviar imediatamente. (não será possível fazer alterações após o envio)
			<input type="submit" value="ENVIAR">
			SEU LAYOUT SERÁ ENVIADO IMEDIATAMENTE.
		</div>

	</div>

</form>

<div id="save-confirm">
	<p>
		Obrigado por preencher previamente os conteúdos da sua participação no Anuário Flex.
	</p>
	<p>
		Você salvou os conteúdos preenchidos até o momento, mas ainda não enviou para a Equipe do Anuário Flex. Você deve retornar a este sistema com seu login e senha para revisar e fazer o envio posteriormente.
	</p>
	<p>
		Gratos.<br>
		Flex Editora
	</p>
</div>