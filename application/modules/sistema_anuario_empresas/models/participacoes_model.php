<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participacoes_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela_cadastro = 'cadastro_anuario_empresas';
		$this->tabela_layout = 'cadastro_anuario_layout_empresas';
		$this->tabela_finalizacao = 'cadastro_anuario_finalizacao_empresas';
		$this->tabela_passos = 'cadastro_anuario_passos_empresas';
		$this->tabela_diagramacao = 'cadastro_anuario_diagramado_empresas';
	}

	function pegarTodos($passo){
		$query = $this->db->group_by('id_cadastro_anuario')->get_where($this->tabela_layout, array('diagramado' => $passo))->result();
		foreach ($query as $key => $value) {
			$value->nome = $this->pegarTitulo($value->id_cadastro_anuario);
			$query_data = $this->db->where('passo', 4)
								   ->where('id_cadastro_anuario', $value->id_cadastro_anuario)
								   ->get($this->tabela_passos)
								   ->result();
			if (isset($query_data[0])) {
				$value->data_submissao = formataTimestamp($query_data[0]->data_cadastro);
			}else{
				$value->data_submissao = "";
			}
		}
		return $query;
	}

	function pegarTitulo($id){
		$query = $this->db->get_where($this->tabela_cadastro, array('id' => $id))->result();
		if (isset($query[0])) {
			return $query[0]->nome_fantasia;
		}else{
			return "--";
		}
	}

	function cadastro($id_cadastro){
		return $this->db->get_where($this->tabela_cadastro, array('id' => $id_cadastro))->result();
	}

	function layout($id_cadastro){
		return $this->db->get_where($this->tabela_layout, array('id_cadastro_anuario' => $id_cadastro))->result();
	}

	function finalizacao($id_cadastro){
		return $this->db->get_where($this->tabela_finalizacao, array('id_cadastro_anuario' => $id_cadastro))->result();
	}

	function arquivo($id_cadastro){
		return $this->db->order_by('versao', 'DESC')
						->where('id_cadastro_anuario', $id_cadastro)
						->get($this->tabela_diagramacao)->result();
	}

	function adicionarVersao($cliente, $arquivo){
		$check = $this->db->get_where($this->tabela_diagramacao, array('id_cadastro_anuario' => $cliente))->num_rows();

		$versao = (int) $check + 1;

		$this->db->set('id_cadastro_anuario', $cliente)
				 ->set('arquivo', $arquivo)
				 ->set('versao', $versao)
				 ->set('aprovado', 1)
				 ->set('data_envio', date('Y-m-d H:i:s'))
				 ->insert($this->tabela_diagramacao);
	}

	function setDiagramado($cliente){
        return $this->db->set('diagramado', 2)
                		->where('id_cadastro_anuario', $cliente)
                 		->update($this->tabela_layout);
    }
}