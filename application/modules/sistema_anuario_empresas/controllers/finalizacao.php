<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Finalizacao extends MX_Controller{

	private $hasLayout, $headervar, $footervar;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();
        if(!$this->session->userdata('logged_in_anuario_empresa'))
            redirect('sistema-anuario/empresas/home/');
    }

    function index(){
        if(finalizado(3))
            redirect('sistema-anuario/empresas/home/finalizado');

        $this->load->view('common/header', $this->headervar);
        $this->load->view('finalizacao');
        $this->load->view('common/footer', $this->footervar);
    }

    function enviar(){

        if(finalizado(3))
            redirect('sistema-anuario/empresas/home/finalizado');

        $qry_passos = $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))
                               ->where('passo', 3)
                               ->get('cadastro_anuario_passos_empresas')
                               ->num_rows();


        $descritivo_empresa = $this->input->post('descritivo_empresa');
        $arraySessao['descritivo_empresa'] = $descritivo_empresa;
        $sobre_empresa = $this->input->post('sobre_empresa');
        $arraySessao['sobre_empresa'] = $sobre_empresa;
        $creditos = $this->input->post('creditos');
        $arraySessao['creditos'] = $creditos;

        $this->session->set_userdata($arraySessao);

        $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                ->set('ano', date('Y'))
                ->set('descritivo_empresa', $descritivo_empresa)
                ->set('sobre_empresa', $sobre_empresa)
                ->set('creditos', $creditos);

        if($qry_passos > 0){

            $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))
                     ->update('cadastro_anuario_finalizacao_empresas');

            $this->db->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                     ->where('id_cadastro_anuario', $this->session->userdata('id'))
                     ->where('passo', 3)
                     ->update('cadastro_anuario_passos_empresas');
        }else{

            $this->db->insert('cadastro_anuario_finalizacao_empresas');

            $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                     ->set('passo', 3)
                     ->set('data_cadastro', date('Y-m-d H:i:s'))
                     ->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                     ->insert('cadastro_anuario_passos_empresas');
        }

        // Criar registro para passo 4 == FINALIZADO
        $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                 ->set('passo', 4)
                 ->set('data_cadastro', date('Y-m-d H:i:s'))
                 ->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                 ->insert('cadastro_anuario_passos_empresas');

        // Iniciar processo de diagramação
        $this->db->set('diagramado', 1)->where('id_cadastro_anuario', $this->session->userdata('id'))->update('cadastro_anuario_layout_empresas');

        // Enviar email de confirmação
        $this->enviaEmail();

        //*******************************************************//
        // Atualizo o cadastro espelhado no sistema de FORNECEDORES
        $this->db->set('apresentacao', $sobre_empresa)
                    ->where('id_cadastro_anuario_empresas', $this->session->userdata('id'))
                    ->update('fornecedores');
        //*******************************************************//

        $this->load->view('common/header', $this->headervar);
        $this->load->view('enviado'); // Botão da view aponta pra um método que faz o logout e redireciona pra home do site da flex
        $this->load->view('common/footer', $this->footervar);
    }

    function finalizar(){
        $this->simplelogin->logout();
        redirect('home');
    }

    private function enviaEmail(){
        $nome = $this->session->userdata('nome');
        $email = $this->session->userdata('email');

        if($nome && $email){
            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
            $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
            $emailconf['smtp_pass'] = 'p@rt.9al';

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'anuario@flexeventos.com.br';
            $fromname = 'Flex Eventos';
            $to = $email;
            $bcc = 'bruno@trupe.net';
            $assunto = 'Participação no Anuário Flex';

            $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de confirmação - Anuário Flex</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Os conteúdos da sua participação no Anuário Flex foram enviados com sucesso. Por favor, aguarde nosso comunicado com instruções para a aprovação da arte diagramada, que será enviado no seu e-mail cadastrado no sistema.
    </p>
    <p>
        Gratos<br>
        Flex Editora
    </p>
</body>
</html>
EML;

            $plain = <<<EML
Os conteúdos da sua participação no Anuário Flex foram enviados com sucesso. Por favor, aguarde nosso comunicado com instruções para a aprovação da arte diagramada, que será enviado no seu e-mail cadastrado no sistema.\r\n
Gratos\r\n
Flex Editora
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($from);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            $this->email->send();
        }
    }

}