<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Identificacao extends MX_Controller {

	private $hasLayout, $headervar, $footervar, $espelhar_cadastro;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();

        // Inserir cadastro espelhado nos fornecedores
        $this->espelhar_cadastro = FALSE;

        if ($this->session->userdata('logged_in_anuario_empresa')) {
            if(finalizado(3))
                redirect('sistema-anuario/empresas/home/finalizado');
        }
    }

    function index(){
		$this->load->view('common/header', $this->headervar);
   		$this->load->view('identificacao');
		$this->load->view('common/footer', $this->footervar);
    }

    function enviar(){
        //Coloca os campos na sessão
        $nome_fantasia = $this->input->post('nome_fantasia');
        $arraySessao['nome_fantasia'] = $nome_fantasia;
        $razao_social = $this->input->post('razao_social');
        $arraySessao['razao_social'] = $razao_social;
        $endereco = $this->input->post('endereco');
        $arraySessao['endereco'] = $endereco;
        $numero = $this->input->post('numero');
        $arraySessao['numero'] = $numero;
        $complemento = $this->input->post('complemento');
        $arraySessao['complemento'] = $complemento;
        $bairro = $this->input->post('bairro');
        $arraySessao['bairro'] = $bairro;
        $cidade = $this->input->post('cidade');
        $arraySessao['cidade'] = $cidade;
        $estado = $this->input->post('estado');
        $arraySessao['estado'] = $estado;
        $cep = $this->input->post('cep');
        $arraySessao['cep'] = $cep;
        $telefone = $this->input->post('telefone');
        $arraySessao['telefone'] = $telefone;
        $website = $this->input->post('website');
        $arraySessao['website'] = $website;
        $nome = $this->input->post('nome');
        $arraySessao['nome'] = $nome;

        $email = $this->input->post('email');
        if(!$email && $this->session->userdata('logged_in_anuario_empresa'))
            $email = $this->session->userdata('email');
        $arraySessao['email'] = $email;

        $senha = $this->input->post('senha');
        $arraySessao['senha'] = $senha;

        $categoria = $this->input->post('categoria');
        $arraySessao['categoria'] = $categoria;

        $this->session->set_userdata($arraySessao);

        $imagem = $this->sobeImagem();

        $mensagem_erro = "";

        // Verifica os campos obrigatórios
        if(!$nome_fantasia)
            $mensagem_erro = "O campo Nome Fantasia é obrigatório!";
        if(!$razao_social && $mensagem_erro == "")
            $mensagem_erro = "O campo Razão Social é obrigatório!";
        if(!$endereco && $mensagem_erro == "")
            $mensagem_erro = "O campo Endereço é obrigatório!";
        if(!$numero && $mensagem_erro == "")
            $mensagem_erro = "O campo Número é obrigatório!";
        if(!$bairro && $mensagem_erro == "")
            $mensagem_erro = "O campo Bairro é obrigatório!";
        if(!$cidade && $mensagem_erro == "")
            $mensagem_erro = "O campo Cidade é obrigatório!";
        if(!$estado && $mensagem_erro == "")
            $mensagem_erro = "O campo Estado é obrigatório!";
        if(!$cep && $mensagem_erro == "")
            $mensagem_erro = "O campo CEP é obrigatório!";
        if(!$telefone && $mensagem_erro == "")
            $mensagem_erro = "O campo Telefone é obrigatório!";
        if(!$nome && $mensagem_erro == "")
            $mensagem_erro = "O campo Nome Completo é obrigatório!";
        if(!$email && $mensagem_erro == "")
            $mensagem_erro = "O campo E-mail é obrigatório!";
        if(!$senha && $mensagem_erro == "" && !$this->session->userdata('logged_in_anuario_empresa'))
            $mensagem_erro = "O campo Senha é obrigatório!";
        if(($senha != $this->input->post('confirma_senha')) && !$this->session->userdata('logged_in_anuario_empresa'))
            $mensagem_erro = "A confirmação de senha não confere com a senha informada!";
        if(!$categoria && $mensagem_erro == "")
            $mensagem_erro = "O campo Categoria é obrigatório!";
        if(!$imagem && $mensagem_erro == "" && !$this->session->userdata('logged_in_anuario_empresa'))
            $mensagem_erro = "A imagem é obrigatória!";

        $query_email = $this->db->get_where('cadastro_anuario_empresas', array('email' => $email))->num_rows();

        if($query_email > 0 && !$this->session->userdata('logged_in_anuario_empresa'))
            $mensagem_erro = "O email já está cadastrado";

        if($mensagem_erro != ""){
            // Se houver erro, redireciona para o form
            $this->session->set_flashdata('mensagem_erro', $mensagem_erro);
            redirect('sistema-anuario/empresas/identificacao', 'refresh');
        }else{

            // SE NÃO ESTIVER LOGADO, FAÇO O CADASTRO
            if(!$this->session->userdata('logged_in_anuario_empresa') && $query_email == 0){

                $insert = $this->db->set('nome_fantasia', $nome_fantasia)
                                   ->set('razao_social', $razao_social)
                                   ->set('endereco', $endereco)
                                   ->set('numero', $numero)
                                   ->set('complemento', $complemento)
                                   ->set('bairro', $bairro)
                                   ->set('cidade', $cidade)
                                   ->set('estado', $estado)
                                   ->set('cep', $cep)
                                   ->set('telefone', $telefone)
                                   ->set('website', $website)
                                   ->set('nome', $nome)
                                   ->set('email', $email)
                                   ->set('senha', cripto($senha))
                                   ->set('imagem', $imagem)
                                   ->set('tipo_cadastro', 3)
                                   ->set('categoria', $categoria)
                                   ->insert('cadastro_anuario_empresas');

                if(!$insert)
                    die("Erro no cadastro. Contate o administrador do sistema.");

                $id = $this->db->insert_id();

                if($this->espelhar_cadastro){
                    //*******************************************************//
                    // Faço o cadastro espelhado no sistema de FORNECEDORES
                    $this->db->set('nome_fantasia', $nome_fantasia)
                                ->set('razao_social', $razao_social)
                                ->set('endereco', $endereco)
                                ->set('numero', $numero)
                                ->set('complemento', $complemento)
                                ->set('bairro', $bairro)
                                ->set('cidade', $cidade)
                                ->set('estado', $estado)
                                ->set('cep', $cep)
                                ->set('telefone', $telefone)
                                ->set('website', $website)
                                ->set('nome', $nome)
                                ->set('email', $email)
                                ->set('senha', cripto($senha))
                                ->set('imagem', $imagem)
                                ->set('categoria', $categoria)
                                ->set('id_cadastro_anuario_empresas', $id)
                                ->insert('fornecedores');
                    //*******************************************************//
                }

                // Atualiza o log dos passos
                $insert_passo = $this->db->set('id_cadastro_anuario', $id)
                                         ->set('passo', 1)
                                         ->set('data_cadastro', date('Y-m-d H:i:s'))
                                         ->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                                         ->insert('cadastro_anuario_passos_empresas');

                // Efetua o login depois do cadastro
                if(!$this->simplelogin->login($email, $senha, 3)){
                    redirect('sistema-anuario/empresas/home');
                }
            }else{

                // SE ESTIVER LOGADO ATUALIZO O REGISTRO
                $id = $this->session->userdata('id');

                if($senha)
                    $this->db->set('senha', cripto($senha));
                if ($imagem)
                    $this->db->set('imagem', $imagem);

                $update = $this->db->set('nome_fantasia', $nome_fantasia)
                                   ->set('razao_social', $razao_social)
                                   ->set('endereco', $endereco)
                                   ->set('numero', $numero)
                                   ->set('complemento', $complemento)
                                   ->set('bairro', $bairro)
                                   ->set('cidade', $cidade)
                                   ->set('estado', $estado)
                                   ->set('cep', $cep)
                                   ->set('telefone', $telefone)
                                   ->set('website', $website)
                                   ->set('nome', $nome)
                                   ->set('categoria', $categoria)
                                   ->where('id', $id)
                                   ->update('cadastro_anuario_empresas');

                if(!$update)
                    die("Erro no cadastro. Contate o administrador do sistema.");

                $update_passo = $this->db->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                                         ->where('id_cadastro_anuario', $id)
                                         ->where('passo', 1)
                                         ->update('cadastro_anuario_passos_empresas');
            }
            redirect('sistema-anuario/empresas/layout', 'refresh');
        }
    }

    private function sobeImagem(){

        $this->load->library('upload');
        ini_set('memory_limit', '256M');

        $original = array(
            'campo' => 'imagem',
            'dir' => '_imgs/anuario/marcas/'
        );
        $campo = $original['campo'];

        $uploadconfig = array(
            'upload_path' => $original['dir'],
            'allowed_types' => 'jpg|png|jpeg|bmp|gif',
            'max_size' => '0',
            'max_width' => '0',
            'max_height' => '0'
        );

        $this->upload->initialize($uploadconfig);

        if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                return false;
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name'], 'underscore', true);
                rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

                $this->image_moo
                      ->load($original['dir'].$filename)
                      ->set_background_colour('#FFFFFF')
                      ->resize(210,110,true)
                      ->save($original['dir'].'thumbs/'.$filename, TRUE);

                return $filename;
            }
        }else{
            return false;
        }
    }

}