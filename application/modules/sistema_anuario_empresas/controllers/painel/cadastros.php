<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once( APPPATH . 'core/MY_AdmincontrollerModular.php');

class Cadastros extends MY_AdmincontrollerModular {

    function __construct(){
        parent::__construct('logged_in_painel_anuario_empresas');

        $this->load->model('cadastros_model', 'model');

        $this->area = 'empresas';
        $this->titulo = "Cadastros";
        $this->unidade = "Cadastro";
    }

    function index($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("sistema-anuario/".$this->area."/painel/".$this->router->class."/index/"),
            'per_page' => 20,
            'uri_segment' => 6,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('sistema-anuario/'.$this->area.'/painel/'.$this->router->class);
        }else{
            $data['registro'] = false;
            redirect('sistema-anuario/'.$this->area.'/painel/'.$this->router->class.'/index', 'refresh');
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/common/header', $this->headervar);
        $this->load->view('painel/common/menu', $this->menuvar);
        $this->load->view('painel/'.$this->router->class.'/form', $data);
        $this->load->view('painel/common/footer', $this->footervar);
    }

    function inserir(){
        // if($this->model->inserir()){
        //     $this->session->set_flashdata('mostrarsucesso', true);
        //     $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        // }else{
        //     $this->session->set_flashdata('mostrarerro', true);
        //     $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        // }

        redirect('sistema-anuario/'.$this->area.'/painel/'.$this->router->class.'/index', 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('sistema-anuario/'.$this->area.'/painel/'.$this->router->class.'/index', 'refresh');
    }

   function excluir($id){
        if($this->model->excluir($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
        }

        redirect('sistema-anuario/'.$this->area.'/painel/cadastros', 'refresh');
   }
}