<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Home extends MX_Controller{

	private $hasLayout, $headervar, $footervar;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();
        $this->load->model('fornecedores_model', 'fornecedores');
    }

    function index(){
        if(!$this->session->userdata('logged_in_anuario_fornecedores')){
            $this->load->view('common/header', $this->headervar);
            $this->load->view('login');
            $this->load->view('common/footer', $this->footervar);
        }else{

            $data['fornecedores'] = $this->fornecedores->pegarDados();
            
            if(!$data['fornecedores'])
                redirect('cadastro-fornecedores/home/logout');


    		$this->load->view('common/header', $this->headervar);
            $this->load->view('inicio', $data);
            $this->load->view('common/footer', $this->footervar);
        }
    }

    function login(){
        if(!$this->simplelogin->login($this->input->post('login'), $this->input->post('pass'), 7))
            $this->session->set_flashdata('errlogin', true);

        redirect('cadastro-fornecedores/home/index');
    }

    function logout(){
        $this->simplelogin->logout();
        redirect('cadastro-fornecedores/home/index');
    }

    function cadastro(){

        if($this->session->flashdata('mostrarerro') === true){
            $data['mostrarerro'] = true;
            $data['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $data['mostrarerro'] = false;
            $data['mostrarerro_mensagem'] = '';
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $data['mostrarsucesso'] = true;
            $data['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $data['mostrarsucesso'] = false;
            $data['mostrarsucesso_mensagem'] = '';
        }

        $data['fornecedores'] = $this->fornecedores->pegarDados();

        if(!$data['fornecedores'])
            redirect('cadastro-fornecedores/home/logout');

        // Keywords cadastradas para o Fornecedor
        $data['keywords'] = $this->fornecedores->pegarRelKeywords();
        foreach ($data['keywords'] as $key => $value) {
            $value->sinonimos = $this->buscarSinonimosPorId($value->id);
        }

        // Imagem cadastradas para o Fornecedor
        $data['imagens'] = $this->fornecedores->pegarImagens();
        foreach ($data['imagens'] as $key => $value) {
            if($value->keyword1){
                $k1 = $this->fornecedores->pegarKeywords($value->keyword1);
                $value->keyword1 = ($k1) ? $k1->palavras : '';
            }
            if($value->keyword2){
                $k2 = $this->fornecedores->pegarKeywords($value->keyword2);
                $value->keyword2 = ($k2) ? $k2->palavras : '';
            }
            if($value->keyword3){
                $k3 = $this->fornecedores->pegarKeywords($value->keyword3);
                $value->keyword3 = ($k3) ? $k3->palavras : '';
            }
        }

        // Listagem de todas as keywords para popular o autocomplete
        $qry_keys = $this->fornecedores->pegarKeywords();
        $data['todas_keywords'] = "";
        foreach ($qry_keys as $key => $value) {
            $data['todas_keywords'] .= ($key == 0) ? $value->palavras : ', '.$value->palavras;
        }

        $this->load->view('common/header', $this->headervar);
        $this->load->view('cadastro', $data);
        $this->load->view('common/footer', $this->footervar);
    }

    function buscarSinonimosPorId($id){        
        $qry = <<<QRY
SELECT palavras.* FROM trupe_fornecedores_keywords_sinonimos sinonimos
LEFT JOIN trupe_fornecedores_keywords palavras ON sinonimos.keyword_2 = palavras.id
WHERE sinonimos.keyword_1 = '{$id}'
ORDER BY palavras.palavras DESC
QRY;
        $query = $this->db->query($qry)->result();

        $retorno = "";
        if($query){
            foreach ($query as $key => $value) {
                $retorno .= ($key==0) ? $value->palavras : ', '.$value->palavras;
            }
        }  
        return $retorno;
    }
    
    function salvar(){
        if($this->fornecedores->atualizar()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Cadastro atualizado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao atualizar cadastro');
        }
        redirect('cadastro-fornecedores/home/index');
    }

    function recuperacao(){
        if(!$this->session->userdata('logged_in_anuario_fornecedores')){
            $this->load->view('common/header', $this->headervar);
            $this->load->view('recuperacao');
            $this->load->view('common/footer', $this->footervar);
        }else{
            redirect('cadastro-fornecedores/home/cadastro');
        }
    }

    function recuperarSenha(){

        $email = $this->input->post('login');

        if(!$email){
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Informe seu e-mail.');
            redirect('cadastro-fornecedores/home/recuperacao', 'refresh');
        }

        if(!$this->fornecedores->existeEmail($email)){
            $this->session->set_flashdata('errrecover', true);
            redirect('cadastro-fornecedores/home/recuperacao', 'refresh');
        }else{
            if($this->fornecedores->recuperarSenha($email)){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Email de recuperação enviado com sucesso!');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao enviar o email de recuperação.');
            }
            redirect('cadastro-fornecedores/home/recuperacao', 'refresh');
        }
    }

    function novaSenha($token){
        if ($this->fornecedores->tokenValido($token) && !$this->session->userdata('logged_in_anuario_fornecedores')) {
            $this->load->view('common/header', $this->headervar);
            $this->load->view('nova-senha', array('token' => $token));
            $this->load->view('common/footer', $this->footervar);
        }else{
            redirect('cadastro-fornecedores/home/');
        }
    }

    function redefinirSenha(){
        $this->simplelogin->logout();
        if($this->fornecedores->redefinirSenha()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Sua senha foi redefinida com sucesso!');
            redirect('cadastro-fornecedores/home/', 'refresh');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao redefinir a senha.');
            redirect('cadastro-fornecedores/home/novaSenha', 'refresh');
        }
    }
}