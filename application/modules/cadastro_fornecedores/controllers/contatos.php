<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Extend MX_Controller para todo controller que chamar um módulo
class Contatos extends MX_Controller{

	private $hasLayout, $headervar, $footervar;

    function __construct(){
   		parent::__construct();
   		$this->hasLayout = TRUE;
   		$this->headervar = array();
		$this->footervar = array();
        $this->load->model('fornecedores_model', 'fornecedores');
        $this->load->model('contatos_model', 'contatos');

        if(!$this->session->userdata('logged_in_anuario_fornecedores'))
            redirect('cadastro-fornecedores/home/index');
    }

    public function index(){
    	redirect('cadastro-fornecedores/contatos/recebidos');        
    }

    public function recebidos(){

        if($this->session->flashdata('mostrarerro') === true){
            $data['mostrarerro'] = true;
            $data['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $data['mostrarerro'] = false;
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $data['mostrarsucesso'] = true.
            $data['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $data['mostrarsucesso'] = false;
        }

        $data['fornecedores'] = $this->fornecedores->pegarDados();
        $data['pedidos_cotacao'] = $this->contatos->pedidosCotacao();
        $data['pedidos_catalogo'] = $this->contatos->pedidosCatalogo();

        $this->load->view('common/header', $this->headervar);
        $this->load->view('contatos/recebidos', $data);
        $this->load->view('common/footer', $this->footervar);
        
    }

    public function historico($tipo = 'cotacao', $pag = 0){

        parse_str($_SERVER['QUERY_STRING'],$filtro);

        if($this->session->flashdata('mostrarerro') === true){
            $data['mostrarerro'] = true;
            $data['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $data['mostrarerro'] = false;
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $data['mostrarsucesso'] = true.
            $data['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $data['mostrarsucesso'] = false;
        }

        $sufixo = "?";
        $filtro['data'] = isset($filtro['filtroData']) ? $filtro['filtroData'] : false;
        $filtro['nome'] = isset($filtro['filtroNome']) ? $filtro['filtroNome'] : false;
        if($filtro['data']){            
            foreach($filtro['data'] as $i => $v)
                $sufixo .= "data%5B".$i."%5D=".$v."&";
        }
        if($filtro['nome']){
            $sufixo .= "nome=".$filtro['nome'];
        }
        
        $data['filtro'] = $filtro;
        $data['tipo'] = $tipo;
        $por_pagina = 40;

        $data['fornecedores'] = $this->fornecedores->pegarDados();

        if($tipo == 'cotacao'){
            $data['pedidos'] = $this->contatos->pedidosHistoricoCotacao($por_pagina, $pag, $filtro);
            $data['pedidos_total'] = $this->contatos->pedidosHistoricoCotacao($por_pagina, $pag, $filtro, true);
        }elseif($tipo == 'catalogo'){
            $data['pedidos'] = $this->contatos->pedidosHistoricoCatalogo($por_pagina, $pag, $filtro);
            $data['pedidos_total'] = $this->contatos->pedidosHistoricoCatalogo($por_pagina, $pag, $filtro, true);
        }else{
            redirect('cadastro-fornecedores/contatos/historico/');
        }

        $this->load->library('pagination');
        $pag_options = array(
            'base_url' => base_url("cadastro-fornecedores/contatos/historico/".$tipo."/"),
            'per_page' => $por_pagina,
            'uri_segment' => 5,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $data['pedidos_total'],
            'suffix' => $sufixo === "?" ? '' : $sufixo,
            'first_url' => base_url("cadastro-fornecedores/contatos/historico/".$tipo."/".$sufixo)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();        

        $this->load->view('common/header', $this->headervar);
        $this->load->view('contatos/historico', $data);
        $this->load->view('common/footer', $this->footervar);

    }

    public function excluir($id_pedido = false){
        
        if($id_pedido){
            $exclude = $this->contatos->excluir($id_pedido);
            if($exclude){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Pedido removido com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao remover Pedido');
            }
        }
        
        redirect('cadastro-fornecedores/contatos/recebidos');
    }
    
    public function encaminharPedidosCotacao(){
        if($this->contatos->encaminhar('cotacao')){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Pedidos encaminhados com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao encaminhar um ou mais Pedidos');
        }
        
        redirect('cadastro-fornecedores/contatos/recebidos');
    }

    public function reencaminharCotacao($id = false){
        if($this->contatos->reencaminhar('cotacao', $id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Pedidos reencaminhados com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao reencaminhar um ou mais Pedidos');
        }
        
        redirect('cadastro-fornecedores/contatos/historico/cotacao');   
    }

    public function encaminharPedidosCatalogo(){
        if($this->contatos->encaminhar('catalogo')){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Pedidos encaminhados com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao encaminhar um ou mais Pedidos');
        }
        
        redirect('cadastro-fornecedores/contatos/recebidos');
    }

    public function reencaminharCatalogo($id = false){
        if($this->contatos->reencaminhar('catalogo', $id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Pedidos reencaminhados com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao reencaminhar um ou mais Pedidos');
        }
        
        redirect('cadastro-fornecedores/contatos/historico/catalogo');   
    }

    public function detalhes($id_pedido = false, $tipo_pedido = '')
    {
        if($id_pedido && $this->contatos->checaPropriedade($id_pedido, $this->session->userdata('id'), $tipo_pedido) && $this->input->is_ajax_request()){
            $data['pedido'] = $this->contatos->pegarPorId($id_pedido, $tipo_pedido);
            $data['tipo'] = $tipo_pedido;
            echo $this->load->view('contatos/solicitacao-modal', $data, TRUE);
        }
    }

    public function download($tipo = false, $arquivo = false)
    // $tipo = anexo || planilha
    // $arquivo = nome do arquivo || cotacao/catalogo
    {
        $id = $this->session->userdata('id');

        if(!$arquivo || !$tipo)
            redirect('cadastro-fornecedores/contatos');
        else{

            if($tipo == 'planilha'){
                
                $this->load->dbutil();

                if($arquivo == 'cotacao'){
                    $str_query = <<<STR
SELECT
nome as Nome,
email as 'E-mail',
telefone as Telefone,
mensagem as Mensagem,
CONCAT('http://www.flex.com.br/cadastro-fornecedores/contatos/download/anexo/', arquivo) as Arquivo,
DATE_FORMAT( data, '%d/%m/%Y' )  as 'Data',
email_encaminhado as 'Encaminhado Para',
DATE_FORMAT( data_encaminhado, '%d/%m/%Y' ) as 'Encaminhado Em'
FROM trupe_fornecedores_pedidos_cotacao
WHERE encaminhado = 1
AND id_fornecedor = $id
ORDER BY data DESC
STR;
                }else{
                    $str_query = <<<STR
SELECT
nome as Nome,
email as 'E-mail',
empresa as Empresa,
observacoes as Observações,
DATE_FORMAT( data, '%d/%m/%Y' )  as 'Data',
email_encaminhado as 'Encaminhado Para',
DATE_FORMAT( data_encaminhado, '%d/%m/%Y' ) as 'Encaminhado Em'
FROM trupe_fornecedores_pedidos_catalogo
WHERE encaminhado = 1
AND id_fornecedor = $id
ORDER BY data DESC
STR;
                }

                $pedidos = $this->db->query($str_query);
                $filename = "pedidos_".$id.'_'.Date('d-m-Y_H-i-s').'.csv';

                $delimiter = ";";
                $newline = "\r\n";

                $this->output->set_header('Content-Type: application/force-download');
                $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
                $this->output->set_content_type('text/csv')->set_output(utf8_decode($this->dbutil->csv_from_result($pedidos, $delimiter, $newline)), "ISO-8859-1", "UTF-8");

            }else{

                $filepath = '_pdfs/solicitacoes/';
                $full = $filepath.$arquivo;                        
            
                if(!file_exists($full))
                    return false;

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header("Content-Type: application/force-download");
                header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($full));
                ob_clean();
                flush();
                readfile($full);
                exit;
            }
        }
    }
  
}