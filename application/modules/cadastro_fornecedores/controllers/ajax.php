<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	var $layouts;

    function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');

   		$this->layouts['impar'][1] = <<<STR
<div class="layout">
    <div class="arquivo w190 h245 mb">
        <div class="dimensoes">190 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $this->layouts['impar'][2] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img6">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][3] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][4] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][7] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][8] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][9] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img5">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][10] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][11] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][12] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][13] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mb mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][14] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][15] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][16] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr fl">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][17] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][18] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][19] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][20] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][21] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][22] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h75">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
        $this->layouts['impar'][23] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][24] = <<<STR
<div class="layout">
    <div class="arquivo w190 h160 mb">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][25] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w190 h160">
        <div class="dimensoes">190 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][26] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 mb">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][27] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr fl">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][28] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][29] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h245 fr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][30] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 fl mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
   		$this->layouts['impar'][31] = <<<STR
<div class="layout">
    <div class="arquivo w220 h290">
        <div class="dimensoes">220 x 290 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $this->layouts['par'][1] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $this->layouts['par'][2] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 fr texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $this->layouts['par'][3] = <<<STR
<div class="layout">
    <div class="arquivo w190 h75 mb">
        <div class="dimensoes">190 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $this->layouts['par'][4] = <<<STR
<div class="layout">
    <div class="arquivo w90 h160 mr mb">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mr">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto" style='margin-top:-160px'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $this->layouts['par'][5] = <<<STR
<div class="layout">
    <div class="arquivo w90 h75 mr mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 mr">
        <div class="dimensoes">90 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $this->layouts['par'][6] = <<<STR
<div class="layout">
    <div class="arquivo w90 h245 mr">
        <div class="dimensoes">90 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-300px'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $this->layouts['e1'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img4">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $this->layouts['e2'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h160 mb mr">
        <div class="dimensoes">296 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto fr" style='margin-top:-160px;'>
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
    <div class="arquivo w296 h75">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
</div>
STR;
    $this->layouts['e3'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h75 mb mr">
        <div class="dimensoes">296 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w296 h160 mr">
        <div class="dimensoes">296 x 160 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img3">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>

</div>
STR;
    $this->layouts['e4'] = <<<STR
<div class="layout">
    <div class="arquivo w296 h245 mr fl">
        <div class="dimensoes">296 x 245 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    $this->layouts['e5'] = <<<STR
<div class="layout">
    <div class="arquivo w320 h290 mr fl">
        <div class="dimensoes">320 x 290 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img1">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h75 mb">
        <div class="dimensoes">90 x 75 mm</div>
        <label>
            <input type="file" name="userfile" id="pag#img2">
        </label>
        <div class="imagem" data-imagem="">
            <div class="aviso-tamanho">Esta imagem preencherá o total desta área do layout. Esta exibição é apenas uma marcação para auxiliar na escolha de imagens de cada área</div>
            <a href="#" title="subtituir imagem" class="troca-imagem">subtituir imagem</a>
            <img src="">
            <input type="text" name="legenda[]" maxlength='60' class="legenda" placeholder="Clique aqui para adicionar legenda. (opcional)">
        </div>
    </div>
    <div class="arquivo w90 h160 texto">
        ÁREA DE TEXTO.
        <p>
        INFORMAÇÕES SOBRE OS PRODUTOS E/OU SOBRE A EMPRESA.<br><span>(estas informações serão solicitadas no próximo passo)</span>
        </p>
    </div>
</div>
STR;
    }

    function pegarLayout(){
    	$id_layout = $this->input->post('id_layout');
        $id_pagina = $this->input->post('id_pagina');

        if(substr($id_layout, 0, 1) == 'e'){
            echo str_replace('pag#', 'pag'.$id_pagina, $this->layouts[$id_layout]);
        }else{
    	    $pagina = ($id_pagina%2 == 0) ? "par" : "impar";
    	    echo str_replace('pag#', 'pag'.$id_pagina, $this->layouts[$pagina][$id_layout]);
        }
    }

    function gravarLayout(){
        $layout = json_decode($this->input->post('layout'));

        $qry_passo = $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))
                                ->where('passo', 2)->get('cadastro_anuario_passos_arquitetos')->num_rows();

        if ($qry_passo > 0) {
            $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))->delete('cadastro_anuario_layout_arquitetos');
        }

        $insert = false;

        foreach ($layout->paginas as $pagina) {

            if ($pagina && ($pagina->layoutEspecial || (!$pagina->layoutEspecial && $pagina->layoutSelecionado != 0))){

                $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                        ->set('ano', date('Y'))
                        ->set('numero_pagina', $pagina->numero)
                        ->set('id_layout', $pagina->layoutSelecionado)
                        ->set('layout_especial', $pagina->layoutEspecial);

                if(isset($pagina->imagens[1]))
                    $this->db->set('imagem1', $pagina->imagens[1]);
                if(isset($pagina->legendas[1]) && $pagina->legendas[1] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda1', $pagina->legendas[1]);

                if(isset($pagina->imagens[2]))
                    $this->db->set('imagem2', $pagina->imagens[2]);
                if(isset($pagina->legendas[2]) && $pagina->legendas[2] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda2', $pagina->legendas[2]);

                if(isset($pagina->imagens[3]))
                    $this->db->set('imagem3', $pagina->imagens[3]);
                if(isset($pagina->legendas[3]) && $pagina->legendas[3] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda3', $pagina->legendas[3]);

                if(isset($pagina->imagens[4]))
                    $this->db->set('imagem4', $pagina->imagens[4]);
                if(isset($pagina->legendas[4]) && $pagina->legendas[4] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda4', $pagina->legendas[4]);

                if(isset($pagina->imagens[5]))
                    $this->db->set('imagem5', $pagina->imagens[5]);
                if(isset($pagina->legendas[5]) && $pagina->legendas[5] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda5', $pagina->legendas[5]);

                if(isset($pagina->imagens[6]))
                    $this->db->set('imagem6', $pagina->imagens[6]);
                if(isset($pagina->legendas[6]) && $pagina->legendas[6] != 'Clique aqui para adicionar legenda. (opcional)')
                    $this->db->set('legenda6', $pagina->legendas[6]);


                $insert = $this->db->insert('cadastro_anuario_layout_arquitetos');
            }
        }

        if($insert){
            if ($qry_passo > 0) {
                $this->db->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                         ->where('id_cadastro_anuario', $this->session->userdata('id'))
                         ->where('passo', 2)
                         ->update('cadastro_anuario_passos_arquitetos');
            }else{
                $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                         ->set('passo', 2)
                         ->set('data_cadastro', date('Y-m-d H:i:s'))
                         ->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                         ->insert('cadastro_anuario_passos_arquitetos');
            }
        }

        echo ($insert) ? 1 : 0;
    }

    function salvarProjeto(){
        $qry_passos = $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))
                                ->where('passo', 3)->get('cadastro_anuario_passos_arquitetos')->num_rows();

        $nome_obra = $this->input->post('nome_obra');
        $this->session->set_userdata('nome_obra', $nome_obra);
        $cidade_obra = $this->input->post('cidade_obra');
        $this->session->set_userdata('cidade_obra', $cidade_obra);
        $estado_obra = $this->input->post('estado_obra');
        $this->session->set_userdata('estado_obra', $estado_obra);
        $equipe_tecnica = $this->input->post('equipe_tecnica');
        $this->session->set_userdata('equipe_tecnica', $equipe_tecnica);
        $conceito_obra = $this->input->post('conceito_obra');
        $this->session->set_userdata('conceito_obra', $conceito_obra);
        $creditos = $this->input->post('creditos');
        $this->session->set_userdata('creditos', $creditos);

        $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                ->set('ano', date('Y'))
                ->set('nome_obra', $nome_obra)
                ->set('cidade_obra', $cidade_obra)
                ->set('estado_obra', $estado_obra)
                ->set('equipe_tecnica', $equipe_tecnica)
                ->set('conceito_obra', $conceito_obra)
                ->set('creditos', $creditos);

        if($qry_passos){
            $this->db->where('id_cadastro_anuario', $this->session->userdata('id'))->update('cadastro_anuario_finalizacao_arquitetos');
            $insert_passo = $this->db->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                             ->where('id_cadastro_anuario', $this->session->userdata('id'))
                             ->where('passo', 3)
                             ->update('cadastro_anuario_passos_arquitetos');
        }else{
            $this->db->insert('cadastro_anuario_finalizacao_arquitetos');
            $insert_passo = $this->db->set('id_cadastro_anuario', $this->session->userdata('id'))
                             ->set('passo', 3)
                             ->set('data_cadastro', date('Y-m-d H:i:s'))
                             ->set('data_ultima_alteracao', date('Y-m-d H:i:s'))
                             ->insert('cadastro_anuario_passos_arquitetos');
        }
    }

    function baixarImagens(){
        $imagens = $this->input->post('imagens');
        $dir = $this->input->post('diretorio');
        $user = $this->input->post('user');

        $imagens = explode(',', $imagens);
        $zip = $this->create_zip($imagens, $dir, $user);
        echo json_encode(array('path' => "index.php/sistema-anuario/arquitetos/painel/diagramar/download?file=".$zip));
    }

    /* creates a compressed zip file */
    private function create_zip($files = array(), $dir, $user) {

        $valid_files = array();
        $path = "_imgs/anuario/imagens/arquitetos/".$dir."/";
        $user = url_title($user,'_', TRUE);

        if(is_array($files)) {
            foreach($files as $file) {
                if(file_exists($path.$file)) {
                    $valid_files[] = $path.$file;
                }
            }
        }

        if(count($valid_files)) {
            $zip = new ZipArchive();
            $zip->open($path.$user."_Imagens_Alta_".date('Y').".zip", ZIPARCHIVE::CREATE);

            foreach($valid_files as $file) {
                $zip->addFile($file,end(explode('/', $file)));
            }
            $zip->close();
            return $path.$user."_Imagens_Alta_".date('Y').".zip";
        }else{
            return false;
        }
    }

    public function excluirPedido()
    {
        $this->load->model('contatos_model', 'contatos');

        $id_pedido = $this->input->post('id_pedido');
        $id_fornecedor = $this->session->userdata('id');
        $tipo_pedido = $this->input->post('tipo');
        if($this->contatos->checaPropriedade($id_pedido, $id_fornecedor, $tipo_pedido)){
            $this->contatos->excluir($id_pedido, $tipo_pedido);
        }
    }
}
