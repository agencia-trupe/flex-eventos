<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fornecedores_model extends CI_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'fornecedores';
		$this->tabela_imagens = "fornecedores_imagens";

		$this->tabela_keywords = "fornecedores_keywords";
		$this->tabela_rel_keywords = "fornecedores_rel_keywords";
		$this->tabela_estatisticas = "fornecedores_estatisticas_keywords";
		$this->tabela_sinonimos = "fornecedores_keywords_sinonimos";	
	}

	function pegarDados(){
		$query = $this->db->get_where($this->tabela, array('id' => $this->session->userdata('id')))->result();
		return isset($query[0]) ? $query[0] : false;
	}

	function pegarRelKeywords(){
		$id = $this->session->userdata('id');
		$qry = <<<QRY
SELECT palavras.* FROM trupe_{$this->tabela_rel_keywords} rel
LEFT JOIN trupe_{$this->tabela_keywords} palavras ON rel.id_keyword = palavras.id
WHERE rel.id_fornecedores = '{$id}'
QRY;
		return $this->db->query($qry)->result();
	}

	function pegarKeywords($id_keyword = false){
		if ($id_keyword) {
			$q = $this->db->get_where($this->tabela_keywords, array('id' => $id_keyword))->result();
			if(isset($q[0]))
				return $q[0];
			else
				return false;
		}else{
			return $this->db->order_by('palavras')->get($this->tabela_keywords)->result();
		}
	}

	function pegarImagens(){
		return $this->db->get_where($this->tabela_imagens, array('id_fornecedores' => $this->session->userdata('id')))->result();
	}

	function existeEmail($email){
		return ($this->db->get_where($this->tabela, array('email' => $email))->num_rows() > 0) ? true : false;
	}

	function recuperarSenha($email){

		$usuario = $this->db->get_where($this->tabela, array('email' => $email))->result();
		
		if(!isset($usuario[0])) return false;

		$token = cripto($usuario[0]->id.$usuario[0]->email);
		$this->db->set('token',$token)->where('id', $usuario[0]->id)->update($this->tabela);
		return $this->enviarToken($usuario, $token);
	}

	function tokenValido($token){

		$qry = $this->db->get_where($this->tabela, array('token' => $token));
		$resultado = $qry->result();

		if(!isset($resultado[0])) return false;

		$qry_token_existe = $qry->num_rows() > 0;

		$qry_token_legitimo = cripto($resultado[0]->id.$resultado[0]->email) == $token;

		return $qry_token_existe && $qry_token_legitimo;
	}

	function redefinirSenha(){
		$senha = $this->input->post('nova_senha');
		$confirmacao_senha = $this->input->post('nova_senha_conf');
		$token = $this->input->post('token');

		if(!$senha || !$confirmacao_senha || $senha != $confirmacao_senha) return false;

		$qry = $this->db->get_where($this->tabela, array('token' => $token));
		$resultado = $qry->result();

		if($this->tokenValido($token)){

			return $this->db->set('senha', cripto($senha))
							->set('token', 'token')
							->where('token', $token)
							->update($this->tabela);

		}else{
			return false;
		}
	}

	private function enviarToken($usuario, $token){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'eventos@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        $to = $usuario[0]->email;
        $bcc = 'bruno@trupe.net';
        $assunto = 'Recuperação de Senha';

        $corpoemail = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Recuperação de senha - Sistema de Fornecedores FLEX</title>
    <meta charset="utf-8">
</head>
<body>
	<p>
    	Você fez a requisição de uma nova senha no Sistema de Fornecedores da Flex Eventos.
    </p>
    <p>
		Clique no link abaixo para REDEFINIR a sua senha.
    </p>
    <p>
		<a href='http://www.flexeventos.com.br/cadastro-fornecedores/home/novaSenha/$token' title='Clique aqui para redefinir sua senha!' target='_blank'>REDEFINIR minha senha</a>
    </p>
</body>
</html>
EML;

            $plain = <<<EML
Você fez a requisição de uma nova senha no Sistema de Fornecedores da Flex Eventos.\r\n
Clique no link abaixo para REDEFINIR a sua senha.\r\n
REDEFINIR minha senha:\r\n
http://www.flexeventos.com.br/cadastro-fornecedores/home/novaSenha/$token
EML;

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to('eventos@flexeventos.com.br');

        $this->email->subject($assunto);
        $this->email->message($corpoemail);
        $this->email->set_alt_message($plain);

        return $this->email->send();
	}

	function atualizar(){
		$nome_fantasia = $this->input->post('nome_fantasia');
		$razao_social = $this->input->post('razao_social');
		$endereco = $this->input->post('endereco');
		$numero = $this->input->post('numero');
		$complemento = $this->input->post('complemento');
		$bairro = $this->input->post('bairro');
		$cidade = $this->input->post('cidade');
		$estado = $this->input->post('estado');
		$cep = $this->input->post('cep');
		$telefone = $this->input->post('telefone');
		$website = $this->input->post('website');
		$categoria = $this->input->post('categoria');
		$nome = $this->input->post('nome');
		$email = $this->input->post('email'); // Não pode ser alterado
		$apresentacao = $this->input->post('apresentacao');

		$senha = $this->input->post('senha');
		$confirma_senha = $this->input->post('confirma_senha');

		$imagem = $this->sobeImagem();
		if($imagem)
			$this->db->set('imagem', $imagem);

		if($senha && $senha != '' && $senha == $confirma_senha)
			$this->db->set('senha', cripto($senha));

		$update = $this->db->set('nome_fantasia', $nome_fantasia)
							->set('razao_social', $razao_social)
							->set('endereco', $endereco)
							->set('numero', $numero)
							->set('complemento', $complemento)
							->set('bairro', $bairro)
							->set('cidade', $cidade)
							->set('estado', $estado)
							->set('cep', $cep)
							->set('telefone', $telefone)
							->set('website', $website)
							->set('categoria', $categoria)
							->set('nome', $nome)
							->set('apresentacao', $apresentacao)
							->where('id', $this->session->userdata('id'))
							->update($this->tabela);

		$this->db->where('id_fornecedores', $this->session->userdata('id'))->delete($this->tabela_rel_keywords);
		
		$keywords = json_decode($this->input->post('keywords'));
		if($keywords && is_array($keywords)){
			foreach ($keywords as $key => $value) {

				$value = mb_strtolower(trim($value));

				// Verifica se já existe registro da palavra
				$check = $this->db->get_where($this->tabela_keywords, array('palavras' => $value))->result();

				// Caso não haja, inserir na tabela
				if(isset($check[0])){
					$id_palavra = $check[0]->id;
				}else{
					$this->db->set('palavras', $value)->insert($this->tabela_keywords);
					$id_palavra = $this->db->insert_id();
				}

				// Cadastra relação
				$this->db->set('id_keyword', $id_palavra)
					 ->set('id_fornecedores', $this->session->userdata('id'))
					 ->insert($this->tabela_rel_keywords);
			}
		}

		$imagens = json_decode($this->input->post('imagens'));
		$this->db->where('id_fornecedores', $this->session->userdata('id'))->delete($this->tabela_imagens);
		if ($imagens && is_array($imagens)) {
			foreach ($imagens as $key => $value) {

				if($value->keyword1){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword1))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword1 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword1)))->insert($this->tabela_keywords);
						$keyword1 = $this->db->insert_id();
					}
				}else{
					$keyword1 = false;
				}

				if($value->keyword2){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword2))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword2 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword2)))->insert($this->tabela_keywords);
						$keyword2 = $this->db->insert_id();
					}
				}else{
					$keyword2 = false;
				}

				if($value->keyword3){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword3))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword3 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword3)))->insert($this->tabela_keywords);
						$keyword3 = $this->db->insert_id();
					}
				}else{
					$keyword3 = false;
				}

				if($keyword1)
					$this->db->set('keyword1', $keyword1);
				if($keyword2)
					$this->db->set('keyword2', $keyword2);
				if($keyword3)
					$this->db->set('keyword3', $keyword3);

				$this->db->set('imagem', $value->imagem)
						 ->set('legenda', $value->legenda)
						 ->set('id_fornecedores', $this->session->userdata('id'))
						 ->insert($this->tabela_imagens);
			}
		}

		return $update;
	}

	function sobeImagem($campo = 'imagem'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/anuario/marcas/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0',
		  'encrypt_name' => TRUE
		);

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo->set_background_colour('#FFFFFF')
                                ->load($original['dir'].$filename)
                                ->resize(204,45,TRUE)
                                ->save($original['dir'].'thumbs/'.$filename);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}