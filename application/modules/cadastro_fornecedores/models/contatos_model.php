<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contatos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela_pedido_cotacoes = 'fornecedores_pedidos_cotacao';
		$this->tabela_pedido_catalogo = 'fornecedores_pedidos_catalogo';

		$this->dados = array();
		$this->dados_tratados = array();
	}

	public function pedidosCotacao(){
		return $this->db->order_by('data', 'DESC')
						->get_where($this->tabela_pedido_cotacoes, array('encaminhado' => '0', 'id_fornecedor' => $this->session->userdata('id')))
						->result();
	}

	public function pedidosHistoricoCotacao($por_pagina, $pag, $filtro, $contagem = false){
		$query = $this->db;
		if($filtro['data']){
			if(isset($filtro['data']['dia']) && $filtro['data']['dia'])
				$query->where('DATE(data)', $filtro['data']['ano'].'-'.$filtro['data']['mes'].'-'.$filtro['data']['dia']);
			else
				$query->where('MONTH(data)', $filtro['data']['mes'])->where('YEAR(data)', $filtro['data']['ano']);
		}
		if($filtro['nome']){
			$query->like('nome', $filtro['nome']);
		}
		$query->order_by('data', 'DESC')
			  ->where('encaminhado', '1')
			  ->where('id_fornecedor', $this->session->userdata('id'));
		
		return $contagem ? $query->get($this->tabela_pedido_cotacoes)->num_rows() : $query->limit($por_pagina, $pag)->get($this->tabela_pedido_cotacoes)->result();
	}

	public function pedidosCatalogo(){
		return $this->db->order_by('data', 'DESC')
						->get_where($this->tabela_pedido_catalogo, array('encaminhado' => '0', 'id_fornecedor' => $this->session->userdata('id')))
						->result();
	}

	public function pedidosHistoricoCatalogo($por_pagina, $pag, $filtro, $contagem = false){
		if($filtro['data']){
			if(isset($filtro['data']['dia']))
				$this->db->where('DATE(data)', $filtro['data']['ano'].'-'.$filtro['data']['mes'].'-'.$filtro['data']['dia']);
			else
				$this->db->where('MONTH(data)', $filtro['data']['mes'])->where('YEAR(data)', $filtro['data']['ano']);
		}
		if($filtro['nome']){
			$this->db->like('nome', $filtro['nome']);
		}
		$query = $this->db->order_by('data', 'DESC')
						  ->where('encaminhado', '1')
						  ->where('id_fornecedor', $this->session->userdata('id'));

		return $contagem ? $query->get($this->tabela_pedido_catalogo)->num_rows() : $query->limit($por_pagina, $pag)->get($this->tabela_pedido_catalogo)->result();
	}

	function pegarPorId($id, $tipo_pedido)
	// pedido = cotacao || catalogo
	{
		if($tipo_pedido=='cotacao'){
			$qry = $this->db->get_where($this->tabela_pedido_cotacoes, array('id' => $id))->result();
		}elseif($tipo_pedido=='catalogo'){
			$qry = $this->db->get_where($this->tabela_pedido_catalogo, array('id' => $id))->result();
		}else{
			return false;
		}
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	public function checaPropriedade($id_pedido, $id_fornecedor, $tipo_pedido)
	{
		$pedido = $this->pegarPorId($id_pedido, $tipo_pedido);
		if($pedido && isset($pedido->id_fornecedor) && $pedido->id_fornecedor == $id_fornecedor)
			return true;
		else
			return false;
	}

	public function excluir($id, $tipo_pedido)
	// pedido = cotacao || catalogo
	{
		if($this->pegarPorId($id, $tipo_pedido) !== FALSE){
			if($tipo_pedido=='cotacao'){
				return $this->db->where('id', $id)->delete($this->tabela_pedido_cotacoes);
			}elseif($tipo_pedido=='catalogo'){
				return $this->db->where('id', $id)->delete($this->tabela_pedido_catalogo);
			}else{
				return false;
			}
		}
	}

	public function encaminhar($tipo)
	{

		$concatenar = TRUE;

		$email = $this->input->post('email_encaminhado');
        
        if($tipo == 'cotacao')
        	$pedidos = $this->pedidosCotacao();
        elseif($tipo == 'catalogo')
        	$pedidos = $this->pedidosCatalogo();
		else
			return false;

        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'eventos@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        $to = $email;
        $bcc = 'bruno@trupe.net';
        $assunto = ($tipo == 'cotacao') ? "Pedidos do Sistema Flex - Cotações" : "Pedidos do Sistema Flex - Catálogo";
        $toUnlink = array();

        if($concatenar){

        	$corpoemail = $this->_cabecalhoEmail($tipo);
        	$plain = "";

        	foreach ($pedidos as $key => $value) {
        		$corpoemail .= $this->_constroiMsgHtml($value, $tipo);
        		$plain .= $this->constroiMsgPlain($value, $tipo);

        		if($tipo == 'cotacao'){
		        	if($value->arquivo && file_exists('_pdfs/solicitacoes/'.$value->arquivo)){
		        		$this->email->attach('_pdfs/solicitacoes/'.$value->arquivo);
		        		$toUnlink[] = '_pdfs/solicitacoes/'.$value->arquivo;		        		
		        	}
		        }
        	}

        	$corpoemail .= $this->_rodapeEmail();
	        
	        $this->email->from($from, $fromname);
	        $this->email->to($to);
	        if($bcc)
	            $this->email->bcc($bcc);
	        $this->email->reply_to($from);

	        $this->email->subject($assunto);
	        $this->email->message($corpoemail);
	        $this->email->set_alt_message($plain);

	        $envio = $this->email->send();

	        if($envio){
	        	$this->marcarEncaminhado($tipo, $to, 'all');
		        //if(sizeof($toUnlink) > 0){
		        	//foreach ($toUnlink as $arquivo) {
		        		//@unlink($arquivo);
		        		// Como os pedidos podem ser Reencaminhados, não vamos remover o arquivo após o envio
		        	//}
		        //}
	    	}

	        return $envio;

        }else{

        	foreach ($pedidos as $key => $value) {

        		$corpoemail = $this->_cabecalhoEmail($tipo);
        		$corpoemail .= $this->_constroiMsgHtml($value, $tipo);
        		$corpoemail .= $this->_rodapeEmail();

        		$plain = $this->constroiMsgPlain($value, $tipo);

        		if($tipo == 'cotacao'){
		        	if($value->arquivo && file_exists('_pdfs/solicitacoes/'.$value->arquivo)){
		        		$this->email->attach('_pdfs/solicitacoes/'.$value->arquivo);
		        		$toUnlink[] = '_pdfs/solicitacoes/'.$value->arquivo;
		        	}
		        }
	        
	        	$this->email->from($from, $fromname);
	        	$this->email->to($to);
	        	if($bcc)
	            	$this->email->bcc($bcc);
	        	$this->email->reply_to($value->email);

		        $this->email->subject($assunto);
		        $this->email->message($corpoemail);
		        $this->email->set_alt_message($plain);

	        	$envio = $this->email->send();

	        	if($envio){
		        	$this->marcarEncaminhado($tipo, $to, $value->id);
			        //if(sizeof($toUnlink) > 0){
			        	//foreach ($toUnlink as $arquivo) {
			        		//@unlink($arquivo);
			        		// Como os pedidos podem ser Reencaminhados, não vamos remover o arquivo após o envio
			        	//}
			        //}
		    	}
        	}

        	return $envio;
        }
	}

	public function reencaminhar($tipo, $id = false)
	{
		if(!$id || !$this->checaPropriedade($id, $this->session->userdata('id'), $tipo) || !$this->pegarPorId($id, $tipo))
			return false;
		else{

			$email = $this->input->post('email_encaminhado');
	        
	        $pedidos[] = $this->pegarPorId($id, $tipo);
	        
	        $emailconf['charset'] = 'utf-8';
	        $emailconf['mailtype'] = 'html';
	        $emailconf['protocol'] = 'smtp';
	        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
	        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
	        $emailconf['smtp_pass'] = 'p@rt.9al';

	        $this->load->library('email');

	        $this->email->initialize($emailconf);

	        $from = 'eventos@flexeventos.com.br';
	        $fromname = 'Flex Eventos';
	        $to = $email;
	        $bcc = 'bruno@trupe.net';
	        $assunto = ($tipo == 'cotacao') ? "Pedidos do Sistema Flex - Cotações" : "Pedidos do Sistema Flex - Catálogo";
	        $toUnlink = array();

        	foreach ($pedidos as $key => $value) {

        		$corpoemail = $this->_cabecalhoEmail($tipo);
        		$corpoemail .= $this->_constroiMsgHtml($value, $tipo);
        		$corpoemail .= $this->_rodapeEmail();

        		$plain = $this->constroiMsgPlain($value, $tipo);

        		if($tipo == 'cotacao'){
		        	if($value->arquivo && file_exists('_pdfs/solicitacoes/'.$value->arquivo)){
		        		$this->email->attach('_pdfs/solicitacoes/'.$value->arquivo);
		        		$toUnlink[] = '_pdfs/solicitacoes/'.$value->arquivo;
		        	}
		        }
	        
	        	$this->email->from($from, $fromname);
	        	$this->email->to($to);
	        	if($bcc)
	            	$this->email->bcc($bcc);
	        	$this->email->reply_to($value->email);

		        $this->email->subject($assunto);
		        $this->email->message($corpoemail);
		        $this->email->set_alt_message($plain);

	        	$envio = $this->email->send();

	        	if($envio){
		        	$this->atualizarInfoEncaminhado($tipo, $to, $value->id);
		    	}
        	}

	        return $envio;
	        
		}
	}

	private function _constroiMsgHtml($pedido, $tipo)
	{
		$data_formatada = formataTimestamp($pedido->data);
		if($tipo == 'cotacao'):
			$msg = <<<EML
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->nome}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->email}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->telefone}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->mensagem}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Data de Envio :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$data_formatada}</span><br>
EML;
			if($pedido->arquivo){
				$msg .= "<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Arquivo :</span>";
				$msg .= "<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->arquivo}</span><br>";
			}

			$msg .= "<br>====================================================<br><br>";
			return $msg;

		elseif($tipo == 'catalogo'):
			return <<<EML
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->nome}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->email}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Empresa :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->empresa}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Observações :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$pedido->observacoes}</span><br>

<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Data de Envio :</span>
<span style='color:#000;font-size:14px;font-family:Verdana;'>{$data_formatada}</span>

<br><br>====================================================<br><br>
EML;
		endif;
	}

	private function _cabecalhoEmail($tipo)
	{
		if($tipo == 'cotacao'):
			return <<<STR
<!DOCTYPE html>
<html>
<head>
    <title>Pedido de Cotações</title>
    <meta charset="utf-8">
</head>
<body>
STR;
		elseif($tipo == 'catalogo'):
			return <<<STR
<!DOCTYPE html>
<html>
<head>
    <title>Pedido de Catálogo</title>
    <meta charset="utf-8">
</head>
<body>
STR;
		endif;
	}

	private function _rodapeEmail()	
	{
		return <<<STR
</body>
</html>	
STR;
	}


	public function constroiMsgPlain($pedido, $tipo)
	{
		$data_formatada = formataTimestamp($pedido->data);

		if($tipo == 'cotacao'):

        	$msg = <<<EML
Nome : {$pedido->nome}\r\n
E-mail : {$pedido->email}\r\n
Telefone : {$pedido->telefone}\r\n
Mensagem : {$pedido->mensagem}\r\n
Data de Envio : {$data_formatada}\r\n
EML;
			if($pedido->arquivo){
				$msg .= "Arquivo : {$pedido->arquivo}\r\n";
			}
			$msg .= "\r\n====================================================\r\n\r\n";
			return $msg;

		elseif($tipo == 'catalogo'):

			return <<<EML
Nome : {$pedido->nome}\r\n
E-mail : {$pedido->email}\r\n
Empresa : {$pedido->empresa}\r\n
Observações : {$pedido->observacoes}\r\n
Data de Envio : {$data_formatada}\r\n\r\n
====================================================\r\n\r\n
EML;

		endif;
	}

	private function marcarEncaminhado($tipo, $email_destino, $id = 'all')
	{	
		$usarTabela = ($tipo == 'cotacao') ? $this->tabela_pedido_cotacoes : $this->tabela_pedido_catalogo;

		if($id == 'all'){
			$this->db->set('encaminhado', '1')
					 ->set('email_encaminhado', $email_destino)
					 ->set('data_encaminhado', Date('Y-m-d H:i:s'))
					 ->where('id_fornecedor', $this->session->userdata('id'))
					 ->where('encaminhado', '0')
					 ->update($usarTabela);
		}else{
			$this->db->set('encaminhado', '1')
					 ->set('email_encaminhado', $email_destino)
					 ->set('data_encaminhado', Date('Y-m-d H:i:s'))
					 ->where('id_fornecedor', $this->session->userdata('id'))
					 ->where('encaminhado', '0')
					 ->where('id', $id)
					 ->update($usarTabela);
		}
	}

	private function atualizarInfoEncaminhado($tipo, $to, $id)
	{
		$usarTabela = ($tipo == 'cotacao') ? $this->tabela_pedido_cotacoes : $this->tabela_pedido_catalogo;

		$this->db->set('email_encaminhado', $to)
				 ->set('data_encaminhado', Date('Y-m-d H:i:s'))
				 ->where('id_fornecedor', $this->session->userdata('id'))
				 ->where('id', $id)
				 ->update($usarTabela);
	}
}