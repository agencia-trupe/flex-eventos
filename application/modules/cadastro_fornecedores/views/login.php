<div id="banner-topo" class="fornecedores">
    <h2>
        SISTEMA DE CADASTRO PARA EMPRESAS FORNECEDORAS<br>
        DADOS DE CONTATO &bull; DESCRIÇÃO &bull; LISTA DE TIPOS DE PRODUTOS OFERECIDOS &bull; IMAGENS
    </h2>
</div>

<div class="centro" id="login-box">
	<p>
		Os dados de acesso (login e senha) são os mesmos utilizados no cadastro do Anuário Flex
	</p>
	<p style="display:none">
		<br>Estamos enfrentado uma sobrecarga de acessos no sistema no momento. Por favor aguarde alguns minutos para que possamos liberar os acessos. <br><br>Obrigado!
	</p>

	<div class="forms-container" style="display:block">
		<form action="cadastro-fornecedores/home/login" method="post" id="login-form">
			
			<?php if ($this->session->flashdata('errlogin')): ?>
				<p class="alerta alerta-erro">
					Erro ao efetuar o login. Verifique o email e a senha informados.
				</p>
			<?php endif ?>
			
			<?php if ($this->session->flashdata('mostrarsucesso')): ?>
				<p class="alerta alerta-sucesso">
					<?php echo $this->session->flashdata('mostrarsucesso_mensagem'); ?>
				</p>
			<?php endif ?>

			<label>
				e-mail (login)<br>
				<input type="text" name="login" required id="input-login">
			</label>
			<label>
				senha<br>
				<input type="password" name="pass" required id="input-pass">
			</label>
		
			<a href="cadastro-fornecedores/home/recuperacao" title="Clique aqui para solicitar uma nome senha" id="esqueci-senha">esqueci minha senha</a>

			<input type="submit" value="ENTRAR">
		</form>
	</div>
</div>