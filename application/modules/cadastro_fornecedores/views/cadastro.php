<div class="saudacoes">
	Olá <?=$fornecedores->nome?>. <a href="cadastro-fornecedores/home/logout" title="Sair do Sistema">[SAIR]</a>
</div>

<div class="menu-inicio">
	<a href="cadastro-fornecedores/home/cadastro" title="MEU CADASTRO" class="ativo">MEU CADASTRO</a>
	<a href="cadastro-fornecedores/contatos/recebidos" title="CONTATOS RECEBIDOS">CONTATOS RECEBIDOS</a>
	<a href="cadastro-fornecedores/contatos/historico" title="HISTÓRICO DE CONTATOS">HISTÓRICO DE CONTATOS</a>
</div>

<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alerta-sucesso alerta"><?=$mostrarsucesso_mensagem?></div>
<?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alerta-erro alerta"><?=$mostrarerro_mensagem?></div>
<?endif;?>

<hr>

<h1>CONFIRME SEUS DADOS DE CADASTRO</h1>

<form action="cadastro-fornecedores/home/salvar" method="post" enctype="multipart/form-data">
	<div class="coluna-esquerda">
		<label>
			Nome Fantasia <input type="text" name="nome_fantasia" required value="<?=$fornecedores->nome_fantasia?>" id="input_nome_fantasia">
		</label>
		<label>
			Razão Social <input type="text" name="razao_social" required value="<?=$fornecedores->razao_social?>" id="input-razao_social">
		</label>
		<label>
			Endereço [logradouro] <input type="text" name="endereco" required value="<?=$fornecedores->endereco?>" id="input-endereco">
		</label>
		<label>
			Número <input type="text" name="numero" required value="<?=$fornecedores->numero?>" class="w100" id="input-numero">
		</label>
		<label>
			Complemento <input type="text" name="complemento" value="<?=$fornecedores->complemento?>" class="w274" id="input-complemento">
		</label>
		<label>
			Bairro <input type="text" name="bairro" required value="<?=$fornecedores->bairro?>" id="input-bairro">
		</label>
		<label>
			Cidade <input type="text" name="cidade" required value="<?=$fornecedores->cidade?>" id="input-cidade">
		</label>
		<label>
			Estado [UF] <input type="text" name="estado" maxlength="2" required value="<?=$fornecedores->estado?>" class="w50" id="input-estado">
		</label>
		<label>
			CEP <input type="text" name="cep" required value="<?=$fornecedores->cep?>" class="w100" id="input-cep">
		</label>
		<label>
			<span style='font-size:12px;'>DDD e Telefone de Contato</span> <input type="text" name="telefone" required value="<?=$fornecedores->telefone?>" class="w274" id="input-telefone">
		</label>
		<label>
			Website <input type="text" name="website" value="<?=$fornecedores->website?>" id="input-website">
		</label>
		<label>
			<span style='font-size:12px;'>Descritivo de Apresentação</span>
			<textarea name="apresentacao" required><?=$fornecedores->apresentacao?></textarea>
		</label>
	</div>

	<div class="coluna-direita">
		<div class="caixa-cinza">
			<h3>IMAGEM DA EMPRESA</h3>
			<?php if ($fornecedores->imagem): ?>
				<img src="_imgs/anuario/marcas/<?=$fornecedores->imagem?>">
			<?php endif ?>
			<label id="fake-label">
				<div id="fake-file">SELECIONAR IMAGEM</div>
				<input type="file" name="imagem" id="input-marca">
			</label>
			<p>Tamanho mínimo: 300 x 200 - 72 dpi.</p>
		</div>
		<div class="caixa-cinza">
			<h3>CATEGORIA</h3>
			<p>Selecione a área para figurar no Anuário:</p>
			<ul>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='mobiliario')echo" checked"?> value="mobiliario" required>MOBILIÁRIO</label></li>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='componentes')echo" checked"?> value="componentes">COMPONENTES</label></li>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='arquishow')echo" checked"?> value="arquishow">ARQUISHOW</label></li>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='hr')echo" checked"?> value="hr">HR &bull; HOSPITAIS E HOTÉIS</label></li>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='construtoras')echo" checked"?> value="construtoras">CONSTRUTORAS</label></li>
				<li><label><input type="radio" name="categoria" <?if($fornecedores->categoria=='arquitetos')echo" checked"?> value="arquitetos">ARQUITETOS</label></li>
			</ul>
		</div>
	</div>

	<hr>

	<h1>PARA ALTERAR A SENHA</h1>

	<div class="coluna-esquerda">
		<label>
			Nome Completo <input type="text" name="nome" required value="<?=$fornecedores->nome?>" id="input-nome">
		</label>
		<label>
			E-mail <input type="email" name="email" required value="<?=$fornecedores->email?>" id="input-email">
		</label>
		<label>
			senha <input type="password" name="senha" id="input-senha">
		</label>
		<label>
			repetir senha <input type="password" name="confirma_senha" id="confirma_senha">
		</label>
	</div>
	<hr>

	<h1>CADASTRE AS PALAVRAS-CHAVE DO SEU NEGÓCIO</h1>

	<p class="simples" style="margin-bottom:20px;">
		Cadastre as palavras ou expressões que representem os produtos/serviços que sua empresa comercializa e pelas quais você imagina que seu público consumidor irá procurá-lo.
	</p>

	<input type="text" id="input-keyword" placeholder="palavra ou expressão"><a href="#" id="salvar-keyword" title="Adicionar Palavra-Chave">ADICIONAR</a>
	<input type="hidden" id="tags" value="<?=$todas_keywords?>">
	
	<ul id="listar-keyword">
		<?php $encoded = array() ?>
		<?php if ($keywords): ?>
			<?php foreach ($keywords as $key => $value): ?>

				<?php if ($value->sinonimos): ?>
					<li title='Sinônimos: <?=$value->sinonimos?>'>					
				<?php else: ?>
					<li title='Nenhum Sinônimo Cadastrado'>
				<?php endif ?>

					<span><?=$value->palavras?></span>
					<a href="#" class="excluir-keyword" title="Remover Keyword">x</a>
				</li>
				<?php $encoded[] = $value->palavras?>
			<?php endforeach ?>
		<?php endif ?>
	</ul>

	<input type="hidden" name="keywords" id="encoded-keywords" value='<?=json_encode($encoded)?>'>

	<hr id="scroll-here">

	<h1>ADICIONE IMAGENS E DESCRIÇÕES DOS SEUS PRODUTOS/SERVIÇOS</h1>

	<p class="simples" style="margin-bottom:20px;">
		Cadastre imagens e legendas que ajudem o usuário a identificar e comprar seus produtos/serviços. DICA: seja objetivo nas legendas.
	</p>

	<div class="linha">
		<span>ENVIO DE IMAGEM</span> <label id="fake-label"><input type="file" name="imagem-uploadify" id="input-imagem"></label>
	</div>

	<div class="linha">
		ADICIONAR LEGENDA <input type="text" id="legenda" placeholder="legenda">
	</div>

	<div class="linha">
		ADICIONAR PALAVRA-CHAVE <input type="text" id="keyword1" class="tag_imagens" placeholder="palavra relacionada 1 - não obrigatório">
	</div>

	<div class="linha">
		<input type="text" id="keyword2" class="tag_imagens" placeholder="palavra relacionada 2 - não obrigatório">
	</div>

	<div class="linha">
		<input type="text" id="keyword3" class="tag_imagens" placeholder="palavra relacionada 3 - não obrigatório">
	</div>

	<div class="linha adicionar">
		<a href="#" id="keyword-save" title="Adicionar Imagem">ADICIONAR</a>
	</div>

	<div class="linha alterar">
		<a href="#" id="keyword-edit" title="Salvar Alterações na Imagem">ATUALIZAR</a>
		<a href="#" id="keyword-cancel" title="Cancelar Alteração">CANCELAR</a>
	</div>

	<input type="hidden" id="idus" value="<?=$fornecedores->id?>">

	<ul id="listar-imagens">
		<?php $encoded = array() ?>
		<?php if ($imagens): ?>
			<?php foreach ($imagens as $key => $value): ?>
				<li>
					<img src="_imgs/fornecedores/imagens/<?=$fornecedores->id?>/thumbs/<?=$value->imagem?>">
					<p><?=$value->legenda?></p>
					<a href="#" class="excluir-imagem" title="Remover Imagem">excluir</a>
					<a href="#" class="alterar-imagem" title="Alterar Imagem">editar</a>
				</li>
				<?php $encoded[] = array('imagem' => $value->imagem, 'legenda' => $value->legenda, 'keyword1' => $value->keyword1, 'keyword2' => $value->keyword2, 'keyword3' => $value->keyword3)?>
			<?php endforeach ?>
		<?php endif ?>
	</ul>

	<input type="hidden" name="imagens" id="encoded-imagens" value='<?=json_encode($encoded)?>'>

	<input type="submit" class="final" value="SALVAR">
</form>