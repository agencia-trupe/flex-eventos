<div id="banner-topo" class="fornecedores">
    <h2>
        SISTEMA DE CADASTRO PARA EMPRESAS FORNECEDORAS<br>
        DADOS DE CONTATO &bull; DESCRIÇÃO &bull; LISTA DE TIPOS DE PRODUTOS OFERECIDOS &bull; IMAGENS
    </h2>
</div>

<div class="centro" id="login-box">
	<h1>Redefinição de Senha</h1>
	<p>
		Insira a nova senha desejada para seu cadastro e confirme-a.
	</p>

	<div class="forms-container">
		<form action="cadastro-fornecedores/home/redefinirSenha" method="post" id="recover-pass-form">

			<?php if ($this->session->flashdata('mostrarerro')): ?>
				<p class="alerta alerta-erro">
					<?php echo $this->session->flashdata('mostrarerro_mensagem'); ?>
				</p>
			<?php endif ?>

			<?php if ($this->session->flashdata('mostrarsucesso')): ?>
				<p class="alerta alerta-sucesso">
					<?php echo $this->session->flashdata('mostrarsucesso_mensagem'); ?>
				</p>
			<?php endif ?>			
			
			<label>
				Nova Senha<br>
				<input type="password" name="nova_senha" required id="input-nova-senha">
			</label>

			<label>
				Confirme a Nova Senha<br>
				<input type="password" name="nova_senha_conf" required id="input-nova-senha-conf">
			</label>

			<input type="hidden" name="token" value="<?=$token?>" required>

			<input type="submit" value="ALTERAR MINHA SENHA">
		</form>
	</div>
</div>