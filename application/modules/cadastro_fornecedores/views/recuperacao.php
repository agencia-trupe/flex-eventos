<div id="banner-topo" class="fornecedores">
    <h2>
        SISTEMA DE CADASTRO PARA EMPRESAS FORNECEDORAS<br>
        DADOS DE CONTATO &bull; DESCRIÇÃO &bull; LISTA DE TIPOS DE PRODUTOS OFERECIDOS &bull; IMAGENS
    </h2>
</div>

<div class="centro" id="login-box">
	<h1>Recuperação de Senha</h1>
	<p>
		Informe seu e-mail de cadastro. Um email será enviado para este endereço com um link para alteração da senha.
	</p>

	<div class="forms-container">
		<form action="cadastro-fornecedores/home/recuperarSenha" method="post" id="recover-form">

			<?php if ($this->session->flashdata('errrecover')): ?>
				<p class="alerta alerta-erro">
					E-mail não encontrado.
				</p>
			<?php endif ?>

			<?php if ($this->session->flashdata('mostrarerro')): ?>
				<p class="alerta alerta-erro">
					<?php echo $this->session->flashdata('mostrarerro_mensagem'); ?>
				</p>
			<?php endif ?>

			<?php if ($this->session->flashdata('mostrarsucesso')): ?>
				<p class="alerta alerta-sucesso">
					<?php echo $this->session->flashdata('mostrarsucesso_mensagem'); ?>
				</p>
			<?php endif ?>			
			
			<label>
				e-mail<br>
				<input type="text" name="login" required id="input-login">
			</label>

			<input type="submit" value="SOLICITAR NOVA SENHA">
		</form>
	</div>
</div>