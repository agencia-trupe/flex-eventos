<div class="saudacoes">
	Olá <?=$fornecedores->nome?>. <a href="cadastro-fornecedores/home/logout" title="Sair do Sistema">[SAIR]</a>
</div>

<?if($this->session->flashdata('mostrarsucesso') && $this->session->flashdata('mostrarsucesso')):?>
	<div class="alerta-sucesso alerta"><?=$this->session->flashdata('mostrarsucesso_mensagem')?></div>
<?elseif($this->session->flashdata('mostrarerro') && $this->session->flashdata('mostrarerro')):?>
	<div class="alerta-erro alerta"><?=$this->session->flashdata('mostrarerro_mensagem')?></div>
<?endif;?>

<hr>

<h1>BEM-VINDO AO SEU PAINEL DE CONTROLE DO SISTEMA 'ENCONTRE UM FORNECEDOR - FLEX'</h1>

<p class="simples margem-b10">Olá!</p>

<p class="simples margem-b10">Você que é anunciante do Anuário Corporativo Flex X5 ganha agora mais um canal de negócios!</p>

<p class="simples margem-b10">A Flex Eventos e Editora criou uma nova ferramenta exclusiva: um Catálogo Online de Fornecedores de produtos e serviços de arquitetura, que vai facilitar a busca para os clientes e a exposição para os fornecedores.</p>

<p class="simples margem-b10">Os dados e imagens que você forneceu para o Anuário já estão incluídos na ferramenta e você pode (deve) agora atualizar e completar as informações para aumentar sua exposição na internet. A ferramenta proporciona uma busca categorizada e fácil de produtos, profissionais e serviços, e pontecializará as buscas de fornecedores de arquitetura na internet. Para o cliente a ferramenta facilita o contato com várias empresas de uma só vez e para o fornecedor ela aumenta - e muito - a prospecção, já que é de grande utilidade para quem busca.</p>

<p class="simples margem-b10">A concentração das buscas de fornecedores, produtos e serviços de arquitetura criará um novo hábito para os clientes, que encontrarão em um só lugar tudo o que precisam em seus projetos.</p>

<p class="simples margem-b10">Para visualizar a ferramenta como se fosse um cliente acesse: <a href="http://www.flexeventos.com.br/fornecedores" target="_blank">www.flexeventos.com.br/fornecedores</a></p>

<p class="simples margem-b10">Durante o próximo mês a ferramenta estará disponível apenas aos fornecedores para conferência e cadastro de novas informações. A partir de 20 de maio ela será lançada para todos os usuários, e então você passará a receber contatos e pedidos de orçamento vindos do sistema FLEX FORNECEDORES.</p>

<p class="simples margem-b10">Se tiver dúvidas escreva para <a href="mailto:flexfornecedores@trupe.net">flexfornecedores@trupe.net</a> e teremos prazer em ajudá-lo.</p>

<p class="simples margem-b10">Gratos,<br>Equipe Flex Eventos e Editora</p>

<hr>

<p class="simples margem-b10">
	Aqui você pode atualizar seu cadastro e incluir produtos para ser melhor encontrado nas buscas dos usuários.
</p>
<p class="simples margem-b10">
	E também pode enviar para seu e-mail os pedidos de orçamento recebidos e consultar seu histórico de contatos recebidos através do sistema.
</p>

<div class="menu-inicio">
	<a href="cadastro-fornecedores/home/cadastro" title="MEU CADASTRO">MEU CADASTRO</a>
	<a href="cadastro-fornecedores/contatos/recebidos" title="CONTATOS RECEBIDOS">CONTATOS RECEBIDOS</a>
	<a href="cadastro-fornecedores/contatos/historico" title="HISTÓRICO DE CONTATOS">HISTÓRICO DE CONTATOS</a>
</div>

<p class="simples margem-t120 margem-b220">
	Para dúvidas e sugestões contate-nos: <a href="mailto:contato@flexeventos.com.br" title="Envie um e-mail!">contato@flexeventos.com.br</a>
</p>