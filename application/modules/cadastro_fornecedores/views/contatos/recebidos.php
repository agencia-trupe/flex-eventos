<div class="saudacoes">
	Olá <?=$fornecedores->nome?>. <a href="cadastro-fornecedores/home/logout" title="Sair do Sistema">[SAIR]</a>
</div>

<div class="menu-inicio">
	<a href="cadastro-fornecedores/home/cadastro" title="MEU CADASTRO">MEU CADASTRO</a>
	<a href="cadastro-fornecedores/contatos/recebidos" title="CONTATOS RECEBIDOS" class="ativo">CONTATOS RECEBIDOS</a>
	<a href="cadastro-fornecedores/contatos/historico" title="HISTÓRICO DE CONTATOS">HISTÓRICO DE CONTATOS</a>
</div>

<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alerta-sucesso alerta"><?=$mostrarsucesso_mensagem?></div>
<?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alerta-erro alerta"><?=$mostrarerro_mensagem?></div>
<?endif;?>

<hr>

<h1 class="titulo-pedidos">PEDIDOS DE COTAÇÃO &bull; POR DATA

<?php if ($pedidos_cotacao): ?>
	<form action="cadastro-fornecedores/contatos/encaminharPedidosCotacao" method="post">
		<label for="email_encaminhado">ENCAMINHAR TODOS PARA O E-MAIL</label>
		<input type="text" name="email_encaminhado" value="<?=$fornecedores->email?>" required>
		<input type="submit" value="OK">
	</form>
<?php endif ?>
</h1>

<?php if ($pedidos_cotacao): ?>
	<table class="tabela-pedidos">
		<?php foreach ($pedidos_cotacao as $key => $value): ?>
			<tr>
				<td>
					<div class="tab-pad">
						<?=formataTimestamp($value->data, TRUE)?>
					</div>
				</td>
				<td>
					<div class="tab-pad">
						<?=$value->nome?>
					</div>
				</td>
				<td>
					<div class="tab-pad">
						<?=$value->email?>
					</div>
				</td>
				<td>
					<div class="tab-pad">
						<?=$value->telefone?>
						<a href="cadastro-fornecedores/contatos/excluir/<?=$value->id?>" class="delete-btn" data-tipo="cotacao" title="Excluir Pedido">x</a>
					</div>
				</td>
			</tr>
		<?php endforeach ?>
	</table>
<?php else: ?>
	<h2>Nenhum novo pedido</h2>
<?php endif ?>

<hr>

<h1 class="titulo-pedidos">SOLICITAÇÕES DE CATÁLOGO &bull; POR DATA

<?php if ($pedidos_catalogo): ?>
	<form action="cadastro-fornecedores/contatos/encaminharPedidosCatalogo" method="post">
		<label for="email_encaminhado">ENCAMINHAR TODOS PARA O E-MAIL</label>
		<input type="text" name="email_encaminhado" value="<?=$fornecedores->email?>" required>
		<input type="submit" value="OK">
	</form>
<?php endif ?>
</h1>

<?php if ($pedidos_catalogo): ?>
	<table class="tabela-pedidos">
		<?php foreach ($pedidos_catalogo as $key => $value): ?>
			<tr>
				<td>
					<div class="tab-pad">
						<?=formataTimestamp($value->data, TRUE)?></td>
					</div>
				<td>
					<div class="tab-pad">
						<?=$value->nome?></td>
					</div>
				<td>
					<div class="tab-pad">
						<?=$value->email?></td>
					</div>
				<td>
					<div class="tab-pad">
						<?=$value->empresa?>
						<a href="cadastro-fornecedores/contatos/excluir/<?=$value->id?>" class="delete-btn" data-tipo="catalogo" title="Excluir Pedido">x</a>
					</div>
				</td>
			</tr>
		<?php endforeach ?>
	</table>
<?php else: ?>
	<h2>Nenhum novo pedido</h2>
<?php endif ?>