<div id="solicitacao-modal">

	<h2>SOLICITAÇÃO DE COTAÇÃO RECEBIDA</h2>

	<div class="linha">
		<div class="celula">data de recebimento:</div>
		<div class="celula"><?=formataTimestamp($pedido->data)?></div>
	</div>
	<div class="linha">
		<div class="celula">nome:</div>
		<div class="celula"><?=$pedido->nome?></div>
	</div>
	<div class="linha">
		<div class="celula">e-mail:</div>
		<div class="celula"><?=$pedido->email?></div>
	</div>
	<div class="linha">
		<?php if($tipo == 'cotacao'): ?>
			<div class="celula">telefone:</div>
			<div class="celula"><?=$pedido->telefone?></div>
		<?php elseif($tipo == 'catalogo'): ?>
			<div class="celula">empresa:</div>
			<div class="celula"><?=$pedido->empresa?></div>
		<?php endif; ?>
	</div>
	<div class="linha">
		<?php if($tipo == 'cotacao'): ?>
			<div class="celula">mensagem:</div>
			<div class="celula">				
				<?=$pedido->mensagem.' '?>				
			</div>
		<?php elseif($tipo == 'catalogo'): ?>
			<div class="celula">observações:</div>
			<div class="celula"><?=$pedido->observacoes?></div>
		<?php endif; ?>		
	</div>
	<?php if($tipo == 'cotacao' && isset($pedido->arquivo) && $pedido->arquivo): ?>
		<div class="linha">
			<div class="celula"></div>
			<div class="celula"><a href="cadastro-fornecedores/contatos/download/anexo/<?=$pedido->arquivo?>" title="Download do arquivo enviado">download de arquivo anexado</a></div>
		</div>
	<?php endif; ?>
	<div class="linha">
		<div class="celula">encaminhada para:</div>
		<div class="celula"><?=$pedido->email_encaminhado?></div>
	</div>
	<div class="linha">
		<div class="celula">em:</div>
		<div class="celula"><?=formataTimestamp($pedido->data_encaminhado)?></div>
	</div>

	<?php if($tipo == 'cotacao'): ?>
		<form action="cadastro-fornecedores/contatos/reencaminharCotacao/<?=$pedido->id?>" method="post">
	<?php elseif($tipo == 'catalogo'): ?>
		<form action="cadastro-fornecedores/contatos/reencaminharCatalogo/<?=$pedido->id?>" method="post">
	<?php endif; ?>

		<label for="email_encaminhado">REENCAMINHAR PARA O E-MAIL</label>
		<input type="text" name="email_encaminhado" value="<?=$pedido->email_encaminhado?>" required>
		<input type="submit" value="OK">
	</form>

</div>