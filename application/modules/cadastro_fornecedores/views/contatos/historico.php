<div class="saudacoes">
	Olá <?=$fornecedores->nome?>. <a href="cadastro-fornecedores/home/logout" title="Sair do Sistema">[SAIR]</a>
</div>

<div class="menu-inicio">
	<a href="cadastro-fornecedores/home/cadastro" title="MEU CADASTRO">MEU CADASTRO</a>
	<a href="cadastro-fornecedores/contatos/recebidos" title="CONTATOS RECEBIDOS">CONTATOS RECEBIDOS</a>
	<a href="cadastro-fornecedores/contatos/historico" title="HISTÓRICO DE CONTATOS" class="ativo">HISTÓRICO DE CONTATOS</a>
</div>

<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alerta-sucesso alerta"><?=$mostrarsucesso_mensagem?></div>
<?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alerta-erro alerta"><?=$mostrarerro_mensagem?></div>
<?endif;?>

<hr>

<div class="historico-pedidos">

	<nav>
		<ul>
			<li>
				<a href="cadastro-fornecedores/contatos/historico/cotacao" title="Histórico de Pedidos de Cotações" <?if($tipo == 'cotacao')echo "class='ativo'"?>>&raquo; COTAÇÃO</a>
			</li>
			<li>
				<a href="cadastro-fornecedores/contatos/historico/catalogo" title="Histórico de Pedidos de Catálogos" <?if($tipo == 'catalogo')echo "class='ativo'"?>>&raquo; CATÁLOGOS</a>
			</li>
		</ul>
	</nav>

	<div id="box-filtros">
		<div class="coluna">
			<h2>BUSCAR POR DATA:</h2>
			<form action="cadastro-fornecedores/contatos/historico/<?=$tipo?>" method="get">
				<input type="text" name="filtroData[dia]" placeholder="DIA" maxlength="2" value="<?=$filtro['data']['dia']?>">
				<input type="text" name="filtroData[mes]" placeholder="MÊS" maxlength="2" required value="<?=$filtro['data']['mes']?>">
				<input type="text" name="filtroData[ano]" placeholder="ANO" maxlength="4" required value="<?=$filtro['data']['ano']?>">
				<input type="submit" value='OK'>
			</form>
			<h3>Você pode fazer a busca por mês deixando o dia sem preenchimento.</h3>
		</div>
		<div class="coluna">
			<h2>BUSCAR POR NOME:</h2>
			<form action="cadastro-fornecedores/contatos/historico/<?=$tipo?>" method="get">
				<input type="text" name="filtroNome" placeholder="Nome" value="<?=$filtro['nome']?>" class="lg">
				<input type="submit" value='OK'>
			</form>
			<h3>As buscas serão realizadas no Nome Completo</h3>
		</div>
		<div class="coluna">
			<a href="cadastro-fornecedores/contatos/download/planilha/<?=$tipo?>" title="Extrair Planilha" id="download-planilha">EXTRAIR PLANILHA COM TODOS OS DADOS</a>
		</div>
	</div>

	<h1 class="titulo-pedidos">PEDIDOS DE <?php echo ($tipo=='cotacao') ? "COTAÇÃO" : "CATÁLOGO"?> RECEBIDOS &bull; POR DATA</h1>

	<?php if ($pedidos): ?>
		<table class="tabela-pedidos clicavel">
			<?php foreach ($pedidos as $key => $value): ?>
				<tr data-id="<?=$value->id?>" data-tipo="<?=$tipo?>">
					<td>
						<div class="tab-pad">
							<?=formataTimestamp($value->data, TRUE)?>
						</div>
					</td>
					<td>
						<div class="tab-pad">
							<?=$value->nome?>
						</div>
					</td>
					<td>
						<div class="tab-pad">
							<?=$value->email?>
						</div>
					</td>
					<td>
						<div class="tab-pad">
							<? echo ($tipo == 'cotacao') ? $value->telefone : $value->empresa?>
							<a href="cadastro-fornecedores/contatos/excluir/<?=$value->id?>" class="delete-btn" data-tipo="<?=$tipo?>" title="Excluir Pedido">x</a>
						</div>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
	<?php else: ?>
		<h2>Nenhum Histórico de Pedidos Encontrado</h2>
	<?php endif ?>

	<?php if($paginacao): ?>
		<ul class="paginacao">
			<?php echo $paginacao ?>
		</ul>
	<?php endif; ?>

</div>