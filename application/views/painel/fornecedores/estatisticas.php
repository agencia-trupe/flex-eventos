<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?>
        </h2>
    </div>

    Filtrar por período:<br>
    <div class="form-inline" style="margin-top:3px">
    	<form action="painel/fornecedores/estatisticas/0/" method="get">
    		<input type="text" class="datepicker input-small" placeholder="Data inicial" name="data_inicio" <?php if(isset($filtro_data_inicio) && $filtro_data_inicio)echo" value='".$filtro_data_inicio."'"?>>
    		<input type="text" class="datepicker input-small" placeholder="Data final" name="data_final" <?php if(isset($filtro_data_final) && $filtro_data_final)echo" value='".$filtro_data_final."'"?>>
			<input type="submit" class="btn btn-info" value="Filtrar">
    	</form>
    </div>
    <hr>
    
    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

                <p class="text-success">
                    Palavras em <strong>VERDE</strong>: Palavras que já estão associadas à Fornecedores ou Produtos
                </p>
                <p class="text-warning">
                    Palavras em <strong>AMARELO</strong>: Palavras que ainda <strong>não</strong> estão associadas à nenhum Fornecedor ou Produto mas estão sendo buscadas pelos visitantes
                </p>

	            <table class="table table-striped table-bordered table-condensed">

	                <thead>
	                    <tr>
	                        <th class="yellow header headerSortDown">Keyword</th>
	                        <th class="header">Buscas Realizadas</th>	                        
	                    </tr>
	                </thead>

	                <tbody>
	                <?php foreach ($registros as $key => $value): ?>

	                    <tr class="tr-row">
	                        <td>
                                <?php if ($value->cadastrada): ?>
                                    <p class="text-success">
                                        <?=$value->palavra_buscada?>
                                    </p>
                                <?php else: ?>
                                    <p class="text-warning">
                                        <?=$value->palavra_buscada?>
                                    </p>
                                <?php endif ?>
                            </td>
	                        <td><?=$value->Buscas?></td>
	                    </tr>

	                <?php endforeach ?>
	                </tbody>

	            </table>

            <?php if ($paginacao): ?>
	            <div class="pagination">
	                <ul>
	                    <?=$paginacao?>
	                </ul>
	            </div>
            <?php endif ?>

            <?php else:?>

                <h3>Nenhum Registro</h2>

            <?php endif ?>

        </div>
    </div>