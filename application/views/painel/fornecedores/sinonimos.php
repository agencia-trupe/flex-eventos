<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            Sinônimos de Keywords
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

	            <p class="muted">
	            	Clique na palavra para associar sinônimos
	            </p>

	        
	            <?php foreach ($registros as $key => $value): ?>

	                    <a href="painel/fornecedores/sinonimosDetalhes/<?=$value->id?>" title="<?=$value->palavras?>" class="btn btn-mini btn-info abre-sinonimos" style="margin-bottom:3px" data-id="<?=$value->id?>"><?=$value->palavras?></a>

	            <?php endforeach ?>
        
            <?php else:?>

                <h3>Nenhum Registro</h2>

            <?php endif ?>

        </div>
    </div>