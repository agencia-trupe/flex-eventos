<div class="container top fornecedores-form">

  	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if (isset($registro) && $registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data" id="form-edit-cad" class="edit-fornecedor">

		<div class="coluna-esquerda">
		<label>
			Nome Fantasia <input type="text" name="nome_fantasia" required value="<?=$registro->nome_fantasia?>" id="input_nome_fantasia">
		</label>
		<label>
			Razão Social <input type="text" name="razao_social" required value="<?=$registro->razao_social?>" id="input-razao_social">
		</label>
		<label>
			Endereço [logradouro] <input type="text" name="endereco" required value="<?=$registro->endereco?>" id="input-endereco">
		</label>
		<label>
			Número <input type="text" name="numero" required value="<?=$registro->numero?>" class="w100" id="input-numero">
		</label>
		<label>
			Complemento <input type="text" name="complemento" value="<?=$registro->complemento?>" class="w274" id="input-complemento">
		</label>
		<label>
			Bairro <input type="text" name="bairro" required value="<?=$registro->bairro?>" id="input-bairro">
		</label>
		<label>
			Cidade <input type="text" name="cidade" required value="<?=$registro->cidade?>" id="input-cidade">
		</label>
		<label>
			Estado [UF] <input type="text" name="estado" maxlength="2" required value="<?=$registro->estado?>" class="w50" id="input-estado">
		</label>
		<label>
			CEP <input type="text" name="cep" required value="<?=$registro->cep?>" class="w100" id="input-cep">
		</label>
		<label>
			<span style='font-size:12px;'>DDD e Telefone de Contato</span> <input type="text" name="telefone" required value="<?=$registro->telefone?>" class="w274" id="input-telefone">
		</label>
		<label>
			Website <input type="text" name="website" value="<?=$registro->website?>" id="input-website">
		</label>
		<label>
			<span style='font-size:12px;'>Descritivo de Apresentação</span>
			<textarea name="apresentacao" required><?=$registro->apresentacao?></textarea>
		</label>
	</div>

	<div class="coluna-direita">
		<div class="caixa-cinza">
			<h3>IMAGEM DA EMPRESA</h3>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/anuario/marcas/<?=$registro->imagem?>">
			<?php endif ?>
			<label id="fake-label">
				<div id="fake-file">SELECIONAR IMAGEM</div>
				<input type="file" name="imagem" id="input-marca">
			</label>
			<p>Tamanho mínimo: 300 x 200 - 72 dpi.</p>
		</div>
		<div class="caixa-cinza">
			<h3>CATEGORIA</h3>
			<p>Selecione a área para figurar no Anuário:</p>
			<ul>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='mobiliario')echo" checked"?> value="mobiliario" required>MOBILIÁRIO</label></li>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='componentes')echo" checked"?> value="componentes">COMPONENTES</label></li>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='arquishow')echo" checked"?> value="arquishow">ARQUISHOW</label></li>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='hr')echo" checked"?> value="hr">HR &bull; HOSPITAIS E HOTÉIS</label></li>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='construtoras')echo" checked"?> value="construtoras">CONSTRUTORAS</label></li>
				<li><label><input type="radio" name="categoria" <?if($registro->categoria=='arquitetos')echo" checked"?> value="arquitetos">ARQUITETOS</label></li>
			</ul>
		</div>
	</div>

	<hr>

	<h2>PARA ALTERAR A SENHA</h2>

	<div class="coluna-esquerda">
		<label>
			Nome Completo <input type="text" name="nome" required value="<?=$registro->nome?>" id="input-nome">
		</label>
		<label>
			E-mail <input type="email" name="email" required value="<?=$registro->email?>" id="input-email">
		</label>
		<label>
			senha <input type="password" name="senha" id="input-senha">
		</label>
		<label>
			repetir senha <input type="password" name="confirma_senha" id="confirma_senha">
		</label>
	</div>
	<hr>

	<h2>CADASTRE AS PALAVRAS-CHAVE DO SEU NEGÓCIO</h2>

	<p class="simples" style="margin-bottom:20px;">
		Cadastre as palavras ou expressões que representem os produtos/serviços que sua empresa comercializa e pelas quais você imagina que seu público consumidor irá procurá-lo.
	</p>

	<input type="text" id="input-keyword" placeholder="palavra ou expressão"><a href="#" id="salvar-keyword" title="Adicionar Palavra-Chave">ADICIONAR</a>
	<input type="hidden" id="tags" value="<?=$todas_keywords?>">
	
	<ul id="listar-keyword">
		<?php $encoded = array() ?>
		<?php if ($keywords): ?>
			<?php foreach ($keywords as $key => $value): ?>

				<?php if ($value->sinonimos): ?>
					<li title='Sinônimos: <?=$value->sinonimos?>'>					
				<?php else: ?>
					<li title='Nenhum Sinônimo Cadastrado'>
				<?php endif ?>

					<span><?=$value->palavras?></span>
					<a href="#" class="excluir-keyword" title="Remover Keyword">x</a>
				</li>
				<?php $encoded[] = $value->palavras?>
			<?php endforeach ?>
		<?php endif ?>
	</ul>

	<input type="hidden" name="keywords" id="encoded-keywords" value='<?=json_encode($encoded)?>'>

	<hr id="scroll-here">

	<h2>ADICIONE IMAGENS E DESCRIÇÕES DOS SEUS PRODUTOS/SERVIÇOS</h2>

	<p class="simples" style="margin-bottom:20px;">
		Cadastre imagens e legendas que ajudem o usuário a identificar e comprar seus produtos/serviços. DICA: seja objetivo nas legendas.
	</p>

	<div class="linha">
		<span>ENVIO DE IMAGEM</span> <label id="fake-label"><input type="file" name="imagem-uploadify" id="input-imagem"></label>
	</div>

	<div class="linha">
		ADICIONAR LEGENDA <input type="text" id="legenda" placeholder="legenda">
	</div>

	<div class="linha">
		ADICIONAR PALAVRA-CHAVE <input type="text" id="keyword1" class="tag_imagens" placeholder="palavra relacionada 1 - não obrigatório">
	</div>

	<div class="linha">
		<input type="text" id="keyword2" class="tag_imagens" placeholder="palavra relacionada 2 - não obrigatório">
	</div>

	<div class="linha">
		<input type="text" id="keyword3" class="tag_imagens" placeholder="palavra relacionada 3 - não obrigatório">
	</div>

	<div class="linha adicionar">
		<a href="#" id="keyword-save" title="Adicionar Imagem">ADICIONAR</a>
	</div>

	<div class="linha alterar">
		<a href="#" id="keyword-edit" title="Salvar Alterações na Imagem">ATUALIZAR</a>
		<a href="#" id="keyword-cancel" title="Cancelar Alteração">CANCELAR</a>
	</div>

	<input type="hidden" id="idus" value="<?=$registro->id?>">

	<ul id="listar-imagens">
		<?php $encoded = array() ?>
		<?php if ($imagens): ?>
			<?php foreach ($imagens as $key => $value): ?>
				<li>
					<img src="_imgs/fornecedores/imagens/<?=$registro->id?>/thumbs/<?=$value->imagem?>">
					<p><?=$value->legenda?></p>
					<a href="#" class="excluir-imagem" title="Remover Imagem">excluir</a>
					<a href="#" class="alterar-imagem" title="Alterar Imagem">editar</a>
				</li>
				<?php $encoded[] = array('imagem' => $value->imagem, 'legenda' => $value->legenda, 'keyword1' => $value->keyword1, 'keyword2' => $value->keyword2, 'keyword3' => $value->keyword3)?>
			<?php endforeach ?>
		<?php endif ?>
	</ul>

	<input type="hidden" name="imagens" id="encoded-imagens" value='<?=json_encode($encoded)?>'>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data" id="form-add-cad">

        <label>
        	<strong>Nome Fantasia <span style="color:red">(obrigatório)</span></strong> : <input type="text" name="nome_fantasia" required id="input-nome_fantasia">
    	</label>
        <label>
        	<strong>Razão Social</strong> : <input type="text" name="razao_social">
    	</label>
        <label>
        	<strong>Endereço</strong> : <input type="text" name="endereco">
    	</label>
        <label>
        	<strong>Número</strong> : <input type="text" name="numero">
    	</label>
        <label>
        	<strong>Complemento</strong> : <input type="text" name="complemento">
    	</label>
        <label>
        	<strong>Bairro</strong> : <input type="text" name="bairro">
    	</label>
        <label>
        	<strong>Cidade</strong> : <input type="text" name="cidade">
    	</label>
        <label>
        	<strong>Estado</strong> : <input type="text" name="estado">
    	</label>
        <label>
        	<strong>Cep</strong> : <input type="text" name="cep">
    	</label>
        <label>
        	<strong>Telefone</strong> : <input type="text" name="telefone">
    	</label>
        <label>
        	<strong>Website</strong> : <input type="text" name="website">
    	</label>
        <label>
        	<strong>Nome do Contato <span style="color:red">(obrigatório)</span></strong> : <input type="text" name="nome" required id="input-nome">
    	</label>

        <label>
        	<strong>E-mail <span style="color:red">(obrigatório)</span></strong> : <input type="text" name="email" required id="input-email">
    	</label>

		<label>Apresentação<br>
			<textarea name="apresentacao"></textarea>
		</label>

        <label>
        	<strong>Imagem</strong> :<br>
        	<input type="file" name="imagem">
    	</label>

		<label>
		Categoria
		<select name="categoria">
			<?php foreach ($categorias as $key => $value): ?>
				<option value="<?=$key?>"><?=$value?></option>
			<?php endforeach ?>
		</select>
		</label>

    	<label>
    		<strong>Senha Temporária <span style="color:red">(obrigatório)</span></strong> :
    		<div class="input-append">
    			<input type="text" name="senha" required id="input-senha">
    			<button type="button" class="btn add-on append-gerar-senha">gerar senha aleatória</button>
    		</div>
    	</label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>