<div id="shadow-sinonimos">

	<?php if ($listaPalavras): ?>

		<h2>Selecione os sinônimos:</h2>
		
		<? $contador = 0 ?>
		<? $numResultados=sizeof($listaPalavras) ?>
		<? $porColuna=floor($numResultados/3) ?>

		<form action="painel/fornecedores/salvarSinonimos/<?=$palavraSelecionada->id?>" method="post" id="form-sinonimos">
		<hr>
		<ul>

			<?php foreach ($listaPalavras as $key => $value): ?>

				<?php if ($palavraSelecionada->id != $value->id): ?>

					<li>
						<label>
							<input type="checkbox" name="sinonimo[]" class="check" value="<?=$value->id?>" titulo="<?=$value->palavras?>" <?php if(in_array($value->id, $listaSinonimos)) echo" checked" ?>> <?=$value->palavras?>
						</label>
					</li>					

					<?php $contador++?>

					<?php if ($contador == $porColuna): ?>
						<?php echo "</ul><ul>"; ?>
						<?php $contador = 0; ?>
					<?php endif ?>

				<?php endif ?>

			<?php endforeach ?>	

		</ul>
		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button onclick="javascript:$.fancybox.close();" class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
		</form>

	<?php endif ?>
</div>

<script defer>
	$('document').ready( function(){
		$('#form-sinonimos').submit( function(e){
			e.preventDefault();
			var destinoForm = $(this).attr('action');
			var values = $('.check:checked').map(function(i,n) {
        		return $(n).val();
    		}).get();

			$.post(destinoForm, {sinonimos : values}, function(resposta){
				$('form').html(resposta+"<div class='form-actions'><button onclick='javascript:$.fancybox.close();' class='btn btn-voltar' type='reset'>Fechar</button></div>");
			});
		});
	});
</script>