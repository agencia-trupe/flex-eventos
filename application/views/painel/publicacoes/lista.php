<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
    	</h2>
  	</div>

  	<div style="margin-bottom:20px">
  		<a href="painel/publicacoes/index/0/0" title="Todas as Revistas" style='margin:3px 0' class="btn btn-small<?if($id_publicacao==0)echo " btn-info"?>">Todas as Revistas</a>
  		<?php if ($revistas): ?>
  			<?php foreach ($revistas as $key => $value): ?>
  				<a href="painel/publicacoes/index/<?=$value->id?>/0" title="<?=$value->titulo?>" style='margin:3px 0' class="btn btn-small<?if($id_publicacao==$value->id)echo " btn-info"?>"><?=$value->titulo?></a>
  			<?php endforeach ?>
  		<?php endif ?>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

    		<?php if ($registros): ?>

      	    <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="trupe_site_publicacoes">

	          <thead>
	            <tr>
	            		<th class="yellow header headerSortDown">Publicação</th>
                  <th class="header">Edição</th>
                  <th class="yellow header headerSortDown">Data de Publicação</th>
	              	<th class="red header">Ações</th>
	            </tr>
	          </thead>

	          <tbody>
	            <?php foreach ($registros as $key => $value): ?>

	                <tr class="tr-row" id="row_<?=$value->id?>">
	                		<td><?=$value->revista?></td>
                      <td><?=$value->numero?></td>
                      <td><?=formataData($value->data_publicacao, 'mysql2br')?></td>
	                  	<td class="crud-actions">
	                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
	                    <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
	                  </td>
	                </tr>

	            <?php endforeach ?>
	          </tbody>

	        </table>

        <?php if ($paginacao): ?>
          	<div class="pagination">
            	<ul>
              		<?=$paginacao?>
            	</ul>
          	</div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>


    </div>
  </div>