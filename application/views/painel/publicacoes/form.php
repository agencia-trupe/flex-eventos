<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Revista<br>
		<select name="id_publicacoes_revistas" required>
			<option value="">Selecione uma Revista</option>
			<?php foreach ($revistas as $key => $value): ?>
				<option value="<?=$value->id?>" <?if($value->id==$registro->id_publicacoes_revistas)echo" selected"?>><?=$value->titulo?></option>
			<?php endforeach ?>
		</select></label><br>

		<label>Número<br>
		<input type="text" name="numero" required value="<?=$registro->numero?>"></label><br>

		<label>Subtítulo<br>
		<input type="text" name="subtitulo" value="<?=$registro->subtitulo?>"></label><br>

		<label>Data de Publicação<br>
		<input type="text" name="data_publicacao" required class="datepicker" value="<?=formataData($registro->data_publicacao, 'mysql2br')?>"></label>

		<label>Imagem da capa<br>
		<?if($registro->capa):?>
			<img src="_imgs/publicacoes/<?=$registro->capa?>"><br>
		<?endif;?>
		<input type="file" name="userfile"></label><br>

		<label>Arquivo PDF<br>
		<?if($registro->pdf):?>
			<a href="_pdfs/publicacoes/<?=$registro->pdf?>" title="Ver Arquivo PDF"><?=$registro->pdf?></a><br>
		<?endif;?>
		<input type="file" name="userfile_pdf"></label><br>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Revista<br>
		<select name="id_publicacoes_revistas" required>
			<option value="">Selecione uma Revista</option>
			<?php foreach ($revistas as $key => $value): ?>
				<option value="<?=$value->id?>"><?=$value->titulo?></option>
			<?php endforeach ?>
		</select></label><br>

		<label>Número<br>
		<input type="text" name="numero" required></label><br>

		<label>Subtítulo<br>
		<input type="text" name="subtitulo"></label><br>

		<label>Data de Publicação<br>
		<input type="text" name="data_publicacao" required class="datepicker"></label>

		<label>Imagem da capa<br>
		<input type="file" name="userfile" required></label><br>

		<label>Arquivo PDF<br>
		<input type="file" name="userfile_pdf" required></label><br>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>