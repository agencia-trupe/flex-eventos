<div class="navbar">

  <div class="navbar-inner">

    <div class="container" style="width:80%">

      <a href="painel/home" class="brand">Flex Eventos</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li class="dropdown <?if($this->router->class=='anuncios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anúncios <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/anuncios/index/top">Top Banner</a></li>
            <li><a href="painel/anuncios/index/home">Home</a></li>
            <li><a href="painel/anuncios/index/eventos">Eventos</a></li>
            <li><a href="painel/anuncios/index/publicacoes">Publicações</a></li>
            <li><a href="painel/anuncios/index/noticias">Notícias</a></li>
            <li><a href="painel/anuncios/index/parceiros">Parceiros</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='banners')echo" class='active'"?>><a href="painel/banners">Banners</a></li>

        <li <?if($this->router->class=='destaques')echo" class='active'"?>><a href="painel/destaques">Destaques</a></li>

        <li class="dropdown  <?if($this->router->class=='eventos' || $this->router->class=='inscricoes')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Eventos <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/eventos/">Cadastro de Eventos</a></li>
            <li><a href="painel/inscricoes/">Inscrições</a></li>
            <li><a href="painel/inscricoes/extrair/">Extrair Inscrições</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='premio')echo" class='active'"?>><a href="painel/premio">Prêmio</a></li>

        <li <?if($this->router->class=='noticias')echo" class='active'"?>><a href="painel/noticias">Notícias</a></li>

        <li class="dropdown <?if($this->router->class=='revistas' || $this->router->class=='publicacoes')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Publicações <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/revistas">Cadastro de Revistas</a></li>
            <li><a href="painel/publicacoes">Publicações</a></li>
          </ul>
        </li>

        <li class="dropdown <?if($this->router->class=='fornecedores')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fornecedores <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/fornecedores">Lista de Fornecedores</a></li>
            <li><a href="painel/fornecedores/estatisticas">Estatísticas</a></li>
            <li><a href="painel/fornecedores/sinonimos">Cadastro de Sinônimos</a></li>
          </ul>
        </li>

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>