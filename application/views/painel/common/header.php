<!DOCTYPE html>
<html lang="en-US" <?if(!$this->session->userdata('logged_in')) echo" class='login'"?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Painel Administrativo - Site Flex</title>

	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2012 True Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<base href="<?= base_url('painel') ?>">
	<script> var BASE = "<?= base_url('painel') ?>"</script>
	<script> var CLASSE = "<?= $this->router->class ?>"</script>

	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700|Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>

    <?CSS(array(
    	'painel/css/global',
    	'uploadify',
    	'jquery-theme-lightness/jquery-ui-1.10.3.custom',
    	'fancybox/fancybox',
    	'painel/jquery.datetimepicker',
    	'painel/embed'
    ))?>

	<?JS(array(
		'modernizr-2.0.6.min',
		'jquery-1.8.0.min',
		'tinymce/tiny_mce',
		'jquery-ui-1.10.3.custom.min',
		'fancybox',
		'jquery.ui.datepicker-pt-BR',
		'jquery.mtz.monthpicker',
		'jquery.datetimepicker',
		'jquery.uploadify.min',
		'placeholder',
		'painel/bootstrap',
		'painel/bootstrap-tooltip',
		'painel/bootbox.min',
		'painel/admin'
	))?>

	<?php if ($this->router->class=='fornecedores' && $this->router->method=='form'): ?>
		<?CSS(array('painel/css/edit-fornecedor'))?>
	<?php endif ?>

</head>
<body>