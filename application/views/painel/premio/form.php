<div class="container top">

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Imagem<br>
			<?if($registro->imagem):?>
				<img src="_imgs/premio/<?=$registro->imagem?>"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<label>Imagem Menor<br>
			<?if($registro->thumb):?>
				<img src="_imgs/premio/<?=$registro->thumb?>"><br>
			<?endif;?>
		<input type="file" name="userfile_thumb"></label>

		<label>Detalhe de Data<br>
		<input type="text" name="detalhe_data" value="<?=$registro->detalhe_data?>"></label>

		<label>Subtítulo<br>
		<input type="text" name="subtitulo" value="<?=$registro->subtitulo?>"></label>

		<label>Texto da Coluna Esquerda<br>
		<textarea name="texto_col_esquerda" class="medio imagem"><?=$registro->texto_col_esquerda?></textarea></label>

		<label>Texto da Coluna Direita<br>
		<textarea name="texto_col_direita" class="medio imagem"><?=$registro->texto_col_direita?></textarea></label>

		<label>Texto da Caixa de Inscrição<br>
		<input type="text" name="texto_caixa_inscricao" value="<?=$registro->texto_caixa_inscricao?>"></label>

		<label>Imagem da Caixa de inscrição<br>
			<?if($registro->thumb_caixa_inscricao):?>
				<img src="_imgs/premio/<?=$registro->thumb_caixa_inscricao?>"><br>
			<?endif;?>
		<input type="file" name="userfile_caixa"></label>

		<label>Regulamento<br>
			<?if($registro->regulamento):?>
				<a href="_pdfs/premio/<?=$registro->regulamento?>" target="_blank"><?=$registro->regulamento?></a><br>
			<?endif;?>
		<input type="file" name="userfile_regulamento"></label>

		<label>Modelo da Prancha de Participação<br>
			<?if($registro->prancha):?>
				<a href="_pdfs/premio/<?=$registro->prancha?>" target="_blank"><?=$registro->prancha?></a><br>
			<?endif;?>
		<input type="file" name="userfile_prancha"></label>

		<label>Texto do Link (aparecerá somente enquanto as inscrições estiverem fechadas)<br>
		<input type="text" name="texto_link" value="<?=$registro->texto_link?>"></label>

		<label>Destino do Link (aparecerá somente enquanto as inscrições estiverem fechadas)<br>
		<input type="text" name="destino_link" value="<?=$registro->destino_link?>"></label>

		<hr>

		Inscrições<br>
		<label><input type="radio" name="inscricoes_abertas" value="1" <?if($registro->inscricoes_abertas==1)echo"checked"?>> Inscrições Abertas </label>
		<label><input type="radio" name="inscricoes_abertas" value="0" <?if($registro->inscricoes_abertas==0)echo"checked"?>> Inscrições Fechadas </label>

		<hr>
		<!--
		Período de votação popular<br>
		<label><input type="radio" name="periodo_votacao_popular" value="1" <?if($registro->periodo_votacao_popular==1)echo"checked"?>> Votações Abertas </label>
		<label><input type="radio" name="periodo_votacao_popular" value="0" <?if($registro->periodo_votacao_popular==0)echo"checked"?>> Votações Fechadas </label>

		<hr>-->

		Período de votação de jurados<br>
		<label><input type="radio" name="periodo_votacao_jurados" value="1" <?if($registro->periodo_votacao_jurados==1)echo"checked"?>> Votações Abertas </label>
		<label><input type="radio" name="periodo_votacao_jurados" value="0" <?if($registro->periodo_votacao_jurados==0)echo"checked"?>> Votações Fechadas </label>

		<hr>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>