<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/noticias/index/">Notícias</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/noticias/form/"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Notícia
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label><br>

		<label>Data
		<input type="text" name="data" class="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>"></label><br>

		Imagem
		<?php if ($registro->imagem): ?>
			<br><img src="_imgs/noticias/<?=$registro->imagem?>"><br>
		<?php endif ?>
		<label><input type="file" name="userfile"></label><br>

		<label>Olho
		<textarea name="olho" class="pequeno basico"><?=$registro->olho?></textarea></label><br>

		<label>Texto
		<textarea name="texto" class="medio video"><?=$registro->texto?></textarea></label><br>

		<label>Autor<br>
		<input type="text" name="autor" value="<?=$registro->autor?>"></label><br>

		<label>Arquivo PDF<br>
		<?php if ($registro->pdf): ?>
			<a href="_pdfs/noticias/<?=$registro->pdf?>" title="Visualizar o PDF" target="_blank"><?=$registro->pdf?></a><br>
		<?php endif ?>
		<input type="file" name="userfile_pdf"></label><br>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Notícia<br>
		<input type="text" name="titulo" required autofocus></label><br>

		<label>Data<br>
		<input type="text" name="data" class="datepicker" required></label><br>

		<label>Imagem<br>
		<input type="file" name="userfile"></label><br>

		<label>Olho<br>
		<textarea name="olho" class="pequeno basico"></textarea></label><br>

		<label>Texto<br>
		<textarea name="texto" class="medio video"></textarea></label><br>

		<label>Autor<br>
		<input type="text" name="autor"></label><br>

		<label>Arquivo PDF<br>
		<input type="file" name="userfile_pdf"></label><br>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>