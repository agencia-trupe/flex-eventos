<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?>
        </h2>
    </div>

    <div class="btn-group" style="margin-bottom:20px">
        <a href="<?=base_url($link.'/0')?>" title="Inscrições sem Avaliação" class="btn btn-large<?if($status==0)echo" btn-info"?>">Inscrições sem Avaliação</a>
        <a href="<?=base_url($link.'/1')?>" title="Inscrições Aprovadas" class="btn btn-large<?if($status==1)echo" btn-info"?>">Inscrições Aprovadas</a>
        <a href="<?=base_url($link.'/2')?>" title="Inscrições Reprovadas" class="btn btn-large<?if($status==2)echo" btn-info"?>">Inscrições Reprovadas</a>
    </div>

    <div class="row">
        <div class="span12 columns">

        <?php if ($registros): ?>

            <table class="table table-striped table-bordered table-condensed table-sortable">

              <thead>
                <tr>
                  <th class="yellow header headerSortDown">Evento</th>
                  <th class="header">Nome</th>
                  <th class="header">Cargo</th>
                  <th class="header">Cidade/Estado</th>
                  <th style="width:135px;">Ações</th>
                </tr>
              </thead>

              <tbody>
                <?php foreach ($registros as $key => $value): ?>

                    <tr class="tr-row">
                      <td><?=$value->titulo_evento?></td>
                      <td><?=$value->nome?></td>
                      <td><?=$value->cargo?></td>
                      <td><?=$value->cidade . '-' . $value->estado?></td>
                      <td class="crud-actions">
                        <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">detalhes</a>
                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                      </td>
                    </tr>

                <?php endforeach ?>
              </tbody>

            </table>

            <?php if ($paginacao): ?>
                <div class="pagination">
                    <ul>
                        <?=$paginacao?>
                    </ul>
                </div>
            <?php endif ?>

        <?php else:?>

      	    <h3>Nenhum Registro</h2>

        <?php endif ?>

      <a href="painel/eventos" class="btn" style="margin-top:15px" type="reset">Voltar</a>

    </div>
  </div>