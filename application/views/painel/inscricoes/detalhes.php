<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/aprovar/index/">Inscrições</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/aprovar/form/"><?=$titulo?></a>
    	</li>
  	</ul>

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<h3>Detalhes da Inscrição:</h3>
	<strong>Evento : </strong> <?=$registro->titulo_evento?></strong> <br>
	<strong>indicado por : </strong><?=$registro->indicado_por?><br>
	<strong>nome : </strong><?=$registro->nome?><br>
	<strong>cpf : </strong><?=$registro->cpf?><br>
	<strong>email : </strong><?=$registro->email?><br>
	<strong>fone : </strong><?=$registro->ddd_fone?> - <?=$registro->fone?><br>
	<strong>celular :  </strong><?=$registro->ddd_celular?> - <?=$registro->celular?><br>
	<strong>cargo : </strong><?=$registro->cargo?><br>
	<strong>escritório : </strong><?=$registro->escritorio?><br>
	<strong>cnpj : </strong><?=$registro->cnpj?><br>
	<strong>endereço : </strong><?=$registro->endereco?><br>
	<strong>bairro : </strong><?=$registro->bairro?><br>
	<strong>cep : </strong><?=$registro->cep?><br>
	<strong>cidade/estado : </strong><?=$registro->cidade?> <?=$registro->estado?><br>
    <strong>data de inscrição : </strong> <?=formataTimestamp($registro->data_inscricao)?> <br>

<?php if ($registro->inscricao_aprovada == 0): ?>

    <hr>

    <a href="<?=base_url('painel/'.$this->router->class.'/aprovarInsc/'.$registro->id)?>" title="Aprovar Inscrição" class="btn btn-success btn-large"><i class="icon-white icon-thumbs-up"></i> Aprovar Inscrição</a>

    <form id="form-reprovar" action="<?=base_url('painel/'.$this->router->class.'/reprovarInsc/'.$registro->id)?>" style="margin:15px 5px; padding:5px;border:1px #e5e5e5 solid;" method="post">
        <label id="motivo-reprovacao">
            <br>
            Para reprovar uma inscrição é necessário informar o motivo da reprovação<br>(Essa informação será enviada para o inscrito via e-mail):<br>
            <textarea name="texto_reprovacao" required></textarea>
        </label>

        <a href="#" id="botao-reprovar-inscricao" title="Reprovar Inscrição" class="btn btn-danger btn-large"><i class="icon-white icon-thumbs-down"></i> Reprovar Inscrição</a>
    </form>

    <hr>

<?php endif ?>

	<div class="form-actions">
    	<button class="btn btn-info btn-voltar" type="reset">Voltar</button>
  	</div>

<?endif ?>