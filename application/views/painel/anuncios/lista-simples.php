<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?> 
            <div class="btn-group" style="float:right">
                <a href="painel/anuncios/form/<?=$tipo?>" class="btn btn-success">Adicionar <?=$unidade?></a>
                <?php if($tipo != 'parceiros'): ?>
                    <a href="painel/anuncios/calhau/<?=$tipo?>" class="btn btn-success">Alterar Calhau</a>
                <?php endif; ?>
            </div>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        <div class="alert alert-info dont-remove alert-block">
            Somente <strong>um anúncio</strong> pode estar ativo.<br>
            Os anúncios expirados podem ser consultados na aba <strong>Histórico</strong>.
        </div>

        <div class="lista lista-top">

            <div class="btn-group" style="width:210px; margin:0 auto 20px auto;">
                <a href="#" class="btn btn-warning select-table-ativos" title="Anúncios Ativos">Anúncios Ativos</a>
                <a href="#" class="btn select-table-historico" title="Histórico">Histórico</a>
            </div>
            
            <div class="lista-ativos">
                <?php if (sizeof($registros) > 0 && is_array($registros)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <th rowspan="2">Ativar</th>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($registros as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td style="width:83px">
                                        <?php if($value->ativo == 1): ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar" checked> <span>Ativo</span></label>
                                        <?php else: ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar"> <span>Desativado</span></label>
                                        <?php endif; ?>                                    
                                    </td>
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum anúncio encontrado</h3>

                <?php endif ?>
            </div>

            <div class="lista-historico" style="display:none;">
                <?php if (sizeof($historico) > 0 && is_array($historico)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <tr>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($historico as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum histórico encontrado</h3>

                <?php endif ?>            
            </div>

        </div>

    </div>
  </div>