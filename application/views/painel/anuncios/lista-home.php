<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2 class="nofloat">
            <?=$titulo?> 
            
            <ul class="btn-group" style="float:right;"> 
                <li class="dropdown" style="display:inline-block; position:relative;">
                    <a href="#" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Adicionar <?=$unidade?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="painel/anuncios/form/destaque" title="Inserir banner Destaque">Destaque</a></li>
                        <li><a href="painel/anuncios/form/halfbanner" title="Inserir banner Half Banner">Half Banner</a></li>
                        <li><a href="painel/anuncios/form/button" title="Inserir banner Button">Button</a></li>
                        <li><a href="painel/anuncios/form/publiespecial" title="Inserir banner Publi Especial">Publi Especial</a></li>
                    </ul>
                </li>
                <li class="dropdown" style="display:inline-block; position:relative;">
                    <a href="#" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Alterar Calhau <span class="caret"></span></a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="painel/anuncios/calhau/destaque" title="Alterar Calhau do banner Destaque">Destaque</a></li>
                        <li><a href="painel/anuncios/calhau/halfbanner" title="Alterar Calhau 1 do banner Half Banner">Half Banner 1</a></li>
                        <li><a href="painel/anuncios/calhau/halfbanner2" title="Alterar Calhau 2 do banner Half Banner">Half Banner 2</a></li>
                        <li><a href="painel/anuncios/calhau/button" title="Alterar Calhau do banner Button">Button</a></li>
                    </ul>
                </li>
            </ul>
            
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

        <div class="alert alert-info dont-remove alert-block">
            É possível exibir os anúncios da seguinte maneira:<br><br>
            Destaques e Half Banners:<br>
            <ul>
                <li><span id="span-exemplo-2destaques">2 Destaques</span></li>
                <li>OU</li>
                <li><span id="span-exemplo-1destaque-1halfbanner">1 Destaque + 2 Half Banners</span></li>
            </ul><br>
            Button e Publi Especial:<br>
            <ul>
                <li><span id="span-exemplo-1button">1 Button</span></li>
                <li>OU</li>
                <li><span id="span-exemplo-1publiespecial">1 Publi Especial</span></li>
            </ul><br>
            Os anúncios expirados podem ser consultados na aba <strong>Histórico</strong>.
        </div>

        <hr>

        <h3>Anúncios Destaque</h3>

        <div class="lista lista-destaque">

            <div class="btn-group" style="width:210px; margin:0 auto 20px auto;">
                <a href="#" class="btn btn-warning select-table-ativos" title="Anúncios Ativos">Anúncios Ativos</a>
                <a href="#" class="btn select-table-historico" title="Histórico">Histórico</a>
            </div>
            
            <div class="lista-ativos">
                <?php if (sizeof($destaques) > 0 && is_array($destaques)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <th rowspan="2">Ativar</th>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($destaques as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td style="width:83px">
                                        <?php if($value->ativo == 1): ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar" checked> <span>Ativo</span></label>
                                        <?php else: ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar"> <span>Desativado</span></label>
                                        <?php endif; ?>                                    
                                    </td>
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum anúncio encontrado</h3>

                <?php endif ?>
            </div>

            <div class="lista-historico" style="display:none;">
                <?php if (sizeof($destaques_historico) > 0 && is_array($destaques_historico)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <tr>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>                            
                        </thead>

                        <tbody>
                            <?php foreach ($destaques_historico as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum histórico encontrado</h3>

                <?php endif ?>            
            </div>

        </div>
        
        <hr>

        <h3>Anúncios Half Banner</h3>

        <div class="lista lista-halfbanner">

            <div class="btn-group" style="width:210px; margin:0 auto 20px auto;">
                <a href="#" class="btn btn-warning select-table-ativos" title="Anúncios Ativos">Anúncios Ativos</a>
                <a href="#" class="btn select-table-historico" title="Histórico">Histórico</a>
            </div>
            
            <div class="lista-ativos">
                <?php if (sizeof($halfbanner) > 0 && is_array($halfbanner)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <th rowspan="2">Ativar</th>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($halfbanner as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td style="width:83px">
                                        <?php if($value->ativo == 1): ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar" checked> <span>Ativo</span></label>
                                        <?php else: ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar"> <span>Desativado</span></label>
                                        <?php endif; ?>                                    
                                    </td>
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum anúncio encontrado</h3>

                <?php endif ?>
            </div>

            <div class="lista-historico" style="display:none;">
                <?php if (sizeof($halfbanner_historico) > 0 && is_array($halfbanner_historico)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <tr>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($halfbanner_historico as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum histórico encontrado</h3>

                <?php endif ?>            
            </div>

        </div>
        
        <hr>  

        <h3>Anúncios Button</h3>

        <div class="lista lista-button">

            <div class="btn-group" style="width:210px; margin:0 auto 20px auto;">
                <a href="#" class="btn btn-warning select-table-ativos" title="Anúncios Ativos">Anúncios Ativos</a>
                <a href="#" class="btn select-table-historico" title="Histórico">Histórico</a>
            </div>
            
            <div class="lista-ativos">
                <?php if (sizeof($button) > 0 && is_array($button)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <th rowspan="2">Ativar</th>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($button as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td style="width:83px">
                                        <?php if($value->ativo == 1): ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar" checked> <span>Ativo</span></label>
                                        <?php else: ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar"> <span>Desativado</span></label>
                                        <?php endif; ?>                                    
                                    </td>
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum anúncio encontrado</h3>

                <?php endif ?>
            </div>

            <div class="lista-historico" style="display:none;">
                <?php if (sizeof($button_historico) > 0 && is_array($button_historico)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <tr>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($button_historico as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum histórico encontrado</h3>

                <?php endif ?>            
            </div>

        </div>

        <hr>  

        <h3>Anúncios Publi Especial</h3>

        <div class="lista lista-publiespecial">

            <div class="btn-group" style="width:210px; margin:0 auto 20px auto;">
                <a href="#" class="btn btn-warning select-table-ativos" title="Anúncios Ativos">Anúncios Ativos</a>
                <a href="#" class="btn select-table-historico" title="Histórico">Histórico</a>
            </div>
            
            <div class="lista-ativos">
                <?php if (sizeof($publiespecial) > 0 && is_array($publiespecial)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <th rowspan="2">Ativar</th>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($publiespecial as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td style="width:83px">
                                        <?php if($value->ativo == 1): ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar" checked> <span>Ativo</span></label>
                                        <?php else: ?>
                                            <label><input type="checkbox" value="<?=$value->id?>" data-ativar="<?=$value->id?>" data-tipo="<?=$value->tipo?>" class="chkAtivar"> <span>Desativado</span></label>
                                        <?php endif; ?>                                    
                                    </td>
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum anúncio encontrado</h3>

                <?php endif ?>
            </div>

            <div class="lista-historico" style="display:none;">
                <?php if (sizeof($publiespecial_historico) > 0 && is_array($publiespecial_historico)): ?>

                    <table class="table table-striped table-bordered table-condensed table-sortable">

                        <thead>
                            <tr>
                                <tr>
                                <th rowspan="2">Título</th>
                                <th rowspan="2">Entrada/Saída</th>
                                <th colspan="3" style="text-align:center; border-bottom: 1px solid #ddd;">Estatísticas</th>
                                <th style="text-align:center;" rowspan="2">Ações</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;border-left: 1px solid #ddd;">Clicks</th>
                                <th style="text-align:center;">Impressões</th>
                                <th style="text-align:center;">CTR <i class="icon-question-sign ctr-hover" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></i></th>                                
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($publiespecial_historico as $key => $value): ?>

                                <tr class="tr-row" id="row_<?=$value->id?>">
                                    <td><?=$value->titulo?></td>
                                    <td>
                                        <span style="width:60px;display:inline-block;">Entrada</span>: <?= formataDT($value->data_entrada) ?><br>
                                        <span style="width:60px;display:inline-block;">Saída</span>: <?= formataDT($value->data_saida) ?>
                                    </td>
                                    <td style="text-align:center;"><?=$value->estatisticas['click']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['impressao']?></td>
                                    <td style="text-align:center;"><?=$value->estatisticas['ctr']?></td>
                                    <td class="crud-actions">
                                        <a href="painel/<?=$this->router->class?>/form/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                                        <a href="painel/<?=$this->router->class?>/excluir/<?=$value->tipo?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                                    </td>
                                </tr>

                            <?php endforeach ?>
                        </tbody>

                    </table>        

                <?php else:?>

                    <h3>Nenhum histórico encontrado</h3>

                <?php endif ?>            
            </div>

        </div>        

    </div>
  </div>