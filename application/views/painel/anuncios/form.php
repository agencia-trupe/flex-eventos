<div class="container top">

  	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

	<?if ($registro): ?>

		<form method="post" action="<?=base_url('painel/anuncios/alterar/'.$registro->tipo.'/'.$registro->id)?>" enctype="multipart/form-data">

			<div id="dialog"></div>

			<label>Titulo<br>
			<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label><br>
			
			<input type="hidden" name="tipo" value="<?=$registro->tipo?>">

			Tipo de anúncio:
			<label><input type="radio" name="switch-tipo-ad" value="img" <?php if($registro->imagem != '') echo "checked"?>> Anúncio Simples (arquivo de imagem)</label>
			<label><input type="radio" name="switch-tipo-ad" value="swf" <?php if($registro->imagem == '') echo "checked"?>> Anúncio em Flash (arquivo swf)</label><br>
			
			<div id="tipo-ad-img"<?php if($registro->imagem != '') echo "class='aberto'"?>>
				<label>
					Imagem do Banner ( jpg,png,gif - <?=$dimensoes?>)<br>
					<?php if($registro->imagem): ?>
						<img src="_anuncios/imagem/<?=$registro->tipo?>/<?=$registro->imagem?>"><br>
					<?php endif;?>
					<input type="file" name="userfile_img">
				</label><br>

				<label>
					Destino do Click<br>
					<input type="text" name="link" value="<?=$registro->link?>">
				</label><br>

				Janela destino<br>
				<label><input type="radio" name="tipo_link" value="_blank" <?php if($registro->tipo_link=='_blank') echo "checked" ?>> Abrir em um nova janela/aba</label>
				<label><input type="radio" name="tipo_link" value="_self" <?php if($registro->tipo_link=='_self') echo "checked" ?>> Abrir na mesma janela/aba</label><br>
			</div>

			<div id="tipo-ad-swf"<?php if($registro->imagem == '') echo "class='aberto'"?>>				
				<label>
					Arquivo do Banner ( swf - <?=$dimensoes?>)<br>
					<?php if($registro->swf): ?>
						<?=embedAnuncio($registro->tipo, $registro->swf)?><br>
					<?php endif;?>
					<input type="file" name="userfile_swf">
				</label><br>
			</div>

			<label>
				Data de início de exibição<br>
				<input type="text" name="data_entrada" class="datetimepicker input-medium" value="<?=formataDT($registro->data_entrada)?>">
			</label><br>

			<label>
				Data de término de exibição<br>
				<input type="text" name="data_saida" class="datetimepicker input-medium" value="<?=formataDT($registro->data_saida)?>">
			</label><br>

			<div class="form-actions">
        		<button class="btn btn-primary" type="submit">Salvar</button>
        		<button class="btn btn-voltar" type="reset">Voltar</button>
      		</div>
		</form>

	<?else: ?>

		<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/'.$tipo)?>" enctype="multipart/form-data">

			<div id="dialog"></div>

			<label>Titulo<br>
			<input type="text" name="titulo" required></label><br>
			
			<input type="hidden" name="tipo" value="<?=$tipo?>">

			Tipo de anúncio:
			<label><input type="radio" name="switch-tipo-ad" value="img"> Anúncio Simples (arquivo de imagem)</label>
			<label><input type="radio" name="switch-tipo-ad" value="swf"> Anúncio em Flash (arquivo swf)</label><br>
			
			<div id="tipo-ad-img">
				<label>
					Imagem do Banner ( jpg,png,gif - <?=$dimensoes?>)<br>
					<input type="file" name="userfile_img">
				</label><br>

				<label>
					Destino do Click<br>
					<input type="text" name="link">
				</label><br>

				Janela destino<br>
				<label><input type="radio" name="tipo_link" value="_blank" checked> Abrir em um nova janela/aba</label>
				<label><input type="radio" name="tipo_link" value="_self"> Abrir na mesma janela/aba</label><br>

			</div>

			<div id="tipo-ad-swf">				
				<label>
					Arquivo do Banner ( swf - <?=$dimensoes?>)<br>
					<input type="file" name="userfile_swf">
				</label><br>
			</div>

			<label>
				Data de início de exibição<br>
				<input type="text" name="data_entrada" class="datetimepicker input-medium">
			</label><br>

			<label>
				Data de término de exibição<br>
				<input type="text" name="data_saida" class="datetimepicker input-medium">
			</label><br>

			<div class="form-actions">
        		<button class="btn btn-primary" type="submit">Inserir</button>
        		<button class="btn btn-voltar" type="reset">Voltar</button>
      		</div>
		</form>

	<?endif ?>