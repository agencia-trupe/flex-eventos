<div class="container top">

  	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
	    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

	<?if ($registro): ?>

		<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

			<div id="dialog"></div>
			
			Destaque 1<br>
			<div class="monta-destaque">
				<label><input type="radio" name="tipo_destaque_1" class="rdDestaque" value="evento" <?php if($registro->id_eventos_1 != 0) echo"checked" ?>> Chamada para Evento</label>
				<label><input type="radio" name="tipo_destaque_1" class="rdDestaque" value="link" <?php if($registro->id_eventos_1 == 0 && $registro->chamada_1_link != '') echo"checked" ?>> Link Simples</label>
				<label><input type="radio" name="tipo_destaque_1" class="rdDestaque" value="nda" <?php if($registro->id_eventos_1 == 0 && $registro->chamada_1_link == '') echo"checked" ?>> Desativado</label>
				
				<label class="sel-evento" <?php if($registro->id_eventos_1 == 0) echo"style='display:none;'" ?>>
					<select name="id_eventos_1">
						<option value="">Selecione um evento</option>
						<?php foreach ($eventos as $key => $value): ?>
							<option value="<?=$value->id?>" <?if($registro->id_eventos_1==$value->id)echo" selected"?>><?=$value->titulo?></option>
						<?php endforeach ?>
					</select>
				</label>

				<div class="sel-link" <?php if($registro->chamada_1_link == '') echo"style='display:none;'" ?>>
					<label>
						Título<br>
						<input type="text" name="chamada_1_titulo" value="<?=$registro->chamada_1_titulo?>">
					</label>
					<label>
						Subtítulo<br>
						<input type="text" name="chamada_1_subtitulo" value="<?=$registro->chamada_1_subtitulo?>">
					</label>
					<label>
						Link<br>
						<input type="text" name="chamada_1_link" value="<?=$registro->chamada_1_link?>">
					</label>
				</div>
			</div>
			
			<hr>

			Destaque 2<br>
			<div class="monta-destaque">
				<label><input type="radio" name="tipo_destaque_2" class="rdDestaque" value="evento" <?php if($registro->id_eventos_2 != 0) echo"checked" ?>> Chamada para Evento</label>
				<label><input type="radio" name="tipo_destaque_2" class="rdDestaque" value="link" <?php if($registro->id_eventos_2 == 0 && $registro->chamada_2_link != '') echo"checked" ?>> Link Simples</label>
				<label><input type="radio" name="tipo_destaque_2" class="rdDestaque" value="nda" <?php if($registro->id_eventos_2 == 0 && $registro->chamada_2_link == '') echo"checked" ?>> Desativado</label>

				<label class="sel-evento" <?php if($registro->id_eventos_2 == 0) echo"style='display:none;'" ?>>
					<select name="id_eventos_2">
						<option value="">Selecione um evento</option>
						<?php foreach ($eventos as $key => $value): ?>
							<option value="<?=$value->id?>" <?if($registro->id_eventos_2==$value->id)echo" selected"?>><?=$value->titulo?></option>
						<?php endforeach ?>
					</select>
				</label>

				<div class="sel-link" <?php if($registro->chamada_2_link == '') echo"style='display:none;'" ?>>
					<label>
						Título<br>
						<input type="text" name="chamada_2_titulo" value="<?=$registro->chamada_2_titulo?>">
					</label>
					<label>
						Subtítulo<br>
						<input type="text" name="chamada_2_subtitulo" value="<?=$registro->chamada_2_subtitulo?>">
					</label>
					<label>
						Link<br>
						<input type="text" name="chamada_2_link" value="<?=$registro->chamada_2_link?>">
					</label>
				</div>

			</div>

			<div class="form-actions">
	        	<button class="btn btn-primary" type="submit">Salvar</button>
	        	<button class="btn btn-voltar" type="reset">Voltar</button>
	      	</div>
		</form>

	<?endif ?>