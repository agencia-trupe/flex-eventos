<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

                <table class="table table-striped table-bordered table-condensed table-sortable">

                    <thead>
                        <tr>
                            <th class="yellow header headerSortDown">Destaques</th>
                            <th class="red header">Ações</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($registros as $key => $value): ?>

                            <tr class="tr-row" id="row_<?=$value->id?>">
                                <td>
                                    <?=$value->titulo_1?><br>
                                    <?=$value->titulo_2?><br>                                    
                                </td>
                                <td class="crud-actions" style="width:55px;">
                                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                                </td>
                            </tr>

                        <?php endforeach ?>
                    </tbody>

                </table>

            <?php else:?>
                <h3>Nenhum Registro</h2>
            <?php endif ?>

        </div>
    </div>