<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form/" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>

  <div class="row">
    <div class="span12 columns">

      <div class="btn-group">
        <a href="painel/eventos/index/1" title="Ver próximos Eventos" class="btn<?php if($proximos==1)echo" btn-warning"?>">Ver próximos Eventos</a>
        <a href="painel/eventos/index/0" title="Ver Eventos passsados" class="btn<?php if($proximos==0)echo" btn-warning"?>">Ver Eventos passsados</a>
      </div><br><br>

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <!-- <th>Ordenar</th> -->
              <th class="yellow header headerSortDown">Título</th>
              <th class="header">Data</th>
              <th class="header">Texto</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                  <!-- <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td> -->
                  <td class="nowrap"><?=$value->titulo?></td>
                  <td><?=formataData($value->data, 'mysql2br')?></td>
                  <td><?=word_limiter(strip_tags($value->texto), 20)?></td>
                  <td class="crud-actions" style="width:215px;">
                    <a href="painel/inscricoes/porEvento/<?=$value->id?>" class="btn">inscrições</a>
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                    <?php if($proximos==0): ?>
                      <a href="painel/<?=$this->router->class?>/imagens/<?=$value->id?>" class="btn" style="display:block; margin:5px auto;">imagens do evento</a>
                    <?php endif;?>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>
          <div class="pagination">
            <ul>
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

        <h2>Nenhum Evento encontrado</h2>

      <?php endif ?>



    </div>
  </div>