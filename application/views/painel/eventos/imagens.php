<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?if(!isset($registro->id)):?><a href="javascript: $('#form-ins').slideToggle('normal'); return false;" class="btn btn-success">Adicionar Imagem</a><?endif;?> <div style="max-width:700px;"><?=$titulo?></div>
    </h2>
  </div>


<?if(isset($registro->id)):?>

    <a href="painel/eventos/imagens/<?=$id_evento?>" class="btn">← voltar</a>

    <br><br>

    <form method="post" id="form-alt" action="<?=base_url('painel/'.$this->router->class.'/editarImagem/'.$registro->id)?>" enctype="multipart/form-data">

        <h3>Alterar Imagem</h3><br>

        <label>Imagem<br>
            <a target="_blank" href="_imgs/eventos/pos/<?=$registro->imagem?>" title="Clique para ver a Imagem em Tamanho Original"><img src="_imgs/eventos/pos/thumbs/<?=$registro->imagem?>"></a><br>
            <i>Clique para ver a Imagem em Tamanho Original em uma nova janela</i><br>
            <input type="file" name="userfile"><br>
        </label>

        <label>
            Legenda<br>
            <textarea name="legenda" style="height:90px;"><?=$registro->legenda?></textarea>
        </label>

        <input type="hidden" name="id_evento" value="<?=$id_evento?>">

        <div class="form-actions">
            <button class="btn btn-primary" type="submit">Salvar</button>
        </div>

    </form>

<?else:?>

    <a href="painel/eventos/index/0" class="btn">← voltar</a>

    <br><br>

    <form method="post" id="form-ins" action="<?=base_url('painel/'.$this->router->class.'/inserirImagem')?>" enctype="multipart/form-data">

        <h3>Inserir Imagem</h3>

        <label>Imagem<br>
        <input type="file" name="userfile" required></label>

        <label>
            Legenda<br>
            <textarea name="legenda" style="height:90px;"></textarea>
        </label>

        <input type="hidden" name="id_evento" value="<?=$id_evento?>">

        <div class="form-actions">
            <button class="btn btn-primary" type="submit">Salvar</button>
        </div>

    </form>

<?endif;?>

<br><br>

<div class="row">
    <div class="span12 columns">

    <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="trupe_eventos_imagens">

          <thead>
            <tr>
                <th>Ordenar</th>
                <th class="yellow header headerSortDown">Imagem</th>
                <th class="header">Legenda</th>
                <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                    <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                    <td><img src="_imgs/eventos/pos/thumbs/<?=$value->imagem?>"></td>
                    <td><?=$value->legenda?></td>
                    <td class="crud-actions" style="width:118px; text-align:center;">
                        <a href="painel/<?=$this->router->class?>/imagens/<?=$id_evento.'/'.$value->id?>" class="btn btn-primary">editar</a>
                        <a href="painel/<?=$this->router->class?>/excluirImagem/<?=$value->id.'/'.$id_evento?>" class="btn btn-danger btn-delete">excluir</a>
                    </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

    <?php else:?>

        <h2>Nenhuma Imagem para o Evento</h2>

    <?php endif ?>
    </div>
</div>