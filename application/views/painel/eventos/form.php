<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/eventos/index/">Eventos</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/eventos/form/"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Evento
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Subtítulo do Evento
		<input type="text" name="subtitulo" value="<?=$registro->subtitulo?>"></label>

		<label>Data do Evento<br>
		<input type="text" name="data" class="datepicker" value="<?=$registro->data?>"></label>

		<label>Detalhe de Data<br>
		<input type="text" name="detalhe_data" value="<?=$registro->detalhe_data?>"></label>

		<label>Estado/Cidade
		<input type="text" name="local" value="<?=$registro->local?>"></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno olho"><?=$registro->olho?></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="medio imagem"><?=$registro->texto?></textarea></label><br>

		Imagem de Destaque
		<?php if ($registro->imagem): ?>
			<br><img src="_imgs/eventos/<?=$registro->imagem?>"><br>
		<?php endif ?>
		<label><input type="file" name="userfile"></label>

		Imagem Menor (Banners da home e Listagem de Eventos)
		<?php if ($registro->thumb): ?>
			<br><img src="_imgs/eventos/thumbs/<?=$registro->thumb?>"><br>
		<?php endif ?>
		<label><input type="file" name="userfile_thumb"></label>

		<hr>

		<label><input type="radio" name="inscricoes_abertas" value="1" <?if($registro->inscricoes_abertas==1)echo"checked"?>> Inscrições Abertas </label>
		<label><input type="radio" name="inscricoes_abertas" value="0" <?if($registro->inscricoes_abertas==0)echo"checked"?>> Inscrições Fechadas </label>

		<hr>

		<label>Texto da caixa de Inscrição<br>(Só aparecerá se as inscrições estiverem abertas)<br>
		<textarea name="texto_inscricao" class="medio inscricoes"><?=$registro->texto_inscricao?></textarea></label><br>

		<label>Término das inscrições<br>
		<input type="text" name="termino_inscricoes" class="datepicker" value="<?=formataData($registro->data_termino, 'mysql2br')?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Evento
		<input type="text" name="titulo" required autofocus></label>

		<label>Subtítulo do Evento
		<input type="text" name="subtitulo"></label>

		<label>Data do Evento<br>
		<input type="text" name="data" class="datepicker"></label>

		<label>Detalhe de Data<br>
		<input type="text" name="detalhe_data"></label>

		<label>Estado/Cidade
		<input type="text" name="local"></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno olho"></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="medio imagem"></textarea></label><br>

		<label>Imagem de Destaque
		<input type="file" name="userfile" required></label><br>

		<label>Imagem Menor (Banners da home e Listagem de Eventos)
		<input type="file" name="userfile_thumb" required></label><br>

		<hr>

		<label><input type="radio" name="inscricoes_abertas" value="1"> Inscrições Abertas </label>
		<label><input type="radio" name="inscricoes_abertas" value="0" checked> Inscrições Fechadas </label>

		<hr>

		<label>Texto da caixa de Inscrição<br>(Só aparecerá se as inscrições estiverem abertas)<br>
		<textarea name="texto_inscricao" class="medio inscricoes"></textarea></label><br>

		<label>Término das inscrições<br>
		<input type="text" name="termino_inscricoes" class="datepicker"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>