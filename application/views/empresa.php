<h1>A EMPRESA</h1>

<h2>O caminho mais rápido e eficiente para o sucesso de seus negócios.</h2>

<h3>A Flex</h3>
<p>
	Desde sua fundação, há 25 anos, a Flex se destaca por detectar as necessidades do mercado e antecipar tendências, criando e abrindo caminhos para a arquitetura corporativa. A Flex tem a preocupação de levar marcas, produtos e serviços aos arquitetos, compradores e clientes finais, dos segmentos corporativo e público, que formam opinião. Com base em informações, pesquisas e observações no mercado mundial, seu conhecimento gera interatividade entre investidores, fornecedores, arquitetos e construtoras, fazendo uso da convergência das mídias em que atua: mídia impressa, eventos e o meio digital através do Portal Flex.
</p>

<h3>Publicações</h3>
<p>
	» Office Style<br>
	» Arquishow<br>
	» FacilityHospitalar<br>
	» Anuário Corporativo
</p>

<h3>Eventos Corporativos</h3>
<p>
	» Simpósios Corporativos de Negócios em Arquitetura<br>
	» Grande Prêmio de Arquitetura<br>
	» Encontros Style
</p>

<h3>Feiras</h3>
<p>
	» Office Solution Arquishow | FacilityShow<br>
	» Arquiday Show – realizada nas principais capitais brasileiras: Rio Arquiday Show, Minas<br>
	Arquiday Show, Recife Arquiday Show, entre outras.
</p>

<h3>Portal Flex</h3>
<p>
	O Portal Flex de Negócios oferece conteúdo dirigido aos profissionais e empresas que atuam em Engenharia, Arquitetura e Design. No conteúdo, recursos que aproximam fornecedores de seus clientes de forma dinâmica e efetiva.
</p>