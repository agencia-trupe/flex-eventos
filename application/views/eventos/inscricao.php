<section class="texto-evento">

	<div class="form-inscricao">
		<h1>
			<img src="_imgs/eventos/thumbs/<?=$evento->thumb?>" alt="<?=$evento->titulo?>">
			<span style="float:right;">FICHA DE INSCRIÇÃO</span>
		</h1>

		<div class="aviso-form">
			<h2><?=$evento->titulo?></h2>
			<?=$evento->detalhe_data?>
		</div>

		<?php if ($this->session->flashdata('inscricao_submetida')): ?>

			<div class="aviso-form noborder-t">
				<h3>Obrigado.</h3>
				Sua inscrição foi enviada com sucesso.<br>
				<br>
				Você receberá um email com uma cópia dos dados inscritos para acompanhamento.<br>
				<?php if($evento->id != '3'): ?>
					A equipe da Flex Eventos enviará confirmação ou deferimento da inscrição de acordo com as regras estabelecidas para seus eventos.<br>
					<br>
					Aguarde.
				<?php endif; ?>
			</div>

		<?php else: ?>

			<form action="eventos/inscrever" method="post" id="form-inscricao-evento" enctype="multipart/form-data">
				<label>
					nome completo <input type="text" name="nome"  value="<?= $this->session->flashdata('inscricao_nome'); ?>" required>
				</label>
				<label>
					indicado por <input type="text" name="indicado_por" value="<?= $this->session->flashdata('inscricao_indicado_por'); ?>">
				</label>
				<label class="label w75">
					CPF <input type="text" name="cpf" class="w178" style="margin-right:111px" value="<?= $this->session->flashdata('inscricao_cpf'); ?>" required>
				</label>
				<label>
					e-mail <input type="email" name="email" value="<?= $this->session->flashdata('inscricao_email'); ?>" required>
				</label>
				<div class="label metade w349">
					telefone <input type="text" name="ddd_fone" required class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_ddd_fone'); ?>"><input type="text" name="fone" required class="w178" value="<?= $this->session->flashdata('inscricao_fone'); ?>">
				</div>
				<div class="label metade">
					celular <input type="text" name="ddd_celular" class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_ddd_celular'); ?>"><input type="text" name="celular" class="w178" value="<?= $this->session->flashdata('inscricao_celular'); ?>">
				</div>
				<label>
					cargo <input type="text" name="cargo" value="<?= $this->session->flashdata('inscricao_cargo'); ?>">
				</label>
				<label>
					escritorio | empresa <input type="text" name="escritorio" value="<?= $this->session->flashdata('inscricao_escritorio'); ?>">
				</label>
				<label class="label w75">
					CNPJ (se houver) <input type="text" name="cnpj" class="w178" style="margin-right:111px" value="<?= $this->session->flashdata('inscricao_cnpj'); ?>">
				</label>
				<label>
					endereço <input type="text" name="endereco" required value="<?= $this->session->flashdata('inscricao_endereco'); ?>">
				</label>
				<label class="w75">
					bairro <input type="text" name="bairro" required value="<?= $this->session->flashdata('inscricao_bairro'); ?>">
				</label>
				<label class="w25">
					CEP <input type="text" name="cep" required value="<?= $this->session->flashdata('inscricao_cep'); ?>">
				</label>
				<label class="w75">
					cidade <input type="text" name="cidade" required value="<?= $this->session->flashdata('inscricao_cidade'); ?>">
				</label>
				<label class="w25">
					Estado [UF] <input type="text" name="estado" required class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_estado'); ?>">
				</label>

				<input type="hidden" name="id_evento" value="<?=$evento->id?>">
				
				<?php if($evento->id != '3'): ?>
					<p class="atencao">
						<span class="vermelho">Atenção:</span> A Participação de fornecedores é limitada exclusivamente a patrocinadores do evento.<br>
						A inscrição requer aprovação da Flex Eventos.
					</p>
				<?php endif; ?>

				<input type="submit" value="ENVIAR MINHA INSCRIÇÃO">

			</form>

			<?php if ($this->session->flashdata('mensagem_erro')): ?>
				<script defer>
				$('document').ready( function(){
					alert("<?=$this->session->flashdata('mensagem_erro')?>");
				});
				</script>
			<?php endif ?>

		<?php endif ?>
	</div>

</section>

<aside>

	<?php if($anuncio_eventos): ?>
		<?php echo $anuncio_eventos ?>
	<?php endif; ?>
	
	<div class="agenda-interna amarelo">
		<?php if ($agenda): ?>

			<h2>AGENDA DE EVENTOS</h2>

			<div class="lista">
				<?php foreach ($agenda as $key => $value): ?>
					<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
						<div class="data"><?=$value->detalhe_data?></div>
						<div class="local"><?=$value->local?></div>
						<div class="titulo"><?=$value->titulo?></div>
						<div class="linha"></div>
					</a>
				<?php endforeach ?>
			</div>
		<?php endif ?>

		<a href="eventos" class="link-voltar" title="ver todos os eventos">&laquo; ver todos os eventos</a>
	</div>

</aside>