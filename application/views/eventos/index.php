<?php if ($proximos_eventos): ?>

	<h1>PRÓXIMOS EVENTOS</h1>

	<ul class="lista-proximos-eventos">
		<?php foreach ($proximos_eventos as $key => $value): ?>
			<li>
				<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
					<img src="_imgs/eventos/thumbs/<?=$value->thumb?>" alt="<?=$value->titulo?>">
					<?=$value->titulo?><br>
					<?=$value->detalhe_data?><br>
					<?=$value->local?>
				</a>
			</li>
		<?php endforeach ?>
	</ul>

<?php endif ?>


<?php if ($ultimos_eventos): ?>

	<h1>ÚLTIMOS EVENTOS REALIZADOS</h1>

	<ul class="lista-ultimos-eventos">
		<?php foreach ($ultimos_eventos as $key => $value): ?>
			<li>
				<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
					<img src="_imgs/eventos/thumbs/<?=$value->thumb?>" alt="<?=$value->titulo?>">
					<?=$value->titulo?><br>
					<?=$value->detalhe_data?><br>
					<?=$value->local?>
				</a>
			</li>
		<?php endforeach ?>
	</ul>

<?php endif ?>