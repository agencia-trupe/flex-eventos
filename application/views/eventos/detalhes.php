<section class="texto-evento">

	<img src="_imgs/eventos/<?=$detalhes->imagem?>" alt="<?=$detalhes->titulo?>" class="banner">

	<div class="full">

		<?php if ($detalhes->titulo): ?>
			<h1><?=$detalhes->titulo?></h1>
		<?php endif ?>

		<?php if ($detalhes->detalhe_data): ?>
			<h2><?=$detalhes->detalhe_data?></h2>
		<?php endif ?>

		<?php if ($detalhes->subtitulo): ?>
			<h2><?=$detalhes->subtitulo?></h2>
		<?php endif ?>

		<?php if ($detalhes->local): ?>
			<h2><?=$detalhes->local?></h2>
		<?php endif ?>


		<?php if ($detalhes->olho): ?>
			<p class="olho">
				<?=strip_tags($detalhes->olho)?>
			</p>
		<?php endif ?>

		<?=$detalhes->texto?>

	<?php $inscricoes_abertas = $detalhes->inscricoes_abertas ?>
	<?php $data_setada = isset($detalhes->data_termino) && $detalhes->data_termino != ''?>
	<?php $data_valida = ($data_setada) ? strtotime(Date('Y-m-d')) <= strtotime($detalhes->data_termino) : false;?>


	<?php if ($inscricoes_abertas): ?>

		<?php if($data_setada): ?>

			<?php if ($data_valida): ?>

				<div class="fundo-cinza">
					<?=$detalhes->texto_inscricao?>
					<a href="eventos/inscricao/<?=$detalhes->id?>" class="vermelho">INSCREVA-SE AQUI!</a>
				</div>

			<?php else: ?>

				<div class="aguarde">AS INSCRIÇÕES PARA ESTE EVENTO JÁ ESTÃO ENCERRADAS.</div>

			<?php endif ?>

		<?php else: ?>

			<div class="aguarde">AGUARDE E ACOMPANHE AQUI NO SITE O PERÍODO DE INSCRIÇÕES. VISITE-NOS EM BREVE.</div>

		<?php endif ?>

	<?php else: ?>

		<?php if($data_setada): ?>

			<?php if ($data_valida): ?>

				<div class="aguarde">AGUARDE E ACOMPANHE AQUI NO SITE O PERÍODO DE INSCRIÇÕES. VISITE-NOS EM BREVE.</div>

			<?php else: ?>
				
				<?php if ($galeria): ?>
					
					<div class="aguarde galeria">
						CONFIRA AQUI AS FOTOS DO EVENTO.
						<div class='thumbs-galeria'>
							<?php for ($i=0; $i < 5; $i++) {  ?>
							<?php foreach ($galeria as $key => $value): ?>
								<a href="_imgs/eventos/pos/<?=$value->imagem?>" rel="galeria" title="<?=$value->legenda?>" class="fancybox">
									<img src="_imgs/eventos/pos/thumbs/<?=$value->imagem?>" alt="<?=$value->legenda?>">
								</a>
							<?php endforeach ?>								
							<?php } ?>
						</div>
					</div>

				<?php else: ?>
					
					<div class="aguarde">AS INSCRIÇÕES PARA ESTE EVENTO JÁ ESTÃO ENCERRADAS.</div>
					
				<?php endif ?>

			<?php endif ?>

		<?php else: ?>

			<div class="aguarde">AGUARDE E ACOMPANHE AQUI NO SITE O PERÍODO DE INSCRIÇÕES. VISITE-NOS EM BREVE.</div>

		<?php endif ?>
	<?php endif ?>


	</div>

</section>

<aside>

	<?php if($anuncio_eventos): ?>
		<?php echo $anuncio_eventos ?>
	<?php endif; ?>
	
	<div class="agenda-interna amarelo">
		<?php if ($agenda): ?>

			<h2>AGENDA DE EVENTOS</h2>

			<div class="lista">
				<?php foreach ($agenda as $key => $value): ?>
					<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
						<div class="data"><?=$value->detalhe_data?></div>
						<div class="local"><?=$value->local?></div>
						<div class="titulo"><?=$value->titulo?></div>
						<div class="linha"></div>
					</a>
				<?php endforeach ?>
			</div>
		<?php endif ?>

		<a href="eventos" class="link-voltar" title="ver todos os eventos">&laquo; ver todos os eventos</a>
	</div>

</aside>