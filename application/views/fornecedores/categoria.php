<h1>
	ENCONTRE UM FORNECEDOR <span>Encontre fornecedores buscando por Categorias, Produtos ou Empresas.</span>
	<a href="javascript:window.history.back()" class="btn-voltar" title="Voltar">&laquo; voltar</a>
</h1>

<div class="main-container">
	<aside>
		<h2>BUSCAR POR CATEGORIA</h2>

		<?php if ($categorias): ?>
			<?php foreach ($categorias as $key => $value): ?>
				<a href="fornecedores/categoria/<?=$key?>" class="link-<?=$key?>" title="<?=$value?>"><?=$value?></a>
			<?php endforeach ?>
		<?php endif ?>
	</aside>

	<section>
		<h2 class="categoria-titulo <?=$categoria_selecionada?>"><?=$titulo_categoria?></h2>
		
		<?$contador = 0?>
		<?$ultima_letra = ''?>
		
		<ul class="lista-keywords">
			<?php if ($resultados): ?>
				<?php foreach ($resultados as $key => $value): ?>
					
					<?php
					if( substr($value->palavras, 0, 1) != $ultima_letra){

						if($contador == $num_linhas_por_colunas){
							echo "</ul><ul class='lista-keywords'>";
							$contador = 0;
						}

						$ultima_letra = substr($value->palavras, 0, 1);
						echo "<h3 class='letra'>&bull; ".mb_strtoupper($ultima_letra)." &bull;</h3>";
						$contador++;
					}
					?>
					
					<li>
						<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>"><?=$value->palavras?></a>
					</li>

				<?php endforeach ?>
			<?php endif ?>
		</ul>

	</section>
</div>