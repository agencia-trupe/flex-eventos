<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Flex Eventos</title>
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2012 Trupe Design" />

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta property="og:title" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
    <meta property="og:url" content="<?=base_url()?>"/>
    <meta property="og:description" content=""/>

    <base href="<?= base_url() ?>">
    <script> var BASE = '<?= base_url() ?>'</script>

    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,700|Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>

    <?CSS(array('reset', 'base', 'fontface/stylesheet', $this->router->class, 'fornecedores-modal'))?>
    
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/fornecedores-modal.js"></script>

</head>
<body>

    <div id="modal">
	
        <div class="coluna coluna-cinza">
        
            <?php if (sizeof($detalhes) == 1): ?>
                
                <div class="imagem">
                    <img src="_imgs/anuario/marcas/thumbs/<?=$detalhes->imagem?>" alt="<?=$detalhes->nome_fantasia?>">
                </div>

                <p class="apresentacao">
                    <?=$detalhes->apresentacao?>
                </p>
                
                <div class="solicitacoes">
                    <a href="fornecedores/solicitacao/cotacao/<?=$detalhes->id?>" title="Solicitar Cotação" class="ativo">SOLICITAR COTAÇÃO</a>
                    <a href="fornecedores/solicitacao/catalogo/<?=$detalhes->id?>" title="Solicitar Catálogo">SOLICITAR CATÁLOGO</a>
                </div>

            <?php else: ?>
                
                <div class="varios-fornecedores">
                    <h1>FORNECEDORES SELECIONADOS</h1>
                    
                    <ul>
                    <?php foreach ($detalhes as $key => $value): ?>
                        <li><?=$value->nome_fantasia?></li>
                    <?php endforeach ?>
                    </ul>
                </div>

            <?php endif ?>
        </div>

        <div class="coluna coluna-extendida">
            <h1>SOLICITAR COTAÇÃO</h1>
            <form action="fornecedores/enviarSolicCotacao" method="post" id="form-solic-cotacao" enctype="multipart/form-data">
                <label>
                    nome
                    <input type="text" name="nome" required data-requiredmsg="Informe seu nome!">
                </label>
                <label>
                    e-mail
                    <input type="email" name="email" required data-requiredmsg="Informe seu e-mail!">
                </label>
                <label>
                    telefone
                    <input type="text" name="telefone" required data-requiredmsg="Informe seu telefone!" maxlength="18">
                </label>
                <label>
                    mensagem
                    <textarea name="mensagem" required data-requiredmsg="Informe uma mensagem para ser enviada!"></textarea>
                </label>
                <label class="file-label">
                    <div class="texto">anexar arquivo</div>
                    <input type="file" name="userfile" id="selec-file">
                    <span id="aviso">não obrigatório</span>
                </label>
                <?php if (sizeof($detalhes) == 1): ?>
                    <input type="hidden" name="ids_fornecedores[]" value="<?=$detalhes->id?>">
                <?php else: ?>
                    <?php foreach ($detalhes as $key => $value): ?>
                        <input type="hidden" name="ids_fornecedores[]" value="<?=$value->id?>">
                    <?php endforeach; ?>                
                <?php endif ?>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

    </div>

</body>
</html>