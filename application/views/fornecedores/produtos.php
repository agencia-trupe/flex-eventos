<h1>ENCONTRE UM FORNECEDOR <span>Encontre fornecedores buscando por Categorias, Produtos ou Empresas.</span></h1>

<div id="busca-efetuada">
	<div class="buscado">
		EXIBINDO PRODUTO BUSCADO:
		<div class="palavra">
			<?if(isset($busca_keyword))echo urldecode(html_entity_decode($busca_keyword))?>
		</div>
	</div>
	<div id="barra-busca">
		<h2>BUSCAR POR PRODUTO</h2>
		<form action="fornecedores/produtos" method="post" id="busca-fornecedores-produto">
			<input type="text" name="busca_produto" id="busca-produto" placeholder="produto" required value="<?if(isset($busca_keyword))echo urldecode(html_entity_decode($busca_keyword))?>">
			<input type="submit" value="BUSCAR" title="BUSCAR">
		</form>
		<a href="fornecedores/categoria/todos" title="ver todos os tipos de produtos">ver todos os tipos de produtos &raquo;</a>
	</div>
</div>

<div class="resultados">
	<?php if ($resultados): ?>

		<p>Para ver mais detalhes do fornecedor ou solicitar cotação individualmente, clique diretamente na foto do produto ou marca da empresa.</p>

		<p class="bold">Marque os fornecedores antes de clicar nos botões abaixo:</p>

		<a href="#" class="link-solicitacao cotacao" data-target="cotacao" title="SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS</a>

		<a href="#" class="link-solicitacao catalogo" data-target="catalogo" title="SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS</a>

		<div class="miolo">
			<?php foreach ($resultados as $key => $value): ?>
				
				<?php if (isset($value->produto[0])): ?>
					<div class="select <?=$value->categoria?>">
						<a href="fornecedores/detalhes/<?=$value->id?>" title="<?=$value->nome_fantasia?>" class="resultado">
							<div class="imagem-produto">
								<img src="_imgs/fornecedores/imagens/<?=$value->id?>/thumbs/<?=$value->produto[0]->imagem?>" alt="<?=$value->nome_fantasia?>">
							</div>
							<div class="imagem-marca">
								<img src="_imgs/anuario/marcas/thumbs/<?=$value->imagem?>" alt="<?=$value->nome_fantasia?>">
							</div>
							<p class="legenda">
								<?=$value->produto[0]->legenda?>
							</p>
						</a>
						<label><input type="checkbox" name="fornecedor-check" value="<?=$value->id?>"> selecionar este</label>
					</div>
				<?php endif ?>

			<?php endforeach ?>
		</div>

		<a href="#" class="link-solicitacao cotacao" data-target="cotacao" title="SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS</a>

		<a href="#" class="link-solicitacao catalogo" data-target="catalogo" title="SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS</a>	

	<?php else: ?>
		
		<h3 class="nenhum-resultado">Nenhum fornecedor encontrado com produto buscado. Tente novamente.</h3>

	<?php endif; ?>
</div>