<h1>ENCONTRE UM FORNECEDOR</h1>

<div class="buscas">

	<div class="produto coluna">
		<h2>BUSCA POR PRODUTO</h2>
		<form action="fornecedores/produto" method="post" id="busca-fornecedores-produto">
			<input type="text" name="busca_produto" id="busca-produto" placeholder="Palavra-chave" required value="<?if(isset($busca_keyword))echo urldecode(html_entity_decode($busca_keyword))?>">
			<input type="submit" value="BUSCAR">
		</form>
		<a href="fornecedores/produto" title="veja todos os produtos">veja todos os produtos</a>
	</div>

	<div class="categoria coluna">
		<h2>BUSCA POR CATEGORIA</h2>
		<form action="fornecedores/categoria" method="post" id="busca-fornecedores-categoria">
			<select name="busca_categoria" id="busca-categoria" required>
				<option value="">Categoria</option>
				<?php if ($categorias): ?>
					<?php foreach ($categorias as $key => $value): ?>
						<option value="<?=$key?>" <?php if(isset($categoria_selecionada) && $categoria_selecionada == $key)echo" selected"?>><?=$value?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
			<input type="submit" value="BUSCAR">
		</form>
	</div>

	<div class="empresa coluna">
		<a href="fornecedores/empresa" title="Busca por Empresa">BUSCA POR EMPRESA ></a>
	</div>

</div>

<div class="detalhes">

	<div class="coluna-cinza">
		<div class="imagem">
			<img src="_imgs/anuario/marcas/thumbs/<?=$detalhes->imagem?>" alt="<?=$detalhes->nome_fantasia?>">
		</div>

		<p>
			<?=$detalhes->endereco?>, <?=$detalhes->numero?> - <?=$detalhes->cep?> - <?=$detalhes->cidade?> - <?=$detalhes->estado?>
		</p>

		<p>
			TEL: <?=$detalhes->telefone?><br>
			<a href="mailto:<?=$detalhes->email?>"><?=$detalhes->email?></a><br>
			<a href="<?=prep_url($detalhes->website)?>" target="_blank"><?=$detalhes->website?></a>
		</p>

		<p class="apresentacao">
			<?=$detalhes->apresentacao?>
		</p>
	</div>

	<div class="coluna-maior">

		<a class="botao-voltar" href="javascript:window.history.back(1)">&laquo; voltar</a>

		<h1><?=$detalhes->nome_fantasia?></h1>

		<?php if ($imagens): ?>

			<div id="ampliada">
				<img src="_imgs/anuario/imagens/empresas/<?=$detalhes->id?>/<?=$imagens[0]->imagem?>">
			</div>

			<?php foreach ($imagens as $key => $value): ?>
				<a href="_imgs/anuario/imagens/empresas/<?=$detalhes->id?>/<?=$value->imagem?>" class="thumb<?if($key==0)echo" thumb-ativo"?>">
					<img src="_imgs/anuario/imagens/empresas/<?=$detalhes->id?>/thumbs/<?=$value->imagem?>">
				</a>
			<?php endforeach ?>

		<?php endif ?>

		<hr>

		<div class="lista-links-keywords">
			<?php if ($keywords): ?>
				<?php foreach ($keywords as $key => $value): ?>
					<?php if ($key > 0) echo"  &bull; ";?>
					<a href="fornecedores/produto/<?=urlencode(htmlentities($value->palavra))?>"><?=$value->palavra?></a>
				<?php endforeach ?>
			<?php endif ?>
		</div>

	</div>

</div>