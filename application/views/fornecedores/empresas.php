<h1>ENCONTRE UM FORNECEDOR <span>Encontre fornecedores buscando por Categorias, Produtos ou Empresas.</span></h1>

<div id="busca-efetuada">
	<div class="buscado">
		EXIBINDO EMPRESA BUSCADA:
		<div class="palavra">
			<?if(isset($busca_empresa) && $busca_empresa) echo urldecode(html_entity_decode($busca_empresa))?>
			<?if(isset($busca_letra) && $busca_letra) echo "Letra : ".urldecode(html_entity_decode($busca_letra))?>
		</div>
	</div>
	<div id="barra-busca">
		<h2>BUSCAR POR PRODUTO</h2>
		<form action="fornecedores/produtos" method="post" id="busca-fornecedores-produto">
			<input type="text" name="busca_produto" id="busca-produto" placeholder="produto" required value="<?if(isset($busca_keyword))echo urldecode(html_entity_decode($busca_keyword))?>">
			<input type="submit" value="BUSCAR" title="BUSCAR">
		</form>
		<a href="fornecedores/categoria/todos" title="ver todos os tipos de produtos">ver todos os tipos de produtos &raquo;</a>
	</div>
</div>

<div class="resultados">
	<?php if ($resultados): ?>

		<p>Para ver mais detalhes do fornecedor ou solicitar cotação individualmente, clique diretamente na foto do produto ou marca da empresa.</p>

		<p class="bold">Marque os fornecedores antes de clicar nos botões abaixo:</p>

		<a href="#" class="link-solicitacao cotacao" data-target="cotacao" title="SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS</a>

		<a href="#" class="link-solicitacao catalogo" data-target="catalogo" title="SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS</a>

		<div class="miolo">
			<?php foreach ($resultados as $key => $value): ?>				
				
				<div class="select <?=$value->categoria?>">
					<a href="fornecedores/detalhes/<?=$value->id?>" title="<?=$value->nome_fantasia?>" class="resultado">
						<div class="imagem-marca">
							<img src="_imgs/anuario/marcas/thumbs/<?=$value->imagem?>" alt="<?=$value->nome_fantasia?>">
						</div>
						<p class="legenda">
							<?=$value->nome_fantasia?>
						</p>
					</a>
					<label><input type="checkbox" name="fornecedor-check" value="<?=$value->id?>"> selecionar este</label>
				</div>				

			<?php endforeach ?>
		</div>

		<a href="#" class="link-solicitacao cotacao" data-target="cotacao" title="SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR COTAÇÃO PARA OS FORNECEDORES SELECIONADOS</a>

		<a href="#" class="link-solicitacao catalogo" data-target="catalogo" title="SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS">SOLICITAR CATÁLOGO PARA OS FORNECEDORES SELECIONADOS</a>	

	<?php else: ?>
		
		<h3 class="nenhum-resultado">Nenhum fornecedor encontrado com produto buscado. Tente novamente.</h3>

	<?php endif; ?>
</div>

<div class="busca-empresas">

	<div class="caixa-letra">
		SELECIONE PELA INICIAL DO NOME DA EMPRESA<br>
		<a href="fornecedores/empresa/letra/A" title="A" <?php if($busca_letra == 'A') echo "class='ativo'" ?>>A</a>
		<a href="fornecedores/empresa/letra/B" title="B" <?php if($busca_letra == 'B') echo "class='ativo'" ?>>B</a>
		<a href="fornecedores/empresa/letra/C" title="C" <?php if($busca_letra == 'C') echo "class='ativo'" ?>>C</a>
		<a href="fornecedores/empresa/letra/D" title="D" <?php if($busca_letra == 'D') echo "class='ativo'" ?>>D</a>
		<a href="fornecedores/empresa/letra/E" title="E" <?php if($busca_letra == 'E') echo "class='ativo'" ?>>E</a>
		<a href="fornecedores/empresa/letra/F" title="F" <?php if($busca_letra == 'F') echo "class='ativo'" ?>>F</a>
		<a href="fornecedores/empresa/letra/G" title="G" <?php if($busca_letra == 'G') echo "class='ativo'" ?>>G</a>
		<a href="fornecedores/empresa/letra/H" title="H" <?php if($busca_letra == 'H') echo "class='ativo'" ?>>H</a>
		<a href="fornecedores/empresa/letra/I" title="I" <?php if($busca_letra == 'I') echo "class='ativo'" ?>>I</a>
		<a href="fornecedores/empresa/letra/J" title="J" <?php if($busca_letra == 'J') echo "class='ativo'" ?>>J</a>
		<a href="fornecedores/empresa/letra/K" title="K" <?php if($busca_letra == 'K') echo "class='ativo'" ?>>K</a>
		<a href="fornecedores/empresa/letra/L" title="L" <?php if($busca_letra == 'L') echo "class='ativo'" ?>>L</a>
		<a href="fornecedores/empresa/letra/M" title="M" <?php if($busca_letra == 'M') echo "class='ativo'" ?>>M</a>
		<a href="fornecedores/empresa/letra/N" title="N" <?php if($busca_letra == 'N') echo "class='ativo'" ?>>N</a>
		<a href="fornecedores/empresa/letra/O" title="O" <?php if($busca_letra == 'O') echo "class='ativo'" ?>>O</a>
		<a href="fornecedores/empresa/letra/P" title="P" <?php if($busca_letra == 'P') echo "class='ativo'" ?>>P</a>
		<a href="fornecedores/empresa/letra/Q" title="Q" <?php if($busca_letra == 'Q') echo "class='ativo'" ?>>Q</a>
		<a href="fornecedores/empresa/letra/R" title="R" <?php if($busca_letra == 'R') echo "class='ativo'" ?>>R</a>
		<a href="fornecedores/empresa/letra/S" title="S" <?php if($busca_letra == 'S') echo "class='ativo'" ?>>S</a>
		<a href="fornecedores/empresa/letra/T" title="T" <?php if($busca_letra == 'T') echo "class='ativo'" ?>>T</a>
		<a href="fornecedores/empresa/letra/U" title="U" <?php if($busca_letra == 'U') echo "class='ativo'" ?>>U</a>
		<a href="fornecedores/empresa/letra/V" title="V" <?php if($busca_letra == 'V') echo "class='ativo'" ?>>V</a>
		<a href="fornecedores/empresa/letra/W" title="W" <?php if($busca_letra == 'W') echo "class='ativo'" ?>>W</a>
		<a href="fornecedores/empresa/letra/X" title="X" <?php if($busca_letra == 'X') echo "class='ativo'" ?>>X</a>
		<a href="fornecedores/empresa/letra/Y" title="Y" <?php if($busca_letra == 'Y') echo "class='ativo'" ?>>Y</a>
		<a href="fornecedores/empresa/letra/Z" title="Z" <?php if($busca_letra == 'Z') echo "class='ativo'" ?>>Z</a>
	</div>

	<div class="caixa-termo">
		BUSQUE PELO NOME DA EMPRESA<br>
		<div>
			<form action="fornecedores/empresa/busca" method="post">
				<input type="text" id="nome-empresa" name="busca_empresa" required value="<?if(isset($busca_empresa))echo urldecode(html_entity_decode($busca_empresa))?>">
				<input type="submit" value="BUSCAR" id="buscar-por-nome">
			</form>
		</div>
	</div>

</div>