<h1>ENCONTRE UM FORNECEDOR <span>Encontre fornecedores buscando por Categorias, Produtos ou Empresas.</span></h1>

<div class="main-container">
	<aside>
		<h2>BUSCAR POR CATEGORIA</h2>

		<?php if ($categorias): ?>
			<?php foreach ($categorias as $key => $value): ?>
				<a href="fornecedores/categoria/<?=$key?>" class="link-<?=$key?>" title="<?=$value?>"><?=$value?></a>
			<?php endforeach ?>
		<?php endif ?>
	</aside>

	<section>
		
		<div id="barra-busca">
			<h2>BUSCAR POR PRODUTO</h2>
			<form action="fornecedores/produtos" method="post" id="busca-fornecedores-produto">
				<input type="text" name="busca_produto" id="busca-produto" placeholder="produto" required value="<?if(isset($busca_keyword))echo urldecode(html_entity_decode($busca_keyword))?>">
				<input type="submit" value="BUSCAR" title="BUSCAR">
			</form>
			<a href="fornecedores/categoria/todos" title="ver todos os tipos de produtos">ver todos os tipos de produtos &raquo;</a>
		</div>

		<?php if (isset($rand_prod['produtos_mobiliario']) && $rand_prod['produtos_mobiliario']): ?>
			<div class="coluna mobiliario">
			<?php foreach ($rand_prod['produtos_mobiliario'] as $key => $value): ?>
				<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>">
					<img src="_imgs/fornecedores/imagens/<?=$value->id_fornecedor?>/thumbs/<?=$value->imagem?>" alt="<?=$value->palavras?>">
					<?=$value->palavras?>
				</a>
			<?php endforeach ?>
			</div>
		<?php endif ?>

		<?php if (isset($rand_prod['produtos_componentes']) && $rand_prod['produtos_componentes']): ?>
			<div class="coluna componentes">
			<?php foreach ($rand_prod['produtos_componentes'] as $key => $value): ?>
				<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>">
					<img src="_imgs/fornecedores/imagens/<?=$value->id_fornecedor?>/thumbs/<?=$value->imagem?>" alt="<?=$value->palavras?>">
					<?=$value->palavras?>
				</a>
			<?php endforeach ?>
			</div>
		<?php endif ?>

		<?php if (isset($rand_prod['produtos_arquishow']) && $rand_prod['produtos_arquishow']): ?>
			<div class="coluna arquishow">
			<?php foreach ($rand_prod['produtos_arquishow'] as $key => $value): ?>
				<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>">
					<img src="_imgs/fornecedores/imagens/<?=$value->id_fornecedor?>/thumbs/<?=$value->imagem?>" alt="<?=$value->palavras?>">
					<?=$value->palavras?>
				</a>
			<?php endforeach ?>
			</div>
		<?php endif ?>

		<?php if (isset($rand_prod['produtos_hospitais']) && $rand_prod['produtos_hospitais']): ?>
			<div class="coluna hospitais">
			<?php foreach ($rand_prod['produtos_hospitais'] as $key => $value): ?>
				<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>">
					<img src="_imgs/fornecedores/imagens/<?=$value->id_fornecedor?>/thumbs/<?=$value->imagem?>" alt="<?=$value->palavras?>">
					<?=$value->palavras?>
				</a>
			<?php endforeach ?>
			</div>
		<?php endif ?>

		<?php if (isset($rand_prod['produtos_arquitetos']) && $rand_prod['produtos_arquitetos']): ?>
			<div class="coluna arquitetos">
			<?php foreach ($rand_prod['produtos_arquitetos'] as $key => $value): ?>
				<a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavras))?>" title="<?=$value->palavras?>">
					<img src="_imgs/fornecedores/imagens/<?=$value->id_fornecedor?>/thumbs/<?=$value->imagem?>" alt="<?=$value->palavras?>">
					<?=$value->palavras?>
				</a>
			<?php endforeach ?>
			</div>
		<?php endif ?>

	</section>

	<h1 class="lined"><span>PRODUTOS MAIS BUSCADOS</span></h1>

	<div>

		<?php if ($top_prod['top_mobiliario']): ?>
			<div class="top mobiliario">
				<ul>
				<?php foreach ($top_prod['top_mobiliario'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/mobiliario" title="Ver Todos os produtos de Mobiliário">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

		<?php if ($top_prod['top_componentes']): ?>
			<div class="top componentes">
				<ul>
				<?php foreach ($top_prod['top_componentes'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/componentes" title="Ver Todos os produtos de Componentes">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

		<?php if ($top_prod['top_arquishow']): ?>
			<div class="top arquishow">
				<ul>
				<?php foreach ($top_prod['top_arquishow'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/arquishow" title="Ver Todos os produtos de Arquishow">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

		<?php if ($top_prod['top_hospitais']): ?>
			<div class="top hospitais">
				<ul>
				<?php foreach ($top_prod['top_hospitais'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/hr" title="Ver Todos os produtos de Hospitais">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

		<?php if ($top_prod['top_construtoras']): ?>
			<div class="top construtoras">
				<ul>
				<?php foreach ($top_prod['top_construtoras'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/construtoras" title="Ver Todos os produtos de Construtoras">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

		<?php if ($top_prod['top_arquitetos']): ?>
			<div class="top arquitetos">
				<ul>
				<?php foreach ($top_prod['top_arquitetos'] as $key => $value): ?>
					<li><a href="fornecedores/produtos/<?=urlencode(htmlentities($value->palavra_buscada))?>" title="<?=mb_strtoupper($value->palavra_buscada)?>"><?=mb_strtoupper($value->palavra_buscada)?></a></li>
				<?php endforeach ?>
				</ul>
				<a class="vertodos" href="fornecedores/categoria/arquitetos" title="Ver Todos os produtos de Arquitetos">VER TODOS &raquo;</a>
			</div>
		<?php endif ?>

	</div>
</div>

<div class="busca-empresas">

	<div class="caixa-letra">
		SELECIONE PELA INICIAL DO NOME DA EMPRESA<br>
		<a href="fornecedores/empresa/letra/A" title="A" data-letra="A">A</a>
		<a href="fornecedores/empresa/letra/B" title="B" data-letra="B">B</a>
		<a href="fornecedores/empresa/letra/C" title="C" data-letra="C">C</a>
		<a href="fornecedores/empresa/letra/D" title="D" data-letra="D">D</a>
		<a href="fornecedores/empresa/letra/E" title="E" data-letra="E">E</a>
		<a href="fornecedores/empresa/letra/F" title="F" data-letra="F">F</a>
		<a href="fornecedores/empresa/letra/G" title="G" data-letra="G">G</a>
		<a href="fornecedores/empresa/letra/H" title="H" data-letra="H">H</a>
		<a href="fornecedores/empresa/letra/I" title="I" data-letra="I">I</a>
		<a href="fornecedores/empresa/letra/J" title="J" data-letra="J">J</a>
		<a href="fornecedores/empresa/letra/K" title="K" data-letra="K">K</a>
		<a href="fornecedores/empresa/letra/L" title="L" data-letra="L">L</a>
		<a href="fornecedores/empresa/letra/M" title="M" data-letra="M">M</a>
		<a href="fornecedores/empresa/letra/N" title="N" data-letra="N">N</a>
		<a href="fornecedores/empresa/letra/O" title="O" data-letra="O">O</a>
		<a href="fornecedores/empresa/letra/P" title="P" data-letra="P">P</a>
		<a href="fornecedores/empresa/letra/Q" title="Q" data-letra="Q">Q</a>
		<a href="fornecedores/empresa/letra/R" title="R" data-letra="R">R</a>
		<a href="fornecedores/empresa/letra/S" title="S" data-letra="S">S</a>
		<a href="fornecedores/empresa/letra/T" title="T" data-letra="T">T</a>
		<a href="fornecedores/empresa/letra/U" title="U" data-letra="U">U</a>
		<a href="fornecedores/empresa/letra/V" title="V" data-letra="V">V</a>
		<a href="fornecedores/empresa/letra/W" title="W" data-letra="W">W</a>
		<a href="fornecedores/empresa/letra/X" title="X" data-letra="X">X</a>
		<a href="fornecedores/empresa/letra/Y" title="Y" data-letra="Y">Y</a>
		<a href="fornecedores/empresa/letra/Z" title="Z" data-letra="Z">Z</a>
	</div>

	<div class="caixa-termo">
		BUSQUE PELO NOME DA EMPRESA<br>
		<div>
			<form action="fornecedores/empresa/busca" method="post">
				<input type="text" id="nome-empresa" name="busca_empresa" required>
				<input type="submit" value="BUSCAR" id="buscar-por-nome">
			</form>
		</div>
	</div>

</div>