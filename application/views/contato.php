<div class="coluna">
	<h1>CONTATO</h1>
	<h2>11 3663-2505</h2>
	<p>
		Av. Paulista 1765, cj 141, 14&ordm; andar - Bela Vista<br>
		01311-200 - São Paulo, SP
	</p>
	<iframe width="550" height="345" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Av+Paulista,+1765&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Paulista,+1765+-+Bela+Vista,+S%C3%A3o+Paulo,+01311-200&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.560512,-46.657494&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com.br/maps?q=Av+Paulista,+1765&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Paulista,+1765+-+Bela+Vista,+S%C3%A3o+Paulo,+01311-200&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.560512,-46.657494&amp;source=embed" style="color:#4E4E4E;text-align:left">ver mapa ampliado &raquo;</a></small>
</div>

<div class="coluna semmargem">
	<h1>FALE CONOSCO</h1>
	<?php if ($this->session->flashdata('contato_enviado')): ?>
		<h2 style="text-align:center; line-height:150%;">
			Obrigado por entrar em contato!<br>
			Responderemos assim que possível!
		</h2>
	<?php else: ?>
		<form action="contato/enviar" method="post">
			<label>
				nome <input type="text" name="nome" required>
			</label>
			<label>
				e-mail <input type="email" name="email" required>
			</label>
			<label>
				telefone <input type="text" name="telefone">
			</label>
			<label>
				mensagem
				<textarea name="mensagem" required></textarea>
			</label>
			<input type="submit" value="ENVIAR">
		</form>
	<?php endif ?>
</div>
