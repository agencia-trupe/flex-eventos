<div class="chamadas-home">

	<div class="chamada-grande">
		<?php if ($banners): ?>
			<?php foreach ($banners as $key => $value): ?>
				<a href="<?=$value->destino?>" title="<?=$value->titulo?>">
					<img src="_imgs/banners/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<div class="texto">
						<h1><?=$value->titulo?></h1>
						<p>
							<?=$value->texto?>
						</p>
					</div>
				</a>
			<?php endforeach ?>
		<?php endif ?>
	</div>

	<div class="chamadas-pequenas">
		<div class="anuncio-placeholder">
			<?php if($anuncio_topo): ?>
				<?php echo $anuncio_topo ?>
			<?php endif; ?>
		</div>
		<?php if (isset($destaque1) && $destaque1): ?>
			<?=$destaque1?>
		<?php endif ?>
		<?php if (isset($destaque2) && $destaque2): ?>
			<?=$destaque2?>
		<?php endif ?>
	</div>

</div>

<div class="publicacoes">

	<div class="chamada">
		<h2>NOSSAS PUBLICAÇÕES</h2>
		<p>Confira aqui nossas edições mais recentes</p>
		<a href="publicacoes" title="Ver Todas">VER TODAS &raquo;</a>
	</div>

	<?php if ($publicacoes): ?>
		<?php foreach ($publicacoes as $key => $value): ?>
			<a href="_pdfs/publicacoes/<?=$value->pdf?>" target="_blank" class="publicacao" title="<?=$value->numero?>">
				<img src="_imgs/publicacoes/thumbs/<?=$value->capa?>" alt="<?=$value->numero?>">
				<?=$value->numero?>
			</a>
		<?php endforeach ?>
	<?php endif ?>

</div>

<div class="clear">

	<div class="chamadas-noticias">
		<?php foreach ($noticias as $key => $value): ?>
			<a href="noticias/ler/<?=$value->slug?>" title="<?=$value->titulo?>" class="noticia">
				<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
				<?=$value->titulo?>
			</a>
		<?php endforeach ?>
		<a href="noticias" class="barra">MAIS NOTÍCIAS &raquo;</a>
	</div>

	<div class="coluna-agenda">

		<?php if($anuncio_inferior): ?>
			<?php echo $anuncio_inferior ?>
		<?php endif; ?>
		
		<div class="chamada-agenda">
			<h2>AGENDA DE EVENTOS</h2>

			<?php if ($agenda): ?>

				<div class="lista">
					<?php foreach ($agenda as $key => $value): ?>
						<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>" class="agenda">
							<div class="local"><?=$value->local?></div>
							<div class="titulo"><?=$value->titulo?></div>
							<div class="data"><?=$value->detalhe_data?></div>
							<div class="linha"></div>
						</a>
					<?php endforeach ?>
				</div>
			<?php endif ?>
		</div>
	</div>

</div>

<?php if ($anuncio_parceiros): ?>
	
	<div class="clear">
		
		<div class="parceiros">
			<h2>PARCEIROS INDICADOS</h2>

				<?php echo $anuncio_parceiros ?>
		</div>

	</div>

<?php endif ?>