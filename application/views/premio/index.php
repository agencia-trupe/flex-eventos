<section class="texto-evento">

	<img src="_imgs/premio/<?=$registro->imagem?>" alt="<?=$registro->titulo?>" class="banner">

	<div class="coluna larga">

		<?php if ($registro->detalhe_data): ?>
			<h1><?=$registro->detalhe_data?></h1>
		<?php endif ?>

		<?php if ($registro->subtitulo): ?>
			<h1><?=$registro->subtitulo?></h1>
		<?php endif ?>

		<div class="texto">
			<?=$registro->texto_col_esquerda?>
		</div>

	</div>

	<div class="coluna">
		<div class="texto">
			<?=$registro->texto_col_direita?>
		</div>
	</div>

	<?php if ($registro->texto_link && $registro->destino_link && $registro->inscricoes_abertas == 0): ?>
		<div class="full">
			<a href="<?=$registro->destino_link?>" title="<?=$registro->texto_link?>" class="vermelho">
				<?=$registro->texto_link?>
			</a>
		</div>
	<?php else: ?>
		<div class="box">
			<h1><?=$registro->texto_caixa_inscricao?></h1>
			<img src="_imgs/premio/<?=$registro->thumb_caixa_inscricao?>" alt="<?=$registro->titulo?>">

			<div class="links-container">
				<a href="_pdfs/premio/<?=$registro->regulamento?>" title="Leia o regulamento" target="_blank" class="download regulamento">
					Leia o regulamento<br>
					<span>Faça o download aqui</span>
				</a>
				<a href="_pdfs/premio/<?=$registro->prancha?>" title="Modelo da ficha de inscrição" target="_blank" class="download modelo<?php if(substr($registro->prancha, -3) == 'zip') echo ' download-zip' ?>">
					Modelo explicativo da<br>
					PRANCHA DIGITAL<br>
					<span>Faça o download aqui</span>
				</a>
				<a href="premio/inscricao" title="CLIQUE AQUI PARA PREENCHER E ENVIAR SUA FICHA DE INSCRIÇÃO" class="ficha">
					CLIQUE AQUI PARA PREENCHER E ENVIAR SUA FICHA DE INSCRIÇÃO
				</a>
			</div>
		</div>
	<?php endif ?>

</section>

<aside class="agenda-interna amarelo">

	<?php if ($agenda): ?>

		<h2>AGENDA DE EVENTOS</h2>

		<div class="lista">
			<?php foreach ($agenda as $key => $value): ?>
				<a href="eventos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
					<div class="data"><?=$value->detalhe_data?></div>
					<div class="local"><?=$value->local?></div>
					<div class="titulo"><?=$value->titulo?></div>
					<div class="linha"></div>
				</a>
			<?php endforeach ?>
		</div>
	<?php endif ?>

	<a href="eventos" class="link-voltar" title="ver todos os eventos">&laquo; ver todos os eventos</a>

</aside>