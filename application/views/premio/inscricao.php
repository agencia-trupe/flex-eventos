<section class="texto-evento">

	<div class="form-inscricao">
		<h1>
			<img src="_imgs/premio/<?=$registro->thumb_caixa_inscricao?>" alt="<?=$registro->titulo?>">
			FICHA DE INSCRIÇÃO
		</h1>

		<?php if ($this->session->flashdata('inscricao_submetida')): ?>

			<div class="aviso-form">
				<h3>Obrigado.</h3>
				Sua inscrição para o<br>
				<?=$registro->titulo?><br>
				foi realizada com sucesso.<br>
				<br>
				Você receberá um email com uma cópia dos dados inscritos para acompanhamento.<br>
				<br>
				BOA SORTE!
			</div>

		<?php else: ?>

			<div class="aviso-form">
				Antes de preencher sua ficha de inscrição certifique-se de ter lido atentamente o Regulamento e preparado sua Ficha de inscrição seguindo as especificações do Modelo.
			</div>
			<form action="premio/inscrever" method="post" id="form-inscricao" enctype="multipart/form-data">
				<label>
					nome completo <input type="text" name="nome"  value="<?= $this->session->flashdata('inscricao_nome'); ?>" required>
				</label>
				<label>
					e-mail <input type="email" name="email"  value="<?= $this->session->flashdata('inscricao_email'); ?>" required>
				</label>
				<div class="label metade w349">
					telefone <input type="text" name="ddd_fone" required class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_ddd_fone'); ?>"><input type="text" name="fone" required class="w178" value="<?= $this->session->flashdata('inscricao_fone'); ?>">
				</div>
				<div class="label metade">
					celular <input type="text" name="ddd_celular" class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_ddd_celular'); ?>"><input type="text" name="celular" class="w178" value="<?= $this->session->flashdata('inscricao_celular'); ?>">
				</div>
				<label>
					escritório <input type="text" name="escritorio" required value="<?= $this->session->flashdata('inscricao_escritorio'); ?>">
				</label>
				<label>
					endereço <input type="text" name="endereco" required value="<?= $this->session->flashdata('inscricao_endereco'); ?>">
				</label>
				<label class="w75">
					bairro <input type="text" name="bairro" required value="<?= $this->session->flashdata('inscricao_bairro'); ?>">
				</label>
				<label class="w25">
					CEP <input type="text" name="cep" required value="<?= $this->session->flashdata('inscricao_cep'); ?>">
				</label>
				<label class="w75">
					cidade <input type="text" name="cidade" required value="<?= $this->session->flashdata('inscricao_cidade'); ?>">
				</label>
				<label class="w25">
					Estado [UF] <input type="text" name="estado" required class="menor" maxlength="2" value="<?= $this->session->flashdata('inscricao_estado'); ?>">
				</label>

				<label class="separador">
					setor
					<select name="setor" required>
						<option value=""></option>
						<?if($this->session->flashdata('inscricao_categoria')== "categoria")echo "selected"?>
						<optgroup label="Saúde (hospitais, clínicas, laboratórios e consultórios)">
							<option value="1">Saúde - Predial</option>
							<option value="2">Saúde - Interiores</option>
						</optgroup>
						<optgroup label="Educação (universidades, escolas e cursos)">
							<option value="3">Educação - Predial</option>
							<option value="4">Educação - Interiores</option>
						</optgroup>
						<optgroup label="Hotelaria (hotéis, flats, resorts e pousadas)">
							<option value="5">Hotelaria - Predial</option>
							<option value="6">Hotelaria - Interiores</option>
						</optgroup>
						<optgroup label="Shoppings">
							<option value="7">Shoppings - Predial</option>
							<option value="8">Shoppings - Interiores</option>
						</optgroup>
						<optgroup label="Comercial (lojas e restaurantes)">
							<option value="9">Comercial - Predial</option>
							<option value="10">Comercial - Interiores</option>
						</optgroup>
						<optgroup label="Obras Públicas (praças, rodoviárias, aeroportos, parques, etc.)">
							<option value="11">Obras Públicas - Predial</option>
							<option value="12">Obras Públicas - Interiores</option>
						</optgroup>
						<optgroup label="Residenciais (complexos residenciais verticais e horizontais)">
							<option value="13">Residenciais - Predial</option>
							<option value="14">Residenciais - Interiores</option>
						</optgroup>
						<optgroup label="Unifamiliar">
							<option value="15">Unifamiliar - Predial</option>
						</optgroup>
						<optgroup label="Indústrias (centros de distribuição, condomínios industriais)">
							<option value="16">Indústrias - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Esportivo">
							<option value="17">Esportivo - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Planejamento Urbano / Máster Plan">
							<option value="18">Planejamento Urbano / Máster Plan - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Infra-estrutura">
							<option value="19">Infra-estrutura - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Green Building">
							<option value="20">Green Building - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Retrofit">
							<option value="21">Retrofit - Predial / Interiores</option>
						</optgroup>
						<optgroup label="Escritórios (pequeno, médio e grande porte)">
							<option value="22">Escritórios - Predial (projeto)</option>
							<option value="23">Escritórios - Predial (obra realizada)</option>
							<option value="24">Escritórios - Interiores (projeto)</option>
							<option value="25">Escritórios - Interiores (obra realizada)</option>
						</optgroup>
					</select>
				</label>

				<label>
					categoria
					<select name="categoria" required>
						<option value=""></option>
						<option value="Profissionais">Profissionais</option>
						<option value="Estudantes">Estudantes</option>
					</select>
				</label>

				<label>
					nome da obra <input type="text" name="nome_obra" required  value="<?= $this->session->flashdata('inscricao_nome_obra'); ?>">
				</label>

				<label>
					cliente final <input type="text" name="cliente" required value="<?= $this->session->flashdata('inscricao_cliente'); ?>">
				</label>

				<label>
					responsável <span style="font-size:9px;">(cliente final)</span> <input type="text" name="responsavel" value="<?= $this->session->flashdata('inscricao_responsavel'); ?>">
				</label>

				<label>
					gerenciadora <input type="text" name="gerenciadora" value="<?= $this->session->flashdata('inscricao_gerenciadora'); ?>">
				</label>

				<label>
					responsável <span style="font-size:9px;">(gerenciadora)</span> <input type="text" name="responsavel_gerenciadora" value="<?= $this->session->flashdata('inscricao_responsavel_gerenciadora'); ?>">
				</label>

				<div class="label">
					cidade/UF da obra <input type="text" name="cidade_obra" required class="w365" value="<?= $this->session->flashdata('inscricao_cidade_obra'); ?>">
					<input type="text" name="uf_obra" required class="menor uf" maxlength="2" value="<?= $this->session->flashdata('inscricao_uf_obra'); ?>">
				</div>

				<label>
					<div class="texto">
						descrição da obra<br>
						<span>[até 400 caracteres]</span>
					</div>
					<textarea name="descricao_obra" required><?= $this->session->flashdata('inscricao_descricao_obra'); ?></textarea>
				</label>

				<div class="label file">
					prancha digital/projeto
					<label id="fileinput">
						<input type="file" name="userfile" required>
						<div class="fakeinput">ANEXAR ARQUIVO</div>
					</label>
					<div id="observacao">
						Atenção para o tamanho e formato do arquivo:<br>
						JPG - 300 dpi - até 10Mb
					</div>
				</div>

				<label class="check">
					<input type="checkbox" value="1"  <? if($this->session->flashdata('inscricao_acordo') == 1)echo" checked" ?> name="acordo"> Li e concordo com o regulamento do<br><?=$registro->titulo?>
				</label>

				<input type="submit" value="ENVIAR MINHA INSCRIÇÃO">

			</form>

			<?php if ($this->session->flashdata('mensagem_erro')): ?>
				<script defer>
				$('document').ready( function(){
					alert("<?=$this->session->flashdata('mensagem_erro')?>");
				});
				</script>
			<?php endif ?>

		<?php endif ?>
	</div>

</section>

<aside class="agenda-interna cinza">

	<img src="_imgs/premio/<?=$registro->thumb?>" alt="<?=$registro->titulo?>">

	<a href="_pdfs/premio/<?=$registro->regulamento?>" class="download regulamento" target="_blank" title="Leia o regulamento">
		Leia o regulamento<br>
		<span>Faça o download aqui</span>
	</a>

	<a href="_pdfs/premio/<?=$registro->prancha?>" class="download modelo<?php if(substr($registro->prancha, -3) == 'zip') echo ' download-zip' ?>" target="_blank" title="Modelo da ficha de inscrição">
		Modelo explicativo da<br>
		PRANCHA DIGITAL<br>
		<span>Faça o download aqui</span>
	</a>

	<h1><?=$registro->texto_caixa_inscricao?></h1>

</aside>