<div class="materia">
	<section>
		
		<h1>Investimento Público em 2013</h1>

		<h2>
			Mercado interno, infraestrutura, logística e energia devem manter o crescimento
		</h2>

		<img src="_imgs/noticias/investimento1.jpg" alt="Investimento Público em 2013">
		
		<p>
			O governo federal conseguiu elevar o volume de investimentos públicos no ano passado em 10,7%, segundo dados do Sistema Integrado de Administração Financeira (Siafi), contando principalmente com o aumento de pagamentos de pastas sociais, como Educação e Saúde. Para 2013, editou uma Medida Provisória (MP) que cria créditos extraordinários de R$ 42,5 bilhões para investimentos do setor público. Segundo a ministra do Planejamento, Miriam Belchior, esses recursos vão garantir os investimentos públicos em portos e aeroportos, anunciados em dezembro passado pela presidente Dilma Rousseff.
		</p>
		<p>
			O orçamento de investimentos este ano chegará ao volume de R$ 186,9 bilhões, o que representa um crescimento de 8,9% em relação a 2012. Entre as prioridades estão os investimentos em saúde, que receberá R$79,3 bilhões; na educação, que terá R$ 38 bilhões; no PAC - incluindo o programa Minha Casa, Minha Vida -, com 52,2 bilhões; e no Brasil Sem Miséria, com R$ 29,9 bilhões. O total destinado ao PAC para 2013 é de R$ 126,3 bilhões, entre orçamento fiscal, de seguridade e estatais. “Este orçamento reflete as grandes prioridades do governo e a decisão da presidenta Dilma Rousseff em relação às medidas necessárias para o crescimento do País”, afirmou a ministra.
		</p>
		<p>
			Pressionada pelo desempenho fraco da economia, a presidente quer já no início de 2013 acelerar os investimentos públicos e privados. A MP, na prática, dará margem para os ministérios gastarem com o pagamento de produtos e serviços ao longo de 2013. Assim, a máquina não parará mesmo que o Orçamento de 2013 atrase.
		</p>
		
		<img src="_imgs/noticias/investimento2.jpg" alt="Investimento Público em 2013">

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/Materia_Investimentos_Publicos.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>
<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
