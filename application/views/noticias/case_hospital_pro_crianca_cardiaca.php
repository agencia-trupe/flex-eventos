<div class="materia">
	<section>
		
		<h1>Hospital Pró Criança Cardíaca</h1>

		<h2>
			Projeto da RAF Arquitetura possui oito pavimentos voltados totalmente às crianças
		</h2>

		<img src="_imgs/noticias/hospital1.jpg" alt="Hospital Pró Criança Cardíaca">
		
		<p>
			A Fundação Pró Criança Cardíaca, fundada pela Dra. Rosa Célia Barbosa, em 1996, tem o objetivo de atender às muitas crianças que necessitam de cuidados cardíacos de qualidade no Rio de Janeiro. Crianças menos favorecidas que nascem com problemas e precisam de cirurgia e, muitas vezes, não recebem o cuidado adequado em hospitais públicos, que geralmente carecem de recursos humanos e de infraestrutura especializada agora estão sendo assistidas, evitando um grande número de mortes de crianças carentes que, até então, não suportavam a espera por um tratamento.
		</p>
		<p>
			Pensando nisso, a Dra. Rosa Célia traçou o objetivo de construir o Hospital Pró Criança Cardíaca, sobretudo para as crianças carentes do Rio de Janeiro e arredores. Misto de particular e público, o Hospital funciona de maneira que a parte lucrativa sustentará a parte sem fins lucrativos, para que continue servindo às famílias que necessitam e que já são atendidas através da Fundação Pró Criança Cardíaca.
		</p>
		<p>
			Fruto de doações da sociedade e de empresários, o projeto também engajou a RAF Arquitetura, que atua em diversos setores, sendo a área de saúde um dos mais atuantes. Este, aliás, foi um dos motivos que levou o também arquiteto João Pedro Backheuser, voluntário e coautor do projeto Pró criança Cardíaca, a convidar a RAF para este projeto. “Depois de meses trabalhando, passamos a contribuir ao programa social do hospital, que junto com o INCA – Instituto Nacional de Câncer e ABBR – Associação Brasileira Beneficente de Reabilitação, faz parte do programa da ação social que a RAF apoia”, afirmou o arquiteto Flávio Kelner, um dos sócios da empresa.
		</p>
		<p>
			Com o desafio de criar um hospital totalmente novo e voltado para crianças, elementos inusitados e alegres estão presentes em todo o projeto, desde a concepção da fachada, com elementos em aço, vidro e alumínio, até o detalhe das luminárias dos quartos. “Tudo foi pensado para criar ambientes com um alto astral e conforto”, disse Kelner. “O cliente (a Dra. Rosa Célia) é uma pessoa especial. Dela compreendemos uma razão humanitária de viver e trabalhar, e foi o que fizemos no projeto. O terreno com restrições urbanísticas, foi um grande desafio para adequar ao programa pretendido, mas conseguimos atender ao vasto programa, em oito pavimentos”, completou.
		</p>

		<img src="_imgs/noticias/hospital2.jpg" alt="Hospital Pró Criança Cardíaca">

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/Hospital Procrianca_70.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
