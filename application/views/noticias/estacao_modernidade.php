<div class="materia">
	<section>
		
		<h1>Estação Modernidade</h1>

		<h2>Esqueça os tons sóbrios e os layouts padronizados<br />A customização está na vanguarda quando o assunto é estação de trabalho</h2>

		<img src="_imgs/noticias/estacao1.jpg" alt="Estação Modernidade">
		
		<p><strong>Texto | Maria Luiza Castelo Branco</strong></p>
		<p>Os valores e identidade das empresas estão, cada vez mais, estampados em seu mobiliário, nas cores adotadas e no projeto corporativo, de modo a transmitir a forma de pensar das organizações. Em sintonia com a crescente evolução tecnológica, as estações de trabalho passaram a ser personalizadas e ganharam a capacidade de atender a diferentes composições. A flexibilidade é fundamental nos dias de hoje, na medida em que muitas empresas desenvolvem projetos com prazo para acabar e ao término é preciso remanejar equipes e, consequentemente, o mobiliário. Arquitetos apontam funcionalidade, a resistência dos materiais, e atenção às normas de ergonomia como pontos importantes na hora de executar os projetos.</p>
		<p>Para a arquiteta Heloísa Dabus, especialista em projetos corporativos há 25 anos, as empresas entenderam a importância de estimular funcionários por meio do conforto e satisfação no ambiente de trabalho. “A criatividade passou a circular livremente pelos projetos. O resultado é uma imensa variedade de materiais que aliam funcionalidade e conforto. Nosso maior desafio é promover o bem estar do usuário que passa horas nas estações de trabalho, além de atender às exigências do projeto”, afirma Heloisa.</p>
		<p>Sempre fundamental é a questão da ergonomia de cadeiras e mesas, bem como de acessórios como gaveteiros e estantes. As dimensões devem seguir normas técnicas, evitando problemas de saúde.  Segundo especialistas, as “baias”, como eram chamadas as antigas estações de trabalho eram estreitas, desconfortáveis e não atendiam às mínimas normas de conforto.</p>
		<p>Hoje, o mercado modernizou-se e é capaz de atender grande parte das demandas específicas desse segmento. Existe uma previsão de que em dois anos a maioria dos monitores será do tipo touch screen. Os arquitetos já trabalham em layouts para integrá-los às estações de trabalho. Essa novidade será um desafio para o setor.</p>
		<h2>Conceito de open space confere amplitude e integração</h2>
		<p>Os projetos de áreas de trabalho possuem vasta gama de combinações e vão desde as tradicionais disposições em L, até as lineares, chamadas de “mesão”. Estas podem ser simples – apenas uma bancada com até sete lugares colocados lado a lado -, ou frontais, com duas bancadas dispostas frente a frente com uma divisória baixa. Já as plataformas podem ser triangulares ou em formato de X, ideais para agregar membros de uma mesma equipe. Se o problema for o emaranhado de fios, atualmente existem vários recursos que ajudam a organizá-los como o sistema de canaletas por baixo da mesa e nos pés que trazem os cabos do chão e distribuem pelos trilhos ate o local do usuário.</p>
		<p>Os arquitetos veem a eliminação de barreiras físicas no ambiente corporativo como forma de ter maior liberdade nos projetos; já para as empresas passou a ser entendida como oportunidade para integrar melhor a equipe, romper com a rigidez hierárquica e, assim, possibilitar o afloramento de ideias e soluções de forma fluida e colaborativa, aumentando a produtividade.</p>
		<p>Outras vantagens dos chamados open spaces são a praticidade de manutenção do espaço, a gestão da equipe, a adequação de mudanças conforme as necessidades e, principalmente, a possibilidade da redução de custos.</p>
		<p>Por outro lado, há estudos mostrando que o stress pelo adensamento de pessoas e a distração são pontos negativos dos escritórios abertos. Entretanto, podemos perceber uma evolução na cultura corporativa, tendo em vista a necessidade de reunir equipes multidisciplinares. O recomendado é que existam áreas complementares, como espaços de convivência, de descompressão e salas de reuniões informais, onde os colaboradores podem estar de maneira descontraída; bem como pequenos espaços que ofereçam privacidade para um conference call, uma ligação pessoal ou um trabalho intelectual.</p>
		<p>Para os especialistas, as empresas do futuro, em determinados segmentos, tenderão a ter um espaço móvel, dinâmico e não mais fixo, priorizando mais as aptidões de cada colaborador do que o próprio local de trabalho.</p>
		<h2>Normas garantem o conforto</h2>
		<p>Somente a partir de 1990 foram criadas normas regulatórias para espaços corporativos no País. Para evitar danos à saúde, o Ministério do Trabalho lançou a norma reguladora NR 17 e as empresas foram obrigadas a mudar o mobiliário para atender às exigências de conforto estabelecidas.</p>
		<p>A norma da ABNT NBR 13967, específica para estações de trabalho, é válida desde 2011 e continua em vigência.</p>
		<p>Ela caracteriza estação de trabalho como mobiliário de uso individual cuja superfície não seja autoportante e sim vinculada estruturalmente a um painel ou divisória.  Podem estar reunidas em conjuntos de estações com possibilidade de acréscimo ou subtração de demais estações por meio de elementos de fixação. Os componentes básicos são: divisórias, superfícies de trabalho, acessórios e suporte e arquivamento de material (armários).</p>
		<p>Quanto ao conforto acústico e térmico, sabe-se que ambientes de trabalho livres de barulho e com temperatura agradável proporcionam maior produtividade, por isso é frequente o uso de forro acústico e ar condicionado insuflado pelo piso elevado para garantir o bem estar dos colaboradores. </p>
		<p>No quesito sustentabilidade, a tendência (e necessidade) mundial de responsabilidade ecológica leva as empresas a optarem pela madeira certificada e os “selos verdes”, além de outras atitudes como a economia de energia, reciclagem de papel e diminuição do desperdício de insumos.</p>

		<img src="_imgs/noticias/estacao2.jpg" alt="Estação Modernidade">

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/Estacao_Modernidade.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
