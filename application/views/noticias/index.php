<h1>ÚLTIMAS NOTÍCIAS</h1>

<div class="destaque">
	<?php if ($destaque): ?>
		<a href="noticias/ler/<?=$destaque->slug?>" title="<?=$destaque->titulo?>">

			<?php if ($destaque->imagem): ?>
				<img src="_imgs/noticias/<?=$destaque->imagem?>" alt="<?=$destaque->titulo?>">
			<?php endif ?>

			<div class="data">
				<div class="dia"><?=$destaque->dia?></div>
				<div class="mes"><?=mes($destaque->mes)?></div>
			</div>
			<div class="pre">
				<h2><?=$destaque->titulo?></h2>
				<?=$destaque->olho?>
			</div>
			<div class="barra">CONTINUE LENDO ></div>
		</a>
	<?php endif ?>
</div>

<ul class="destaque-lateral">

	<?php if ($destaques_laterais && is_array($destaques_laterais)): ?>

		<?php foreach ($destaques_laterais as $key => $value): ?>
			<li>
				<a href="noticias/ler/<?=$value->slug?>" title="<?=$value->titulo?>">
					<div class="data">
						<div class="dia"><?=$value->dia?></div>
						<div class="mes"><?=mes($value->mes)?></div>
					</div>

					<?php if ($value->imagem): ?>
						<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<?php endif ?>

					<div class="pre">
						<h2><?=$value->titulo?></h2>
					</div>
				</a>
			</li>
		<?php endforeach ?>

	<?php endif ?>

</ul>

<ul class="lista-noticias">

	<?php if ($registros && is_array($registros)): ?>

		<?php foreach ($registros as $key => $value): ?>
			<li>
				<a href="noticias/ler/<?=$value->slug?>" title="<?=$value->titulo?>">
					<div class="data">
						<div class="dia"><?=$value->dia?></div>
						<div class="mes"><?=mes($value->mes)?></div>
					</div>

					<?php if ($value->imagem): ?>
						<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<?php endif ?>

					<div class="pre">
						<h2><?=$value->titulo?></h2>
					</div>
				</a>
			</li>
		<?php endforeach ?>

	<?php endif ?>

</ul>