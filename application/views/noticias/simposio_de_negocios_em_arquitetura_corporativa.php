<div class="materia">
	<section>
		
		<h1>55º Simpósio de Negócios em Arquitetura Corporativa em BRASÍLIA</h1>

		<h2>
			A Flex Eventos realizou no dia 09 de abril, no Convenções Brasil 21, o 55º Simpósio de Negócios em Arquitetura Corporativa em Brasília
		</h2>

		<img src="_imgs/noticias/simposio1.jpg" alt="55º Simpósio de Negócios em Arquitetura Corporativa em BRASÍLIA">
		
		<p>
			O evento contou com a presença de mais de 200 profissionais, dos principais órgãos públicos, escritórios de arquitetura e construtoras. O grande diferencial do evento foi a qualidade dos participantes, com real interesse, que proporcionou muito network, troca de informações e fechamento de negócios com os patrocinadores do evento.
		</p>
		<p>
			As palestras, realizadas por grande profissionais da área de arquitetura e construção, estavam completamente lotadas.
		</p>
		<p>
			O Simpósio teve o apoio da Ademi-DF, CAU-DF, ABDEH-DF, ABIH, Asbea e OPB e como patrocinadores as empresas: Alberflex, Cavaletti, Caviglia-Cartoon, Cerâmica Atlas, Design On, Eliane, Favegrup, Flexform, Hansgrohe, Interact, Itaim, John Richard, Levantina, Madepar e Móveis Sular.
		</p>
		
		<img src="_imgs/noticias/simposio2.jpg" alt="55º Simpósio de Negócios em Arquitetura Corporativa em BRASÍLIA">

	</section>

	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/EVENTOS_73.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
