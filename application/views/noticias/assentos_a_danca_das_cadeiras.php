<div class="materia">
	<section>
		
		<h1>A Dança das Cadeiras</h1>

		<h2>
			As diferenças entre assentos corporativos e para auditórios e estádios são inúmeras, porém conforto e durabilidade são itens essenciais
		</h2>

		<img src="_imgs/noticias/assentos-1.jpg" alt="A Dança das Cadeiras">
		
		<p>
			Pergunte a um profissional que passa cerca de 12 hs (ou mais!) sentado diante do computador, como está o corpo e a postura ao final da jornada de trabalho. Ele provavelmente dirá: dor nas costas, nos ante-braços e uma sensação de pernas dormentes. Imposições da vida contemporânea? Não, necessariamente. Basta escolher os assentos específicos para cada função, levando-se em conta o maior critério: a saúde.
		</p>
		<p>
			Muitas empresas da área de desenvolvimento de produtos investem pesado em pesquisa de novos materiais e aprofundam-se, a cada dia, no estudo da ergonomia – ciência que alinha a interação humana com os sistemas à sua volta, de modo a torná-los compatíveis com as necessidades das pessoas. O Ministério do Trabalho, por meio da NR 17 (Ergonomia), estabelece parâmetros que criam condições de trabalho com conforto, segurança e desempenho eficiente.
		</p>
		<p>
			O desafio é reunir bem-estar, sustentabilidade e resultado estético. Parece simples, visto que o desenho de uma cadeira compõem-se de um assento, encosto e dois pares de pés. Mas, o que faz uma cadeira ser mais confortável que outra? Segundo o gerente de engenharia de uma indústria de assentos, o desenho ideal é aquele que alia ergonomia, antropometria – estudo das medidas do corpo humano -, além de seguir as normas da ABNT. “Para cadeiras de escritório, a norma NBR 13962:2006 especifica características físicas e dimensionais, bem como estabelece métodos para a determinação da estabilidade, resistência e durabilidade, em qualquer material”.
		</p>
		<p>
			De forma geral, a norma determina que as cadeiras operacionais, usadas em escritórios, devem ter a superfície do assento um pouco acentuada; borda frontal arredondada; regulagem de altura e de apoio lombar; base giratória, com pelo menos cinco pontos de apoio, providas ou não de rodízios. Os padrões para elaboração da norma baseiam-se para pessoas com peso até 110 Kg, com altura entre 1,51m e 1,92m, com uso por 8 hs ao dia.
		</p>
		<p>
			Hoje em dia já houve um grande avanço no setor de compras públicas, onde muitas concorrências já exigem produtos com certificação, de acordo com as normas brasileiras.
		</p>
		<p>
			Cada vez mais os ambientes corporativos passaram a ter múltiplos usos. Sendo assim, salas de reunião, palestras e recepção devem seguir suas especificidades. No primeiro caso, são indicadas cadeiras de espaldar médio, escolhidas de acordo com o tamanho do espaço e tempo de permanência no local. Para os ambientes destinados a treinamento e palestras são indicadas as longarinas, regidas pela norma NBR 16031 : 2012, caracterizadas por assentos múltiplos conjugados, sem fixação no chão, que permitem acomodar ao menos dois ocupantes. Requisitos de segurança, ensaio de durabilidade e aplicação de cargas são alguns dos itens que compõem o documento.
		</p>
		<p>
			Já para áreas de recepção, onde o usuário ficará por pouco tempo, é possível inovar, apostando em modelos diferenciados e criativos.
		</p>
		<p>
			Para locais de grande fluxo de pessoas, muitas vezes com espera prolongada, como aeroportos e rodoviárias devem ser usadas longarinas confortáveis, robustas e de fácil manutenção, além de possuir elementos de conexão entre os assentos para facilitar a reconfiguração dos ambientes.
		</p>

	</section>

	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/materia_assentos.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>

