<div class="materia">
	<section>
		
		<h1>Centro Cívico del Bicentenario Juan Bautista Bustos</h1>

		<h2>
			A obra permitiu acolher em um só espaço todos os colaboradores da administração pública da Província de Córdoba, além de oferecer à cidade, um novo local para realização de congressos e eventos
		</h2>

		<img src="_imgs/noticias/noticia1-img1.jpg" alt="Centro Cívico del Bicentenario Juan Bautista Bustos">
		
		<p>
			O Rio Primeiro – ou Suquía, nome que lhe foi dado pelas etnias nativas que habitavam a província de Córdoba antes da colonização espanhola – é um braço de água caudaloso, formado pela conjunção de vários outros rios menores, cujo curso coincide com os limites do centro histórico cordobense. Funciona, assim, como uma barreira física natural, separando a cidade em dois. Determinar a construção de um complexo de edifícios que abrigue a administração municipal justamente sobre essa linha divisória, diluindo‐a, não é por acaso.
		</p>
		<p>
			O “Centro Cívico del Bicentenario Juan Bautista Bustos”, nome extenso que fica abreviado em apenas “Centro Cívico Bicentenário”, fica localizado em cima de uma antiga linha férrea, agora desativada, a Ferrocacil Mitre. Idealizado conjuntamente por dois escritórios – O GGMPU e o Lucio Morini – o ambicioso projeto teve a construção dividida em três etapas e teve a construção orçada no valor de 427 milhões de pesos argentinos, algo em torno de 176 milhões de reais. O conjunto de edifícios conta com duas pontes sobre o Suquía, e pela primeira vez abrigará a administração da cidade em prédios projetados exclusivamente para essa finalidade. Entre eles, uma torre de 12 andares será a sede de todas as dependências do Governo, conjugando 950 postos de trabalho.
		</p>
		<p>
			De desenho moderno e arrojado, a torre se ergue sobre um espelho d’água, de frente para outro. Sua estrutura é um prisma multifacetado de concreto, permeado por orifícios quadriláteros que, juntos, formam projeções tridimensionais de cubo. As faces que os abrigam não sobem em linha reta, mas funcionam como um polígono, construídas em formas triangulares. O desenho das formas possibilita um jogo com as sombras projetadas que se transforma no transcorrer do dia. No topo da edificação, ainda há um terraço com uma área verde.
		</p>
		<p>
			Contrastando com a verticalidade do primeiro edifício, a segunda edificação se caracteriza pela extensão no vetor horizontal. Suas duas construções seguem a mesma orientação estética do primeiro, com as formas poligonais, e tem no diferencial o verde da superfície, completamente coberto por vinhas. Essa opção, além de colaborar para a integração harmônica do prédio ao ambiente, tem uma finalidade prática: as vinhas amenizam a temperatura, conservando‐ a durante o inverno e arejando o espaço durante o verão. A construção abriga as funções administrativas do governo, onde diariamente trabalham 1.250 pessoas, mais a casa do governador. Situada no teto do prédio, a casa é um sólido de paredes de vidro reflexivo, com um espaço vazio no centro. A escolha dos materiais foi somada à inclinação vertical das paredes, permitindo que suas paredes reflitam, ora o azul do céu, ora o verde do terraço onde ela está situada. No anexo desse segundo edifício, o baixo, um centro de convenções ainda será construído. Parcialmente enterrado, ele terá a forma de uma colina artificial de vidro, coberta de grama, integrando‐se naturalmente à paisagem. Anunciada em 2010, a obra vem em parte como comemoração do bicentenário da Argentina, cuja independência foi proclamada em 1810. Seu nome é homenagem a Juan Bautista Bustos, o primeiro governador da província. “A construção do Centro Cívico é uma dívida pendente para os cordobenses, que queremos fazer realidade no Bicentenário da Pátria”, afirmou, à época, Juan Schiaretti, então governador da província. A obra “deve servir para que todos os habitantes da cidade de Córdoba recuperem a autoestima, o orgulho de pertencer à Docta.”, concluiu.
		</p>

		<img src="_imgs/noticias/noticia1-img2.jpg" alt="Centro Cívico del Bicentenario Juan Bautista Bustos">

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>
<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
