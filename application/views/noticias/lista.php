<div class="chamadas-noticias">
	<a href="noticias/estacao_modernidade" title="ESTAÇÃO MODERNIDADE" class="noticia">
		<img src="_imgs/noticias/thumbs/thumb_estacao.jpg" alt="ESTAÇÃO MODERNIDADE">
		ESTAÇÃO MODERNIDADE
	</a>
	<a href="noticias/internacional_zahner" title="ZAHNER DESENVOLVE PROJETOS DE FACHADAS QUE CONCILIAM ARTE E FUNÇÃO" class="noticia">
		<img src="_imgs/noticias/thumbs/thumb_zahner.jpg" alt="ZAHNER DESENVOLVE PROJETOS DE FACHADAS QUE CONCILIAM ARTE E FUNÇÃO">
		ZAHNER DESENVOLVE PROJETOS DE FACHADAS QUE CONCILIAM ARTE E FUNÇÃO
	</a>
	<a href="noticias/rabobank" title="RABOBANK" class="noticia">
		<img src="_imgs/noticias/thumbs/thumb_rabobank.jpg" alt="RABOBANK">
		RABOBANK
	</a>
	<a href="noticias/sustentabilidade_saude" title="SUSTENTABILIDADE NA ÁREA DA SAÚDE" class="noticia">
		<img src="_imgs/noticias/thumbs/thumb_sustensaude.jpg" alt="SUSTENTABILIDADE NA ÁREA DA SAÚDE">
		SUSTENTABILIDADE NA ÁREA DA SAÚDE
	</a>
	
	<a href="noticias/investimento_publico_em_2013" title="ESPECIAL - INVESTIMENTO PÚBLICO EM 2013" class="noticialista">
		» ESPECIAL - INVESTIMENTO PÚBLICO EM 2013
	</a>
	<a href="noticias/assentos_a_danca_das_cadeiras" title="ASSENTOS - A DANÇA DAS CADEIRAS" class="noticialista">
		» ASSENTOS - A DANÇA DAS CADEIRAS
	</a>
	<a href="noticias/case_hospital_pro_crianca_cardiaca" title="CASE - HOSPITAL PRÓ CRIANÇA CARDÍACA" class="noticialista">
		» CASE - HOSPITAL PRÓ CRIANÇA CARDÍACA
	</a>
	<a href="noticias/simposio_de_negocios_em_arquitetura_corporativa" title="55&ordm; SIMPÓSIO DE NEGÓCIOS EM ARQUITETURA CORPORATIVA EM BRASÍLIA" class="noticialista">
		» 55&ordm; SIMPÓSIO DE NEGÓCIOS EM ARQUITETURA CORPORATIVA EM BRASÍLIA
	</a>
	<a href="noticias/case_intenacional_centro_civico" title="CASE INTERNACIONAL - CENTRO CÍVICO DEL BICENTENÁRIO JUAN BATISTA BUSTOS" class="noticialista">
		» CASE INTERNACIONAL - CENTRO CÍVICO DEL BICENTENÁRIO JUAN BATISTA BUSTOS
	</a>
	<a href="noticias/banco_do_brasil" title="O BANCO DO BRASIL COLOCOU EM SUA PÁGINA TODO O PORTIFÓLIO PARA ATENDER A DEMANDA" class="noticialista">
		» O BANCO DO BRASIL COLOCOU EM SUA PÁGINA TODO O PORTIFÓLIO PARA ATENDER A DEMANDA
	</a>
</div>