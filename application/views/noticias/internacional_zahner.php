<div class="materia">
	<section>
		
		<h1>Zahner desenvolve projetos de fachadas que conciliam arte e função</h1>

		<h2>Crawford Architecture projeta a revitalização de um grande galpão com a missão de transformá-lo em um cartão postal para a empresa</h2>

		<img src="_imgs/noticias/zahner1.jpg" alt="Zahner desenvolve projetos de fachadas que conciliam arte e função">
		
		<p><strong>Texto | Igor Câmara</strong></p>
		
		<p>Fundada há 115 anos por Andrew Zahner, sob o nome de Eagle Cornice Works, a futura A. Zahner Co. nasceu fornecendo serviços de decoração e reparo para sua região. Anos depois, em 1913, a companhia se tornou A. Zahner Sheet Metal Company, e durante o curso do século produziu trabalhos em metal para diversos fins. Com a experiência acumulada no manuseio dessa matéria prima, o herdeiro da companhia, L. William Zahner III, tornou-se presidente e foi responsável por sua transformação em uma companhia internacional especializada em produção de metais para arquitetura e fachadas.</p>
		<p>Nas últimas décadas, a A. Zahner foi responsável pela fabricação de obras notáveis, como a execução do Experience Music Project (EMP), projetado pelo arquiteto Frank Gehry, ou o de Young Museum, em São Francisco. </p>
		<p>Como informa a própria empresa, o cerne de suas realizações é a intersecção entre arte e função. Uma de suas grandes criações, os telhados de malha invertida (Inverted Seam™) são compostos de planos metálicos que coletam, controlam e divergem toda a precipitação, mesmo durante as mais pesadas tempestades.</p>
		<p>Expandir a sede de uma empresa é sempre um desafio, sobretudo quando a empresa em questão é a A. Zahner Co., famigerada companhia voltada para a realização de projetos arquitetônicos em metais e vidro. Parte da dificuldade apresentada consiste em que o prédio de uma empresa desse tipo, além de abrigar as funções práticas, também serve de portfólio, de cartão postal para os futuros clientes, como prova do que realmente ela é capaz de fazer. Foi com esse intuito – o de mostrar a capacidade e as diretrizes de seu empreendimento – que L. William Zahner procurou a Crawford Architecture para ser responsável por seu projeto: a revitalização de um grande galpão, que deveria ter espaço interno suficiente que assegurasse a movimentação de até dois grandes guindastes. A única diretriz que Zahner traçou foi a de que o projeto fizesse uso do ZEEP (Zahner Engineered Profile Panel), sistema autoral que envolve a aplicação de estruturas metálicas.</p>
		<p>O projeto foi, então, desenvolvido por Crawford. Inspirando-se na matéria-prima cujo trabalho é justamente a excelência de Zahner, os metais, os esboços foram baseados em padrões de corrosão encontrados em uma peça de aço do próprio edifício. O rascunho, de autoria de Stephen Colin, designer de projetos de Crawford, foi submetido a uma técnica computacional que o transformou em um modelo tridimensional, baseando-se simplesmente em seus valores tonais. A conclusão foi um conjunto de barbatanas metálicas postadas em intervalos regulares, com o espaço preenchido por painéis de vidro dry-set. Todo o conjunto foi encaixado em uma malha invertida (Inverted Seam™), que é justamente uma das grandes inovações tecnológicas e arquitetônicas patenteadas pela Zahner. As barbatanas, fabricadas após uma série de estudos em menor escala, foram cortadas a laser pela própria Zahner. Especializada nesse tipo de corte de alta precisão, muitos de seus projetos – que não se limitam a edificações, mas incluem também obras artísticas – levam esse tipo de acabamento. A única grande diferença, em relação às demais realizações nas quais se empregou a mesma tecnologia é que as peças metálicas, geralmente cobertas por uma “pele”, dessa vez figuravam nuas, expondo o esqueleto estrutural do prédio. </p>
		<p>A organicidade foi a grande inspiração do desenho, alegou Stacey Jones, também da Crawford. A ondulação da fachada teve inspiração em elementos naturais, como árvores, água, mas sobretudo nas sinuosidades encontradas na areia. </p>
		<p>Buscou-se integrar a construção ao entorno de modo a aproveitar a maior quantidade de recursos naturais, dentre eles a iluminação e ventilação. De frente para o norte, a luz natural se infiltra pelos intervalos das peças metálicas; ao mesmo tempo, nos lados leste e oeste, os menores, as barbatanas refletem a luz solar incidente nas manhãs. O resultado é um ambiente interior inundado por luz natural e que precisa apelar para poucas lâmpadas fluorescentes no período diário.</p>
		<p>Outra preocupação do projeto, a ventilação foi pensada de modo a não deixar o ambiente esquentar muito. Exaustores, posicionados no lado sul da construção, expelem o ar quente acumulado nas zonas altas da fábricas, enquanto que ventiladores baixos, localizados na ala norte, garantem a entrada de ar frio. Muito do ambiente interno foi reaproveitado do antigo galpão, inclusive colunas, contribuindo para a economia de recursos, sem que estes se gastassem desnecessariamente.</p>

		<img src="_imgs/noticias/zahner2.jpg" alt="Zahner desenvolve projetos de fachadas que conciliam arte e função">

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/Internacional_Zahner.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>
