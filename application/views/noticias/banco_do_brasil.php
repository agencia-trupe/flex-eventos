<div class="materia">
	<section>
		
		<h1>O Banco do Brasil colocou em sua página todo o portifólio para atender a demanda</h1>

		<h2>
			O Banco do Brasil democratizando, cada vez mais transparente, servindo de exemplo nacional colocou em sua página todo o portifólio desenvolvido para atender a sua demanda.
		</h2>

		<img src="_imgs/noticias/bb1.jpg" alt="O Banco do Brasil colocou em sua página todo o portifólio para atender a demanda">
		
		<p>
			Basta seguir as informações passo a passo como indicado na matéria completa e terá acesso ao portifólio, ou acessar diretamente ao link.
		</p>
		<p>
			Disponibilizando na página do BB na internet (link abaixo), de maneira simplifi cada, parte do enxoval de mobiliário do Banco do Brasil e suas características básicas de construção, materiais e acabamentos.
		</p>
		<p>
			<a href="http://www.bb.com.br/portalbb/page3,8899,32425,0,0,1,6.bb?codigoMenu=4725&codigoRet=17544&bread=1_1" title="Enxoval Mobiliário BB" target="_blank">Enxoval Mobiliário BB</a>
		</p>
		<p>
			<a href="http://www.bb.com.br/docs/pub/inst/dwn/EnxovalMobiliarioBB.pdf" title="Enxoval Mobiliário BB" target="_blank">Enxoval Mobiliário BB (download do pdf)</a>
		</p>

	</section>
	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/banco_do_brasil.pdf" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>
<aside>
	<?php $this->load->view('noticias/lista'); ?>
</aside>

