<div class="materia">
	<section>

		<h1><?=$noticia->titulo?></h1>

		<h2>
			<?=strip_tags($noticia->olho)?>
		</h2>
		<?php if ($noticia->imagem): ?>
			<img src="_imgs/noticias/<?=$noticia->imagem?>" alt="<?=$noticia->titulo?>">
		<?php endif ?>

		<?php if ($noticia->autor): ?>
			<p><strong>Texto | <?=$noticia->autor?></strong></p>
		<?php endif ?>

		<!-- Trocar thumb do video pelo embed -->
		<?php $noticia->texto = tinyMCEyoutube($noticia->texto) ?>
		
		<!-- URL de imagens Relativas ao documento -->
		<?=str_replace('../../../', '', $noticia->texto)?>

	</section>

	<div class="download-materia">
		<a href="#" class="botao-topo" title="Voltar ao Topo">^ TOPO</a>
		<a href="_pdfs/noticias/<?=$noticia->pdf?>" target="_blank" class="download" title="Download da Matéria Completa">DOWNLOAD DA MATÉRIA COMPLETA</a>
	</div>
</div>

<aside>

	<?php if($anuncio_noticias): ?>
		<?php echo $anuncio_noticias ?>
	<?php endif; ?>

	<div class="chamadas-noticias">

		<?php if ($lateral): ?>
			<?php foreach ($lateral as $key => $value): ?>

				<?php if ($key < 4): ?>

					<a href="noticias/ler/<?=$value->slug?>" title="<?=$value->titulo?>" class="noticia">
						<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
						<?=$value->titulo?>
					</a>

				<?php else: ?>

					<a href="noticias/ler/<?=$value->slug?>" title="<?=$value->titulo?>" class="noticialista">
						» <?=$value->titulo?>
					</a>

				<?php endif ?>

			<?php endforeach ?>
		<?php endif ?>

	</div>
</aside>

