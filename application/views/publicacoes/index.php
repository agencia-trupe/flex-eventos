<h1>NOSSAS PUBLICAÇÕES</h1>

<ul class="lista-revistas">

	<?php if ($revistas): ?>
		<?php foreach ($revistas as $key => $value): ?>
			<li>
				<a href="#" data-id="<?=$value->id?>" <?if($value->id==$revistas[0]->id)echo"class='ativo'"?> title="<?=$value->titulo?>">
					<div class="quadrado"></div>
					<span><?=$value->titulo?></span>
					<div class="quadrado direita"></div>
				</a>
			</li>
		<?php endforeach ?>
	<?php endif ?>

</ul>

<?php if ($revistas): ?>
	<div class="detalhes-revistas-container">
		<?php foreach ($revistas as $key => $value): ?>
			<div class="detalhes-revistas<?if($key > 0)echo" escondido hid-up"?>" id="target-<?=$value->id?>">
				<?php if ($value->capa != ''): ?>
					<div class="imagem">
						<img src="_imgs/publicacoes/thumbs/<?=$value->capa?>" alt="<?=$value->titulo?>">
					</div>
				<?php endif ?>
				<h2><?=$value->titulo?></h2>
				<h2><?=$value->subtitulo?></h2>
				<?=$value->texto?>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>

	<div class="anuncio-placeholder">
		<?php if($anuncio_publicacoes): ?>
			<?php echo $anuncio_publicacoes ?>
		<?php endif; ?>
	</div>

<h1>CONFIRA AS ÚLTIMAS EDIÇÕES</h1>

<?php if ($publicacoes): ?>

	<div class="downloads">
		<?php foreach ($publicacoes as $key => $value): ?>
			<a href="_pdfs/publicacoes/<?=$value->pdf?>" target="_blank" class="publicacao" title="<?=$value->numero?>">
				<img src="_imgs/publicacoes/thumbs/<?=$value->capa?>" alt="<?=$value->numero?>">
				<?=$value->numero?>
			</a>
		<?php endforeach ?>
	</div>
<?php endif ?>

