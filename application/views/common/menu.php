<header>

	<div class="topo">

		<div class="centro">

			<div class="vermelho">
				<a id="link-home" href="<?=base_url()?>" title="Página Inicial"><img src="_imgs/layout/marca-flexeventos.png" alt="Marca Flex Eventos"></a>
			</div>

			<div class="anuncio-placeholder">
				<?php if($anuncio_top): ?>
					<?php echo $anuncio_top ?>
				<?php endif; ?>
			</div>		
		</div>
	</div>

	<nav>

		<div class="centro">		

			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>HOME</a></li>
				<li><a href="premio" title="Prêmio" id="mn-premio" <?if($this->router->class=='premio')echo" class='ativo'"?>>PRÊMIO</a></li>
				<li><a href="eventos" title="Eventos" id="mn-eventos" <?if($this->router->class=='eventos')echo" class='ativo'"?>>EVENTOS</a></li>
				<!-- <li><a href="fornecedores" title="Fornecedores" id="mn-fornecedores" <?if($this->router->class=='fornecedores')echo" class='ativo'"?>>ENCONTRE UM FORNECEDOR</a></li> -->
				<li><a href="publicacoes" title="Publicações" id="mn-publicacoes" <?if($this->router->class=='publicacoes')echo" class='ativo'"?>>PUBLICAÇÕES</a></li>
				<li><a href="noticias" title="Notícias" id="mn-noticias" <?if($this->router->class=='noticias')echo" class='ativo'"?>>NOTÍCIAS</a></li>
				<li><a href="empresa" title="A Empresa" id="mn-empresa" <?if($this->router->class=='empresa')echo" class='ativo'"?>>EMPRESA</a></li>
				<li><a href="contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>CONTATO</a></li>
			</ul>
			
			<div class="social">
				<a href="https://www.facebook.com/pages/Flex-Eventos/205784879527141" title="Nossa página no Facebook" target="_blank"><img src="_imgs/layout/icone-facebook.png" alt="Logo Facebook"></a>
				<a href="http://www.twitter.com/flexeventos" title="Nosso perfil no Twitter" target="_blank"><img src="_imgs/layout/icone-twitter.png" alt="Logo Twitter"></a>
			</div>
		</div>
	</nav>

</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">