<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('noticias_model', 'model');
    }

    function index(){
   		$data['registros'] = $this->model->pegarTodos();

   		foreach ($data['registros'] as $key => $value) {
   			$value->dia = substr($value->data, 8);
   			$value->mes = substr($value->data, 5, 2);
   		}

   		if (isset($data['registros'][0]))
   			$data['destaque'] = array_shift($data['registros']);

   		if (isset($data['registros'][0]))
   			$data['destaques_laterais'][0] = array_shift($data['registros']);

   		if (isset($data['registros'][0]))
   			$data['destaques_laterais'][1] = array_shift($data['registros']);

   		$this->load->view('noticias/index', $data);
    }

    function ler($slug = false){
    	if(!$slug)
    		redirect('noticias/index');

    	$data['noticia'] = $this->model->pegarPorSlug($slug);
    	$data['lateral'] = $this->model->pegarLaterais($data['noticia']->id);
    	$data['anuncio_noticias'] = $this->anuncios->imprimir('noticias');
    	$this->load->view('noticias/ler', $data);
    }

	function estacao_modernidade(){
		$this->load->view('noticias/estacao_modernidade');
	}

	function internacional_zahner(){
		$this->load->view('noticias/internacional_zahner');
	}

	function rabobank(){
		$this->load->view('noticias/rabobank');
	}

	function sustentabilidade_saude(){
		$this->load->view('noticias/sustentabilidade_saude');
	}

	function investimento_publico_em_2013(){
		$this->load->view('noticias/investimento_publico_em_2013');
	}

	function assentos_a_danca_das_cadeiras(){
		$this->load->view('noticias/assentos_a_danca_das_cadeiras');
	}

	function case_hospital_pro_crianca_cardiaca(){
		$this->load->view('noticias/case_hospital_pro_crianca_cardiaca');
	}

	function simposio_de_negocios_em_arquitetura_corporativa(){
		$this->load->view('noticias/simposio_de_negocios_em_arquitetura_corporativa');
	}

	function case_intenacional_centro_civico(){
		$this->load->view('noticias/case_intenacional_centro_civico');
	}

	function banco_do_brasil(){
		$this->load->view('noticias/banco_do_brasil');
	}

}