<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

        $this->load->model('eventos_model', 'eventos');
    }

    // Home de Eventos Gerenciáveis
    function index(){
        $data['proximos_eventos'] = $this->eventos->pegarProximos();
        $data['ultimos_eventos'] = $this->eventos->pegarUltimos();

        $this->load->view('eventos/index', $data);
    }

    // Detalhes de Eventos Gerenciáveis
    function detalhes($id = false){
        if (!$id)
            redirect('eventos/index');

        $data['detalhes'] = $this->eventos->pegarPorId($id);
        $data['agenda'] = $this->eventos->pegarProximos();
        $data['galeria'] = $this->eventos->imagens($id);
        $data['anuncio_eventos'] = $this->anuncios->imprimir('eventos');

        $this->headervar['load_css'] = 'fancybox/fancybox';
        $this->headervar['load_js'] = 'fancybox';

        $this->load->view('eventos/detalhes', $data);
    }

    // Inscrição dos Eventos
    function inscricao($id = false){
        if(!$id)
            redirect('eventos/index/');

        $data['evento'] = $this->eventos->pegarPorId($id);
        if(!$data['evento'])
            redirect('eventos/index/');

        if(!$this->eventos->verificaInscricoesAbertas($data['evento']))
            redirect('eventos/index/');

        $data['agenda'] = $this->eventos->pegarProximos();
        $data['anuncio_eventos'] = $this->anuncios->imprimir('eventos');
        $this->load->view('eventos/inscricao', $data);
    }

    // Envio do Form de Inscrição dos Eventos
    // Separar parte do banco de dados no model
    function inscrever(){

        //$primeiro_nome = $this->input->post('primeiro_nome');
        //$this->session->set_flashdata('inscricao_primeiro_nome', $primeiro_nome);
        $indicado_por = $this->input->post('indicado_por');
        $this->session->set_flashdata('inscricao_indicado_por', $indicado_por);
        $nome = $this->input->post('nome');
        $this->session->set_flashdata('inscricao_nome', $nome);
        $cpf = $this->input->post('cpf');
        $this->session->set_flashdata('inscricao_cpf', $cpf);
        $email = $this->input->post('email');
        $this->session->set_flashdata('inscricao_email', $email);
        $ddd_fone = $this->input->post('ddd_fone');
        $this->session->set_flashdata('inscricao_ddd_fone', $ddd_fone);
        $fone = $this->input->post('fone');
        $this->session->set_flashdata('inscricao_fone', $fone);
        $ddd_celular = $this->input->post('ddd_celular');
        $this->session->set_flashdata('inscricao_ddd_celular', $ddd_celular);
        $celular = $this->input->post('celular');
        $this->session->set_flashdata('inscricao_celular', $celular);
        $cargo = $this->input->post('cargo');
        $this->session->set_flashdata('inscricao_cargo', $cargo);
        $escritorio = $this->input->post('escritorio');
        $this->session->set_flashdata('inscricao_escritorio', $escritorio);
        $cnpj = $this->input->post('cnpj');
        $this->session->set_flashdata('inscricao_cnpj', $cnpj);
        $endereco = $this->input->post('endereco');
        $this->session->set_flashdata('inscricao_endereco', $endereco);
        $bairro = $this->input->post('bairro');
        $this->session->set_flashdata('inscricao_bairro', $bairro);
        $cep = $this->input->post('cep');
        $this->session->set_flashdata('inscricao_cep', $cep);
        $cidade = $this->input->post('cidade');
        $this->session->set_flashdata('inscricao_cidade', $cidade);
        $estado = $this->input->post('estado');
        $this->session->set_flashdata('inscricao_estado', $estado);

        $id_evento = $this->input->post('id_evento');

        $query_evento = $this->eventos->pegarPorId($id_evento);
        if ($query_evento) {
            $nome_evento = $query_evento->titulo;
        }else{
            $mensagem_erro = "Erro ao efetuar inscrição!";
        }

        if(!$estado)
            $mensagem_erro = "O campo Estado é obrigatório!";
        if(!$cidade)
            $mensagem_erro = "O campo Cidade é obrigatório!";
        if(!$cep)
            $mensagem_erro = "O campo CEP é obrigatório!";
        if(!$bairro)
            $mensagem_erro = "O campo Bairro é obrigatório!";
        if(!$endereco)
            $mensagem_erro = "O campo Endereço é obrigatório!";
        if(!$fone)
            $mensagem_erro = "O campo Telefone é obrigatório!";
        if(!$ddd_fone)
            $mensagem_erro = "O campo DDD do telefone é obrigatório!";
        if(!$email)
            $mensagem_erro = "O campo E-mail é obrigatório!";
        if(!$nome)
            $mensagem_erro = "O campo Nome é obrigatório!";
        //if(!$primeiro_nome)
        //    $mensagem_erro = "O campo Primeiro Nome é obrigatório!";

        if(isset($mensagem_erro) && $mensagem_erro){
            $this->session->set_flashdata('mensagem_erro', $mensagem_erro);
            redirect('eventos/inscricao/'.$id_evento, 'refresh');
        }else{

            if($id_evento == '3'){
                $this->db->set('inscricao_aprovada', '1');
            }
            // Armazena a Inscrição
            //->set('primeiro_nome', $primeiro_nome)
            $this->db->set('nome', $nome)
                     ->set('cpf', $cpf)
                     ->set('email', $email)
                     ->set('ddd_fone', $ddd_fone)
                     ->set('fone', $fone)
                     ->set('ddd_celular', $ddd_celular)
                     ->set('celular', $celular)
                     ->set('cargo', $cargo)
                     ->set('escritorio', $escritorio)
                     ->set('cnpj', $cnpj)
                     ->set('endereco', $endereco)
                     ->set('bairro', $bairro)
                     ->set('cep', $cep)
                     ->set('cidade', $cidade)
                     ->set('estado', $estado)
                     ->set('id_eventos', $id_evento)
                     ->set('indicado_por', $indicado_por)
                     ->set('data_inscricao', Date('Y-m-d H:i:s'))
                     ->insert('eventos_inscricoes');


            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
            $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
            $emailconf['smtp_pass'] = 'p@rt.9al';

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'eventos@flexeventos.com.br';
            $fromname = 'Flex Eventos';
            $to = 'eventos@flexeventos.com.br';
            $bcc = 'bruno@trupe.net, ricardo@flexeventos.com.br, vanda@flexeventos.com.br,'.$email;

            $assunto = "Inscrição $nome_evento";

        $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Dados da Inscrição no evento $nome_evento</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Dados da Inscrição no evento $nome_evento</h1>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>indicado por : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$indicado_por</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>nome : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cpf : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cpf</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>email : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>fone : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_fone - $fone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>celular : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_celular - $celular</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cargo : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cargo</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>escritório : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$escritorio</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cnpj : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cnpj</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>endereço : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$endereco</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>bairro : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$bairro</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cep : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cep</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cidade/estado : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cidade $estado</span><br>
</body>
</html>
EML;

$plain = <<<EML
Dados da Inscrição no evento $nome_evento\r\n
indicado por : $indicado_por\r\n
nome : $nome\r\n
cpf : $cpf\r\n
email : $email\r\n
fone : $ddd_fone - $fone\r\n
celular : $ddd_celular - $celular\r\n
cargo : $cargo\r\n
escritório : $escritorio\r\n
cnpj : $cnpj\r\n
endereço : $endereco\r\n
bairro : $bairro\r\n
cep : $cep\r\n
cidade/estado : $cidade $estado\r\n
EML;
            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($email);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            $this->email->send();
            $this->session->set_flashdata('inscricao_submetida', true);
        }
        redirect('eventos/inscricao/'.$id_evento);
    }

    /* Eventos da época em que eram cadastrados manualmente */
    /* Redirecionar para página dinâmica correspondente     */
    /*********************************************************/
    function bahia_arquiday_show(){
		redirect('eventos/detalhes/');
    }

    function sexta_rodada_de_negocios(){
		redirect('eventos/detalhes/');
    }

    function x_grande_premio_de_arquitetura_corporativa(){
		redirect('premio');
    }

    function office_solution(){
        redirect('eventos/detalhes/');
    }

    function quinto_rio_arquiday_show(){
		redirect('eventos/detalhes/');
    }
    /*********************************************************/
}