<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Premio extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('premios_model', 'premios');
    }

    function index(){
        $data['registro'] = $this->premios->pegarPorId('1');

        $this->load->model('eventos_model', 'eventos');
        $data['agenda'] = $this->eventos->pegarProximos();

   		$this->load->view('premio/index', $data);
    }

    // Inscrição dos Prêmios
    function inscricao(){
        $data['registro'] = $this->premios->pegarPorId('1');

        $this->load->view('premio/inscricao', $data);
    }

    // Envio do Form de Inscrição do Prêmio
    // Separar parte do banco de dados no model
    function inscrever(){

        $nome = $this->input->post('nome');
        $this->session->set_flashdata('inscricao_nome', $nome);
        $email = $this->input->post('email');
        $this->session->set_flashdata('inscricao_email', $email);
        $ddd_fone = $this->input->post('ddd_fone');
        $this->session->set_flashdata('inscricao_ddd_fone', $ddd_fone);
        $fone = $this->input->post('fone');
        $this->session->set_flashdata('inscricao_fone', $fone);
        $ddd_celular = $this->input->post('ddd_celular');
        $this->session->set_flashdata('inscricao_ddd_celular', $ddd_celular);
        $celular = $this->input->post('celular');
        $this->session->set_flashdata('inscricao_celular', $celular);
        $escritorio = $this->input->post('escritorio');
        $this->session->set_flashdata('inscricao_escritorio', $escritorio);
        $endereco = $this->input->post('endereco');
        $this->session->set_flashdata('inscricao_endereco', $endereco);
        $bairro = $this->input->post('bairro');
        $this->session->set_flashdata('inscricao_bairro', $bairro);
        $cep = $this->input->post('cep');
        $this->session->set_flashdata('inscricao_cep', $cep);
        $cidade = $this->input->post('cidade');
        $this->session->set_flashdata('inscricao_cidade', $cidade);
        $estado = $this->input->post('estado');
        $this->session->set_flashdata('inscricao_estado', $estado);
        $categoria = $this->input->post('categoria');
        $this->session->set_flashdata('inscricao_categoria', $categoria);
        $setor = $this->input->post('setor');
        $this->session->set_flashdata('inscricao_setor', $setor);
        $nome_obra = $this->input->post('nome_obra');
        $this->session->set_flashdata('inscricao_nome_obra', $nome_obra);
        $cliente = $this->input->post('cliente');
        $this->session->set_flashdata('inscricao_cliente', $cliente);

        $responsavel = $this->input->post('responsavel');
        $this->session->set_flashdata('inscricao_responsavel', $responsavel);

        $gerenciadora = $this->input->post('gerenciadora');
        $this->session->set_flashdata('inscricao_gerenciadora', $gerenciadora);

        $responsavel_gerenciadora = $this->input->post('responsavel_gerenciadora');
        $this->session->set_flashdata('inscricao_responsavel_gerenciadora', $responsavel_gerenciadora);

        $cidade_obra = $this->input->post('cidade_obra');
        $this->session->set_flashdata('inscricao_cidade_obra', $cidade_obra);
        $uf_obra = $this->input->post('uf_obra');
        $this->session->set_flashdata('inscricao_uf_obra', $uf_obra);
        $descricao_obra = $this->input->post('descricao_obra');
        $this->session->set_flashdata('inscricao_descricao_obra', $descricao_obra);
        $userfile = $this->sobeArquivo();
        $acordo = $this->input->post('acordo');
        $this->session->set_flashdata('inscricao_acordo', $acordo);

        if($acordo != 1)
            $mensagem_erro = "É nevessário aceitar os termos do Regulamento!";
        if (!$userfile)
            $mensagem_erro = "O campo Prancha Digital/Projeto é obrigatório! Se você indicou seu arquivo, verifique se ele se encontra dentro dos limites  de tamanho: A3: 420 x 297 mm - 300dpi (máximo de 10Mb)";
        if(!$descricao_obra)
            $mensagem_erro = "O campo Descrição da Obra é obrigatório!";
        if(!$uf_obra)
            $mensagem_erro = "O campo UF da obra é obrigatório!";
        if(!$cidade_obra)
            $mensagem_erro = "O campo Cidade da Obra é obrigatório!";
        if(!$cliente)
            $mensagem_erro = "O campo Cliente é obrigatório!";
        if(!$nome_obra)
            $mensagem_erro = "O campo Nome da Obra é obrigatório!";
        if(!$setor)
            $mensagem_erro = "O campo Setor da Obra é obrigatório!";
        if(!$categoria)
            $mensagem_erro = "O campo Categoria da Obra é obrigatório!";
        if(!$estado)
            $mensagem_erro = "O campo Estado é obrigatório!";
        if(!$cidade)
            $mensagem_erro = "O campo Cidade é obrigatório!";
        if(!$cep)
            $mensagem_erro = "O campo CEP é obrigatório!";
        if(!$bairro)
            $mensagem_erro = "O campo Bairro é obrigatório!";
        if(!$endereco)
            $mensagem_erro = "O campo Endereço é obrigatório!";
        if(!$escritorio)
            $mensagem_erro = "O campo Escritório é obrigatório!";
        if(!$fone)
            $mensagem_erro = "O campo Telefone é obrigatório!";
        if(!$ddd_fone)
            $mensagem_erro = "O campo DDD do telefone é obrigatório!";
        if(!$email)
            $mensagem_erro = "O campo E-mail é obrigatório!";
        if(!$nome)
            $mensagem_erro = "O campo Nome é obrigatório!";

        if(isset($mensagem_erro) && $mensagem_erro){
            $this->session->set_flashdata('mensagem_erro', $mensagem_erro);
            redirect('eventos/inscricao', 'refresh');
        }else{
            $id_projeto = '1';

            $registro_premio = $this->db->get('premios')->row();
            $titulo = $registro_premio->titulo;

            $this->db->set('id_trupe_premios', $id_projeto)
                     ->set('nome', $nome)
                     ->set('email', $email)
                     ->set('ddd_fone', $ddd_fone)
                     ->set('fone', $fone)
                     ->set('ddd_celular', $ddd_celular)
                     ->set('celular', $celular)
                     ->set('escritorio', $escritorio)
                     ->set('endereco', $endereco)
                     ->set('bairro', $bairro)
                     ->set('cep', $cep)
                     ->set('cidade', $cidade)
                     ->set('estado', $estado)
                     ->set('categoria', $categoria)
                     ->set('setor', $setor)
                     ->set('nome_obra', $nome_obra)
                     ->set('cliente', $cliente)
                     ->set('cidade_obra', $cidade_obra)
                     ->set('uf_obra', $uf_obra)
                     ->set('descricao_obra', $descricao_obra)
                     ->set('prancha', $userfile)
                     ->set('data_submissao', date('Y-m-d H:i:s'))
                     ->set('projeto_aceito', '0')
                     ->set('responsavel', $responsavel)
                     ->set('gerenciadora', $gerenciadora)
                     ->set('responsavel_gerenciadora', $responsavel_gerenciadora)
                     ->insert('premios_inscricoes');

            $id = $this->db->insert_id();
            $codigo_inscricao = $id_projeto.$id;

            $this->db->set('codigo_inscricao', $codigo_inscricao)->where('id', $id)->update('premios_inscricoes');
            $codigo_inscricao_pad = str_pad($codigo_inscricao, 10, "0", STR_PAD_LEFT);

            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
            $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
            $emailconf['smtp_pass'] = 'p@rt.9al';

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'eventos@flexeventos.com.br';
            $fromname = 'Flex Eventos';
            $to = 'eventos@flexeventos.com.br';
            $bcc = 'bruno@trupe.net, ricardo@flexeventos.com.br, vanda@flexeventos.com.br,'.$email;

            $assunto = "Inscrição $titulo";
            $path_arquivo = "_fichas/reduzido/".$userfile;

            $setor = $this->tituloSetor($setor);

        $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Dados da Inscrição no $titulo</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Dados da Inscrição no $titulo</h1>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>código de inscrição : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$codigo_inscricao_pad</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>nome : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>email : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>fone : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_fone - $fone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>celular : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_celular - $celular</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>escritório : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$escritorio</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>endereço : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$endereco</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>bairro : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$bairro</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cep : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cep</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cidade/estado : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cidade $estado</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>categoria : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$categoria</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>setor : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$setor</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>nome da obra : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$nome_obra</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cliente : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cliente</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>responsável (cliente final) : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$responsavel</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>gerenciadora : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$gerenciadora</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>responsável (gerenciadora) : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$responsavel_gerenciadora</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cidade/estado da obra : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cidade_obra $uf_obra</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>descrição da obra : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$descricao_obra</span>
</body>
</html>
EML;

$plain = <<<EML
Dados da Inscrição no $titulo\r\n
código de inscrição : $codigo_inscricao_pad\r\n
nome : $nome\r\n
email : $email\r\n
fone : $ddd_fone - $fone\r\n
celular : $ddd_celular - $celular\r\n
escritório : $escritorio\r\n
endereço : $endereco\r\n
bairro : $bairro\r\n
cep : $cep\r\n
cidade/estado : $cidade $estado\r\n
categoria : $categoria\r\n
setor : $setor\r\n
nome da obra : $nome_obra\r\n
cliente : $cliente\r\n
responsável (cliente final): $responsavel\r\n
gerenciadora : $gerenciadora\r\n
responsável (gerenciadora) : $responsavel_gerenciadora\r\n
cidade/estado da obra : $cidade_obra $uf_obra\r\n
descrição da obra : $descricao_obra
EML;
            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($email);

            if($path_arquivo)
                $this->email->attach($path_arquivo);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            $this->email->send();
            $this->session->set_flashdata('inscricao_submetida', true);
        }
        redirect('premio/inscricao');
    }


    // Pega o Setor para enviar no email
    // Mover para o Controller de Prêmios
    private function tituloSetor($setor){
        $qry = $this->db->get_where('premios_setores', array('id' => $setor))->result();
        if (isset($qry[0])) {
            return $qry[0]->titulo;
        }else{
            return "Não Encontrado";
        }
    }


    // Upload da Ficha de Participação do Prêmio
    // Mover para o Controller de Prêmios
    private function sobeArquivo(){
        $this->load->library('upload');

        $original = array(
            'campo' => 'userfile',
            'dir' => '_fichas/'
        );
        $campo = $original['campo'];

        $uploadconfig = array(
          'upload_path' => $original['dir'],
          'allowed_types' => 'jpg|png|jpeg|bmp|gif',
          'max_size' => '11500',
          'max_width' => '0',
          'max_height' => '0');

        $this->upload->initialize($uploadconfig);

        if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                return false;
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name'], 'underscore', true);
                rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

                $this->image_moo->clear()->clear_temp();
                $this->image_moo->load($original['dir'].$filename)->resize(900,900)->save($original['dir'].'reduzido/'.$filename, TRUE);
                $this->image_moo->clear()->clear_temp();
                $this->image_moo->load($original['dir'].'reduzido/'.$filename)->resize_crop(300, 300)->save($original['dir'].'thumbs/'.$filename, TRUE);
                $this->image_moo->clear()->clear_temp();

                return $filename;
            }
        }else{
            return false;
        }
    }
}