<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importar extends CI_Controller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

        if(3 === 1){

        	$cadastros_empresas = $this->db->get('cadastro_anuario_empresas')->result();

        	foreach ($cadastros_empresas as $key => $value) {

        		$check_sobre = $this->db->get_where('cadastro_anuario_finalizacao_empresas', array('id_cadastro_anuario' => $value->id))->result();
        		
                if ($check_sobre){
        			if (isset($check_sobre[0])){
        				$this->db->set('apresentacao', $check_sobre[0]->sobre_empresa);
                    }
                }

                $senha = $this->gerarSenha();

        		$insert = $this->db->set('nome_fantasia', $value->nome_fantasia)
        				->set('razao_social', $value->razao_social)
        				->set('endereco', $value->endereco)
        				->set('numero', $value->numero)
        				->set('complemento', $value->complemento)
        				->set('bairro', $value->bairro)
        				->set('cidade', $value->cidade)
        				->set('estado', $value->estado)
        				->set('cep', $value->cep)
        				->set('telefone', $value->telefone)
        				->set('website', $value->website)
        				->set('nome', $value->nome)
        				->set('email', $value->email)
        				->set('senha', cripto($senha))
        				->set('imagem', $value->imagem)
        				->set('categoria', $value->categoria)
        				->set('id_cadastro_anuario_empresas', $value->id);
        				//->insert('fornecedores');

                // Enviar Email Inicial
                $email = $this->enviaEmailCadastro($value->nome_fantasia, $value->email, $senha);

                if($insert)
                    echo $value->id.") Fornecedor ".$value->nome_fantasia." <span style='color:green'>incluido</span> - ";
                else
                    echo $value->id.") Fornecedor ".$value->nome_fantasia." <span style=\"color:red\">não incluido</span> - ";

                if($email)
                    echo "<span style=\"color:green\">Email Enviado com sucesso</span><br>";
                else
                    echo "<span style=\"color:red\">Email NÃO Enviado</span><br>";
        	}


            $cadastros_arquitetos = $this->db->get('cadastro_anuario_arquitetos')->result();

            foreach ($cadastros_arquitetos as $key => $value) {

                $check_sobre = $this->db->get_where('cadastro_anuario_finalizacao_arquitetos', array('id_cadastro_anuario' => $value->id))->result();
                
                if ($check_sobre){
                    if (isset($check_sobre[0])){
                        $this->db->set('apresentacao', $check_sobre[0]->sobre_empresa);
                    }
                }

                $senha = $this->gerarSenha();

                $insert = $this->db->set('nome_fantasia', $value->nome_fantasia)
                        ->set('razao_social', $value->razao_social)
                        ->set('endereco', $value->endereco)
                        ->set('numero', $value->numero)
                        ->set('complemento', $value->complemento)
                        ->set('bairro', $value->bairro)
                        ->set('cidade', $value->cidade)
                        ->set('estado', $value->estado)
                        ->set('cep', $value->cep)
                        ->set('telefone', $value->telefone)
                        ->set('website', $value->website)
                        ->set('nome', $value->nome)
                        ->set('email', $value->email)
                        ->set('senha', cripto($senha))
                        ->set('imagem', $value->imagem)
                        ->set('categoria', 'arquitetos')
                        ->set('id_cadastro_anuario_empresas', $value->id);
                        //->insert('fornecedores');

                // Enviar Email Inicial
                $email = $this->enviaEmailCadastro($value->nome, $value->email, $senha);

                if($insert)
                    echo $value->id.") Arquiteto ".$value->nome." <span style='color:green'>incluido</span> - ";
                else
                    echo $value->id.") Arquiteto ".$value->nome." <span style=\"color:red\">não incluido</span> - ";

                if($email)
                    echo "<span style=\"color:green\">Email Enviado com sucesso</span><br>";
                else
                    echo "<span style=\"color:red\">Email NÃO Enviado</span><br>";
                
            }
        }
    }

    private function gerarSenha(){
        $retorno = '';
        $letras = str_split('abcdefghijklmnopqrstuvxwyzABCDEFGHIJKLMNOPQRSTUVXWYZ1234567890');
        for ($i = 10; $i > 0; $i--) {
            $retorno .= $letras[rand(0, 58)];
        }
        return $retorno;        
    }

    private function enviaEmailCadastro($nome, $email, $senha){
        if($nome && $email){
            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
            $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
            $emailconf['smtp_pass'] = 'p@rt.9al';

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'anuario@flexeventos.com.br';
            $fromname = 'Flex Eventos';
            $to = $email;
            $bcc = 'bruno@trupe.net';
            $assunto = 'Cadastro no Sistema de Fornecedores Flex - Errata';

            $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de confirmação - Cadastro de Fornecedores</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Boa tarde! Por motivos técnicos o cadastro inicial no sistema de Fornecedores Flex teve que ser refeito.
    </p>
    <p>
        Pedimos para desconsiderar as senhas enviadas anteriormente e agradecemos a compreensão.
    </p>
    <p>
        $nome, Você foi cadastrado no Sistema de Fornecedores Flex.
    </p>
    <p>
        Seus dados para acesso são: <br>
        Usuário : $email <br>
        Senha : $senha <br>
    </p>
    <p>
        Acesse o endereço: <a href="http://www.flexeventos.com.br/cadastro-fornecedores">http://www.flexeventos.com.br/cadastro-fornecedores</a> para efetuar o Login e complementar suas informações de cadastro.
    </p>
    <p>
        Gratos<br>
        Flex Editora
    </p>
</body>
</html>
EML;

            $plain = <<<EML
$nome, Você foi cadastrado no Sistema de Fornecedores Flex.\r\n
Seus dados para acesso são:\r\n
Usuário : $email\r\n
Senha : $senha\r\n
Acesse o endereço: http://www.flexeventos.com.br/cadastro-fornecedores para efetuar o Login e complementar suas informações de cadastro.\r\n
Gratos\r\n
Flex Editora
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($from);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            //return $this->email->send();
        }
    }    

}