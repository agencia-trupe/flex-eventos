<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fornecedores extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

        $this->headervar['load_js'] = 'fancybox';
        $this->headervar['load_css'] = 'fancybox/fancybox';
        $this->load->model('fornecedores_model', 'model');
    }

    function index(){
    	$data['categorias'] = array(
			'mobiliario' => 'MOBILIÁRIO',
			'componentes' => 'COMPONENTES',
			'arquishow' => 'ARQUISHOW',
			'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
			'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS'
    	);

        $data['rand_prod']['produtos_mobiliario'] = $this->model->pegarProdutosAleatoriosPorCategoria('mobiliario');
        $data['rand_prod']['produtos_componentes'] = $this->model->pegarProdutosAleatoriosPorCategoria('componentes');
        $data['rand_prod']['produtos_arquishow'] = $this->model->pegarProdutosAleatoriosPorCategoria('arquishow');
        $data['rand_prod']['produtos_hospitais'] = $this->model->pegarProdutosAleatoriosPorCategoria('hr');
        $data['rand_prod']['produtos_arquitetos'] = $this->model->pegarProdutosAleatoriosPorCategoria('arquitetos');
        
        $removerCategoria = true;
        foreach ($data['rand_prod'] as $key => $value) {
            if(sizeof($value) == 0){
                // Caso alguma categoria não tenha produtos, não preciso esconder um aleatório
                $removerCategoria = false;
            }
        }
        // Esconder uma categoria aleatória
        if($removerCategoria){
            unset($data['rand_prod'][array_rand($data['rand_prod'])]);
        }

        $data['top_prod']['top_mobiliario'] = $this->model->pegarMaisBuscadasPorCategoria('mobiliario');
        $data['top_prod']['top_componentes'] = $this->model->pegarMaisBuscadasPorCategoria('componentes');
        $data['top_prod']['top_arquishow'] = $this->model->pegarMaisBuscadasPorCategoria('arquishow');
        $data['top_prod']['top_hospitais'] = $this->model->pegarMaisBuscadasPorCategoria('hr');
        $data['top_prod']['top_construtoras'] = $this->model->pegarMaisBuscadasPorCategoria('construtoras');
        $data['top_prod']['top_arquitetos'] = $this->model->pegarMaisBuscadasPorCategoria('arquitetos');
        
        $removerTop = true;
        foreach ($data['top_prod'] as $key => $value) {
            if(sizeof($value) == 0){
                // Caso alguma categoria não tenha produtos, não preciso esconder um aleatório
                $removerTop = false;
            }
        }
        // Esconder uma categoria aleatória
        if($removerTop){
            unset($data['top_prod'][array_rand($data['top_prod'])]);
        }        

        $data['resultados'] = $this->model->pegarAleatorios();
   		$this->load->view('fornecedores/index', $data);
    }

    /* BUSCA POR PRODUTO */
    // Se houver $_POST['busca_produto'] OU $termo como argumento fazer a busca
    // Se não houver mostrar todos os produtos
    // Na tela de todos os produtos cada produto é um link e leva pra busca por esse termo
    // O resultado da busca é como a home, com alguma identificação no topo que indique o termo da busca
    function produtos($termo = false){
        $data['categorias'] = array(
            'mobiliario' => 'MOBILIÁRIO',
            'componentes' => 'COMPONENTES',
            'arquishow' => 'ARQUISHOW',
            'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
            'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS'
        );
        $termo_post = $this->input->post('busca_produto');
        
        if($termo_post)
            $termo = urlencode(htmlentities($termo_post));

        if($termo){
            $data['resultados'] = $this->model->buscarProduto($termo);
            $data['busca_keyword'] = urldecode(html_entity_decode(utf8_decode($termo)));

            if($data['resultados']){
                foreach ($data['resultados'] as $key => $value) {
                    $value->produto = $this->model->pegarProduto($value->id, $termo);
                }
            }

            $this->model->adicionarEstatisticas($termo);

            $this->load->view('fornecedores/produtos', $data);
        }else{
            redirect('fornecedores/index');
        }
    }

    /* BUSCA POR CATEGORIA */
    // Se houver $_POST['busca_categoria'] fazer a busca
    // Se não houver redirecionar para a home
    // Na tela de resultados cada resultado é um link para o detalhe da empresa
    function categoria($termo_arg = false){
        $data['categorias'] = array(
            'mobiliario' => 'MOBILIÁRIO',
            'componentes' => 'COMPONENTES',
            'arquishow' => 'ARQUISHOW',
            'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
            'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS',
            'todos' => 'Todas as Categorias'
        );

        $termo = $termo_arg ? $termo_arg : $this->input->post('busca_categoria');

        if (!in_array($termo, array('mobiliario', 'componentes', 'arquishow', 'hr', 'construtoras', 'todos', 'arquitetos')))
            redirect('fornecedores/index');

        $num_letras = $this->model->palavrasPorCategoria($termo, TRUE);
        $data['num_linhas_por_colunas'] = $num_letras > 3 ? ceil($num_letras / 3) : 3;

        $data['resultados'] = $this->model->palavrasPorCategoria($termo);
        $data['titulo_categoria'] = $data['categorias'][$termo_arg];
        $data['categoria_selecionada'] = $termo;
        $this->load->view('fornecedores/categoria', $data);
	}

	/* BUSCA POR EMPRESA */
	// $termo = busca || letra
	function empresa($tipo = 'busca', $termo = ''){
        $data['categorias'] = array(
            'mobiliario' => 'MOBILIÁRIO',
            'componentes' => 'COMPONENTES',
            'arquishow' => 'ARQUISHOW',
            'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
            'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS'
        );

        if($tipo == 'busca'){
            $termo = $this->input->post('busca_empresa');
            $data['busca_empresa'] = $termo;
            $data['busca_letra'] = false;
            $data['resultados'] = $this->model->buscarEmpresaPorNome(urldecode(html_entity_decode($termo)));
        }elseif($tipo == 'letra'){
            $data['busca_empresa'] = false;
            $data['busca_letra'] = $termo;
            $data['resultados'] = $this->model->buscarEmpresaPorLetra(urldecode(html_entity_decode($termo)));
        }else{
            redirect('fornecedores/empresa/busca');
        }

        $this->load->view('fornecedores/empresas', $data);        
	}

    function detalhes($id = false){

        $data['categorias'] = array(
            'mobiliario' => 'MOBILIÁRIO',
            'componentes' => 'COMPONENTES',
            'arquishow' => 'ARQUISHOW',
            'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
            'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS'
        );

        if(!$id)
            redirect('fornecedores/index');

        $detalhes = $this->model->pegarPorId($id);

        if(!$detalhes)
            redirect('fornecedores/index');

        $data['detalhes'] = $detalhes;
        $data['keywords'] = $this->model->pegarRelKeywords($id);
        $data['imagens'] = $this->model->pegarImagens($id);

        $this->hasLayout = FALSE;
        $this->load->view('fornecedores/detalhes-modal', $data);        
    }

    // TIPO : CATALOGO || COTACAO
    function solicitacao($tipo = false, $id_fornecedor = false){
        $this->hasLayout = FALSE;

        // Verifica se é solicitação múltipla
        if(strpos($id_fornecedor, '_') !== FALSE){
            $detalhes = $this->model->pegarMultiplosPorId($id_fornecedor);
        }else{
            $detalhes = $this->model->pegarPorId($id_fornecedor);
        }

        if($detalhes){
            if($tipo == 'catalogo')
                echo $this->load->view('fornecedores/solicitacao-catalogo', array('detalhes' => $detalhes), TRUE);
            elseif($tipo == 'cotacao')
                echo $this->load->view('fornecedores/solicitacao-cotacao', array('detalhes' => $detalhes), TRUE);
        }
    }

    function enviarSolicCotacao(){
        $this->hasLayout = FALSE;
        $this->model->inserirPedidoCotacao();
        $this->load->view('fornecedores/solicitacao-efetuada');
    }
    
    function enviarSolicCatalogo(){
        $this->hasLayout = FALSE;
        $this->model->inserirPedidoCatalogo();
        $this->load->view('fornecedores/solicitacao-efetuada');
    }
}