<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('banners_model', 'banners');
   		$this->load->model('destaques_model', 'destaques');
   		$this->load->model('eventos_model', 'eventos');
   		$this->load->model('publicacoes_model', 'publicacoes');
   		$this->load->model('noticias_model', 'noticias');        
    }

    function index(){
    	$data['banners'] = $this->banners->pegarTodos();
    	$destaques = $this->destaques->pegarTodos();

        $data['destaque1'] = false;
        $data['destaque2'] = false;
    	foreach ($destaques as $key => $value) {
            if($value->id_eventos_1){
                $data['destaque1'] = $this->montaChamadaPorId($value->id_eventos_1);
            }else{
                if($value->chamada_1_titulo && $value->chamada_1_subtitulo){
                    $data['destaque1'] = $this->montaChamadaSimples($value->chamada_1_titulo,$value->chamada_1_subtitulo,$value->chamada_1_link);
                }else{
                    $data['destaque1'] = false;
                }
            }
            if($value->id_eventos_2){
        		$data['destaque2'] = $this->montaChamadaPorId($value->id_eventos_2);
            }else{
                if($value->chamada_2_titulo && $value->chamada_2_subtitulo){
                    $data['destaque2'] = $this->montaChamadaSimples($value->chamada_2_titulo,$value->chamada_2_subtitulo,$value->chamada_2_link); 
                }else{
                    $data['destaque2'] = false;
                }
            }
    	}

    	$data['publicacoes'] = $this->publicacoes->pegarRecentes();
    	$data['noticias'] = $this->noticias->pegarRecentes();
    	$data['agenda'] = $this->eventos->pegarProximos();

        $data['anuncio_topo'] = $this->anuncios->imprimir('home-topo');
        $data['anuncio_inferior'] = $this->anuncios->imprimir('home-inferior');
        $data['anuncio_parceiros'] = $this->anuncios->imprimir('parceiros');

    	$this->load->view('home', $data);
    }

    private function montaChamadaPorId($id){
        $evento = $this->eventos->pegarPorId($id);
        $retorno  = "<a href='eventos/detalhes/".$evento->id."' class='link-chamada' title='".$evento->titulo."'>";
        $retorno .= $evento->titulo."<br>".formataMesAno($evento->data)." &bull; ".$evento->local;
        $retorno .= "</a>";
        return $retorno;
    }

    private function montaChamadaSimples($titulo,$subtitulo,$link){
        $retorno = "";
        $retorno  = "<a href='".$link."' class='link-chamada' title='".$titulo."'>";
        $retorno .= $titulo."<br>".$subtitulo;
        $retorno .= "</a>";
        return $retorno;
    }

}

