<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Eventos";
		$this->unidade = "Evento";
		$this->load->model('eventos_model', 'model');
	}

    function index($proximos = 1, $pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/".$proximos."/index/"),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados($proximos)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag, $proximos);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = ($proximos) ? 'Próximos '.$this->titulo : $this->titulo.' passados';
        $data['unidade'] = $this->unidade;
        $data['proximos'] = $proximos;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);

            $data['titulo'] = 'Alterar Evento';
            $data['registro']->data = formataData($data['registro']->data, 'mysql2br');
        }else{
            $data['registro'] = FALSE;
            $data['titulo'] = 'Inserir Evento';
        }

        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function imagens($id_parent, $id_imagem =  FALSE){
        if($id_imagem){
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['registros'] = $this->model->imagens($id_parent);
            $data['registro'] = $this->model->imagens($id_parent, $id_imagem);
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['registros'] = $this->model->imagens($id_parent);
            $data['registro'] = FALSE;
        }

        if(isset($data['parent']->titulo))
            $titulo_atual = $data['parent']->titulo;
        elseif(isset($data['parent']->nome))
            $titulo_atual = $data['parent']->nome;
        else
            $titulo_atual = $this->titulo;

        $data['campo_1'] = "Imagem";
        $data['titulo'] = 'Galeria de imagens de : '.$titulo_atual;
        $data['unidade'] = $this->unidade;
        $data['id_evento'] = $data['parent']->id;
        $this->load->view('painel/'.$this->router->class.'/imagens', $data);
    }
//120x120 crop
//verticais cortar no topo
//ampliado lightbox
    function inserirImagem(){
        if($this->model->inserirImagem()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_evento'), 'refresh');
    }

    function editarImagem($id_imagem){
        if($this->model->editarImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_evento'), 'refresh');
    }

    function excluirImagem($id_imagem, $id_album){
        if($this->model->excluirImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$id_album, 'refresh');
    }
}