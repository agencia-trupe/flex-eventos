<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('painel/home');
   }

	function gravaOrdem(){
        $menu = $this->input->post('data');
        for ($i = 0; $i < count($menu); $i++) {
            $this->db->set('ordem', $i)
            		 ->where('id', $menu[$i])
            		 ->update($this->input->post('tabela'));
        }
	} 

  function buscarSinonimos(){
    $palavra = $this->input->post('palavra');

    $qry_palavra = $this->db->get_where('fornecedores_keywords', array('palavras' => trim(mb_strtolower($palavra))))->result();

    if(!isset($qry_palavra[0])){
      echo "0";
    }else{
      $qry = <<<QRY
SELECT palavras.* FROM trupe_fornecedores_keywords_sinonimos sinonimos
LEFT JOIN trupe_fornecedores_keywords palavras ON sinonimos.keyword_2 = palavras.id
WHERE sinonimos.keyword_1 = '{$qry_palavra[0]->id}'
ORDER BY palavras.palavras DESC
QRY;
      $query = $this->db->query($qry)->result();

      $retorno = "";
      foreach ($query as $key => $value) {
        $retorno .= ($key==0) ? $value->palavras : ', '.$value->palavras;
      }
      echo $retorno;
    }
  }
}
