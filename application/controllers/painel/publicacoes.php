<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publicacoes extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Publicações";
		$this->unidade = "Publicação";
		$this->load->model('publicacoes_model', 'model');
		$this->load->model('revistas_model', 'revistas');
	}

    function index($id_publicacao = 0, $pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/index/".$id_publicacao.'/'),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados($id_publicacao)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        if($id_publicacao == 0)
        	$data['registros'] = $this->model->pegarPaginado($id_publicacao, $pag_options['per_page'], $pag);
        else
        	$data['registros'] = $this->model->pegarPaginado($id_publicacao, 1000, 0);

        foreach ($data['registros'] as $key => $value) {
        	$value->revista = $this->revistas->pegarTitulo($value->id_publicacoes_revistas);
        }

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['id_publicacao'] = $id_publicacao;
        $data['revistas'] = $this->revistas->pegarTodos();
        $data['titulo'] = ($id_publicacao == 0) ? $this->titulo : $this->titulo.' - '.$this->revistas->pegarTitulo($id_publicacao);
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['revistas'] = $this->revistas->pegarTodos();
        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

}