<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Premio extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Prêmio";
		$this->unidade = "Prêmio";
		$this->load->model('premios_model', 'model');
	}

}