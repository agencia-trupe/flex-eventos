<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revistas extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Revistas";
		$this->unidade = "Revista";
		$this->load->model('revistas_model', 'model');
	}

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);

            $data['titulo'] = 'Alterar Revista';
        }else{
        	$data['titulo'] = 'Inserir Revista';
            $data['registro'] = FALSE;
        }
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }
}