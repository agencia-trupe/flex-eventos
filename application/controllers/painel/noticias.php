<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Admincontroller {

    function __construct(){
        parent::__construct();

        $this->load->model('noticias_model', 'model');

        $this->titulo = "Notícias";
        $this->unidade = "Notícia";
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
            $data['titulo'] = 'Alterar '.$this->unidade;
        }else{
            $data['registro'] = FALSE;
            $data['titulo'] = 'Inserir '.$this->unidade;
        }

        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }
}