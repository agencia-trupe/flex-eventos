<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaques extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Destaques";
		$this->unidade = "Destaque";
		$this->load->model('destaques_model', 'model');
		$this->load->model('eventos_model', 'eventos');
	}

    function index($pag = 0){

        $data['registros'] = $this->model->pegarTodos();

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        foreach ($data['registros'] as $key => $value) {
            if($value->id_eventos_1){
	           $value->titulo_1 = $this->eventos->pegarTitulo($value->id_eventos_1);
            }elseif($value->chamada_1_titulo !== ''){
                $value->titulo_1 = "Link Simples : ".$value->chamada_1_titulo;
            }else{
                $value->titulo_1 = "Desativado";
            }

            if($value->id_eventos_2){
	           $value->titulo_2 = $this->eventos->pegarTitulo($value->id_eventos_2);
            }elseif($value->chamada_2_titulo !== ''){
                $value->titulo_2 = "Link Simples : ".$value->chamada_2_titulo;
            }else{
                $value->titulo_2 = "Desativado";
            }
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['eventos'] = $this->eventos->pegarTodos('titulo', 'id');
        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }
}