<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inscricoes extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Inscrições";
		$this->unidade = "Inscrição";
		$this->load->model('aprovar_model', 'model');
        $this->load->model('eventos_model', 'eventos');
	}

	function index($status = 0, $pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/index/".$status),
            'per_page' => 20,
            'uri_segment' => 5,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados($status)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarTodos($status, $pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        foreach ($data['registros'] as $key => $value)
        	$value->titulo_evento = $this->model->pegarTituloEvento($value->id_eventos);

        switch ($status) {
            case 0:
                $data['titulo'] = "Inscrições sem Avaliação";
                break;
            case 1:
                $data['titulo'] = "Inscrições Aprovadas";
                break;
            case 2:
                $data['titulo'] = "Inscrições Reprovadas";
                break;
            default:
                redirect('painel/inscricoes/index/1');
                break;
        }
        $data['link'] = "painel/inscricoes/index";
        $data['status'] = $status;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
	}

    function porEvento($id_evento = false, $status = 0, $pag = 0){

        if(!$id_evento)
            redirect('painel/inscricoes/index');

        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$id_evento.'/'.$this->router->class."/index/".$status),
            'per_page' => 20,
            'uri_segment' => 5,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultadosPorEvento($id_evento, $status)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPorEvento($id_evento, $status, $pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $titulo_evento = $this->model->pegarTituloEvento($id_evento);

        foreach ($data['registros'] as $key => $value)
            $value->titulo_evento = $titulo_evento;

        switch ($status) {
            case 0:
                $data['titulo'] = "Evento : ".$titulo_evento." - Inscrições sem Avaliação";
                break;
            case 1:
                $data['titulo'] = "Evento : ".$titulo_evento." - Inscrições Aprovadas";
                break;
            case 2:
                $data['titulo'] = "Evento : ".$titulo_evento." - Inscrições Reprovadas";
                break;
            default:
                redirect('painel/inscricoes/index/');
                break;
        }
        $data['link'] = "painel/inscricoes/porEvento/".$id_evento;
        $data['status'] = $status;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['registro']->titulo_evento = $this->model->pegarTituloEvento($data['registro']->id_eventos);

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/detalhes', $data);
    }

	function aprovarInsc($id){
        if($this->model->aprovar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Inscrição aprovada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao aprovar a inscrição');
        }
        $this->enviaEmail($id, 'aprovado');
        redirect('painel/'.$this->router->class.'/index', 'refresh');
	}

	function reprovarInsc($id){
        $motivo = $this->input->post('texto_reprovacao');
        if(isset($motivo) && $motivo && $motivo != '' && strlen($motivo) > 3){
    		if($this->model->reprovar($id)){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', 'Inscrição reprovada com sucesso');
                $this->enviaEmail($id, 'reprovado');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao reprovar a inscrição');
            }
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Informe o motivo da reprovação');
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
	}

    function extrair(){
        $data['eventos'] = $this->eventos->pegarTodos();
        $this->load->view('painel/inscricoes/extrair', $data);
    }

    function download(){
        $this->hasLayout = FALSE;
        $this->load->dbutil();

        $id_evento = $this->input->post('id_evento');
        $aprovados = $this->input->post('aprovados');

$qry = <<<STR
SELECT
inscricoes.nome as 'Nome Completo',
inscricoes.cpf as 'CPF',
inscricoes.email as 'e-mail',
CONCAT('(',inscricoes.ddd_fone,') ', inscricoes.fone) as 'Telefone',
CONCAT('(',inscricoes.ddd_celular,') ', inscricoes.celular) as 'Celular',
inscricoes.cargo as 'Cargo',
inscricoes.escritorio as 'Escritório',
inscricoes.cnpj as 'CNPJ',
inscricoes.endereco as 'Endereço',
inscricoes.bairro as 'Bairro',
inscricoes.cep as 'CEP',
inscricoes.cidade as 'Cidade',
inscricoes.estado as 'Estado',
eventos.titulo as 'Evento',
inscricoes.indicado_por as 'Indicado Por',
(CASE inscricoes.inscricao_aprovada
WHEN 0 THEN 'Aguardando Aprovação'
WHEN 1 THEN 'Aprovada'
WHEN 2 THEN 'Reprovada'
END) as 'Inscrições Aprovadas'
FROM flexeventos.trupe_eventos_inscricoes inscricoes LEFT JOIN trupe_site_eventos eventos ON inscricoes.id_eventos = eventos.id
STR;

        if ($id_evento == 'todos') {
            if($aprovados != 3)
                $qry .= " WHERE inscricoes.inscricao_aprovada = '$aprovados'";

            $filename = "inscricoes_".Date('d-m-Y_H-i-s').'.csv';
        }else{
            if($aprovados == 3)
                $qry .= " WHERE inscricoes.id_eventos = '$id_evento'";
            else
                $qry .= " WHERE inscricoes.id_eventos = '$id_evento' AND inscricoes.inscricao_aprovada = '$aprovados'";

            $qry_evento = $this->eventos->pegarPorId($id_evento);
            $filename = "inscricoes_".url_title($qry_evento->titulo, '_', TRUE).'_'.Date('d-m-Y_H-i-s').'.csv';
        }

        $inscricoes = $this->db->query($qry);

        if($inscricoes->num_rows() == 0){
            $this->session->set_flashdata('zero_cadastros', true);
            redirect('painel/inscricoes/extrair');
        }

        $delimiter = ";";
        $newline = "\r\n";

        $this->output->set_header('Content-Type: application/force-download');
        $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
        $this->output->set_content_type('text/csv')->set_output(utf8_decode($this->dbutil->csv_from_result($inscricoes, $delimiter, $newline)), "ISO-8859-1", "UTF-8");
    }

	private function enviaEmail($id, $status){
		$emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
        $emailconf['smtp_pass'] = 'p@rt.9al';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'eventos@flexeventos.com.br';
        $fromname = 'Flex Eventos';
        
        $bcc = 'bruno@trupe.net';

        $info = $this->model->pegarPorId($id);
		$indicado_por = $info->indicado_por;
		$nome = $info->nome;
		$cpf = $info->cpf;
		$email = $info->email;
		$ddd_fone = $info->ddd_fone;
		$fone = $info->fone;
		$ddd_celular = $info->ddd_celular;
		$celular = $info->celular;
		$cargo = $info->cargo;
		$escritorio = $info->escritorio;
		$cnpj = $info->cnpj;
		$endereco = $info->endereco;
		$bairro = $info->bairro;
		$cep = $info->cep;
		$cidade = $info->cidade;
		$estado = $info->estado;
        $motivo_reprovacao = $info->texto_reprovacao;

        $to = $email;

		$nome_evento = $this->model->pegarTituloEvento($info->id_eventos);

        $assunto = "Inscrição $nome_evento";

        if($status == 'reprovado'):

        	$email = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Dados da Inscrição no evento $nome_evento</title>
    <meta charset="utf-8">
</head>
<body>
	<h1>Sua inscrição no evento $nome_evento foi <strong>REPROVADA</strong></h1>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Motivo da reprovação : </span>
	<p>
		$motivo_reprovacao
	</p>
    <h1>Dados da Inscrição no evento $nome_evento</h1>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>indicado por : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$indicado_por</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>nome : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cpf : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cpf</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>email : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>fone : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_fone - $fone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>celular : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_celular - $celular</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cargo : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cargo</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>escritório : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$escritorio</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cnpj : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cnpj</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>endereço : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$endereco</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>bairro : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$bairro</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cep : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cep</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cidade/estado : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cidade $estado</span><br>
</body>
</html>
EML;

			$plain = <<<EML
Dados da Inscrição no evento $nome_evento\r\n
Status: Inscrição reprovada\r\n
indicado por : $indicado_por\r\n
nome : $nome\r\n
cpf : $cpf\r\n
email : $email\r\n
fone : $ddd_fone - $fone\r\n
celular : $ddd_celular - $celular\r\n
cargo : $cargo\r\n
escritório : $escritorio\r\n
cnpj : $cnpj\r\n
endereço : $endereco\r\n
bairro : $bairro\r\n
cep : $cep\r\n
cidade/estado : $cidade $estado\r\n
EML;

		else:
        	$email = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Dados da Inscrição no evento $nome_evento</title>
    <meta charset="utf-8">
</head>
<body>
	<h1>Sua inscrição no evento $nome_evento foi <strong>CONFIRMADA</strong></h1>
    <h1>Dados da Inscrição no evento $nome_evento</h1>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>indicado por : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$indicado_por</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>nome : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cpf : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cpf</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>email : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>fone : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_fone - $fone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>celular : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$ddd_celular - $celular</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cargo : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cargo</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>escritório : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$escritorio</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cnpj : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cnpj</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>endereço : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$endereco</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>bairro : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$bairro</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cep : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cep</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>cidade/estado : </span><span style='color:#000;font-size:14px;font-family:Verdana;'>$cidade $estado</span><br>
</body>
</html>
EML;

			$plain = <<<EML
Dados da Inscrição no evento $nome_evento\r\n
Status: Inscrição confirmada\r\n
indicado por : $indicado_por\r\n
nome : $nome\r\n
cpf : $cpf\r\n
email : $email\r\n
fone : $ddd_fone - $fone\r\n
celular : $ddd_celular - $celular\r\n
cargo : $cargo\r\n
escritório : $escritorio\r\n
cnpj : $cnpj\r\n
endereço : $endereco\r\n
bairro : $bairro\r\n
cep : $cep\r\n
cidade/estado : $cidade $estado\r\n
EML;
		endif;

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($email);

        $this->email->subject($assunto);
        $this->email->message($email);
        $this->email->set_alt_message($plain);

        $this->email->send();
	}

}