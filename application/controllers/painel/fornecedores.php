<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fornecedores extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Fornecedores";
		$this->unidade = "Fornecedor";
		$this->load->model('fornecedores_model', 'model');
	}

    function index($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/index/"),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        foreach ($data['registros'] as $value) {
            $value->keywords = $this->model->pegarRelKeywords($value->id);
            $value->imagens = $this->model->pegarImagens($value->id);
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);

            if(!$data['registro'])
                redirect('painel/'.$this->router->class);

            // Keywords cadastradas para o Fornecedor
            $data['keywords'] = $this->model->pegarRelKeywords($id);
            foreach ($data['keywords'] as $key => $value) {
                $value->sinonimos = $this->buscarSinonimosPorId($value->id);
            }

            // Imagem cadastradas para o Fornecedor
            $data['imagens'] = $this->model->pegarImagens($id);
            foreach ($data['imagens'] as $key => $value) {
                if($value->keyword1){
                    $k1 = $this->model->pegarKeywords($value->keyword1);
                    $value->keyword1 = ($k1) ? $k1->palavras : '';                    
                }
                if($value->keyword2){
                    $k2 = $this->model->pegarKeywords($value->keyword2);
                    $value->keyword2 = ($k2) ? $k2->palavras : '';
                }
                if($value->keyword3){
                    $k3 = $this->model->pegarKeywords($value->keyword3);
                    $value->keyword3 = ($k3) ? $k3->palavras : '';
                }
            }
            foreach ($data['keywords'] as $key => $value) {
                $value->sinonimos = $this->buscarSinonimosPorId($value->id);
            }
        }else{
            $data['registro'] = FALSE;
        }

        $data['categorias'] = array(
            'mobiliario' => 'MOBILIÁRIO',
            'componentes' => 'COMPONENTES',
            'arquishow' => 'ARQUISHOW',
            'hr' => 'HR &bull; HOSPITAIS E HOTÉIS',
            'construtoras' => 'CONSTRUTORAS',
            'arquitetos' => 'ARQUITETOS'
        );

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $qry_keys = $this->model->pegarKeywords();
        $data['todas_keywords'] = "";
        foreach ($qry_keys as $key => $value) {
            $data['todas_keywords'] .= ($key == 0) ? $value->palavras : ','.$value->palavras;            
        }
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function buscarSinonimosPorId($id){        
        $qry = <<<QRY
SELECT palavras.* FROM trupe_fornecedores_keywords_sinonimos sinonimos
LEFT JOIN trupe_fornecedores_keywords palavras ON sinonimos.keyword_2 = palavras.id
WHERE sinonimos.keyword_1 = '{$id}'
ORDER BY palavras.palavras DESC
QRY;
        $query = $this->db->query($qry)->result();

        $retorno = "";
        foreach ($query as $key => $value) {
            $retorno .= ($key==0) ? $value->palavras : ', '.$value->palavras;
        }
        return $retorno;
    }

    function inserir(){
        if($this->db->get_where('fornecedores', array('email' => $this->input->post('email')))->num_rows() == 0){
            $insert = $this->model->inserir();
            if($insert){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);                
            }
            redirect('/painel/'.$this->router->class.'/index', 'refresh');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Este email já está em uso no Sistema de Fornecedores');
            redirect('/painel/'.$this->router->class.'/form', 'refresh');
        }
    }

    function estatisticas($pag = 0){
        $this->load->library('pagination');

        $data_inicio = $this->input->get('data_inicio');
        $data_final = $this->input->get('data_final');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/estatisticas/0/"),
            'per_page' => 60,
            'uri_segment' => 5,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->listarEstatisticas(TRUE, $data_inicio, $data_final),
            'suffix' => '?'.$_SERVER['QUERY_STRING']
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->listarEstatisticas(FALSE, $data_inicio, $data_final, $pag_options['per_page'], $pag);

        foreach ($data['registros'] as $key => $value) {
            $value->cadastrada = $this->model->palavraCadastrada($value->palavra_buscada);
        }

        $data['filtro_data_inicio'] = $data_inicio;
        $data['filtro_data_final'] = $data_final;
        $data['titulo'] = "Estatísticas de keywords";

        if($data_inicio && !$data_final)
            $data['titulo'] .= " a partir de $data_inicio";
        if(!$data_inicio && $data_final)
            $data['titulo'] .= " até $data_final";
        if($data_inicio && $data_final)
            $data['titulo'] .= " de $data_inicio até $data_final";
        
        $this->load->view('painel/'.$this->router->class.'/estatisticas', $data);
    }

    function sinonimos(){
        $data['registros'] = $this->model->pegarKeywords();
        $this->load->view('painel/'.$this->router->class.'/sinonimos', $data);
    }

    function sinonimosDetalhes($id = false){
        $this->hasLayout = FALSE;

        if(!$id)
            return false;
        else{
            $data['listaPalavras'] = $this->model->pegarKeywords();
            $data['palavraSelecionada'] = $this->model->pegarKeywords($id);

            $qrySinonimos = $this->model->pegarSinonimos($id);
            $data['listaSinonimos'] = array();
            foreach ($qrySinonimos as $key => $value)
                $data['listaSinonimos'][] = $value->keyword_2;
            
            $this->load->view('painel/'.$this->router->class.'/detalhes-sinonimos', $data);
        }
    }

    function salvarSinonimos($id){
        $this->hasLayout = false;
        if($this->model->salvarSinonimos($id))
            echo "<div class='alert alert-block alert-success fade in' data-dismiss='alert'>Sinônimos gravados com sucesso</div>";
        else
            echo "<div class='alert alert-block alert-error fade in' data-dismiss='alert'>Erro ao gravar Sinônimos</div>";        
    }

}