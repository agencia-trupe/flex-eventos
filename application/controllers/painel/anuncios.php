<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anuncios extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Anúncios";
		$this->unidade = "Anúncio";
		$this->load->model('anuncios_model', 'model');
	}

	// Tipos:
	// top
	// home (destaque, half banner, button, publiespecial)
    //       1 destaque + 2 half banners || 2 destaques
    //       1 button || 1 publiespecial
	// eventos
	// publicacoes
	// noticias
	// parceiros
	public function index($tipo = false)
	{
		if(!$tipo)
			$tipo = 'top';

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['unidade'] = $this->unidade;
        $data['tipo'] = $tipo;

        switch ($tipo) {
        	case 'top':
		        $data['registros'] = $this->model->pegarTodos('top');
		        $data['historico'] = $this->model->pegarHistorico('top');
		        $data['titulo'] = $this->titulo . ' - Top Banner';
		        $this->load->view('painel/'.$this->router->class.'/lista-top-banner', $data);		
        		break;
        	case 'home':
            case 'destaque':
            case 'halfbanner':
            case 'halfbanner2':
            case 'button':
            case 'publiespecial':
        		$data['destaques'] = $this->model->pegarTodos('destaque');
                $data['destaques_historico'] = $this->model->pegarHistorico('destaque');
                $data['halfbanner'] = $this->model->pegarTodos('halfbanner');
                $data['halfbanner_historico'] = $this->model->pegarHistorico('halfbanner');
                $data['button'] = $this->model->pegarTodos('button');
                $data['button_historico'] = $this->model->pegarHistorico('button');
                $data['publiespecial'] = $this->model->pegarTodos('publiespecial');
                $data['publiespecial_historico'] = $this->model->pegarHistorico('publiespecial');
		        $data['titulo'] = $this->titulo . ' - Banners Home';
		        $this->load->view('painel/'.$this->router->class.'/lista-home', $data);
        		break;
        	case 'eventos':
        		$data['registros'] = $this->model->pegarTodos('eventos');
                $data['historico'] = $this->model->pegarHistorico('eventos');
		        $data['titulo'] = $this->titulo . ' - Banners Square Eventos';
		        $this->load->view('painel/'.$this->router->class.'/lista-simples', $data);
        		break;
        	case 'publicacoes':
        		$data['registros'] = $this->model->pegarTodos('publicacoes');
                $data['historico'] = $this->model->pegarHistorico('publicacoes');
		        $data['titulo'] = $this->titulo . ' - Banners Especial Publicações';
		        $this->load->view('painel/'.$this->router->class.'/lista-simples', $data);
        		break;
        	case 'noticias':
        		$data['registros'] = $this->model->pegarTodos('noticias');
                $data['historico'] = $this->model->pegarHistorico('noticias');
		        $data['titulo'] = $this->titulo . ' - Banners Square Notícias';
		        $this->load->view('painel/'.$this->router->class.'/lista-simples', $data);
        		break;
        	case 'parceiros':
        		$data['registros'] = $this->model->pegarTodos('parceiros');
                $data['historico'] = $this->model->pegarHistorico('parceiros');
		        $data['titulo'] = $this->titulo . ' - Parceiros';
		        $this->load->view('painel/'.$this->router->class.'/lista-simples', $data);
        		break;
        }        
	}

	public function form($tipo = false, $id = false)
	{
		if(!$tipo)
			$tipo = 'top';

        $data['unidade'] = $this->unidade;
        $data['tipo'] = $tipo;
        $data['registro'] = ($id) ? $this->model->pegarPorId($id) : FALSE;
        
        switch ($tipo) {
        	case 'top':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Top Banner' : $this->titulo . ' - Adicionar Top Banner';
		        $data['dimensoes'] = "830x90";		        
        		break;
        	case 'destaque':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Banner Destaque' : $this->titulo . ' - Adicionar Banner Destaque';
                $data['dimensoes'] = "220x300";
                break;
            case 'halfbanner':
            case 'halfbanner2':
                $data['titulo'] = ($id) ? $this->titulo . ' - Alterar Half Banner' : $this->titulo . ' - Adicionar Half Banner';
                $data['dimensoes'] = "220x145";
                break;
            case 'button':
                $data['titulo'] = ($id) ? $this->titulo . ' - Alterar Banner Button' : $this->titulo . ' - Adicionar Banner Button';
                $data['dimensoes'] = "350x90";
                break;
            case 'publiespecial':
                $data['titulo'] = ($id) ? $this->titulo . ' - Alterar Publi Especial' : $this->titulo . ' - Adicionar Publi Especial';
                $data['dimensoes'] = "350x500";
                break;
        	case 'eventos':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Banner Square Eventos' : $this->titulo . ' - Adicionar Banner Square Eventos';
                $data['dimensoes'] = "350x350";
        		break;
        	case 'publicacoes':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Banner Especial Publicações' : $this->titulo . ' - Adicionar Banner Especial Publicações';
                $data['dimensoes'] = "280x380";
        		break;
        	case 'noticias':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Banner Square Notícias' : $this->titulo . ' - Adicionar Banner Square Notícias';
                $data['dimensoes'] = "230x230";
        		break;
        	case 'parceiros':
        		$data['titulo'] = ($id) ? $this->titulo . ' - Alterar Parceiro' : $this->titulo . ' - Adicionar Parceiro';
                $data['dimensoes'] = "160x90";
        		break;
        }
        $this->load->view('painel/'.$this->router->class.'/form', $data); 
	}

    public function calhau($tipo = false)
    {

        if(!$tipo)
            redirect("painel/anuncios/index/top");

        $data['tipo'] = $tipo;
        $data['registro'] = $this->model->pegarCalhau($tipo);

        switch ($tipo) {
            case 'top':                
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Top Banner';
                $data['dimensoes'] = "830x90";
                break;
            case 'destaque':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Banner Destaque';
                $data['dimensoes'] = "220x300";
                break;
            case 'halfbanner':                
            case 'halfbanner2':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Half Banner';
                $data['dimensoes'] = "220x145";
                break;
            case 'button':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Banner Button';
                $data['dimensoes'] = "350x90";
                break;
            case 'publiespecial':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Publi Especial';
                $data['dimensoes'] = "350x500";
                break;
            case 'eventos':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Square Eventos';
                $data['dimensoes'] = "350x350";
                break;
            case 'publicacoes':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Especial Publicações';
                $data['dimensoes'] = "280x380";
                break;
            case 'noticias':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Square Notícias';
                $data['dimensoes'] = "230x230";
                break;
            case 'parceiros':
                $data['titulo'] = $this->titulo . ' - Alterar Calhau Parceiros';
                $data['dimensoes'] = "160x90";
                break;       
        }
        $this->load->view('painel/'.$this->router->class.'/calhau', $data);
    }

    public function inserir($tipo = false)
    {
        
        if(!$tipo)
            redirect("painel/anuncios/index/top");

        if($this->model->inserir($tipo)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$tipo, 'refresh');
    }

    public function inserirCalhau($tipo = false)
    {
        
        if(!$tipo)
            redirect("painel/anuncios/index/top");

        if($this->model->inserirCalhau($tipo)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Calhau inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir calhau');
        }

        redirect('painel/'.$this->router->class.'/index/'.$tipo, 'refresh');
    }    

    public function alterar($tipo = false, $id = false)
    {
        
        if(!$tipo || !$id)
            redirect("painel/anuncios/index/top");

        if($this->model->atualizar($tipo, $id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$tipo, 'refresh');
    }

    public function excluir($tipo = false, $id = false)
    {
        if(!$tipo || !$id)
            redirect("painel/anuncios/index/top");

        if($this->model->excluir($tipo, $id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$tipo, 'refresh');
    }

    public function ativarAnuncio()
    {   
        $this->hasLayout = false;
        $id = $this->input->post('ativar');
        $tipo = $this->input->post('tipo');
        echo $this->model->ativar($tipo, $id);        
    }

    public function desativarAnuncio()
    {   
        $this->hasLayout = false;
        $id = $this->input->post('desativar');
        $tipo = $this->input->post('tipo');
        echo $this->model->desativar($tipo, $id);
    }

}