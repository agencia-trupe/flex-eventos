<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publicacoes extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('revistas_model', 'revistas');
   		$this->load->model('publicacoes_model', 'publicacoes');
    }

	function index(){
   		$data['revistas'] = $this->revistas->pegarTodos();
   		$data['publicacoes'] = $this->publicacoes->pegarTodos();
        $data['anuncio_publicacoes'] = $this->anuncios->imprimir('publicacoes');

   		foreach ($data['revistas'] as $key => $value)
   			$value->capa = $this->publicacoes->pegarCapa($value->id);


        $this->load->view('publicacoes/index', $data);
    }
}