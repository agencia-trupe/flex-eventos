<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
   	}

   	public function logClick()
   	{
   		$this->load->model('anuncios_model', 'model');
   		$this->model->logClick();
   	}

}
