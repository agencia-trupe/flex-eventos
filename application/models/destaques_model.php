<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaques_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "site_destaques";

		$this->dados = array(
			'id_eventos_1',
			'id_eventos_2',
			'chamada_1_titulo',
			'chamada_1_subtitulo',
			'chamada_1_link',
			'chamada_2_titulo',
			'chamada_2_subtitulo',
			'chamada_2_link',
		);
		$this->dados_tratados = array();
	}

}