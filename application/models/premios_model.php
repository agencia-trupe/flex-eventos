<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Premios_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'premios';

		$this->dados = array(
			'titulo',
			'inscricoes_abertas',
			'imagem',
			'detalhe_data',
			'subtitulo',
			'texto_col_esquerda',
			'texto_col_direita',
			'texto_caixa_inscricao',
			'thumb_caixa_inscricao',
			'thumb',
			'regulamento',
			'prancha',
			'texto_link',
			'destino_link',
			'periodo_votacao_popular',
			'periodo_votacao_jurados'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem('userfile'),
			'thumb_caixa_inscricao' => $this->sobeImagem('userfile_caixa'),
			'thumb' => $this->sobeImagem('userfile_thumb'),
			'regulamento' => $this->sobePdf('userfile_regulamento'),
			'prancha' => $this->sobePdf('userfile_prancha')
		);
	}

	function sobeImagem($campo){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/premio/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        if ($campo == 'userfile') {

		        	$this->image_moo
		        	 	->load($original['dir'].$filename)
		         		->resize(700, 700)
		         		->save($original['dir'].$filename, TRUE);

		        } elseif($campo == 'userfile_caixa') {

		        	$this->image_moo
		        	 	->load($original['dir'].$filename)
		         		->resize(360, 360)
		         		->save($original['dir'].$filename, TRUE);

		        } elseif($campo == 'userfile_thumb'){

		        	$this->image_moo
		        	 	->load($original['dir'].$filename)
		         		->resize(180, 180)
		         		->save($original['dir'].$filename, TRUE);

		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function sobePdf($campo){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_pdfs/premio/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'zip|pdf',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}