<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revistas_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'site_publicacoes_revistas';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array(
			'titulo',
			'slug',
			'subtitulo',
			'texto'
		);
		$this->dados_tratados = array(
			'slug' => url_title($this->input->post('titulo'), '_', TRUE)
		);
	}

	function pegarTodos($order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarTitulo($id_revista){
		$qry = $this->pegarPorId($id_revista);
		return (isset($qry)) ? $qry->titulo : 'N/A';
	}
}