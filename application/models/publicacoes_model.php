<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publicacoes_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'site_publicacoes';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array(
			'id_publicacoes_revistas',
			'numero',
			'subtitulo',
			'capa',
			'pdf',
			'data_publicacao'
		);
		$this->dados_tratados = array(
			'capa' => $this->sobeImagem(),
			'pdf' => $this->sobePdf(),
			'data_publicacao' => formataData($this->input->post('data_publicacao'), 'br2mysql')
		);
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		$retorno = $this->db->insert($this->tabela);

		$this->atualizaOrdem($this->db->insert_id(), $this->input->post('id_publicacoes_revistas'));

		return $retorno;
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			$retorno = $this->db->where('id', $id)->update($this->tabela);
			
			$this->atualizaOrdem($id, $this->input->post('id_publicacoes_revistas'));

			return $retorno;
		}else{
			return false;
		}
	}

	private function atualizaOrdem($id_publicacao, $id_publicacoes_revistas){
		$qry = $this->db->where('id_publicacoes_revistas', $id_publicacoes_revistas)
						->where('id !=', $id_publicacao)
						->get($this->tabela)
						->result();

		foreach ($qry as $key => $value)
			$this->db->set('ordem', ((int) $value->ordem + 1))->where('id', $value->id)->update($this->tabela);
	}

	function pegarTodos($id_publicacao = false, $order_campo = 'data_publicacao', $order = 'DESC'){
		if($id_publicacao)
			return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('id_publicacoes_revistas' => $id_publicacao))->result();
		else
			return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarRecentes(){
		return $this->db->order_by('data_publicacao', 'DESC')->get($this->tabela, 5, 0)->result();
	}

	function pegarPaginado($id_publicacao, $por_pagina, $inicio, $order_campo = 'data_publicacao', $order = 'DESC'){
		if($id_publicacao)
			return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('id_publicacoes_revistas' => $id_publicacao), $por_pagina, $inicio)->result();
		else
			return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarCapa($id){
		$qry = $this->db->order_by('data_publicacao', 'DESC')->get_where($this->tabela, array('id_publicacoes_revistas' => $id))->result();
		return (isset($qry[0])) ? $qry[0]->capa : '';
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/publicacoes/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
			if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize_crop(165, 220)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function sobePdf($campo = 'userfile_pdf'){
		
 
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_pdfs/publicacoes/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'pdf',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	return $filename;
		    }
		}else{
		    return false;
		}
	}
}