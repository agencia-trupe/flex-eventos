<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fornecedores_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'fornecedores';
		$this->tabela_imagens = "fornecedores_imagens";

		$this->tabela_keywords = "fornecedores_keywords";
		$this->tabela_rel_keywords = "fornecedores_rel_keywords";
		$this->tabela_estatisticas = "fornecedores_estatisticas_keywords";
		$this->tabela_sinonimos = "fornecedores_keywords_sinonimos";

		$this->tabela_pedidos_cotacao = "fornecedores_pedidos_cotacao";
		$this->tabela_pedidos_catalogo = "fornecedores_pedidos_catalogo";
	}

	function pegarPorId($id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function pegarMultiplosPorId($ids){
		$ids_array = explode('_', $ids);
		$query = "SELECT * FROM trupe_{$this->tabela} WHERE";
		foreach ($ids_array as $key => $value) {
			if($key == 0)
				$query .= " id = '$value'";
			else
				$query .= " OR id = '$value'";
		}
		return $this->db->query($query)->result();
	}

	function pegarRelKeywords($id){
		$qry = <<<QRY
SELECT palavras.* FROM trupe_{$this->tabela_rel_keywords} rel
LEFT JOIN trupe_{$this->tabela_keywords} palavras ON rel.id_keyword = palavras.id
WHERE rel.id_fornecedores = '{$id}'
QRY;
		return $this->db->query($qry)->result();
	}

	function pegarKeywords($id_keyword = false){
		if ($id_keyword) {
			$q = $this->db->get_where($this->tabela_keywords, array('id' => $id_keyword))->result();
			if(isset($q[0]))
				return $q[0];
			else
				return false;
		}else{
			return $this->db->order_by('palavras')->get($this->tabela_keywords)->result();
		}
	}

	function pegarSinonimos($id){
		return $this->db->get_where($this->tabela_sinonimos, array('keyword_1' => $id))->result();
	}

	function salvarSinonimos($id){
		$retorno = false;
		$del1 = $this->db->where('keyword_1', $id)->delete($this->tabela_sinonimos);
		$del2 = $this->db->where('keyword_2', $id)->delete($this->tabela_sinonimos);
		$retorno = $del1 && $del2;
		$sinonimos = $this->input->post('sinonimos');
		if(is_array($sinonimos)){
			foreach ($sinonimos as $key => $value) {
				$retorno1 = $this->db->set('keyword_1', $value)->set('keyword_2', $id)->insert($this->tabela_sinonimos);
				$retorno2 = $this->db->set('keyword_2', $value)->set('keyword_1', $id)->insert($this->tabela_sinonimos);
				$retorno = $retorno1 && $retorno2;
			}
		}
		return $retorno;
	}

	function pegarRelKeywordsID($id){
		return $this->db->get_where($this->tabela_rel_keywords, array('id_fornecedores' => $id))->result();	
	}

	function pegarImagens($id){
		return $this->db->get_where($this->tabela_imagens, array('id_fornecedores' => $id))->result();
	}

	function pegarAleatorios(){
		return $this->db->order_by('id', 'RANDOM')->limit(12)->get($this->tabela)->result();
	}

	function pegarProdutosAleatoriosPorCategoria($categoria){
		$qry = <<<QRY
SELECT forn.id as 'id_fornecedor', imgs.imagem, palavras.palavras FROM trupe_{$this->tabela} forn
RIGHT JOIN trupe_{$this->tabela_imagens} imgs ON forn.id = imgs.id_fornecedores
LEFT JOIN trupe_{$this->tabela_keywords} palavras ON imgs.keyword1 = palavras.id
WHERE forn.categoria = '{$categoria}'
AND imgs.keyword1 != ''
AND palavras.id != ''
GROUP BY palavras.palavras
ORDER BY RAND()
LIMIT 0,3
QRY;
		return $this->db->query($qry)->result();
	}

	function pegarMaisBuscadasPorCategoria($categoria){
		$qry = <<<QRY
SELECT stats.palavra_buscada, COUNT(*) as 'nro_buscas'
FROM trupe_{$this->tabela_estatisticas} stats
JOIN trupe_{$this->tabela_keywords} keywords ON keywords.palavras = stats.palavra_buscada
JOIN trupe_{$this->tabela_rel_keywords} rel ON keywords.id = rel.id_keyword
JOIN trupe_{$this->tabela} forn ON rel.id_fornecedores = forn.id
WHERE forn.categoria = '{$categoria}'
GROUP BY stats.palavra_buscada
ORDER BY nro_buscas DESC, stats.palavra_buscada ASC
LIMIT 0,7
QRY;
		return $this->db->query($qry)->result();
	}

	function palavrasPorCategoria($categoria, $primeira_letra = FALSE){
		if($categoria == 'todos'):
			if($primeira_letra){
				$qry = <<<QRY
SELECT 
DISTINCT LEFT(palavras, 1)
FROM trupe_{$this->tabela_keywords} 
ORDER BY palavras ASC
QRY;
				return $this->db->query($qry)->num_rows();
			}else{
				$qry = <<<QRY
SELECT 
DISTINCT palavras
FROM trupe_{$this->tabela_keywords} 
ORDER BY palavras ASC
QRY;
				return $this->db->query($qry)->result();
			}

		else:
			if($primeira_letra){
						$qry = <<<QRY
SELECT 
DISTINCT LEFT(keywords.palavras, 1)
FROM trupe_{$this->tabela} forn 
LEFT JOIN trupe_{$this->tabela_rel_keywords} rel
ON forn.id = rel.id_fornecedores
JOIN trupe_{$this->tabela_keywords} keywords
ON rel.id_keyword = keywords.id
WHERE forn.categoria = '{$categoria}'
ORDER BY keywords.palavras ASC
QRY;
					return $this->db->query($qry)->num_rows();
					}else{
						$qry = <<<QRY
SELECT 
DISTINCT keywords.palavras
FROM trupe_{$this->tabela} forn 
LEFT JOIN trupe_{$this->tabela_rel_keywords} rel
ON forn.id = rel.id_fornecedores
JOIN trupe_{$this->tabela_keywords} keywords
ON rel.id_keyword = keywords.id
WHERE forn.categoria = '{$categoria}'
ORDER BY keywords.palavras ASC
QRY;
					return $this->db->query($qry)->result();
					}
		endif;
		
	}

	function buscarCategoria($termo){
		return $this->db->get_where($this->tabela, array('categoria' => $termo))->result();
	}

	function pegarTodos(){
		return $this->db->order_by('nome_fantasia', 'asc')->get($this->tabela)->result();
	}

	function pegarListaProdutos(){
		$resultado = array();
		$query = <<<STR
SELECT DISTINCT palavra FROM trupe_{$this->tabela_keywords} ORDER BY palavra ASC
STR;
		$query = $this->db->query($query)->result();
		foreach ($query as $key => $value) {
			$resultado[substr($value->palavra, 0, 1)][] = $value;
		}

		return $resultado;
	}

	function buscarProduto($produto){
		$key = $this->db->get_where($this->tabela_keywords, array('palavras' => $produto))->result();
		if(isset($key[0])){
			$verifica_sinonimos = $this->pegarSinonimos($key[0]->id);
		}else{
			$verifica_sinonimos = false;
		}


		$qry = <<<QRY
SELECT DISTINCT
fornecedores.*
FROM trupe_{$this->tabela} fornecedores 
LEFT JOIN trupe_{$this->tabela_rel_keywords} rel
ON fornecedores.id = rel.id_fornecedores 
LEFT JOIN trupe_{$this->tabela_keywords} keywords
ON rel.id_keyword = keywords.id
LEFT JOIN trupe_{$this->tabela_imagens} imgs ON
fornecedores.id = imgs.id_fornecedores
LEFT JOIN trupe_{$this->tabela_keywords} keys1 ON imgs.keyword1 = keys1.id
LEFT JOIN trupe_{$this->tabela_keywords} keys2 ON imgs.keyword2 = keys2.id
LEFT JOIN trupe_{$this->tabela_keywords} keys3 ON imgs.keyword3 = keys3.id
WHERE (keywords.palavras LIKE '%{$produto}%'
OR keys1.palavras LIKE '%{$produto}%'
OR keys2.palavras LIKE '%{$produto}%'
OR keys3.palavras LIKE '%{$produto}%')
QRY;
		
		if ($verifica_sinonimos) {
			foreach ($verifica_sinonimos as $key => $value) {
				$sinonimo = $this->pegarKeywords($value->keyword_2);
				if($sinonimo){
					$qry .= " OR (keywords.palavras LIKE '%{$sinonimo->palavras}%'
OR keys1.palavras LIKE '%{$sinonimo->palavras}%'
OR keys2.palavras LIKE '%{$sinonimo->palavras}%'
OR keys3.palavras LIKE '%{$sinonimo->palavras}%')";
				}
			}
		}
		$q = $this->db->query($qry)->result();
		return $this->db->query($qry)->result();
	}

	function pegarProduto($id_fornecedor, $termo){
		// Pega o Produto que tem uma legenda LIKE o termo buscado
		$qry = <<<QRY
SELECT * FROM trupe_{$this->tabela_imagens} imgs
LEFT JOIN trupe_{$this->tabela_keywords} keys1 ON imgs.keyword1 = keys1.id
LEFT JOIN trupe_{$this->tabela_keywords} keys2 ON imgs.keyword2 = keys2.id
LEFT JOIN trupe_{$this->tabela_keywords} keys3 ON imgs.keyword3 = keys3.id
WHERE
imgs.id_fornecedores = '{$id_fornecedor}' AND
(keys1.palavras LIKE '%{$termo}%' OR
keys2.palavras LIKE '%{$termo}%' OR
keys3.palavras LIKE '%{$termo}%')
LIMIT 0,1
QRY;

		if($this->db->query($qry)->num_rows() > 0){
			return $this->db->query($qry)->result();
		}else{

			// Pega o primeiro Produto do fornecedor do resultado
			$qry = <<<QRY
SELECT * FROM trupe_{$this->tabela_imagens} imgs
WHERE
imgs.id_fornecedores = '{$id_fornecedor}'
LIMIT 0,1
QRY;
			return $this->db->query($qry)->result();
		}
	}

    function inserir(){
        $nome_fantasia = $this->input->post('nome_fantasia');
        $razao_social = $this->input->post('razao_social');
        $endereco = $this->input->post('endereco');
        $numero = $this->input->post('numero');
        $complemento = $this->input->post('complemento');
        $bairro = $this->input->post('bairro');
        $cidade = $this->input->post('cidade');
        $estado = $this->input->post('estado');
        $cep = $this->input->post('cep');
        $telefone = $this->input->post('telefone');
        $website = $this->input->post('website');
        $categoria = $this->input->post('categoria');
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $apresentacao = $this->input->post('apresentacao');

        if($razao_social)
            $this->db->set('razao_social', $razao_social);
        if($endereco)
            $this->db->set('endereco', $endereco);
        if($numero)
            $this->db->set('numero', $numero);
        if($complemento)
            $this->db->set('complemento', $complemento);
        if($bairro)
            $this->db->set('bairro', $bairro);
        if($cidade)
            $this->db->set('cidade', $cidade);
        if($estado)
            $this->db->set('estado', $estado);
        if($cep)
            $this->db->set('cep', $cep);
        if($telefone)
            $this->db->set('telefone', $telefone);
        if($website)
            $this->db->set('website', $website);
        if($categoria)
            $this->db->set('categoria', $categoria);
        if($apresentacao)
            $this->db->set('apresentacao', $apresentacao);

        $this->db->set('nome', $nome);
        $this->db->set('nome_fantasia', $nome_fantasia);
        $this->db->set('email', $email);
        $this->db->set('senha', cripto($senha));
        $imagem = $this->sobeImagem();

        if($imagem)
            $this->db->set('imagem', $imagem);

        $insert = $this->db->insert($this->tabela);

        $this->enviaEmailCadastro($nome, $email, $senha);

        if($insert)
        	return $insert;
        else
        	return $this->db->last_query();        
    }

	function alterar($id){
		$nome_fantasia = $this->input->post('nome_fantasia');
		$razao_social = $this->input->post('razao_social');
		$endereco = $this->input->post('endereco');
		$numero = $this->input->post('numero');
		$complemento = $this->input->post('complemento');
		$bairro = $this->input->post('bairro');
		$cidade = $this->input->post('cidade');
		$estado = $this->input->post('estado');
		$cep = $this->input->post('cep');
		$telefone = $this->input->post('telefone');
		$website = $this->input->post('website');
		$categoria = $this->input->post('categoria');
		$nome = $this->input->post('nome');
		$email = $this->input->post('email'); // Não pode ser alterado
		$apresentacao = $this->input->post('apresentacao');

		$imagem = $this->sobeImagem();
		if($imagem)
			$this->db->set('imagem', $imagem);

		$update = $this->db->set('nome_fantasia', $nome_fantasia)
							->set('razao_social', $razao_social)
							->set('endereco', $endereco)
							->set('numero', $numero)
							->set('complemento', $complemento)
							->set('bairro', $bairro)
							->set('cidade', $cidade)
							->set('estado', $estado)
							->set('cep', $cep)
							->set('telefone', $telefone)
							->set('website', $website)
							->set('categoria', $categoria)
							->set('nome', $nome)
							->set('apresentacao', $apresentacao)
							->where('id', $id)
							->update($this->tabela);

		$this->db->where('id_fornecedores', $id)->delete($this->tabela_rel_keywords);
		
		$keywords = json_decode($this->input->post('keywords'));
		if($keywords && is_array($keywords)){
			foreach ($keywords as $key => $value) {

				$value = mb_strtolower(trim($value));

				// Verifica se já existe registro da palavra
				$check = $this->db->get_where($this->tabela_keywords, array('palavras' => $value))->result();

				// Caso não haja, inserir na tabela
				if(isset($check[0])){
					$id_palavra = $check[0]->id;
				}else{
					$this->db->set('palavras', $value)->insert($this->tabela_keywords);
					$id_palavra = $this->db->insert_id();
				}

				// Cadastra relação
				$this->db->set('id_keyword', $id_palavra)
					 ->set('id_fornecedores', $id)
					 ->insert($this->tabela_rel_keywords);
			}
		}

		$imagens = json_decode($this->input->post('imagens'));
		$this->db->where('id_fornecedores', $id)->delete($this->tabela_imagens);
		if ($imagens && is_array($imagens)) {
			foreach ($imagens as $key => $value) {

				if($value->keyword1){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword1))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword1 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword1)))->insert($this->tabela_keywords);
						$keyword1 = $this->db->insert_id();
					}
				}else{
					$keyword1 = false;
				}

				if($value->keyword2){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword2))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword2 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword2)))->insert($this->tabela_keywords);
						$keyword2 = $this->db->insert_id();
					}
				}else{
					$keyword2 = false;
				}

				if($value->keyword3){
					// Verifica se já existe registro da palavra
					$check = $this->db->get_where($this->tabela_keywords, array('palavras' => mb_strtolower(trim($value->keyword3))))->result();

					// Caso não haja, inserir na tabela
					if(isset($check[0])){
						$keyword3 = $check[0]->id;
					}else{
						$this->db->set('palavras', mb_strtolower(trim($value->keyword3)))->insert($this->tabela_keywords);
						$keyword3 = $this->db->insert_id();
					}
				}else{
					$keyword3 = false;
				}

				if($keyword1)
					$this->db->set('keyword1', $keyword1);
				if($keyword2)
					$this->db->set('keyword2', $keyword2);
				if($keyword3)
					$this->db->set('keyword3', $keyword3);

				$this->db->set('imagem', $value->imagem)
						 ->set('legenda', $value->legenda)
						 ->set('id_fornecedores', $id)
						 ->insert($this->tabela_imagens);
			}
		}

		return $update;
	}

	function sobeImagem($campo = 'imagem'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/anuario/marcas/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
                      ->load($original['dir'].$filename)
                      ->set_background_colour('#FFFFFF')
                      ->resize(204,45,true)
                      ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	private function enviaEmailCadastro($nome, $email, $senha){
        if($nome && $email){
            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
            $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
            $emailconf['smtp_pass'] = 'p@rt.9al';

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'anuario@flexeventos.com.br';
            $fromname = 'Flex Eventos';
            $to = $email;
            $bcc = 'bruno@trupe.net';
            $assunto = 'Cadastro no Sistema de Fornecedores Flex';

            $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de confirmação - Cadastro de Fornecedores</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
		$nome, Você foi cadastrado no Sistema de Fornecedores Flex.
    </p>
    <p>
		Seus dados para acesso são: <br>
		Usuário : $email <br>
		Senha : $senha <br>
    </p>
    <p>
		Acesse o endereço: <a href="http://www.flexeventos.com.br/cadastro-fornecedores">http://www.flexeventos.com.br/cadastro-fornecedores</a> para efetuar o Login e complementar suas informações de cadastro.
    </p>
    <p>
        Gratos<br>
        Flex Editora
    </p>
</body>
</html>
EML;

            $plain = <<<EML
$nome, Você foi cadastrado no Sistema de Fornecedores Flex.\r\n
Seus dados para acesso são:\r\n
Usuário : $email\r\n
Senha : $senha\r\n
Acesse o endereço: http://www.flexeventos.com.br/cadastro-fornecedores para efetuar o Login e complementar suas informações de cadastro.\r\n
Gratos\r\n
Flex Editora
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($from);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            $this->email->send();
        }
	}

	/*
	ESTATÍSTICAS DAS PALAVRAS BUSCADAS
	*/
	function listarEstatisticas($numrows = FALSE, $data_inicio = false, $data_final = false, $per_page = 0, $atual = 0){
		$qry = <<<QRY
SELECT COUNT( * ) AS  `Buscas` , `palavra_buscada`
FROM  `trupe_{$this->tabela_estatisticas}`
QRY;
		
		if($data_inicio) $qry .= " WHERE DATE(data_busca) >= '".formataData($data_inicio, 'br2mysql')."'";

		if($data_final){
			if($data_inicio)
				$qry .= " AND DATE(data_busca) <= '".formataData($data_final, 'br2mysql')."'";
			else
				$qry .= " WHERE DATE(data_busca) <= '".formataData($data_final, 'br2mysql')."'";
		}

		$qry .= "GROUP BY  `palavra_buscada` ORDER BY  `Buscas` DESC";

		if(!$numrows) $qry .= " LIMIT $atual , $per_page";

		$busca = $this->db->query($qry);
		return $numrows ? $busca->num_rows() : $busca->result();
	}

	function adicionarEstatisticas($termo){
		$this->db->set('palavra_buscada', urldecode(html_entity_decode($termo)))
				 ->set('data_busca', Date('Y-m-d H:i:s'))
				 ->set('ip_busca', ip())
				 ->set('user_agent', user_agent())
				 ->insert($this->tabela_estatisticas);
	}

	function palavraCadastrada($palavra){
		$qry = <<<QRY
SELECT DISTINCT forn.* FROM trupe_{$this->tabela} forn
LEFT JOIN trupe_{$this->tabela_rel_keywords} rel ON forn.id = rel.id_fornecedores
LEFT JOIN trupe_{$this->tabela_keywords} key1 ON rel.id_keyword = key1.id
LEFT JOIN trupe_{$this->tabela_imagens} imgs ON forn.id = imgs.id_fornecedores
LEFT JOIN trupe_{$this->tabela_keywords} key2 ON imgs.keyword1 = key2.id
LEFT JOIN trupe_{$this->tabela_keywords} key3 ON imgs.keyword2 = key3.id
LEFT JOIN trupe_{$this->tabela_keywords} key4 ON imgs.keyword3 = key4.id
WHERE key1.palavras = '{$palavra}'
OR key2.palavras = '{$palavra}'
OR key3.palavras = '{$palavra}'
OR key4.palavras = '{$palavra}'
QRY;
		$q = $this->db->query($qry)->num_rows();
		if($palavra == 'pálavrã 1'){
			echo "<pre>";
			print_r($this->db->last_query());
			echo "</pre>";
			die();
		}
		return $this->db->query($qry)->num_rows() > 0;
	}

	function inserirPedidoCotacao(){
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $telefone = $this->input->post('telefone');
        $mensagem = $this->input->post('mensagem');
        $arquivo = $this->sobeArquivoCotacao();
        $ids_fornecedores = $this->input->post('ids_fornecedores');

        if(is_array($ids_fornecedores) && sizeof($ids_fornecedores) > 0){

        	foreach ($ids_fornecedores as $key => $id_fornecedor) {

        		if($arquivo)
            		$this->db->set('arquivo', $arquivo);

				$r = $this->db->set('id_fornecedor', $id_fornecedor)
		        			  ->set('nome', $nome)
		        			  ->set('email', $email)
		        			  ->set('telefone', $telefone)
		        			  ->set('mensagem', $mensagem)
		        			  ->set('data', date('Y-m-d H:i:s'))
		        			  ->set('encaminhado', 0)
		        			  ->insert($this->tabela_pedidos_cotacao);

	        	if($r)
	            	$this->avisarFornecedor($id_fornecedor, 'cotacao');
        	}

        	return $r;

        }else{        	
	        $ret = $this->db->set('id_fornecedor', $ids_fornecedores)
    	            		->set('nome', $nome)
			        		->set('email', $email)
			        		->set('telefone', $telefone)
			        		->set('mensagem', $mensagem)
			        		->set('data', date('Y-m-d H:i:s'))
			        		->set('encaminhado', 0)
                			->insert($this->tabela_pedidos_cotacao);
            
            if($ret)
            	$this->avisarFornecedor($ids_fornecedores, 'cotacao');

            return $ret;
		}
	}

	private function sobeArquivoCotacao($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_pdfs/solicitacoes/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'pdf|doc|docx|xls|xlsx',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	return $filename;
		    }
		}else{
		    return false;
		}
	}

	function inserirPedidoCatalogo(){
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $empresa = $this->input->post('empresa');
        $observacoes = $this->input->post('observacoes');
        $ids_fornecedores = $this->input->post('ids_fornecedores');

        if(is_array($ids_fornecedores) && sizeof($ids_fornecedores) > 0){

        	foreach ($ids_fornecedores as $key => $id_fornecedor) {
				$r = $this->db->set('id_fornecedor', $id_fornecedor)
		        			  ->set('nome', $nome)
		        			  ->set('email', $email)
		        			  ->set('empresa', $empresa)
		        			  ->set('observacoes', $observacoes)
		        			  ->set('data', date('Y-m-d H:i:s'))
		        			  ->set('encaminhado', 0)
		        			  ->insert($this->tabela_pedidos_catalogo);

		        if($r)
	            	$this->avisarFornecedor($id_fornecedor, 'catalogo');
        	}

        	return $r;

        }else{
	        $ret = $this->db->set('id_fornecedor', $ids_fornecedores)
    	            		->set('nome', $nome)
			        		->set('email', $email)
			        		->set('empresa', $empresa)
			        		->set('observacoes', $observacoes)
			        		->set('data', date('Y-m-d H:i:s'))
			        		->set('encaminhado', 0)
                			->insert($this->tabela_pedidos_catalogo);

            if($ret)
            	$this->avisarFornecedor($ids_fornecedores, 'catalogo');

            return $ret;
		}
	}

	public function buscarEmpresaPorNome($termo)
	{
		return $this->db->like('nome_fantasia', $termo)->get($this->tabela)->result();
	}

	public function buscarEmpresaPorLetra($termo)
	{
		return $this->db->like('nome_fantasia', $termo, 'after')->get($this->tabela)->result();
	}

	public function avisarFornecedor($id, $tipo)
	{
		$forn = $this->pegarPorId($id);
		$tipo_extenso = ($tipo == 'cotacao') ? "Cotação" : "Catálogo";

		if($forn){
			$emailconf['charset'] = 'utf-8';
	        $emailconf['mailtype'] = 'html';
	        $emailconf['protocol'] = 'smtp';
	        $emailconf['smtp_host'] = 'smtp.flexeventos.com.br';
	        $emailconf['smtp_user'] = 'portalflex@flexeventos.com.br';
	        $emailconf['smtp_pass'] = 'p@rt.9al';

	        $this->load->library('email');

	        $this->email->initialize($emailconf);

	        $from = 'eventos@flexeventos.com.br';
	        $fromname = 'Flex Eventos';
	        $to = $forn->email;
	        $bcc = 'bruno@trupe.net';
	        $assunto = "Nova Solicitação de $tipo_extenso - Sistema Flex";

	        $emailbody = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Nova Solicitação de $tipo_extenso - Sistema Flex</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
		Você recebeu uma nova solicitação de $tipo_extenso no sistema de Fornecedores Flex.
    </p>
    <p>
		Para ter acesso à solicitação basta efetuar o login em:
    </p>
    <p>
		<a href="http://www.flexeventos.com.br/cadastro-fornecedores">http://www.flexeventos.com.br/cadastro-fornecedores</a>.
    </p>
    <p>
        Gratos<br>
        Flex Editora
    </p>
</body>
</html>
EML;

            $plain = <<<EML
Você recebeu uma nova solicitação de $tipo_extenso no sistema de Fornecedores Flex.\r\n
Para ter acesso à solicitação basta efetuar o login em:\r\n
<a href="http://www.flexeventos.com.br/cadastro-fornecedores">http://www.flexeventos.com.br/cadastro-fornecedores</a>.\r\n
Gratos\r\n
Flex Editora
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($from);

            $this->email->subject($assunto);
            $this->email->message($emailbody);
            $this->email->set_alt_message($plain);

            $this->email->send();
        }
	}
}