<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aprovar_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'eventos_inscricoes';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array();
		$this->dados_tratados = array();
	}

	function numeroResultados($status){
		return $this->db->get_where($this->tabela, array('inscricao_aprovada' => $status))->num_rows();
	}

	// 0 - Não Avaliado
	// 1 - Aprovado
	// 2 - Reprovado
	function pegarTodos($status = 0, $per_page, $pag){
		return $this->db->order_by('data_inscricao', 'desc')->get_where($this->tabela, array('inscricao_aprovada' => $status), $per_page, $pag)->result();
	}

	function aprovar($id){
		return $this->db->set('inscricao_aprovada', 1)->where('id', $id)->update($this->tabela);
	}

	function reprovar($id){
		return $this->db->set('inscricao_aprovada', 2)->set('texto_reprovacao', $this->input->post('texto_reprovacao'))->where('id', $id)->update($this->tabela);
	}

	function pegarTituloEvento($id_evento){
		$qry = $this->db->get_where('site_eventos', array('id' => $id_evento))->result();
		if (isset($qry[0]) && $qry[0]) {
			return $qry[0]->titulo;
		}else{
			return "Evento não encontrado";
		}
	}

	function numeroResultadosPorEvento($id_evento, $status){
		return $this->db->get_where($this->tabela, array('inscricao_aprovada' => $status, 'id_eventos' => $id_evento))->num_rows();
	}

	function pegarPorEvento($id_evento, $status, $per_page, $pag){
		return $this->db->order_by('data_inscricao', 'desc')->get_where($this->tabela, array('inscricao_aprovada' => $status, 'id_eventos' => $id_evento), $per_page, $pag)->result();
	}
}