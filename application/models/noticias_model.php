<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'site_noticias';
        $this->tabela_comentarios = 'noticias_comentarios';

        $this->dados = array('titulo', 'slug', 'data', 'imagem', 'olho', 'texto', 'autor', 'pdf');
        $this->dados_tratados = array(
        	'slug' => url_title($this->input->post('titulo'), '_', TRUE),
        	'data' => formataData($this->input->post('data'), 'br2mysql'),
        	'imagem' => $this->sobeImagem(),
        	'pdf' => $this->sobePdf()
        );
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where($this->tabela, array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$this->db->set('slug', $teste_slug);

		return $this->db->insert($this->tabela);
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			$slug = url_title($this->input->post('titulo'), '_', TRUE);
			$teste_slug = $slug;

			$inicio = 1;
			while($this->db->get_where($this->tabela, array('slug' => $teste_slug, 'id !=' => $id))->num_rows() != 0){
				$teste_slug = $slug.'_'.$inicio;
				$inicio++;
			}
			$this->db->set('slug', $teste_slug);

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function pegarTodos($order_campo = 'data', $order = 'DESC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'data', $order = 'DESC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarLaterais($id_atual){
		return $this->db->order_by('data', 'desc')->get_where($this->tabela, array('id !=' => $id_atual))->result();
	}

	function pegarRecentes(){
		return $this->db->order_by('data', 'desc')->get($this->tabela, 6, 0)->result();
	}

	function pegarComentarios($id, $retornaContagem = FALSE){
		if ($retornaContagem) {
			return $this->db->get_where($this->tabela_comentarios, array('id_noticia' => $id))->num_rows();
		} else {
			$comentarios = $this->db->order_by('data', 'DESC')->get_where($this->tabela_comentarios, array('id_noticia' => $id))->result();
			foreach ($comentarios as $key => $value) {
				$value->autor = $this->pegarAutorComentario($value->autor);
			}
			return $comentarios;
		}
	}

	function pegarComentariosPaginados($id_noticia, $por_pagina, $inicio){
		return $this->db->order_by('data', 'desc')->get_where($this->tabela_comentarios, array('id_noticia' => $id_noticia), $por_pagina, $inicio)->result();
	}

	function pegarAutorComentario($id){
		$qry = $this->db->get_where("cadastros_comentarios", array('id' => $id))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}

	function excluirComentario($id){
		return $this->db->where('id', $id)->delete($this->tabela_comentarios);
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/noticias/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize(723,480)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize_crop(345, 230)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function sobePdf($campo = 'userfile_pdf'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_pdfs/noticias/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'pdf',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	return $filename;
		    }
		}else{
		    return false;
		}
	}
}
