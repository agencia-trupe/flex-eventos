<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anuncios_model extends MY_Model {

	function __construct(){
		parent::__construct();

		date_default_timezone_set('America/Sao_Paulo');		

		$this->tabela = 'anuncios';
		$this->tabela_estatistica = 'anuncios_estatisticas';

		$this->dados = array();
		$this->dados_tratados = array();

		$this->atualizaHistorico();
	}

	/****************************************************************/
	/****************************************************************/
	/*							SELEÇÃO								*/
	/****************************************************************/
	/****************************************************************/

	function pegarTodos($tipo = false)
	{
		$this->db->query("set time_zone= '-3:00';");
		if($tipo){
			
			$query = $this->db->where('tipo', $tipo)
							  ->where('calhau', '0')
							  ->where('delete', '0')
							  ->where('historico' , '0')
							  ->order_by('ativo', 'DESC')
							  ->order_by('data_saida', 'DESC')
							  ->get($this->tabela)
							  ->result();
			
			foreach ($query as $value)
				$value->estatisticas = $this->estatisticas($value->id);
			
			return $query;
		}else
			return false;
	}

	function pegarHistorico($tipo = false)
	{
		$this->db->query("set time_zone= '-3:00';");
		if($tipo){
			
			$query = $this->db->where('tipo', $tipo)
							  ->where('calhau', '0')
							  ->where('delete', '0')
							  ->where('historico' , '1')
							  ->order_by('data_saida', 'DESC')
							  ->get($this->tabela)
							  ->result();

			foreach ($query as $value)
				$value->estatisticas = $this->estatisticas($value->id);

			return $query;
		}else
			return false;
	}

	function pegarAtivos($tipo = false, $random = false, $somenteUm = false)
	{
		$this->db->query("set time_zone= '-3:00';");
		if($tipo){

			if($random)
				$this->db->order_by('id', 'RANDOM');

			$query = $this->db->where('tipo', $tipo)
							->where('calhau', '0')
							->where('delete', '0')
							->where('historico' , '0')
							->where('ativo' , '1')
							->get($this->tabela);

			return ($somenteUm) ? $query->row() : $query->result();
		}else
			return false;
	}

	function pegarAtivosImprimiveis($tipo = false, $random = false, $somenteUm = false)
	{
		$this->db->query("set time_zone= '-3:00';");
		if($tipo){

			if($random)
				$this->db->order_by('id', 'RANDOM');

			$query = $this->db->where('tipo', $tipo)
							->where('calhau', '0')
							->where('delete', '0')
							->where('historico' , '0')
							->where('ativo' , '1')
							->where('data_entrada <= ', 'NOW()', FALSE)
							->get($this->tabela);
							
			return ($somenteUm) ? $query->row() : $query->result();
		}else
			return false;
	}	

	function pegarPorId($id = false)
	{
		$this->db->query("set time_zone= '-3:00';");
		if($id){
			$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;
		}else
			return false;
	}

	function pegarCalhau($tipo, $retornaLista = false)
	{	
		$this->db->query("set time_zone= '-3:00';");
		$qry = $this->db->get_where($this->tabela, array('tipo' => $tipo, 'calhau' => 1))->result();
		if(!$retornaLista){
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;		
		}else{
			return $qry;
		}
	}

	/****************************************************************/
	/****************************************************************/
	/*							INSERÇÃO							*/
	/****************************************************************/
	/****************************************************************/

	public function inserir($tipo)
	{
		$this->db->query("set time_zone= '-3:00';");
        $titulo = $this->input->post("titulo");
        $tipo = $this->input->post("tipo");
        
        $switch_tipo_ad = $this->input->post("switch-tipo-ad"); // img || swf
        
        if($switch_tipo_ad == "img"){

            $userfile_img = $this->sobeImagem("userfile_img", $tipo);
            $link = $this->input->post("link");

            if($userfile_img){
                $this->db->set('imagem', $userfile_img);
                $this->db->set('swf', '');
            }

            $this->db->set('link', $link);
            $this->db->set('tipo_link', $this->input->post('tipo_link'));

        }elseif($switch_tipo_ad == "swf"){

            $userfile_swf = $this->sobeFlash("userfile_swf", $tipo);

            if($userfile_swf){
                $this->db->set('swf', $userfile_swf);
                $this->db->set('imagem', '');
                $this->db->set('tipo_link', '');
            }

        }else{
            return false;
        }
        
        $data_entrada = $this->input->post("data_entrada");
        $data_saida = $this->input->post("data_saida");

        return $this->db->set('titulo', $titulo)
                 		->set('tipo', $tipo)
						->set('data_entrada', formataDT($data_entrada))
						->set('data_saida', formataDT($data_saida))
						->set('ativo', 0)
						->set('calhau', 0)
						->set('delete', 0)
						->set('historico', 0)
						->set('data_insercao', Date("Y-m-d H:i:s"))
						->insert($this->tabela);
	}

	public function inserirCalhau($tipo)
	{
		$this->db->query("set time_zone= '-3:00';");
        $titulo = $this->input->post("titulo");
        $tipo = $this->input->post("tipo");
        
        $switch_tipo_ad = $this->input->post("switch-tipo-ad"); // img || swf
        
        if($switch_tipo_ad == "img"){

            $userfile_img = $this->sobeImagem("userfile_img", $tipo);
            $link = $this->input->post("link");

            if($userfile_img){
                $this->db->set('imagem', $userfile_img);
                $this->db->set('swf', '');
            }

            $this->db->set('link', $link);
            $this->db->set('tipo_link', $this->input->post('tipo_link'));

        }elseif($switch_tipo_ad == "swf"){

            $userfile_swf = $this->sobeFlash("userfile_swf", $tipo);

            if($userfile_swf){
                $this->db->set('swf', $userfile_swf);
                $this->db->set('imagem', '');
                $this->db->set('tipo_link', '');
            }

        }else{
            return false;
        }
        
        $data_entrada = $this->input->post("data_entrada");
        $data_saida = $this->input->post("data_saida");

        return $this->db->set('titulo', $titulo)
                 		->set('tipo', $tipo)
						->set('data_entrada', formataDT($data_entrada))
						->set('data_saida', formataDT($data_saida))
						->set('ativo', 0)
						->set('calhau', 1)
						->set('delete', 0)
						->set('historico', 0)
						->set('data_insercao', Date("Y-m-d H:i:s"))
						->insert($this->tabela);
	}

	/****************************************************************/
	/****************************************************************/
	/*							ATUALIZAÇÃO							*/
	/****************************************************************/
	/****************************************************************/
	public function atualizar($tipo, $id)
	{
		$this->db->query("set time_zone= '-3:00';");
        $titulo = $this->input->post("titulo");
        $tipo = $tipo;
        
        $switch_tipo_ad = $this->input->post("switch-tipo-ad"); // img || swf

        if($switch_tipo_ad == "img"){

            $userfile_img = $this->sobeImagem("userfile_img", $tipo);
            $link = $this->input->post("link");

            if($userfile_img){
                $this->db->set('imagem', $userfile_img);
                $this->db->set('swf', '');
            }

            $this->db->set('link', $link);
            $this->db->set('tipo_link', $this->input->post('tipo_link'));

        }elseif($switch_tipo_ad == "swf"){

            $userfile_swf = $this->sobeFlash("userfile_swf", $tipo);

            if($userfile_swf){
                $this->db->set('swf', $userfile_swf);
                $this->db->set('imagem', '');
                $this->db->set('tipo_link', '');
            }

        }else{
            return false;
        }
        
        $data_entrada = $this->input->post("data_entrada");
        $data_saida = $this->input->post("data_saida");

        return $this->db->set('titulo', $titulo)
                 		->set('data_entrada', formataDT($data_entrada))
						->set('data_saida', formataDT($data_saida))
						->set('data_alteracao', Date("Y-m-d H:i:s"))
						->set('historico', 0)
						->where('id', $id)
						->update($this->tabela);
	}

	public function ativar($tipo, $id)
	{
		return $this->db->set('ativo', 1)->where('id', $id)->update($this->tabela);        
	}

	public function desativar($tipo, $id)
	{
		return $this->db->set('ativo', 0)->where('id', $id)->update($this->tabela);        
	}	

	/****************************************************************/
	/****************************************************************/
	/*							EXCLUSÃO							*/
	/****************************************************************/
	/****************************************************************/
	public function excluir($tipo, $id)
	{
		if($this->pegarPorId($id) !== FALSE){
			return $this->db->set('delete', 1)->where('id', $id)->update($this->tabela);
		}
	}

	/****************************************************************/
	/****************************************************************/
	/*							UPLOADS								*/
	/****************************************************************/
	/****************************************************************/

    public function sobeImagem($campo, $tipo){
		$this->load->library('upload');
		
		$dir = '_anuncios/imagem/'.$tipo.'/';
		
		$uploadconfig = array(
		  'upload_path' => $dir,
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($dir.$arquivo['file_name'] , $dir.$filename);

		        switch ($tipo) {
		        	case 'top':
		        		$x = 830;
		        		$y = 90;
		        		break;
		        	case 'destaque':
		        		$x = 220;
		        		$y = 300;
		        		break;
		        	case 'halfbanner':
		        	case 'halfbanner2':
		        		$x = 220;
		        		$y = 145;
		        		break;
		        	case 'button':
		        		$x = 350;
		        		$y = 90;
		        		break;
		        	case 'publiespecial':
		        		$x = 350;
		        		$y = 500;
		        		break;
		        	case 'eventos':
		        		$x = 350;
		        		$y = 350;
		        		break;
		        	case 'publicacoes':
		        		$x = 280;
		        		$y = 380;
		        		break;
		        	case 'noticias':
		        		$x = 230;
		        		$y = 230;
		        		break;
		        	case 'parceiros':
		        		$x = 160;
		        		$y = 90;
		        		break;
		        }

	        	 $this->image_moo
	        	 		->load($dir.$filename)
	         	  		->resize_crop($x, $y)
	         	  		->save($dir.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
    }

    public function sobeFlash($campo, $tipo){
		$this->load->library('upload');
		
		$dir = '_anuncios/flash/'.$tipo.'/';
		
		$uploadconfig = array(
		  'upload_path' => $dir,
		  'allowed_types' => 'swf',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($dir.$arquivo['file_name'] , $dir.$filename);

		        return $filename;
		    }
		}else{
		    return false;
		}	
    }

	/****************************************************************/
	/****************************************************************/
	/*						CONTROLE DE HISTORICO					*/
	/****************************************************************/
	/****************************************************************/
    public function atualizaHistorico()
    {
    	$this->db->query("SET time_zone = '-3:00';");
    	$this->db->query("UPDATE trupe_anuncios SET historico = 1, ativo = 0 WHERE data_saida <= NOW() AND calhau = 0");
    }

	/****************************************************************/
	/****************************************************************/
	/*						IMPRESSÃO DE ANÚNCIOS					*/
	/****************************************************************/
	/****************************************************************/
	public function imprimir($tipo)
	{
		switch ($tipo) {
			case 'top':
				$ads = $this->pegarAtivosImprimiveis('top', TRUE, TRUE);
				if(!$ads){
					$ads = $this->pegarCalhau('top');
				}
				return $this->_imprimirSimples($ads);
				break;
			case 'home-topo':
				$ads['destaque'] = $this->pegarAtivosImprimiveis('destaque');
				$ads['halfbanner'] = $this->pegarAtivosImprimiveis('halfbanner');

				if(sizeof($ads['destaque']) == 0)
				// Sem nenhumd destaque ativo
				{

					if(sizeof($ads['halfbanner']) == 0)
					// Sem half banner ativo
					{
						$ads['destaque'] = array($this->pegarCalhau('destaque'));
						$ads['halfbanner'] = array($this->pegarCalhau('halfbanner'), $this->pegarCalhau('halfbanner2'));
					}
					elseif(sizeof($ads['halfbanner']) == 1)
					// Com 1 half banner ativo
					{
						$ads['destaque'] = array($this->pegarCalhau('destaque'));
						$ads['halfbanner'][] = $this->pegarCalhau('halfbanner');
					}
					elseif(sizeof($ads['halfbanner']) == 2)
					// Com 2 half banners ativos
					{
						$ads['destaque'] = array($this->pegarCalhau('destaque'));
					}

				}
				elseif(sizeof($ads['destaque']) == 1)
				// Com 1 destaque ativo
				{
					
					if(sizeof($ads['halfbanner']) == 0)
					// Sem half banner ativo
					{
						$ads['halfbanner'] = array($this->pegarCalhau('halfbanner'), $this->pegarCalhau('halfbanner2'));
					}
					elseif(sizeof($ads['halfbanner']) == 1)
					// Com 1 half banner ativo
					{
						$ads['halfbanner'][] = $this->pegarCalhau('halfbanner');
					}

				}elseif(sizeof($ads['destaque']) == 2){
					$ads['halfbanner'] = false;
				}
				return $this->_imprimirHomeTopo($ads);
				break;
			
			case 'home-inferior':
				$ads['button'] = $this->pegarAtivosImprimiveis('button');
				$ads['publiespecial'] = $this->pegarAtivosImprimiveis('publiespecial');
				
				if(sizeof($ads['button']) == 1){
					$ads['publiespecial'] = false;
				}else{
					if(sizeof($ads['publiespecial']) == 0){
						$ads['button'] = $this->pegarCalhau('button', TRUE);
					}else{
						$ads['button'] = false;
					}
				}				
				return $this->_imprimirHomeInferior($ads);
				break;
			case 'eventos':
				$ads = $this->pegarAtivosImprimiveis('eventos');
				if(!$ads){
					$ads = $this->pegarCalhau('eventos');
				}
				return $this->_imprimirSimples($ads);
				break;
			case 'publicacoes':
				$ads = $this->pegarAtivosImprimiveis('publicacoes');
				if(!$ads){
					$ads = $this->pegarCalhau('publicacoes');
				}
				return $this->_imprimirSimples($ads);
				break;
			case 'noticias':
				$ads = $this->pegarAtivosImprimiveis('noticias');
				if(!$ads){
					$ads = $this->pegarCalhau('noticias');
				}
				return $this->_imprimirSimples($ads);
				break;
			case 'parceiros':
				$ads = $this->pegarAtivosImprimiveis('parceiros');
				return $this->_imprimirSimples($ads);
				break;
		}
	}

	private function _imprimirSimples($ads)
	{
		$retorno = "";
		if(is_array($ads)){
			foreach ($ads as $key => $value) {
				$retorno .= $this->_imprimirAnuncio($value);
			}
		}else{
			$retorno .= $this->_imprimirAnuncio($ads);
		}
		return $retorno;
	}

	private function _imprimirHomeTopo($ads)
	{
		$retorno = "";
		if($ads['halfbanner']){
			$retorno .= "<div class='coluna'>";				
			foreach ($ads['destaque'] as $key => $value) {

				$retorno .= $this->_imprimirAnuncio($value);
			}
			$retorno .= "</div>";
			$retorno .= "<div class='coluna'>";
			foreach ($ads['halfbanner'] as $key => $value) {
				$retorno .= $this->_imprimirAnuncio($value);
			}
			$retorno .= "</div>";
		}else{
			foreach ($ads['destaque'] as $key => $value) {
				$retorno .= "<div class='coluna'>";
				$retorno .= $this->_imprimirAnuncio($value);
				$retorno .= "</div>";
			}
		}
		return $retorno;
	}

	private function _imprimirHomeInferior($ads){
		$retorno = "";

		if($ads['button']){
			$retorno .= "<div class='anuncio-placeholder'>";
			foreach ($ads['button'] as $key => $value) {
				$retorno .= $this->_imprimirAnuncio($value);
			}
		}elseif($ads['publiespecial']){
			$retorno .= "<div class='anuncio-placeholder hide-agenda'>";
			foreach ($ads['publiespecial'] as $key => $value) {
				$retorno .= $this->_imprimirAnuncio($value);
			}
		}
		return $retorno."</div>";
	}

	private function _imprimirAnuncio($value){
		$retorno = "";
		if(gettype($value) === 'object'){
			if($value->imagem){
				// Anúncio Imagem
				if($value->link){
					$retorno .= "<a href='".$value->link."' title='".$value->titulo."' class='ad ad-img ad-".$value->tipo."' target='".$value->tipo_link."' data-track='".$value->id."'>";
				}else{
					$retorno .= "<div title='".$value->titulo."' class='ad ad-".$value->tipo."' data-track='".$value->id."'>";
				}

				$retorno .= "<img src='_anuncios/imagem/".$value->tipo."/".$value->imagem."' alt='".$value->titulo."'>";

				if($value->link){
					$retorno .= "</a>";
				}else{
					$retorno .= "</div>";
				}
			}else{
				// Anúncio Flash
				$retorno .= "<div title='".$value->titulo."' class='ad ad-swf ad-".$value->tipo."' data-track='".$value->id."'>";
				$retorno .= embedAnuncio($value->tipo, $value->swf);
				$retorno .= "</div>";
			}
			if($value->calhau == '0')
				$this->logImpressao($value->id);
		}
		return $retorno;
	}

	/****************************************************************/
	/****************************************************************/
	/*			ESTATÍSTICAS DE IMPRESSÃO DE ANÚNCIOS				*/
	/****************************************************************/
	/****************************************************************/
	private function logImpressao($id)
	// A impressão dos anúncios vai ser rastreada pelo php, de dentro do model
	// então o método recebe argumentos
	{
		$this->db->query("set time_zone= '-3:00';");
		$this->db->set('acao', 'impressao')
				 ->set('trupe_anuncios_id', $id)
				 ->set('url', current_url())
				 ->set('ip', ip())
				 ->set('user_agent', user_agent())
				 ->set('data', date('Y-m-d H:i:s'))				 
				 ->insert($this->tabela_estatistica);
	}

	public function logClick()
	// O controle dos clicks nos anúncios vai ser feito via ajax, então
	// os argumentos virão por post
	{
		$this->db->query("set time_zone= '-3:00';");
		$id = $this->input->post('t');
		$url = $this->input->post('u');

		$anuncioExiste = $this->pegarPorId($id);

		if($anuncioExiste !== false && isset($anuncioExiste->calhau) && $anuncioExiste->calhau == '0')
		{
			$this->db->set('acao', 'click')
					 ->set('trupe_anuncios_id', $id)
					 ->set('url', $url)
					 ->set('ip', ip())
					 ->set('user_agent', user_agent())
					 ->set('data', date('Y-m-d H:i:s'))				 
					 ->insert($this->tabela_estatistica);
		}
	}

	private function contar($acao, $id)
	// $acao = click || impressao
	{
		return $this->db->get_where($this->tabela_estatistica, array('trupe_anuncios_id' => $id, 'acao' => $acao))->num_rows();
	}

	private function estatisticas($id)
	{
		$retorno['click'] = $this->contar('click', $id);
		$retorno['impressao'] = $this->contar('impressao', $id);
		$retorno['ctr'] = ($retorno['click'] == 0 || $retorno['impressao'] == 0) ? '--' : round($retorno['click'] / $retorno['impressao'], 4) * 100 . "%";
		return $retorno;
	}
}