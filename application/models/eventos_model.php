<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'site_eventos';
		$this->tabela_imagens = 'eventos_imagens';

		$this->dados = array(
			'titulo',
			'subtitulo',
			'data',
			'detalhe_data',
			'local',
			'olho',
			'texto',
			'imagem',
			'texto_inscricao',
			'thumb',
			'data_termino'
		);
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql'),
			'imagem' => $this->sobeImagem(),
			'thumb' => $this->sobeThumb(),
			'data_termino' => formataData($this->input->post('termino_inscricoes'), 'br2mysql')
		);
	}

	function pegarTodos($order_campo = 'data', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarPaginado($por_pagina, $inicio, $proximos = 1){
		if($proximos)
			return $this->db->order_by('data', 'asc')->where('data >=', Date('Y-m-d'))->get($this->tabela, $por_pagina, $inicio)->result();
		else
			return $this->db->order_by('data', 'desc')->where('data <', Date('Y-m-d'))->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarProximos($retorna_numero_registros = FALSE){
		$qry = $this->db->order_by('data', 'asc')->get_where($this->tabela, array('data >=' => Date('Y-m-d')));
		return ($retorna_numero_registros) ? $qry->num_rows() : $qry->result();
	}

	function pegarUltimos($retorna_numero_registros = FALSE){
		$qry = $this->db->order_by('data', 'desc')->get_where($this->tabela, array('data <' => Date('Y-m-d')));
		return ($retorna_numero_registros) ? $qry->num_rows() : $qry->result();
	}

	function pegarTitulo($id = false){
		if ($id) {
			$qry = $this->pegarPorId($id);
			if ($qry) {
				return $qry->titulo;
			}else{
				return 'Não Encontrado';
			}
		}else{
			return 'Não Encontrado';
		}
	}

	function verificaInscricoesAbertas($evento){
		$data_setada = isset($evento->data_termino) && $evento->data_termino != '';
		$data_valida = ($data_setada) ? strtotime(Date('Y-m-d')) <= strtotime($evento->data_termino) : false;
		return $data_setada && $data_valida;
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/eventos/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        	 	->load($original['dir'].$filename)
	         		->resize(700, 700)
	         		->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function sobeThumb($campo = 'userfile_thumb'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/eventos/thumbs/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        	 	->load($original['dir'].$filename)
	         		->resize_crop(210,130)
	         		->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function formataMonthPicker($data){
		if($data != '')
			return (strlen($data) == 7) ? formataData('01/'.$data, 'br2mysql') : substr(formataData($data, 'mysql2br'), 3);
		else
			return '';
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}

		$inscricoes = $this->input->post('inscricoes_abertas');
		if($inscricoes == 1){
			$this->db->set('inscricoes_abertas', 1);
		}else{
			$this->db->set('inscricoes_abertas', 0);
		}

		return $this->db->insert($this->tabela);
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			$inscricoes = $this->input->post('inscricoes_abertas');
			if($inscricoes == 1){
				$this->db->set('inscricoes_abertas', 1);
			}else{
				$this->db->set('inscricoes_abertas', 0);
			}

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function imagens($id_parent, $id_imagem = FALSE){
		if(!$id_imagem){
			return $this->db->order_by('ordem', 'asc')->get_where($this->tabela_imagens, array('id_evento' => $id_parent))->result();
		}else{
			$query = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inserirImagem(){
		$imagem = $this->sobeImagemPosEvento();
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem);

			return $this->db->set('id_evento', $this->input->post('id_evento'))->set('legenda', $this->input->post('legenda'))->insert($this->tabela_imagens);
		}else
			return false;
	}

	function editarImagem($id_imagem){
		$imagem = $this->sobeImagemPosEvento();
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem);

			return  $this->db->set('id_evento', $this->input->post('id_evento'))->set('legenda', $this->input->post('legenda'))
							 ->where('id', $id_imagem)->update($this->tabela_imagens);
		}else
			return $this->db->set('legenda', $this->input->post('legenda'))->where('id', $id_imagem)->update($this->tabela_imagens);
	}

	function excluirImagem($id_imagem){
		$imagem = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
		@unlink('../'.$this->imagemOriginal['dir'].$imagem[0]->imagem);
		@unlink('../'.$this->imagemThumb['dir'].$imagem[0]->imagem);
		return $this->db->delete($this->tabela_imagens, array('id' => $id_imagem));
	}

	private function sobeImagemPosEvento($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/eventos/pos/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0',
		  'encrypt_name' => true);

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        	 	 ->load($original['dir'].$filename)
	        	 	 ->resize(900,800)
	        	 	 ->save($original['dir'].$filename, TRUE)
	         		 ->resize_crop(150,75)
	         		 ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}