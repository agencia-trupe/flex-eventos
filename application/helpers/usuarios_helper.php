<?php

function finalizado($tp = 2){

	$CI =& get_instance();

	if($tp == 2)
		$tabela = 'cadastro_anuario_passos_arquitetos';
	elseif($tp == 3)
		$tabela = 'cadastro_anuario_passos_empresas';

	$query = $CI->db->where('id_cadastro_anuario', $CI->session->userdata('id'))
					->where('passo', 4)
					->get($tabela)->num_rows();

	return ($query > 0) ? true : false;
}

function cripto($pwd){
    $CI =& get_instance();
    return sha1($CI->config->item('encryption_key').$pwd);
}

function nomeCategoria($slug_categoria){
	switch ($slug_categoria) {
		case 'mobiliario':
			return "MOBILIÁRIO";
			break;
		case 'componentes':
			return "COMPONENTES";
			break;
		case 'arquishow':
			return "ARQUISHOW";
			break;
		case 'hr':
			return "HR &bull; HOSPITAIS E HOTÉIS";
			break;
		case 'construtoras':
			return "CONSTRUTORAS";
			break;
	}
}

function nomePremio($id_premio){
	$CI =& get_instance();

	$qry = $CI->db->get_where('trupe_premios', array('id' => $id_premio))->result();

	if($qry && isset($qry[0]))
		return $qry[0]->titulo;
	else
		return 'Não encontrado';
}
?>